package com.sun.mail.smtp;

import com.sun.mail.auth.Ntlm;
import com.sun.mail.util.ASCIIUtility;
import com.sun.mail.util.BASE64EncoderStream;
import com.sun.mail.util.LineInputStream;
import com.sun.mail.util.MailConnectException;
import com.sun.mail.util.MailLogger;
import com.sun.mail.util.PropUtil;
import com.sun.mail.util.SocketConnectException;
import com.sun.mail.util.SocketFetcher;
import com.sun.mail.util.TraceInputStream;
import com.sun.mail.util.TraceOutputStream;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.lang.reflect.Constructor;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.logging.Level;
import javax.mail.Address;
import javax.mail.AuthenticationFailedException;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.URLName;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimePart;
import javax.mail.internet.ParseException;
import javax.net.ssl.SSLSocket;






































public class SMTPTransport
  extends Transport
{
  private String name = "smtp";
  private int defaultPort = 25;
  private boolean isSSL = false;
  
  private String host;
  
  private MimeMessage message;
  private Address[] addresses;
  private Address[] validSentAddr;
  private Address[] validUnsentAddr;
  private Address[] invalidAddr;
  private boolean sendPartiallyFailed = false;
  

  private MessagingException exception;
  
  private SMTPOutputStream dataStream;
  
  private Hashtable<String, String> extMap;
  
  private Map<String, Authenticator> authenticators = new HashMap();
  
  private String defaultAuthenticationMechanisms;
  
  private boolean quitWait = false;
  
  private String saslRealm = "UNKNOWN";
  private String authorizationID = "UNKNOWN";
  private boolean enableSASL = false;
  private boolean useCanonicalHostName = false;
  private String[] saslMechanisms = UNKNOWN_SA;
  
  private String ntlmDomain = "UNKNOWN";
  
  private boolean reportSuccess;
  private boolean useStartTLS;
  private boolean requireStartTLS;
  private boolean useRset;
  private boolean noopStrict = true;
  
  private MailLogger logger;
  
  private MailLogger traceLogger;
  
  private String localHostName;
  private String lastServerResponse;
  private int lastReturnCode;
  private boolean notificationDone;
  private SaslAuthenticator saslAuthenticator;
  private boolean noauthdebug = true;
  
  private boolean debugusername;
  
  private boolean debugpassword;
  private boolean allowutf8;
  private int chunkSize;
  private static final String[] ignoreList = { "Bcc", "Content-Length" };
  private static final byte[] CRLF = { 13, 10 };
  private static final String UNKNOWN = "UNKNOWN";
  private static final String[] UNKNOWN_SA = new String[0];
  private BufferedInputStream serverInput;
  private LineInputStream lineInputStream;
  private OutputStream serverOutput;
  private Socket serverSocket;
  private TraceInputStream traceInput;
  private TraceOutputStream traceOutput;
  
  public SMTPTransport(Session session, URLName urlname)
  {
    this(session, urlname, "smtp", false);
  }
  








  protected SMTPTransport(Session session, URLName urlname, String name, boolean isSSL)
  {
    super(session, urlname);
    logger = new MailLogger(getClass(), "DEBUG SMTP", session);
    traceLogger = logger.getSubLogger("protocol", null);
    noauthdebug = (!PropUtil.getBooleanSessionProperty(session, "mail.debug.auth", false));
    
    debugusername = PropUtil.getBooleanSessionProperty(session, "mail.debug.auth.username", true);
    
    debugpassword = PropUtil.getBooleanSessionProperty(session, "mail.debug.auth.password", false);
    
    if (urlname != null)
      name = urlname.getProtocol();
    this.name = name;
    if (!isSSL) {
      isSSL = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".ssl.enable", false);
    }
    if (isSSL) {
      defaultPort = 465;
    } else
      defaultPort = 25;
    this.isSSL = isSSL;
    


    quitWait = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".quitwait", true);
    


    reportSuccess = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".reportsuccess", false);
    


    useStartTLS = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".starttls.enable", false);
    


    requireStartTLS = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".starttls.required", false);
    



    useRset = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".userset", false);
    


    noopStrict = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".noop.strict", true);
    


    enableSASL = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".sasl.enable", false);
    
    if (enableSASL)
      logger.config("enable SASL");
    useCanonicalHostName = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".sasl.usecanonicalhostname", false);
    
    if (useCanonicalHostName) {
      logger.config("use canonical host name");
    }
    allowutf8 = PropUtil.getBooleanSessionProperty(session, "mail.mime.allowutf8", false);
    
    if (allowutf8) {
      logger.config("allow UTF-8");
    }
    chunkSize = PropUtil.getIntSessionProperty(session, "mail." + name + ".chunksize", -1);
    
    if ((chunkSize > 0) && (logger.isLoggable(Level.CONFIG))) {
      logger.config("chunk size " + chunkSize);
    }
    
    Authenticator[] a = { new LoginAuthenticator(), new PlainAuthenticator(), new DigestMD5Authenticator(), new NtlmAuthenticator(), new OAuth2Authenticator() };
    





    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < a.length; i++) {
      authenticators.put(a[i].getMechanism(), a[i]);
      sb.append(a[i].getMechanism()).append(' ');
    }
    defaultAuthenticationMechanisms = sb.toString();
  }
  







  public synchronized String getLocalHost()
  {
    if ((localHostName == null) || (localHostName.length() <= 0))
    {
      localHostName = session.getProperty("mail." + name + ".localhost"); }
    if ((localHostName == null) || (localHostName.length() <= 0))
    {
      localHostName = session.getProperty("mail." + name + ".localaddress"); }
    try {
      if ((localHostName == null) || (localHostName.length() <= 0)) {
        InetAddress localHost = InetAddress.getLocalHost();
        localHostName = localHost.getCanonicalHostName();
        
        if (localHostName == null)
        {
          localHostName = ("[" + localHost.getHostAddress() + "]");
        }
      }
    }
    catch (UnknownHostException localUnknownHostException) {}
    
    if (((localHostName == null) || (localHostName.length() <= 0)) && 
      (serverSocket != null) && (serverSocket.isBound())) {
      InetAddress localHost = serverSocket.getLocalAddress();
      localHostName = localHost.getCanonicalHostName();
      
      if (localHostName == null)
      {
        localHostName = ("[" + localHost.getHostAddress() + "]");
      }
    }
    return localHostName;
  }
  





  public synchronized void setLocalHost(String localhost)
  {
    localHostName = localhost;
  }
  








  public synchronized void connect(Socket socket)
    throws MessagingException
  {
    serverSocket = socket;
    super.connect();
  }
  






  public synchronized String getAuthorizationId()
  {
    if (authorizationID == "UNKNOWN")
    {
      authorizationID = session.getProperty("mail." + name + ".sasl.authorizationid");
    }
    return authorizationID;
  }
  







  public synchronized void setAuthorizationID(String authzid)
  {
    authorizationID = authzid;
  }
  






  public synchronized boolean getSASLEnabled()
  {
    return enableSASL;
  }
  






  public synchronized void setSASLEnabled(boolean enableSASL)
  {
    this.enableSASL = enableSASL;
  }
  






  public synchronized String getSASLRealm()
  {
    if (saslRealm == "UNKNOWN") {
      saslRealm = session.getProperty("mail." + name + ".sasl.realm");
      if (saslRealm == null)
        saslRealm = session.getProperty("mail." + name + ".saslrealm");
    }
    return saslRealm;
  }
  







  public synchronized void setSASLRealm(String saslRealm)
  {
    this.saslRealm = saslRealm;
  }
  






  public synchronized boolean getUseCanonicalHostName()
  {
    return useCanonicalHostName;
  }
  







  public synchronized void setUseCanonicalHostName(boolean useCanonicalHostName)
  {
    this.useCanonicalHostName = useCanonicalHostName;
  }
  








  public synchronized String[] getSASLMechanisms()
  {
    if (saslMechanisms == UNKNOWN_SA) {
      List<String> v = new ArrayList(5);
      String s = session.getProperty("mail." + name + ".sasl.mechanisms");
      if ((s != null) && (s.length() > 0)) {
        if (logger.isLoggable(Level.FINE))
          logger.fine("SASL mechanisms allowed: " + s);
        StringTokenizer st = new StringTokenizer(s, " ,");
        while (st.hasMoreTokens()) {
          String m = st.nextToken();
          if (m.length() > 0)
            v.add(m);
        }
      }
      saslMechanisms = new String[v.size()];
      v.toArray(saslMechanisms);
    }
    if (saslMechanisms == null)
      return null;
    return (String[])saslMechanisms.clone();
  }
  








  public synchronized void setSASLMechanisms(String[] mechanisms)
  {
    if (mechanisms != null)
      mechanisms = (String[])mechanisms.clone();
    saslMechanisms = mechanisms;
  }
  






  public synchronized String getNTLMDomain()
  {
    if (ntlmDomain == "UNKNOWN")
    {
      ntlmDomain = session.getProperty("mail." + name + ".auth.ntlm.domain");
    }
    return ntlmDomain;
  }
  







  public synchronized void setNTLMDomain(String ntlmDomain)
  {
    this.ntlmDomain = ntlmDomain;
  }
  












  public synchronized boolean getReportSuccess()
  {
    return reportSuccess;
  }
  







  public synchronized void setReportSuccess(boolean reportSuccess)
  {
    this.reportSuccess = reportSuccess;
  }
  







  public synchronized boolean getStartTLS()
  {
    return useStartTLS;
  }
  






  public synchronized void setStartTLS(boolean useStartTLS)
  {
    this.useStartTLS = useStartTLS;
  }
  






  public synchronized boolean getRequireStartTLS()
  {
    return requireStartTLS;
  }
  






  public synchronized void setRequireStartTLS(boolean requireStartTLS)
  {
    this.requireStartTLS = requireStartTLS;
  }
  





  public synchronized boolean isSSL()
  {
    return serverSocket instanceof SSLSocket;
  }
  







  public synchronized boolean getUseRset()
  {
    return useRset;
  }
  







  public synchronized void setUseRset(boolean useRset)
  {
    this.useRset = useRset;
  }
  







  public synchronized boolean getNoopStrict()
  {
    return noopStrict;
  }
  







  public synchronized void setNoopStrict(boolean noopStrict)
  {
    this.noopStrict = noopStrict;
  }
  










  public synchronized String getLastServerResponse()
  {
    return lastServerResponse;
  }
  






  public synchronized int getLastReturnCode()
  {
    return lastReturnCode;
  }
  




















  protected synchronized boolean protocolConnect(String host, int port, String user, String password)
    throws MessagingException
  {
    boolean useAuth = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".auth", false);
    







    if ((useAuth) && ((user == null) || (password == null))) {
      if (logger.isLoggable(Level.FINE)) {
        logger.fine("need username and password for authentication");
        logger.fine("protocolConnect returning false, host=" + host + ", user=" + 
        
          traceUser(user) + ", password=" + 
          tracePassword(password));
      }
      return false;
    }
    

    boolean useEhlo = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".ehlo", true);
    
    if (logger.isLoggable(Level.FINE)) {
      logger.fine("useEhlo " + useEhlo + ", useAuth " + useAuth);
    }
    



    if (port == -1) {
      port = PropUtil.getIntSessionProperty(session, "mail." + name + ".port", -1);
    }
    if (port == -1) {
      port = defaultPort;
    }
    if ((host == null) || (host.length() == 0)) {
      host = "localhost";
    }
    



    boolean connected = false;
    try
    {
      if (serverSocket != null) {
        openServer();
      } else {
        openServer(host, port);
      }
      boolean succeed = false;
      if (useEhlo)
        succeed = ehlo(getLocalHost());
      if (!succeed) {
        helo(getLocalHost());
      }
      if ((useStartTLS) || (requireStartTLS)) {
        if ((serverSocket instanceof SSLSocket)) {
          logger.fine("STARTTLS requested but already using SSL");
        } else if (supportsExtension("STARTTLS")) {
          startTLS();
          





          ehlo(getLocalHost());
        } else if (requireStartTLS) {
          logger.fine("STARTTLS required but not supported");
          throw new MessagingException("STARTTLS is required but host does not support STARTTLS");
        }
      }
      


      if ((allowutf8) && (!supportsExtension("SMTPUTF8"))) {
        logger.log(Level.INFO, "mail.mime.allowutf8 set but server doesn't advertise SMTPUTF8 support");
      }
      boolean bool1;
      if (((useAuth) || ((user != null) && (password != null))) && (
        (supportsExtension("AUTH")) || 
        (supportsExtension("AUTH=LOGIN")))) {
        if (logger.isLoggable(Level.FINE))
          logger.fine("protocolConnect login, host=" + host + ", user=" + 
          
            traceUser(user) + ", password=" + 
            tracePassword(password));
        connected = authenticate(user, password);
        return connected;
      }
      

      connected = true;
      return true;

    }
    finally
    {
      if (!connected) {
        try {
          closeConnection();
        }
        catch (MessagingException localMessagingException2) {}
      }
    }
  }
  






  private boolean authenticate(String user, String passwd)
    throws MessagingException
  {
    String mechs = session.getProperty("mail." + name + ".auth.mechanisms");
    if (mechs == null) {
      mechs = defaultAuthenticationMechanisms;
    }
    String authzid = getAuthorizationId();
    if (authzid == null)
      authzid = user;
    if (enableSASL) {
      logger.fine("Authenticate with SASL");
      try {
        if (sasllogin(getSASLMechanisms(), getSASLRealm(), authzid, user, passwd))
        {
          return true;
        }
        logger.fine("SASL authentication failed");
        return false;
      }
      catch (UnsupportedOperationException ex) {
        logger.log(Level.FINE, "SASL support failed", ex);
      }
    }
    

    if (logger.isLoggable(Level.FINE)) {
      logger.fine("Attempt to authenticate using mechanisms: " + mechs);
    }
    





    StringTokenizer st = new StringTokenizer(mechs);
    while (st.hasMoreTokens()) {
      String m = st.nextToken();
      m = m.toUpperCase(Locale.ENGLISH);
      Authenticator a = (Authenticator)authenticators.get(m);
      if (a == null) {
        logger.log(Level.FINE, "no authenticator for mechanism {0}", m);


      }
      else if (!supportsAuthentication(m)) {
        logger.log(Level.FINE, "mechanism {0} not supported by server", m);


      }
      else
      {


        if (mechs == defaultAuthenticationMechanisms)
        {
          String dprop = "mail." + name + ".auth." + m.toLowerCase(Locale.ENGLISH) + ".disable";
          boolean disabled = PropUtil.getBooleanSessionProperty(session, dprop, 
            !a.enabled());
          if (disabled) {
            if (!logger.isLoggable(Level.FINE)) continue;
            logger.fine("mechanism " + m + " disabled by property: " + dprop); continue;
          }
        }
        



        logger.log(Level.FINE, "Using mechanism {0}", m);
        return a.authenticate(host, authzid, user, passwd);
      }
    }
    
    throw new AuthenticationFailedException("No authentication mechanisms supported by both server and client");
  }
  

  private abstract class Authenticator
  {
    protected int resp;
    
    private final String mech;
    private final boolean enabled;
    
    Authenticator(String mech)
    {
      this(mech, true);
    }
    
    Authenticator(String mech, boolean enabled) {
      this.mech = mech.toUpperCase(Locale.ENGLISH);
      this.enabled = enabled;
    }
    
    String getMechanism() {
      return mech;
    }
    
    boolean enabled() {
      return enabled;
    }
    




    boolean authenticate(String host, String authzid, String user, String passwd)
      throws MessagingException
    {
      Throwable thrown = null;
      try
      {
        String ir = getInitialResponse(host, authzid, user, passwd);
        if ((noauthdebug) && (SMTPTransport.this.isTracing())) {
          logger.fine("AUTH " + mech + " command trace suppressed");
          SMTPTransport.this.suspendTracing();
        }
        if (ir != null) {
          resp = simpleCommand("AUTH " + mech + " " + (ir
            .length() == 0 ? "=" : ir));
        } else {
          resp = simpleCommand("AUTH " + mech);
        }
        



        if (resp == 530) {
          startTLS();
          if (ir != null) {
            resp = simpleCommand("AUTH " + mech + " " + ir);
          } else
            resp = simpleCommand("AUTH " + mech);
        }
        if (resp == 334)
          doAuth(host, authzid, user, passwd);
      } catch (IOException ex) {
        logger.log(Level.FINE, "AUTH " + mech + " failed", ex);
      } catch (Throwable t) {
        logger.log(Level.FINE, "AUTH " + mech + " failed", t);
        thrown = t;
      } finally {
        if ((noauthdebug) && (SMTPTransport.this.isTracing())) {
          logger.fine("AUTH " + mech + " " + (resp == 235 ? "succeeded" : "failed"));
        }
        SMTPTransport.this.resumeTracing();
        if (resp != 235) {
          SMTPTransport.this.closeConnection();
          if (thrown != null) {
            if ((thrown instanceof Error))
              throw ((Error)thrown);
            if ((thrown instanceof Exception))
            {
              throw new AuthenticationFailedException(getLastServerResponse(), (Exception)thrown);
            }
            if (!$assertionsDisabled) { throw new AssertionError("unknown Throwable");
            }
          }
          throw new AuthenticationFailedException(getLastServerResponse());
        }
      }
      return true;
    }
    




    String getInitialResponse(String host, String authzid, String user, String passwd)
      throws MessagingException, IOException
    {
      return null;
    }
    
    abstract void doAuth(String paramString1, String paramString2, String paramString3, String paramString4)
      throws MessagingException, IOException;
  }
  
  private class LoginAuthenticator
    extends SMTPTransport.Authenticator
  {
    LoginAuthenticator()
    {
      super("LOGIN");
    }
    

    void doAuth(String host, String authzid, String user, String passwd)
      throws MessagingException, IOException
    {
      resp = simpleCommand(BASE64EncoderStream.encode(user
        .getBytes(StandardCharsets.UTF_8)));
      if (resp == 334)
      {
        resp = simpleCommand(BASE64EncoderStream.encode(passwd
          .getBytes(StandardCharsets.UTF_8)));
      }
    }
  }
  
  private class PlainAuthenticator
    extends SMTPTransport.Authenticator
  {
    PlainAuthenticator()
    {
      super("PLAIN");
    }
    

    String getInitialResponse(String host, String authzid, String user, String passwd)
      throws MessagingException, IOException
    {
      ByteArrayOutputStream bos = new ByteArrayOutputStream();
      OutputStream b64os = new BASE64EncoderStream(bos, Integer.MAX_VALUE);
      
      if (authzid != null)
        b64os.write(authzid.getBytes(StandardCharsets.UTF_8));
      b64os.write(0);
      b64os.write(user.getBytes(StandardCharsets.UTF_8));
      b64os.write(0);
      b64os.write(passwd.getBytes(StandardCharsets.UTF_8));
      b64os.flush();
      
      return ASCIIUtility.toString(bos.toByteArray());
    }
    

    void doAuth(String host, String authzid, String user, String passwd)
      throws MessagingException, IOException
    {
      throw new AuthenticationFailedException("PLAIN asked for more");
    }
  }
  
  private class DigestMD5Authenticator
    extends SMTPTransport.Authenticator
  {
    private DigestMD5 md5support;
    
    DigestMD5Authenticator()
    {
      super("DIGEST-MD5");
    }
    
    private synchronized DigestMD5 getMD5() {
      if (md5support == null)
        md5support = new DigestMD5(logger);
      return md5support;
    }
    
    void doAuth(String host, String authzid, String user, String passwd)
      throws MessagingException, IOException
    {
      DigestMD5 md5 = getMD5();
      assert (md5 != null);
      
      byte[] b = md5.authClient(host, user, passwd, getSASLRealm(), 
        getLastServerResponse());
      resp = simpleCommand(b);
      if (resp == 334) {
        if (!md5.authServer(getLastServerResponse()))
        {
          resp = -1;
        }
        else {
          resp = simpleCommand(new byte[0]);
        }
      }
    }
  }
  
  private class NtlmAuthenticator
    extends SMTPTransport.Authenticator
  {
    private Ntlm ntlm;
    private int flags;
    
    NtlmAuthenticator()
    {
      super("NTLM");
    }
    

    String getInitialResponse(String host, String authzid, String user, String passwd)
      throws MessagingException, IOException
    {
      ntlm = new Ntlm(getNTLMDomain(), getLocalHost(), user, passwd, logger);
      
      flags = PropUtil.getIntProperty(
        session.getProperties(), "mail." + 
        name + ".auth.ntlm.flags", 0);
      
      String type1 = ntlm.generateType1Msg(flags);
      return type1;
    }
    
    void doAuth(String host, String authzid, String user, String passwd)
      throws MessagingException, IOException
    {
      assert (ntlm != null);
      String type3 = ntlm.generateType3Msg(
        getLastServerResponse().substring(4).trim());
      
      resp = simpleCommand(type3);
    }
  }
  

  private class OAuth2Authenticator
    extends SMTPTransport.Authenticator
  {
    OAuth2Authenticator()
    {
      super("XOAUTH2", false);
    }
    
    String getInitialResponse(String host, String authzid, String user, String passwd)
      throws MessagingException, IOException
    {
      String resp = "user=" + user + "\001auth=Bearer " + passwd + "\001\001";
      
      byte[] b = BASE64EncoderStream.encode(resp
        .getBytes(StandardCharsets.UTF_8));
      return ASCIIUtility.toString(b);
    }
    

    void doAuth(String host, String authzid, String user, String passwd)
      throws MessagingException, IOException
    {
      throw new AuthenticationFailedException("OAUTH2 asked for more");
    }
  }
  



  private boolean sasllogin(String[] allowed, String realm, String authzid, String u, String p)
    throws MessagingException
  {
    String serviceHost;
    


    String serviceHost;
    


    if (useCanonicalHostName) {
      serviceHost = serverSocket.getInetAddress().getCanonicalHostName();
    } else
      serviceHost = host;
    if (saslAuthenticator == null) {
      try {
        Class<?> sac = Class.forName("com.sun.mail.smtp.SMTPSaslAuthenticator");
        
        Constructor<?> c = sac.getConstructor(new Class[] { SMTPTransport.class, String.class, Properties.class, MailLogger.class, String.class });
        





        saslAuthenticator = ((SaslAuthenticator)c.newInstance(new Object[] { this, name, session
        


          .getProperties(), logger, serviceHost }));

      }
      catch (Exception ex)
      {
        logger.log(Level.FINE, "Can't load SASL authenticator", ex);
        
        return false;
      }
    }
    
    List<String> v;
    StringTokenizer st;
    if ((allowed != null) && (allowed.length > 0))
    {
      List<String> v = new ArrayList(allowed.length);
      for (int i = 0; i < allowed.length; i++) {
        if (supportsAuthentication(allowed[i]))
          v.add(allowed[i]);
      }
    } else {
      v = new ArrayList();
      if (extMap != null) {
        String a = (String)extMap.get("AUTH");
        if (a != null) {
          st = new StringTokenizer(a);
          while (st.hasMoreTokens())
            v.add(st.nextToken());
        }
      }
    }
    String[] mechs = (String[])v.toArray(new String[v.size()]);
    try {
      if ((noauthdebug) && (isTracing())) {
        logger.fine("SASL AUTH command trace suppressed");
        suspendTracing();
      }
      return saslAuthenticator.authenticate(mechs, realm, authzid, u, p);
    } finally {
      resumeTracing();
    }
  }
  




































  public synchronized void sendMessage(Message message, Address[] addresses)
    throws MessagingException, SendFailedException
  {
    sendMessageStart(message != null ? message.getSubject() : "");
    checkConnected();
    


    if (!(message instanceof MimeMessage)) {
      logger.fine("Can only send RFC822 msgs");
      throw new MessagingException("SMTP can only send RFC822 messages");
    }
    for (int i = 0; i < addresses.length; i++) {
      if (!(addresses[i] instanceof InternetAddress)) {
        throw new MessagingException(addresses[i] + " is not an InternetAddress");
      }
    }
    
    if (addresses.length == 0) {
      throw new SendFailedException("No recipient addresses");
    }
    this.message = ((MimeMessage)message);
    this.addresses = addresses;
    validUnsentAddr = addresses;
    expandGroups();
    
    boolean use8bit = false;
    if ((message instanceof SMTPMessage))
      use8bit = ((SMTPMessage)message).getAllow8bitMIME();
    if (!use8bit) {
      use8bit = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".allow8bitmime", false);
    }
    if (logger.isLoggable(Level.FINE))
      logger.fine("use8bit " + use8bit);
    if ((use8bit) && (supportsExtension("8BITMIME")) && 
      (convertTo8Bit(this.message)))
    {
      try
      {
        this.message.saveChanges();
      }
      catch (MessagingException localMessagingException1) {}
    }
    

    try
    {
      mailFrom();
      rcptTo();
      if ((chunkSize > 0) && (supportsExtension("CHUNKING")))
      {








        this.message.writeTo(bdat(), ignoreList);
        finishBdat();
      } else {
        this.message.writeTo(data(), ignoreList);
        finishData();
      }
      if (sendPartiallyFailed)
      {

        logger.fine("Sending partially failed because of invalid destination addresses");
        
        notifyTransportListeners(3, validSentAddr, validUnsentAddr, invalidAddr, this.message);
        



        throw new SMTPSendFailedException(".", lastReturnCode, lastServerResponse, exception, validSentAddr, validUnsentAddr, invalidAddr);
      }
      

      logger.fine("message successfully delivered to mail server");
      notifyTransportListeners(1, validSentAddr, validUnsentAddr, invalidAddr, this.message);
    }
    catch (MessagingException mex)
    {
      logger.log(Level.FINE, "MessagingException while sending", mex);
      
      if ((mex.getNextException() instanceof IOException))
      {

        logger.fine("nested IOException, closing");
        try {
          closeConnection();
        } catch (MessagingException localMessagingException2) {}
      }
      addressesFailed();
      notifyTransportListeners(2, validSentAddr, validUnsentAddr, invalidAddr, this.message);
      


      throw mex;
    } catch (IOException ex) {
      logger.log(Level.FINE, "IOException while sending, closing", ex);
      
      try
      {
        closeConnection();
      } catch (MessagingException localMessagingException3) {}
      addressesFailed();
      notifyTransportListeners(2, validSentAddr, validUnsentAddr, invalidAddr, this.message);
      


      throw new MessagingException("IOException while sending message", ex);
    }
    finally
    {
      validSentAddr = (this.validUnsentAddr = this.invalidAddr = null);
      this.addresses = null;
      this.message = null;
      exception = null;
      sendPartiallyFailed = false;
      notificationDone = false;
    }
    sendMessageEnd();
  }
  


  private void addressesFailed()
  {
    if (validSentAddr != null) {
      if (validUnsentAddr != null) {
        Address[] newa = new Address[validSentAddr.length + validUnsentAddr.length];
        
        System.arraycopy(validSentAddr, 0, newa, 0, validSentAddr.length);
        
        System.arraycopy(validUnsentAddr, 0, newa, validSentAddr.length, validUnsentAddr.length);
        
        validSentAddr = null;
        validUnsentAddr = newa;
      } else {
        validUnsentAddr = validSentAddr;
        validSentAddr = null;
      }
    }
  }
  


  public synchronized void close()
    throws MessagingException
  {
    if (!super.isConnected())
      return;
    try {
      if (serverSocket != null) {
        sendCommand("QUIT");
        if (quitWait) {
          int resp = readServerResponse();
          if ((resp != 221) && (resp != -1) && 
            (logger.isLoggable(Level.FINE))) {
            logger.fine("QUIT failed with " + resp);
          }
        }
      }
      closeConnection(); } finally { closeConnection();
    }
  }
  
  private void closeConnection() throws MessagingException {
    try {
      if (serverSocket != null)
        serverSocket.close();
    } catch (IOException ioex) {
      throw new MessagingException("Server Close Failed", ioex);
    } finally {
      serverSocket = null;
      serverOutput = null;
      serverInput = null;
      lineInputStream = null;
      if (super.isConnected()) {
        super.close();
      }
    }
  }
  



  public synchronized boolean isConnected()
  {
    if (!super.isConnected())
    {
      return false;
    }
    
    try
    {
      if (useRset) {
        sendCommand("RSET");
      } else
        sendCommand("NOOP");
      int resp = readServerResponse();
      

















      if ((resp >= 0) && (noopStrict ? resp == 250 : resp != 421)) {
        return true;
      }
      try {
        closeConnection();
      }
      catch (MessagingException localMessagingException) {}
      
      return false;
    }
    catch (Exception ex) {
      try {
        closeConnection();
      }
      catch (MessagingException localMessagingException1) {}
    }
    return false;
  }
  










  protected void notifyTransportListeners(int type, Address[] validSent, Address[] validUnsent, Address[] invalid, Message msg)
  {
    if (!notificationDone) {
      super.notifyTransportListeners(type, validSent, validUnsent, invalid, msg);
      
      notificationDone = true;
    }
  }
  


  private void expandGroups()
  {
    List<Address> groups = null;
    for (int i = 0; i < addresses.length; i++) {
      InternetAddress a = (InternetAddress)addresses[i];
      if (a.isGroup()) {
        if (groups == null)
        {
          groups = new ArrayList();
          for (int k = 0; k < i; k++) {
            groups.add(addresses[k]);
          }
        }
        try {
          InternetAddress[] ia = a.getGroup(true);
          if (ia != null) {
            for (int j = 0; j < ia.length; j++)
              groups.add(ia[j]);
          } else {
            groups.add(a);
          }
        } catch (ParseException pex) {
          groups.add(a);
        }
        
      }
      else if (groups != null) {
        groups.add(a);
      }
    }
    

    if (groups != null) {
      InternetAddress[] newa = new InternetAddress[groups.size()];
      groups.toArray(newa);
      addresses = newa;
    }
  }
  









  private boolean convertTo8Bit(MimePart part)
  {
    boolean changed = false;
    try {
      if (part.isMimeType("text/*")) {
        String enc = part.getEncoding();
        if ((enc != null) && ((enc.equalsIgnoreCase("quoted-printable")) || 
          (enc.equalsIgnoreCase("base64")))) {
          InputStream is = null;
          try {
            is = part.getInputStream();
            if (is8Bit(is))
            {








              part.setContent(part.getContent(), part
                .getContentType());
              part.setHeader("Content-Transfer-Encoding", "8bit");
              changed = true;
            }
          } finally {
            if (is != null) {
              try {
                is.close();
              }
              catch (IOException localIOException1) {}
            }
          }
        }
      }
      if (part.isMimeType("multipart/*")) {
        MimeMultipart mp = (MimeMultipart)part.getContent();
        int count = mp.getCount();
        for (int i = 0; i < count; i++) {
          if (convertTo8Bit((MimePart)mp.getBodyPart(i))) {
            changed = true;
          }
        }
      }
    }
    catch (IOException localIOException2) {}catch (MessagingException localMessagingException) {}
    

    return changed;
  }
  







  private boolean is8Bit(InputStream is)
  {
    int linelen = 0;
    boolean need8bit = false;
    try { int b;
      while ((b = is.read()) >= 0) {
        b &= 0xFF;
        if ((b == 13) || (b == 10)) {
          linelen = 0;
        } else { if (b == 0) {
            return false;
          }
          linelen++;
          if (linelen > 998)
            return false;
        }
        if (b > 127)
          need8bit = true;
      }
    } catch (IOException ex) {
      return false; }
    int b;
    if (need8bit)
      logger.fine("found an 8bit part");
    return need8bit;
  }
  
  /* Error */
  protected void finalize()
    throws Throwable
  {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial 4	com/sun/mail/smtp/SMTPTransport:closeConnection	()V
    //   4: aload_0
    //   5: invokespecial 310	javax/mail/Transport:finalize	()V
    //   8: goto +18 -> 26
    //   11: astore_1
    //   12: aload_0
    //   13: invokespecial 310	javax/mail/Transport:finalize	()V
    //   16: goto +10 -> 26
    //   19: astore_2
    //   20: aload_0
    //   21: invokespecial 310	javax/mail/Transport:finalize	()V
    //   24: aload_2
    //   25: athrow
    //   26: return
    // Line number table:
    //   Java source line #1636	-> byte code offset #0
    //   Java source line #1640	-> byte code offset #4
    //   Java source line #1641	-> byte code offset #8
    //   Java source line #1637	-> byte code offset #11
    //   Java source line #1640	-> byte code offset #12
    //   Java source line #1641	-> byte code offset #16
    //   Java source line #1640	-> byte code offset #19
    //   Java source line #1642	-> byte code offset #26
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	27	0	this	SMTPTransport
    //   11	1	1	localMessagingException	MessagingException
    //   19	6	2	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   0	4	11	javax/mail/MessagingException
    //   0	4	19	finally
  }
  
  protected void helo(String domain)
    throws MessagingException
  {
    if (domain != null) {
      issueCommand("HELO " + domain, 250);
    } else {
      issueCommand("HELO", 250);
    }
  }
  


  protected boolean ehlo(String domain)
    throws MessagingException
  {
    String cmd;
    

    String cmd;
    
    if (domain != null) {
      cmd = "EHLO " + domain;
    } else
      cmd = "EHLO";
    sendCommand(cmd);
    int resp = readServerResponse();
    if (resp == 250)
    {
      BufferedReader rd = new BufferedReader(new StringReader(lastServerResponse));
      

      extMap = new Hashtable();
      try {
        boolean first = true;
        String line; while ((line = rd.readLine()) != null)
          if (first) {
            first = false;

          }
          else if (line.length() >= 5)
          {
            line = line.substring(4);
            int i = line.indexOf(' ');
            String arg = "";
            if (i > 0) {
              arg = line.substring(i + 1);
              line = line.substring(0, i);
            }
            if (logger.isLoggable(Level.FINE)) {
              logger.fine("Found extension \"" + line + "\", arg \"" + arg + "\"");
            }
            extMap.put(line.toUpperCase(Locale.ENGLISH), arg);
          }
      } catch (IOException localIOException) {}
    }
    return resp == 250;
  }
  













  protected void mailFrom()
    throws MessagingException
  {
    String from = null;
    if ((message instanceof SMTPMessage))
      from = ((SMTPMessage)message).getEnvelopeFrom();
    if ((from == null) || (from.length() <= 0))
      from = session.getProperty("mail." + name + ".from");
    if ((from == null) || (from.length() <= 0)) { Address[] fa;
      Address me;
      Address me;
      if ((message != null) && ((fa = message.getFrom()) != null) && (fa.length > 0))
      {
        me = fa[0];
      } else {
        me = InternetAddress.getLocalAddress(session);
      }
      if (me != null) {
        from = ((InternetAddress)me).getAddress();
      } else {
        throw new MessagingException("can't determine local email address");
      }
    }
    
    String cmd = "MAIL FROM:" + normalizeAddress(from);
    
    if ((allowutf8) && (supportsExtension("SMTPUTF8"))) {
      cmd = cmd + " SMTPUTF8";
    }
    
    if (supportsExtension("DSN")) {
      String ret = null;
      if ((message instanceof SMTPMessage))
        ret = ((SMTPMessage)message).getDSNRet();
      if (ret == null) {
        ret = session.getProperty("mail." + name + ".dsn.ret");
      }
      if (ret != null) {
        cmd = cmd + " RET=" + ret;
      }
    }
    




    if (supportsExtension("AUTH")) {
      String submitter = null;
      if ((message instanceof SMTPMessage))
        submitter = ((SMTPMessage)message).getSubmitter();
      if (submitter == null) {
        submitter = session.getProperty("mail." + name + ".submitter");
      }
      if (submitter != null) {
        try {
          String s = xtext(submitter, (allowutf8) && 
            (supportsExtension("SMTPUTF8")));
          cmd = cmd + " AUTH=" + s;
        } catch (IllegalArgumentException ex) {
          if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "ignoring invalid submitter: " + submitter, ex);
          }
        }
      }
    }
    



    String ext = null;
    if ((message instanceof SMTPMessage))
      ext = ((SMTPMessage)message).getMailExtension();
    if (ext == null)
      ext = session.getProperty("mail." + name + ".mailextension");
    if ((ext != null) && (ext.length() > 0)) {
      cmd = cmd + " " + ext;
    }
    try {
      issueSendCommand(cmd, 250);
    } catch (SMTPSendFailedException ex) {
      int retCode = ex.getReturnCode();
      switch (retCode) {
      case 501: case 503: case 550: 
      case 551: case 553: 
        try {
          ex.setNextException(new SMTPSenderFailedException(new InternetAddress(from), cmd, retCode, ex
          
            .getMessage()));
        }
        catch (AddressException localAddressException) {}
      }
      
      


      throw ex;
    }
  }
  




















  protected void rcptTo()
    throws MessagingException
  {
    List<InternetAddress> valid = new ArrayList();
    List<InternetAddress> validUnsent = new ArrayList();
    List<InternetAddress> invalid = new ArrayList();
    int retCode = -1;
    MessagingException mex = null;
    boolean sendFailed = false;
    MessagingException sfex = null;
    validSentAddr = (this.validUnsentAddr = this.invalidAddr = null);
    boolean sendPartial = false;
    if ((message instanceof SMTPMessage))
      sendPartial = ((SMTPMessage)message).getSendPartial();
    if (!sendPartial) {
      sendPartial = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".sendpartial", false);
    }
    if (sendPartial) {
      logger.fine("sendPartial set");
    }
    boolean dsn = false;
    String notify = null;
    if (supportsExtension("DSN")) {
      if ((message instanceof SMTPMessage))
        notify = ((SMTPMessage)message).getDSNNotify();
      if (notify == null) {
        notify = session.getProperty("mail." + name + ".dsn.notify");
      }
      if (notify != null) {
        dsn = true;
      }
    }
    
    for (int i = 0; i < addresses.length; i++)
    {
      sfex = null;
      InternetAddress ia = (InternetAddress)addresses[i];
      String cmd = "RCPT TO:" + normalizeAddress(ia.getAddress());
      if (dsn) {
        cmd = cmd + " NOTIFY=" + notify;
      }
      sendCommand(cmd);
      
      retCode = readServerResponse();
      switch (retCode) {
      case 250: case 251: 
        valid.add(ia);
        if (reportSuccess)
        {





          sfex = new SMTPAddressSucceededException(ia, cmd, retCode, lastServerResponse);
          
          if (mex == null) {
            mex = sfex;
          } else
            mex.setNextException(sfex); }
        break;
      case 501: case 503: 
      case 550: case 551: 
      case 553: 
        if (!sendPartial)
          sendFailed = true;
        invalid.add(ia);
        
        sfex = new SMTPAddressFailedException(ia, cmd, retCode, lastServerResponse);
        
        if (mex == null) {
          mex = sfex;
        } else
          mex.setNextException(sfex);
        break;
      case 450: case 451: 
      case 452: 
      case 552: 
        if (!sendPartial)
          sendFailed = true;
        validUnsent.add(ia);
        
        sfex = new SMTPAddressFailedException(ia, cmd, retCode, lastServerResponse);
        
        if (mex == null) {
          mex = sfex;
        } else
          mex.setNextException(sfex);
        break;
      

      default: 
        if ((retCode >= 400) && (retCode <= 499))
        {
          validUnsent.add(ia);
        } else if ((retCode >= 500) && (retCode <= 599))
        {
          invalid.add(ia);
        }
        else {
          if (logger.isLoggable(Level.FINE)) {
            logger.fine("got response code " + retCode + ", with response: " + lastServerResponse);
          }
          String _lsr = lastServerResponse;
          int _lrc = lastReturnCode;
          if (serverSocket != null)
            issueCommand("RSET", -1);
          lastServerResponse = _lsr;
          lastReturnCode = _lrc;
          throw new SMTPAddressFailedException(ia, cmd, retCode, _lsr);
        }
        
        if (!sendPartial) {
          sendFailed = true;
        }
        sfex = new SMTPAddressFailedException(ia, cmd, retCode, lastServerResponse);
        
        if (mex == null) {
          mex = sfex;
        } else {
          mex.setNextException(sfex);
        }
        
        break;
      }
      
    }
    if ((sendPartial) && (valid.size() == 0)) {
      sendFailed = true;
    }
    
    if (sendFailed)
    {
      invalidAddr = new Address[invalid.size()];
      invalid.toArray(invalidAddr);
      

      validUnsentAddr = new Address[valid.size() + validUnsent.size()];
      int i = 0;
      for (int j = 0; j < valid.size(); j++)
        validUnsentAddr[(i++)] = ((Address)valid.get(j));
      for (int j = 0; j < validUnsent.size(); j++)
        validUnsentAddr[(i++)] = ((Address)validUnsent.get(j));
    } else if ((reportSuccess) || ((sendPartial) && (
      (invalid.size() > 0) || (validUnsent.size() > 0))))
    {

      sendPartiallyFailed = true;
      exception = mex;
      

      invalidAddr = new Address[invalid.size()];
      invalid.toArray(invalidAddr);
      

      validUnsentAddr = new Address[validUnsent.size()];
      validUnsent.toArray(validUnsentAddr);
      

      validSentAddr = new Address[valid.size()];
      valid.toArray(validSentAddr);
    } else {
      validSentAddr = addresses;
    }
    


    if (logger.isLoggable(Level.FINE)) {
      if ((validSentAddr != null) && (validSentAddr.length > 0)) {
        logger.fine("Verified Addresses");
        for (int l = 0; l < validSentAddr.length; l++) {
          logger.fine("  " + validSentAddr[l]);
        }
      }
      if ((validUnsentAddr != null) && (validUnsentAddr.length > 0)) {
        logger.fine("Valid Unsent Addresses");
        for (int j = 0; j < validUnsentAddr.length; j++) {
          logger.fine("  " + validUnsentAddr[j]);
        }
      }
      if ((invalidAddr != null) && (invalidAddr.length > 0)) {
        logger.fine("Invalid Addresses");
        for (int k = 0; k < invalidAddr.length; k++) {
          logger.fine("  " + invalidAddr[k]);
        }
      }
    }
    

    if (sendFailed) {
      logger.fine("Sending failed because of invalid destination addresses");
      
      notifyTransportListeners(2, validSentAddr, validUnsentAddr, invalidAddr, message);
      



      String lsr = lastServerResponse;
      int lrc = lastReturnCode;
      try {
        if (serverSocket != null) {
          issueCommand("RSET", -1);
        }
      } catch (MessagingException ex) {
        try {
          close();
        }
        catch (MessagingException ex2) {
          logger.log(Level.FINE, "close failed", ex2);
        }
      } finally {
        lastServerResponse = lsr;
        lastReturnCode = lrc;
      }
      
      throw new SendFailedException("Invalid Addresses", mex, validSentAddr, validUnsentAddr, invalidAddr);
    }
  }
  








  protected OutputStream data()
    throws MessagingException
  {
    assert (Thread.holdsLock(this));
    issueSendCommand("DATA", 354);
    dataStream = new SMTPOutputStream(serverOutput);
    return dataStream;
  }
  





  protected void finishData()
    throws IOException, MessagingException
  {
    assert (Thread.holdsLock(this));
    dataStream.ensureAtBOL();
    issueSendCommand(".", 250);
  }
  





  protected OutputStream bdat()
    throws MessagingException
  {
    assert (Thread.holdsLock(this));
    dataStream = new BDATOutputStream(serverOutput, chunkSize);
    return dataStream;
  }
  





  protected void finishBdat()
    throws IOException, MessagingException
  {
    assert (Thread.holdsLock(this));
    dataStream.ensureAtBOL();
    dataStream.close();
  }
  





  protected void startTLS()
    throws MessagingException
  {
    issueCommand("STARTTLS", 220);
    try
    {
      serverSocket = SocketFetcher.startTLS(serverSocket, host, session
        .getProperties(), "mail." + name);
      initStreams();
    } catch (IOException ioex) {
      closeConnection();
      throw new MessagingException("Could not convert socket to TLS", ioex);
    }
  }
  






  private void openServer(String host, int port)
    throws MessagingException
  {
    if (logger.isLoggable(Level.FINE)) {
      logger.fine("trying to connect to host \"" + host + "\", port " + port + ", isSSL " + isSSL);
    }
    try
    {
      Properties props = session.getProperties();
      
      serverSocket = SocketFetcher.getSocket(host, port, props, "mail." + name, isSSL);
      



      port = serverSocket.getPort();
      
      this.host = host;
      
      initStreams();
      
      int r = -1;
      if ((r = readServerResponse()) != 220) {
        serverSocket.close();
        serverSocket = null;
        serverOutput = null;
        serverInput = null;
        lineInputStream = null;
        if (logger.isLoggable(Level.FINE)) {
          logger.fine("could not connect to host \"" + host + "\", port: " + port + ", response: " + r + "\n");
        }
        
        throw new MessagingException("Could not connect to SMTP host: " + host + ", port: " + port + ", response: " + r);
      }
      


      if (logger.isLoggable(Level.FINE)) {
        logger.fine("connected to host \"" + host + "\", port: " + port + "\n");
      }
    }
    catch (UnknownHostException uhex) {
      throw new MessagingException("Unknown SMTP host: " + host, uhex);
    } catch (SocketConnectException scex) {
      throw new MailConnectException(scex);
    } catch (IOException ioe) {
      throw new MessagingException("Could not connect to SMTP host: " + host + ", port: " + port, ioe);
    }
  }
  



  private void openServer()
    throws MessagingException
  {
    int port = -1;
    host = "UNKNOWN";
    try {
      port = serverSocket.getPort();
      host = serverSocket.getInetAddress().getHostName();
      if (logger.isLoggable(Level.FINE)) {
        logger.fine("starting protocol to host \"" + host + "\", port " + port);
      }
      
      initStreams();
      
      int r = -1;
      if ((r = readServerResponse()) != 220) {
        serverSocket.close();
        serverSocket = null;
        serverOutput = null;
        serverInput = null;
        lineInputStream = null;
        if (logger.isLoggable(Level.FINE)) {
          logger.fine("got bad greeting from host \"" + host + "\", port: " + port + ", response: " + r + "\n");
        }
        
        throw new MessagingException("Got bad greeting from SMTP host: " + host + ", port: " + port + ", response: " + r);
      }
      


      if (logger.isLoggable(Level.FINE)) {
        logger.fine("protocol started to host \"" + host + "\", port: " + port + "\n");
      }
    }
    catch (IOException ioe) {
      throw new MessagingException("Could not start protocol to SMTP host: " + host + ", port: " + port, ioe);
    }
  }
  

  private void initStreams()
    throws IOException
  {
    boolean quote = PropUtil.getBooleanSessionProperty(session, "mail.debug.quote", false);
    


    traceInput = new TraceInputStream(serverSocket.getInputStream(), traceLogger);
    traceInput.setQuote(quote);
    

    traceOutput = new TraceOutputStream(serverSocket.getOutputStream(), traceLogger);
    traceOutput.setQuote(quote);
    
    serverOutput = new BufferedOutputStream(traceOutput);
    
    serverInput = new BufferedInputStream(traceInput);
    
    lineInputStream = new LineInputStream(serverInput);
  }
  


  private boolean isTracing()
  {
    return traceLogger.isLoggable(Level.FINEST);
  }
  



  private void suspendTracing()
  {
    if (traceLogger.isLoggable(Level.FINEST)) {
      traceInput.setTrace(false);
      traceOutput.setTrace(false);
    }
  }
  


  private void resumeTracing()
  {
    if (traceLogger.isLoggable(Level.FINEST)) {
      traceInput.setTrace(true);
      traceOutput.setTrace(true);
    }
  }
  








  public synchronized void issueCommand(String cmd, int expect)
    throws MessagingException
  {
    sendCommand(cmd);
    


    int resp = readServerResponse();
    if ((expect != -1) && (resp != expect)) {
      throw new MessagingException(lastServerResponse);
    }
  }
  

  private void issueSendCommand(String cmd, int expect)
    throws MessagingException
  {
    sendCommand(cmd);
    

    int ret;
    
    if ((ret = readServerResponse()) != expect)
    {

      int vsl = validSentAddr == null ? 0 : validSentAddr.length;
      int vul = validUnsentAddr == null ? 0 : validUnsentAddr.length;
      Address[] valid = new Address[vsl + vul];
      if (vsl > 0)
        System.arraycopy(validSentAddr, 0, valid, 0, vsl);
      if (vul > 0)
        System.arraycopy(validUnsentAddr, 0, valid, vsl, vul);
      validSentAddr = null;
      validUnsentAddr = valid;
      if (logger.isLoggable(Level.FINE)) {
        logger.fine("got response code " + ret + ", with response: " + lastServerResponse);
      }
      String _lsr = lastServerResponse;
      int _lrc = lastReturnCode;
      if (serverSocket != null)
        issueCommand("RSET", -1);
      lastServerResponse = _lsr;
      lastReturnCode = _lrc;
      throw new SMTPSendFailedException(cmd, ret, lastServerResponse, exception, validSentAddr, validUnsentAddr, invalidAddr);
    }
  }
  









  public synchronized int simpleCommand(String cmd)
    throws MessagingException
  {
    sendCommand(cmd);
    return readServerResponse();
  }
  







  protected int simpleCommand(byte[] cmd)
    throws MessagingException
  {
    assert (Thread.holdsLock(this));
    sendCommand(cmd);
    return readServerResponse();
  }
  






  protected void sendCommand(String cmd)
    throws MessagingException
  {
    sendCommand(toBytes(cmd));
  }
  
  private void sendCommand(byte[] cmdBytes) throws MessagingException {
    assert (Thread.holdsLock(this));
    

    try
    {
      serverOutput.write(cmdBytes);
      serverOutput.write(CRLF);
      serverOutput.flush();
    } catch (IOException ex) {
      throw new MessagingException("Can't send command to SMTP host", ex);
    }
  }
  







  protected int readServerResponse()
    throws MessagingException
  {
    assert (Thread.holdsLock(this));
    String serverResponse = "";
    int returnCode = 0;
    StringBuffer buf = new StringBuffer(100);
    

    try
    {
      String line = null;
      do
      {
        line = lineInputStream.readLine();
        if (line == null) {
          serverResponse = buf.toString();
          if (serverResponse.length() == 0)
            serverResponse = "[EOF]";
          lastServerResponse = serverResponse;
          lastReturnCode = -1;
          logger.log(Level.FINE, "EOF: {0}", serverResponse);
          return -1;
        }
        buf.append(line);
        buf.append("\n");
      } while (isNotLastLine(line));
      
      serverResponse = buf.toString();
    } catch (IOException ioex) {
      logger.log(Level.FINE, "exception reading response", ioex);
      
      lastServerResponse = "";
      lastReturnCode = 0;
      throw new MessagingException("Exception reading response", ioex);
    }
    






    if (serverResponse.length() >= 3) {
      try {
        returnCode = Integer.parseInt(serverResponse.substring(0, 3));
      } catch (NumberFormatException nfe) {
        try {
          close();
        }
        catch (MessagingException mex) {
          logger.log(Level.FINE, "close failed", mex);
        }
        returnCode = -1;
      } catch (StringIndexOutOfBoundsException ex) {
        try {
          close();
        }
        catch (MessagingException mex) {
          logger.log(Level.FINE, "close failed", mex);
        }
        returnCode = -1;
      }
    } else {
      returnCode = -1;
    }
    if (returnCode == -1) {
      logger.log(Level.FINE, "bad server response: {0}", serverResponse);
    }
    lastServerResponse = serverResponse;
    lastReturnCode = returnCode;
    return returnCode;
  }
  







  protected void checkConnected()
  {
    if (!super.isConnected()) {
      throw new IllegalStateException("Not connected");
    }
  }
  
  private boolean isNotLastLine(String line) {
    return (line != null) && (line.length() >= 4) && (line.charAt(3) == '-');
  }
  
  private String normalizeAddress(String addr)
  {
    if ((!addr.startsWith("<")) && (!addr.endsWith(">"))) {
      return "<" + addr + ">";
    }
    return addr;
  }
  











  public boolean supportsExtension(String ext)
  {
    return (extMap != null) && 
      (extMap.get(ext.toUpperCase(Locale.ENGLISH)) != null);
  }
  








  public String getExtensionParameter(String ext)
  {
    return extMap == null ? null : 
      (String)extMap.get(ext.toUpperCase(Locale.ENGLISH));
  }
  









  protected boolean supportsAuthentication(String auth)
  {
    assert (Thread.holdsLock(this));
    if (extMap == null)
      return false;
    String a = (String)extMap.get("AUTH");
    if (a == null)
      return false;
    StringTokenizer st = new StringTokenizer(a);
    while (st.hasMoreTokens()) {
      String tok = st.nextToken();
      if (tok.equalsIgnoreCase(auth)) {
        return true;
      }
    }
    if ((auth.equalsIgnoreCase("LOGIN")) && (supportsExtension("AUTH=LOGIN"))) {
      logger.fine("use AUTH=LOGIN hack");
      return true;
    }
    return false;
  }
  
  private static char[] hexchar = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
  























  protected static String xtext(String s)
  {
    return xtext(s, false);
  }
  







  protected static String xtext(String s, boolean utf8)
  {
    StringBuffer sb = null;
    byte[] bytes;
    byte[] bytes; if (utf8) {
      bytes = s.getBytes(StandardCharsets.UTF_8);
    } else
      bytes = ASCIIUtility.getBytes(s);
    for (int i = 0; i < bytes.length; i++) {
      char c = (char)(bytes[i] & 0xFF);
      if ((!utf8) && (c >= '')) {
        throw new IllegalArgumentException("Non-ASCII character in SMTP submitter: " + s);
      }
      if ((c < '!') || (c > '~') || (c == '+') || (c == '='))
      {
        if (sb == null) {
          sb = new StringBuffer(s.length() + 4);
          sb.append(s.substring(0, i));
        }
        sb.append('+');
        sb.append(hexchar[((c & 0xF0) >> '\004')]);
        sb.append(hexchar[(c & 0xF)]);
      }
      else if (sb != null) {
        sb.append(c);
      }
    }
    return sb != null ? sb.toString() : s;
  }
  
  private String traceUser(String user) {
    return debugusername ? user : "<user name suppressed>";
  }
  
  private String tracePassword(String password) {
    return password == null ? "<null>" : debugpassword ? password : "<non-null>";
  }
  




  private byte[] toBytes(String s)
  {
    if (allowutf8) {
      return s.getBytes(StandardCharsets.UTF_8);
    }
    
    return ASCIIUtility.getBytes(s);
  }
  




  private void sendMessageStart(String subject) {}
  




  private void sendMessageEnd() {}
  




  private class BDATOutputStream
    extends SMTPOutputStream
  {
    public BDATOutputStream(OutputStream out, int size)
    {
      super();
    }
    




    public void close()
      throws IOException
    {
      out.close();
    }
  }
  

  private class ChunkedOutputStream
    extends OutputStream
  {
    private final OutputStream out;
    
    private final byte[] buf;
    private int count = 0;
    






    public ChunkedOutputStream(OutputStream out, int size)
    {
      this.out = out;
      buf = new byte[size];
    }
    





    public void write(int b)
      throws IOException
    {
      buf[(count++)] = ((byte)b);
      if (count >= buf.length) {
        flush();
      }
    }
    






    public void write(byte[] b, int off, int len)
      throws IOException
    {
      while (len > 0) {
        int size = Math.min(buf.length - count, len);
        if (size == buf.length)
        {
          bdat(b, off, size, false);
        } else {
          System.arraycopy(b, off, buf, count, size);
          count += size;
        }
        off += size;
        len -= size;
        if (count >= buf.length) {
          flush();
        }
      }
    }
    



    public void flush()
      throws IOException
    {
      bdat(buf, 0, count, false);
      count = 0;
    }
    




    public void close()
      throws IOException
    {
      bdat(buf, 0, count, true);
      count = 0;
    }
    


    private void bdat(byte[] b, int off, int len, boolean last)
      throws IOException
    {
      if ((len > 0) || (last)) {
        try {
          if (last) {
            sendCommand("BDAT " + len + " LAST");
          } else
            sendCommand("BDAT " + len);
          out.write(b, off, len);
          out.flush();
          int ret = readServerResponse();
          if (ret != 250)
            throw new IOException(lastServerResponse);
        } catch (MessagingException mex) {
          throw new IOException("BDAT write exception", mex);
        }
      }
    }
  }
}
