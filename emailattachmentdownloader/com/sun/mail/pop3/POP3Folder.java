package com.sun.mail.pop3;

import com.sun.mail.util.LineInputStream;
import com.sun.mail.util.MailLogger;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.util.StringTokenizer;
import java.util.logging.Level;
import javax.mail.FetchProfile;
import javax.mail.FetchProfile.Item;
import javax.mail.Flags;
import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.FolderClosedException;
import javax.mail.FolderNotFoundException;
import javax.mail.Message;
import javax.mail.MessageRemovedException;
import javax.mail.MessagingException;
import javax.mail.MethodNotSupportedException;
import javax.mail.UIDFolder.FetchProfileItem;










































public class POP3Folder
  extends Folder
{
  private String name;
  private POP3Store store;
  private volatile Protocol port;
  private int total;
  private int size;
  private boolean exists = false;
  private volatile boolean opened = false;
  private POP3Message[] message_cache;
  private boolean doneUidl = false;
  private volatile TempFile fileCache = null;
  private boolean forceClose;
  MailLogger logger;
  
  protected POP3Folder(POP3Store store, String name)
  {
    super(store);
    this.name = name;
    this.store = store;
    if (name.equalsIgnoreCase("INBOX")) {
      exists = true;
    }
    logger = new MailLogger(getClass(), "DEBUG POP3", store.getSession());
  }
  
  public String getName()
  {
    return name;
  }
  
  public String getFullName()
  {
    return name;
  }
  
  public Folder getParent()
  {
    return new DefaultFolder(store);
  }
  






  public boolean exists()
  {
    return exists;
  }
  





  public Folder[] list(String pattern)
    throws MessagingException
  {
    throw new MessagingException("not a directory");
  }
  





  public char getSeparator()
  {
    return '\000';
  }
  





  public int getType()
  {
    return 1;
  }
  





  public boolean create(int type)
    throws MessagingException
  {
    return false;
  }
  





  public boolean hasNewMessages()
    throws MessagingException
  {
    return false;
  }
  





  public Folder getFolder(String name)
    throws MessagingException
  {
    throw new MessagingException("not a directory");
  }
  






  public boolean delete(boolean recurse)
    throws MessagingException
  {
    throw new MethodNotSupportedException("delete");
  }
  





  public boolean renameTo(Folder f)
    throws MessagingException
  {
    throw new MethodNotSupportedException("renameTo");
  }
  







  public synchronized void open(int mode)
    throws MessagingException
  {
    checkClosed();
    if (!exists) {
      throw new FolderNotFoundException(this, "folder is not INBOX");
    }
    try {
      port = store.getPort(this);
      Status s = port.stat();
      total = total;
      size = size;
      this.mode = mode;
      if (store.useFileCache) {
        try {
          fileCache = new TempFile(store.fileCacheDir);
        } catch (IOException ex) {
          logger.log(Level.FINE, "failed to create file cache", ex);
          throw ex;
        }
      }
      opened = true;
    } catch (IOException ioex) {
      try {
        if (port != null) {
          port.quit();
        }
      }
      catch (IOException localIOException1) {}finally {
        port = null;
        store.closePort(this);
      }
      throw new MessagingException("Open failed", ioex);
    }
    

    message_cache = new POP3Message[total];
    doneUidl = false;
    
    notifyConnectionListeners(1);
  }
  
  public synchronized void close(boolean expunge) throws MessagingException
  {
    checkOpen();
    








    try
    {
      if ((store.rsetBeforeQuit) && (!forceClose)) {
        port.rset();
      }
      if ((expunge) && (mode == 2) && (!forceClose))
      {
        for (int i = 0; i < message_cache.length; i++) { POP3Message m;
          if (((m = message_cache[i]) != null) && 
            (m.isSet(Flags.Flag.DELETED))) {
            try {
              port.dele(i + 1);
            } catch (IOException ioex) {
              throw new MessagingException("Exception deleting messages during close", ioex);
            }
          }
        }
      }
      





      for (int i = 0; i < message_cache.length; i++) { POP3Message m;
        if ((m = message_cache[i]) != null) {
          m.invalidate(true);
        }
      }
      if (forceClose) {
        port.close();
      } else {
        port.quit();
      }
    }
    catch (IOException localIOException1) {}finally {
      port = null;
      store.closePort(this);
      message_cache = null;
      opened = false;
      notifyConnectionListeners(3);
      if (fileCache != null) {
        fileCache.close();
        fileCache = null;
      }
    }
  }
  
  public synchronized boolean isOpen()
  {
    if (!opened)
      return false;
    try {
      if (!port.noop())
        throw new IOException("NOOP failed");
    } catch (IOException ioex) {
      try {
        close(false);
      }
      catch (MessagingException localMessagingException) {}
      
      return false;
    }
    return true;
  }
  






  public Flags getPermanentFlags()
  {
    return new Flags();
  }
  




  public synchronized int getMessageCount()
    throws MessagingException
  {
    if (!opened)
      return -1;
    checkReadable();
    return total;
  }
  
  public synchronized Message getMessage(int msgno)
    throws MessagingException
  {
    checkOpen();
    

    POP3Message m;
    
    if ((m = message_cache[(msgno - 1)]) == null) {
      m = createMessage(this, msgno);
      message_cache[(msgno - 1)] = m;
    }
    return m;
  }
  
  protected POP3Message createMessage(Folder f, int msgno) throws MessagingException
  {
    POP3Message m = null;
    Constructor<?> cons = store.messageConstructor;
    if (cons != null) {
      try {
        Object[] o = { this, Integer.valueOf(msgno) };
        m = (POP3Message)cons.newInstance(o);
      }
      catch (Exception localException) {}
    }
    
    if (m == null)
      m = new POP3Message(this, msgno);
    return m;
  }
  





  public void appendMessages(Message[] msgs)
    throws MessagingException
  {
    throw new MethodNotSupportedException("Append not supported");
  }
  








  public Message[] expunge()
    throws MessagingException
  {
    throw new MethodNotSupportedException("Expunge not supported");
  }
  









  public synchronized void fetch(Message[] msgs, FetchProfile fp)
    throws MessagingException
  {
    checkReadable();
    if ((!doneUidl) && (store.supportsUidl) && 
      (fp.contains(UIDFolder.FetchProfileItem.UID)))
    {







      String[] uids = new String[message_cache.length];
      try {
        if (!port.uidl(uids))
          return;
      } catch (EOFException eex) {
        close(false);
        throw new FolderClosedException(this, eex.toString());
      } catch (IOException ex) {
        throw new MessagingException("error getting UIDL", ex);
      }
      for (int i = 0; i < uids.length; i++)
        if (uids[i] != null)
        {
          POP3Message m = (POP3Message)getMessage(i + 1);
          uid = uids[i];
        }
      doneUidl = true;
    }
    if (fp.contains(FetchProfile.Item.ENVELOPE)) {
      for (int i = 0; i < msgs.length; i++) {
        try {
          POP3Message msg = (POP3Message)msgs[i];
          
          msg.getHeader("");
          
          msg.getSize();
        }
        catch (MessageRemovedException localMessageRemovedException) {}
      }
    }
  }
  







  public synchronized String getUID(Message msg)
    throws MessagingException
  {
    checkOpen();
    if (!(msg instanceof POP3Message))
      throw new MessagingException("message is not a POP3Message");
    POP3Message m = (POP3Message)msg;
    try {
      if (!store.supportsUidl)
        return null;
      if (uid == "UNKNOWN")
        uid = port.uidl(m.getMessageNumber());
      return uid;
    } catch (EOFException eex) {
      close(false);
      throw new FolderClosedException(this, eex.toString());
    } catch (IOException ex) {
      throw new MessagingException("error getting UIDL", ex);
    }
  }
  






  public synchronized int getSize()
    throws MessagingException
  {
    checkOpen();
    return size;
  }
  








  public synchronized int[] getSizes()
    throws MessagingException
  {
    checkOpen();
    sizes = new int[total];
    InputStream is = null;
    LineInputStream lis = null;
    try {
      is = port.list();
      lis = new LineInputStream(is);
      String line;
      while ((line = lis.readLine()) != null) {
        try {
          StringTokenizer st = new StringTokenizer(line);
          int msgnum = Integer.parseInt(st.nextToken());
          int size = Integer.parseInt(st.nextToken());
          if ((msgnum > 0) && (msgnum <= total)) {
            sizes[(msgnum - 1)] = size;
          }
        }
        catch (RuntimeException localRuntimeException) {}
      }
      










      return sizes;
    }
    catch (IOException localIOException2) {}finally
    {
      try
      {
        if (lis != null)
          lis.close();
      } catch (IOException localIOException5) {}
      try {
        if (is != null) {
          is.close();
        }
      }
      catch (IOException localIOException6) {}
    }
  }
  







  public synchronized InputStream listCommand()
    throws MessagingException, IOException
  {
    checkOpen();
    return port.list();
  }
  


  protected void finalize()
    throws Throwable
  {
    forceClose = (!store.finalizeCleanClose);
    try {
      if (opened) {
        close(false);
      }
      super.finalize();
      forceClose = false;
    }
    finally
    {
      super.finalize();
      forceClose = false;
    }
  }
  
  private void checkOpen() throws IllegalStateException
  {
    if (!opened) {
      throw new IllegalStateException("Folder is not Open");
    }
  }
  
  private void checkClosed() throws IllegalStateException {
    if (opened) {
      throw new IllegalStateException("Folder is Open");
    }
  }
  
  private void checkReadable() throws IllegalStateException {
    if ((!opened) || ((mode != 1) && (mode != 2))) {
      throw new IllegalStateException("Folder is not Readable");
    }
  }
  










  Protocol getProtocol()
    throws MessagingException
  {
    Protocol p = port;
    checkOpen();
    
    return p;
  }
  



  protected void notifyMessageChangedListeners(int type, Message m)
  {
    super.notifyMessageChangedListeners(type, m);
  }
  


  TempFile getFileCache()
  {
    return fileCache;
  }
}
