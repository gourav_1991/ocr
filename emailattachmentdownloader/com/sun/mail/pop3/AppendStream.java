package com.sun.mail.pop3;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;











































class AppendStream
  extends OutputStream
{
  private final WritableSharedFile tf;
  private RandomAccessFile raf;
  private final long start;
  private long end;
  
  public AppendStream(WritableSharedFile tf)
    throws IOException
  {
    this.tf = tf;
    raf = tf.getWritableFile();
    start = raf.length();
    raf.seek(start);
  }
  
  public void write(int b) throws IOException
  {
    raf.write(b);
  }
  
  public void write(byte[] b) throws IOException
  {
    raf.write(b);
  }
  
  public void write(byte[] b, int off, int len) throws IOException
  {
    raf.write(b, off, len);
  }
  
  public synchronized void close() throws IOException
  {
    end = tf.updateLength();
    raf = null;
  }
  
  public synchronized InputStream getInputStream() throws IOException {
    return tf.newStream(start, end);
  }
}
