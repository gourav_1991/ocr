package com.sun.mail.pop3;

import com.sun.mail.util.LineInputStream;
import com.sun.mail.util.MailLogger;
import com.sun.mail.util.PropUtil;
import com.sun.mail.util.SharedByteArrayOutputStream;
import com.sun.mail.util.SocketFetcher;
import com.sun.mail.util.TraceInputStream;
import com.sun.mail.util.TraceOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.SocketException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.logging.Level;
import javax.net.ssl.SSLSocket;

































class Protocol
{
  private Socket socket;
  private String host;
  private Properties props;
  private String prefix;
  private BufferedReader input;
  private PrintWriter output;
  private TraceInputStream traceInput;
  private TraceOutputStream traceOutput;
  private MailLogger logger;
  private MailLogger traceLogger;
  private String apopChallenge = null;
  private Map<String, String> capabilities = null;
  private boolean pipelining;
  private boolean noauthdebug = true;
  
  private boolean traceSuspended;
  
  private static final int POP3_PORT = 110;
  
  private static final String CRLF = "\r\n";
  
  private static final int SLOP = 128;
  

  Protocol(String host, int port, MailLogger logger, Properties props, String prefix, boolean isSSL)
    throws IOException
  {
    this.host = host;
    this.props = props;
    this.prefix = prefix;
    this.logger = logger;
    traceLogger = logger.getSubLogger("protocol", null);
    noauthdebug = (!PropUtil.getBooleanProperty(props, "mail.debug.auth", false));
    


    boolean enableAPOP = getBoolProp(props, prefix + ".apop.enable");
    boolean disableCapa = getBoolProp(props, prefix + ".disablecapa");
    try {
      if (port == -1)
        port = 110;
      if (logger.isLoggable(Level.FINE)) {
        logger.fine("connecting to host \"" + host + "\", port " + port + ", isSSL " + isSSL);
      }
      
      socket = SocketFetcher.getSocket(host, port, props, prefix, isSSL);
      initStreams();
      r = simpleCommand(null);
    } catch (IOException ioe) { Response r;
      throw cleanupAndThrow(socket, ioe);
    }
    Response r;
    if (!ok) {
      throw cleanupAndThrow(socket, new IOException("Connect failed"));
    }
    if ((enableAPOP) && (data != null)) {
      int challStart = data.indexOf('<');
      int challEnd = data.indexOf('>', challStart);
      if ((challStart != -1) && (challEnd != -1))
        apopChallenge = data.substring(challStart, challEnd + 1);
      logger.log(Level.FINE, "APOP challenge: {0}", apopChallenge);
    }
    

    if (!disableCapa) {
      setCapabilities(capa());
    }
    
    pipelining = ((hasCapability("PIPELINING")) || (PropUtil.getBooleanProperty(props, prefix + ".pipelining", false)));
    if (pipelining)
      logger.config("PIPELINING enabled");
  }
  
  private static IOException cleanupAndThrow(Socket socket, IOException ife) {
    try {
      socket.close();
    } catch (Throwable thr) {
      if (isRecoverable(thr)) {
        ife.addSuppressed(thr);
      } else {
        thr.addSuppressed(ife);
        if ((thr instanceof Error)) {
          throw ((Error)thr);
        }
        if ((thr instanceof RuntimeException)) {
          throw ((RuntimeException)thr);
        }
        throw new RuntimeException("unexpected exception", thr);
      }
    }
    return ife;
  }
  
  private static boolean isRecoverable(Throwable t) {
    return ((t instanceof Exception)) || ((t instanceof LinkageError));
  }
  




  private final synchronized boolean getBoolProp(Properties props, String prop)
  {
    boolean val = PropUtil.getBooleanProperty(props, prop, false);
    if (logger.isLoggable(Level.CONFIG))
      logger.config(prop + ": " + val);
    return val;
  }
  
  private void initStreams() throws IOException {
    boolean quote = PropUtil.getBooleanProperty(props, "mail.debug.quote", false);
    

    traceInput = new TraceInputStream(socket.getInputStream(), traceLogger);
    traceInput.setQuote(quote);
    

    traceOutput = new TraceOutputStream(socket.getOutputStream(), traceLogger);
    traceOutput.setQuote(quote);
    

    input = new BufferedReader(new InputStreamReader(traceInput, "iso-8859-1"));
    
    output = new PrintWriter(new BufferedWriter(new OutputStreamWriter(traceOutput, "iso-8859-1")));
  }
  
  protected void finalize()
    throws Throwable
  {
    try
    {
      if (socket != null) {
        quit();
      }
      super.finalize(); } finally { super.finalize();
    }
  }
  


  synchronized void setCapabilities(InputStream in)
  {
    if (in == null) {
      capabilities = null;
      return;
    }
    
    capabilities = new HashMap(10);
    BufferedReader r = null;
    try {
      r = new BufferedReader(new InputStreamReader(in, "us-ascii"));
    }
    catch (UnsupportedEncodingException ex) {
      if (!$assertionsDisabled) throw new AssertionError();
    }
    try {
      String s;
      while ((s = r.readLine()) != null) {
        String cap = s;
        int i = cap.indexOf(' ');
        if (i > 0)
          cap = cap.substring(0, i);
        capabilities.put(cap.toUpperCase(Locale.ENGLISH), s);
      }
      return;
    }
    catch (IOException localIOException1) {}finally {
      try {
        in.close();
      }
      catch (IOException localIOException3) {}
    }
  }
  



  synchronized boolean hasCapability(String c)
  {
    return (capabilities != null) && 
      (capabilities.containsKey(c.toUpperCase(Locale.ENGLISH)));
  }
  


  synchronized Map<String, String> getCapabilities()
  {
    return capabilities;
  }
  




  synchronized String login(String user, String password)
    throws IOException
  {
    boolean batch = (pipelining) && ((socket instanceof SSLSocket));
    
    try
    {
      if ((noauthdebug) && (isTracing())) {
        logger.fine("authentication command trace suppressed");
        suspendTracing();
      }
      String dpw = null;
      if (apopChallenge != null)
        dpw = getDigest(password);
      Response r; String cmd; Response r; if ((apopChallenge != null) && (dpw != null)) {
        r = simpleCommand("APOP " + user + " " + dpw);
      } else if (batch) {
        cmd = "USER " + user;
        batchCommandStart(cmd);
        issueCommand(cmd);
        cmd = "PASS " + password;
        batchCommandContinue(cmd);
        issueCommand(cmd);
        Response r = readResponse();
        if (!ok) {
          String err = data != null ? data : "USER command failed";
          readResponse();
          batchCommandEnd();
          return err;
        }
        r = readResponse();
        batchCommandEnd();
      } else {
        r = simpleCommand("USER " + user);
        if (!ok)
          return data != null ? data : "USER command failed";
        r = simpleCommand("PASS " + password);
      }
      if ((noauthdebug) && (isTracing())) {
        logger.log(Level.FINE, "authentication command {0}", ok ? "succeeded" : "failed");
      }
      if (!ok)
        return data != null ? data : "login failed";
      return null;
    }
    finally {
      resumeTracing();
    }
  }
  












  private String getDigest(String password)
  {
    String key = apopChallenge + password;
    try
    {
      MessageDigest md = MessageDigest.getInstance("MD5");
      digest = md.digest(key.getBytes("iso-8859-1"));
    } catch (NoSuchAlgorithmException nsae) { byte[] digest;
      return null;
    } catch (UnsupportedEncodingException uee) {
      return null; }
    byte[] digest;
    return toHex(digest);
  }
  
  private static char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
  





  private static String toHex(byte[] bytes)
  {
    char[] result = new char[bytes.length * 2];
    
    int index = 0; for (int i = 0; index < bytes.length; index++) {
      int temp = bytes[index] & 0xFF;
      result[(i++)] = digits[(temp >> 4)];
      result[(i++)] = digits[(temp & 0xF)];
    }
    return new String(result);
  }
  

  synchronized boolean quit()
    throws IOException
  {
    boolean ok = false;
    try {
      Response r = simpleCommand("QUIT");
      ok = ok;
      
      close(); } finally { close();
    }
    return ok;
  }
  
  /* Error */
  void close()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 31	com/sun/mail/pop3/Protocol:socket	Ljava/net/Socket;
    //   4: invokevirtual 53	java/net/Socket:close	()V
    //   7: aload_0
    //   8: aconst_null
    //   9: putfield 31	com/sun/mail/pop3/Protocol:socket	Ljava/net/Socket;
    //   12: aload_0
    //   13: aconst_null
    //   14: putfield 82	com/sun/mail/pop3/Protocol:input	Ljava/io/BufferedReader;
    //   17: aload_0
    //   18: aconst_null
    //   19: putfield 89	com/sun/mail/pop3/Protocol:output	Ljava/io/PrintWriter;
    //   22: goto +40 -> 62
    //   25: astore_1
    //   26: aload_0
    //   27: aconst_null
    //   28: putfield 31	com/sun/mail/pop3/Protocol:socket	Ljava/net/Socket;
    //   31: aload_0
    //   32: aconst_null
    //   33: putfield 82	com/sun/mail/pop3/Protocol:input	Ljava/io/BufferedReader;
    //   36: aload_0
    //   37: aconst_null
    //   38: putfield 89	com/sun/mail/pop3/Protocol:output	Ljava/io/PrintWriter;
    //   41: goto +21 -> 62
    //   44: astore_2
    //   45: aload_0
    //   46: aconst_null
    //   47: putfield 31	com/sun/mail/pop3/Protocol:socket	Ljava/net/Socket;
    //   50: aload_0
    //   51: aconst_null
    //   52: putfield 82	com/sun/mail/pop3/Protocol:input	Ljava/io/BufferedReader;
    //   55: aload_0
    //   56: aconst_null
    //   57: putfield 89	com/sun/mail/pop3/Protocol:output	Ljava/io/PrintWriter;
    //   60: aload_2
    //   61: athrow
    //   62: return
    // Line number table:
    //   Java source line #376	-> byte code offset #0
    //   Java source line #380	-> byte code offset #7
    //   Java source line #381	-> byte code offset #12
    //   Java source line #382	-> byte code offset #17
    //   Java source line #383	-> byte code offset #22
    //   Java source line #377	-> byte code offset #25
    //   Java source line #380	-> byte code offset #26
    //   Java source line #381	-> byte code offset #31
    //   Java source line #382	-> byte code offset #36
    //   Java source line #383	-> byte code offset #41
    //   Java source line #380	-> byte code offset #44
    //   Java source line #381	-> byte code offset #50
    //   Java source line #382	-> byte code offset #55
    //   Java source line #384	-> byte code offset #62
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	63	0	this	Protocol
    //   25	1	1	localIOException	IOException
    //   44	17	2	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   0	7	25	java/io/IOException
    //   0	7	44	finally
  }
  
  synchronized Status stat()
    throws IOException
  {
    Response r = simpleCommand("STAT");
    Status s = new Status();
    








    if (!ok) {
      throw new IOException("STAT command failed: " + data);
    }
    if (data != null) {
      try {
        StringTokenizer st = new StringTokenizer(data);
        total = Integer.parseInt(st.nextToken());
        size = Integer.parseInt(st.nextToken());
      }
      catch (RuntimeException localRuntimeException) {}
    }
    return s;
  }
  

  synchronized int list(int msg)
    throws IOException
  {
    Response r = simpleCommand("LIST " + msg);
    int size = -1;
    if ((ok) && (data != null)) {
      try {
        StringTokenizer st = new StringTokenizer(data);
        st.nextToken();
        size = Integer.parseInt(st.nextToken());
      }
      catch (RuntimeException localRuntimeException) {}
    }
    
    return size;
  }
  

  synchronized InputStream list()
    throws IOException
  {
    Response r = multilineCommand("LIST", 128);
    return bytes;
  }
  






  synchronized InputStream retr(int msg, int size)
    throws IOException
  {
    boolean batch = (size == 0) && (pipelining);
    Response r; if (batch) {
      String cmd = "LIST " + msg;
      batchCommandStart(cmd);
      issueCommand(cmd);
      cmd = "RETR " + msg;
      batchCommandContinue(cmd);
      issueCommand(cmd);
      Response r = readResponse();
      if ((ok) && (data != null)) {
        try
        {
          StringTokenizer st = new StringTokenizer(data);
          st.nextToken();
          size = Integer.parseInt(st.nextToken());
          
          if ((size > 1073741824) || (size < 0)) {
            size = 0;
          } else {
            if (logger.isLoggable(Level.FINE))
              logger.fine("pipeline message size " + size);
            size += 128;
          }
        }
        catch (RuntimeException localRuntimeException) {}
      }
      r = readResponse();
      if (ok)
        bytes = readMultilineResponse(size + 128);
      batchCommandEnd();
    } else {
      String cmd = "RETR " + msg;
      multilineCommandStart(cmd);
      issueCommand(cmd);
      r = readResponse();
      if (!ok) {
        multilineCommandEnd();
        return null;
      }
      







      if ((size <= 0) && (data != null)) {
        try {
          StringTokenizer st = new StringTokenizer(data);
          String s = st.nextToken();
          String octets = st.nextToken();
          if (octets.equals("octets")) {
            size = Integer.parseInt(s);
            
            if ((size > 1073741824) || (size < 0)) {
              size = 0;
            } else {
              if (logger.isLoggable(Level.FINE))
                logger.fine("guessing message size: " + size);
              size += 128;
            }
          }
        }
        catch (RuntimeException localRuntimeException1) {}
      }
      bytes = readMultilineResponse(size);
      multilineCommandEnd();
    }
    if ((ok) && 
      (size > 0) && (logger.isLoggable(Level.FINE))) {
      logger.fine("got message size " + bytes.available());
    }
    return bytes;
  }
  


  synchronized boolean retr(int msg, OutputStream os)
    throws IOException
  {
    String cmd = "RETR " + msg;
    multilineCommandStart(cmd);
    issueCommand(cmd);
    Response r = readResponse();
    if (!ok) {
      multilineCommandEnd();
      return false;
    }
    
    Throwable terr = null;
    int lastb = 10;
    try { int b;
      while ((b = input.read()) >= 0) {
        if ((lastb == 10) && (b == 46)) {
          b = input.read();
          if (b == 13)
          {
            b = input.read();
            break;
          }
        }
        




        if (terr == null) {
          try {
            os.write(b);
          } catch (IOException ex) {
            logger.log(Level.FINE, "exception while streaming", ex);
            terr = ex;
          } catch (RuntimeException ex) {
            logger.log(Level.FINE, "exception while streaming", ex);
            terr = ex;
          }
        }
        lastb = b;
      }
    }
    catch (InterruptedIOException iioex)
    {
      try
      {
        socket.close();
      } catch (IOException localIOException1) {}
      throw iioex; }
    int b;
    if (b < 0) {
      throw new EOFException("EOF on socket");
    }
    
    if (terr != null) {
      if ((terr instanceof IOException))
        throw ((IOException)terr);
      if ((terr instanceof RuntimeException))
        throw ((RuntimeException)terr);
      if (!$assertionsDisabled) throw new AssertionError();
    }
    multilineCommandEnd();
    return true;
  }
  

  synchronized InputStream top(int msg, int n)
    throws IOException
  {
    Response r = multilineCommand("TOP " + msg + " " + n, 0);
    return bytes;
  }
  

  synchronized boolean dele(int msg)
    throws IOException
  {
    Response r = simpleCommand("DELE " + msg);
    return ok;
  }
  

  synchronized String uidl(int msg)
    throws IOException
  {
    Response r = simpleCommand("UIDL " + msg);
    if (!ok)
      return null;
    int i = data.indexOf(' ');
    if (i > 0) {
      return data.substring(i + 1);
    }
    return null;
  }
  


  synchronized boolean uidl(String[] uids)
    throws IOException
  {
    Response r = multilineCommand("UIDL", 15 * uids.length);
    if (!ok)
      return false;
    LineInputStream lis = new LineInputStream(bytes);
    String line = null;
    while ((line = lis.readLine()) != null) {
      int i = line.indexOf(' ');
      if ((i >= 1) && (i < line.length()))
      {
        int n = Integer.parseInt(line.substring(0, i));
        if ((n > 0) && (n <= uids.length))
          uids[(n - 1)] = line.substring(i + 1);
      }
    }
    try { bytes.close();
    }
    catch (IOException localIOException) {}
    
    return true;
  }
  

  synchronized boolean noop()
    throws IOException
  {
    Response r = simpleCommand("NOOP");
    return ok;
  }
  

  synchronized boolean rset()
    throws IOException
  {
    Response r = simpleCommand("RSET");
    return ok;
  }
  


  synchronized boolean stls()
    throws IOException
  {
    if ((socket instanceof SSLSocket))
      return true;
    Response r = simpleCommand("STLS");
    if (ok)
      try
      {
        socket = SocketFetcher.startTLS(socket, host, props, prefix);
        initStreams();
      } catch (IOException ioex) {
        for (;;) { IOException sioex;
          try { socket.close();
            
            socket = null;
            input = null;
            output = null;
          }
          finally
          {
            socket = null;
            input = null;
            output = null;throw localObject;
            
            sioex = new IOException("Could not convert socket to TLS");
            
            sioex.initCause(ioex);
          }
        }
      }
    return ok;
  }
  


  synchronized boolean isSSL()
  {
    return socket instanceof SSLSocket;
  }
  


  synchronized InputStream capa()
    throws IOException
  {
    Response r = multilineCommand("CAPA", 128);
    if (!ok)
      return null;
    return bytes;
  }
  

  private Response simpleCommand(String cmd)
    throws IOException
  {
    simpleCommandStart(cmd);
    issueCommand(cmd);
    Response r = readResponse();
    simpleCommandEnd();
    return r;
  }
  

  private void issueCommand(String cmd)
    throws IOException
  {
    if (socket == null) {
      throw new IOException("Folder is closed");
    }
    if (cmd != null) {
      cmd = cmd + "\r\n";
      output.print(cmd);
      output.flush();
    }
  }
  

  private Response readResponse()
    throws IOException
  {
    String line = null;
    try {
      line = input.readLine();

    }
    catch (InterruptedIOException iioex)
    {

      try
      {

        socket.close();
      } catch (IOException localIOException) {}
      throw new EOFException(iioex.getMessage());

    }
    catch (SocketException ex)
    {

      try
      {

        socket.close();
      } catch (IOException localIOException1) {}
      throw new EOFException(ex.getMessage());
    }
    
    if (line == null) {
      traceLogger.finest("<EOF>");
      throw new EOFException("EOF on socket");
    }
    Response r = new Response();
    if (line.startsWith("+OK")) {
      ok = true;
    } else if (line.startsWith("-ERR")) {
      ok = false;
    } else
      throw new IOException("Unexpected response: " + line);
    int i;
    if ((i = line.indexOf(' ')) >= 0)
      data = line.substring(i + 1);
    return r;
  }
  


  private Response multilineCommand(String cmd, int size)
    throws IOException
  {
    multilineCommandStart(cmd);
    issueCommand(cmd);
    Response r = readResponse();
    if (!ok) {
      multilineCommandEnd();
      return r;
    }
    bytes = readMultilineResponse(size);
    multilineCommandEnd();
    return r;
  }
  




  private InputStream readMultilineResponse(int size)
    throws IOException
  {
    SharedByteArrayOutputStream buf = new SharedByteArrayOutputStream(size);
    int lastb = 10;
    try { int b;
      while ((b = input.read()) >= 0) {
        if ((lastb == 10) && (b == 46)) {
          b = input.read();
          if (b == 13)
          {
            b = input.read();
            break;
          }
        }
        buf.write(b);
        lastb = b;
      }
    }
    catch (InterruptedIOException iioex)
    {
      try
      {
        socket.close();
      } catch (IOException localIOException) {}
      throw iioex; }
    int b;
    if (b < 0)
      throw new EOFException("EOF on socket");
    return buf.toStream();
  }
  


  protected boolean isTracing()
  {
    return traceLogger.isLoggable(Level.FINEST);
  }
  



  private void suspendTracing()
  {
    if (traceLogger.isLoggable(Level.FINEST)) {
      traceInput.setTrace(false);
      traceOutput.setTrace(false);
    }
  }
  


  private void resumeTracing()
  {
    if (traceLogger.isLoggable(Level.FINEST)) {
      traceInput.setTrace(true);
      traceOutput.setTrace(true);
    }
  }
  
  private void simpleCommandStart(String command) {}
  
  private void simpleCommandEnd() {}
  
  private void multilineCommandStart(String command) {}
  
  private void multilineCommandEnd() {}
  
  private void batchCommandStart(String command) {}
  
  private void batchCommandContinue(String command) {}
  
  private void batchCommandEnd() {}
}
