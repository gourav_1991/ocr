package com.sun.mail.pop3;

import com.sun.mail.util.MailLogger;
import com.sun.mail.util.ReadableMime;
import java.io.BufferedOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.SoftReference;
import java.util.Enumeration;
import java.util.logging.Level;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.FolderClosedException;
import javax.mail.Header;
import javax.mail.IllegalWriteException;
import javax.mail.MessageRemovedException;
import javax.mail.MessagingException;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.SharedInputStream;









































public class POP3Message
  extends MimeMessage
  implements ReadableMime
{
  static final String UNKNOWN = "UNKNOWN";
  private POP3Folder folder;
  private int hdrSize = -1;
  private int msgSize = -1;
  String uid = "UNKNOWN";
  

  private SoftReference<InputStream> rawData = new SoftReference(null);
  
  public POP3Message(Folder folder, int msgno)
    throws MessagingException
  {
    super(folder, msgno);
    assert ((folder instanceof POP3Folder));
    this.folder = ((POP3Folder)folder);
  }
  






  public synchronized void setFlags(Flags newFlags, boolean set)
    throws MessagingException
  {
    Flags oldFlags = (Flags)flags.clone();
    super.setFlags(newFlags, set);
    if (!flags.equals(oldFlags)) {
      folder.notifyMessageChangedListeners(1, this);
    }
  }
  
  /* Error */
  public int getSize()
    throws MessagingException
  {
    // Byte code:
    //   0: aload_0
    //   1: dup
    //   2: astore_1
    //   3: monitorenter
    //   4: aload_0
    //   5: getfield 3	com/sun/mail/pop3/POP3Message:msgSize	I
    //   8: ifle +10 -> 18
    //   11: aload_0
    //   12: getfield 3	com/sun/mail/pop3/POP3Message:msgSize	I
    //   15: aload_1
    //   16: monitorexit
    //   17: ireturn
    //   18: aload_1
    //   19: monitorexit
    //   20: goto +8 -> 28
    //   23: astore_2
    //   24: aload_1
    //   25: monitorexit
    //   26: aload_2
    //   27: athrow
    //   28: aload_0
    //   29: getfield 22	com/sun/mail/pop3/POP3Message:headers	Ljavax/mail/internet/InternetHeaders;
    //   32: ifnonnull +7 -> 39
    //   35: aload_0
    //   36: invokespecial 23	com/sun/mail/pop3/POP3Message:loadHeaders	()V
    //   39: aload_0
    //   40: dup
    //   41: astore_1
    //   42: monitorenter
    //   43: aload_0
    //   44: getfield 3	com/sun/mail/pop3/POP3Message:msgSize	I
    //   47: ifge +26 -> 73
    //   50: aload_0
    //   51: aload_0
    //   52: getfield 14	com/sun/mail/pop3/POP3Message:folder	Lcom/sun/mail/pop3/POP3Folder;
    //   55: invokevirtual 24	com/sun/mail/pop3/POP3Folder:getProtocol	()Lcom/sun/mail/pop3/Protocol;
    //   58: aload_0
    //   59: getfield 25	com/sun/mail/pop3/POP3Message:msgnum	I
    //   62: invokevirtual 26	com/sun/mail/pop3/Protocol:list	(I)I
    //   65: aload_0
    //   66: getfield 2	com/sun/mail/pop3/POP3Message:hdrSize	I
    //   69: isub
    //   70: putfield 3	com/sun/mail/pop3/POP3Message:msgSize	I
    //   73: aload_0
    //   74: getfield 3	com/sun/mail/pop3/POP3Message:msgSize	I
    //   77: aload_1
    //   78: monitorexit
    //   79: ireturn
    //   80: astore_3
    //   81: aload_1
    //   82: monitorexit
    //   83: aload_3
    //   84: athrow
    //   85: astore_1
    //   86: aload_0
    //   87: getfield 14	com/sun/mail/pop3/POP3Message:folder	Lcom/sun/mail/pop3/POP3Folder;
    //   90: iconst_0
    //   91: invokevirtual 28	com/sun/mail/pop3/POP3Folder:close	(Z)V
    //   94: new 29	javax/mail/FolderClosedException
    //   97: dup
    //   98: aload_0
    //   99: getfield 14	com/sun/mail/pop3/POP3Message:folder	Lcom/sun/mail/pop3/POP3Folder;
    //   102: aload_1
    //   103: invokevirtual 30	java/io/EOFException:toString	()Ljava/lang/String;
    //   106: invokespecial 31	javax/mail/FolderClosedException:<init>	(Ljavax/mail/Folder;Ljava/lang/String;)V
    //   109: athrow
    //   110: astore_1
    //   111: new 33	javax/mail/MessagingException
    //   114: dup
    //   115: ldc 34
    //   117: aload_1
    //   118: invokespecial 35	javax/mail/MessagingException:<init>	(Ljava/lang/String;Ljava/lang/Exception;)V
    //   121: athrow
    // Line number table:
    //   Java source line #115	-> byte code offset #0
    //   Java source line #117	-> byte code offset #4
    //   Java source line #118	-> byte code offset #11
    //   Java source line #119	-> byte code offset #18
    //   Java source line #136	-> byte code offset #28
    //   Java source line #137	-> byte code offset #35
    //   Java source line #139	-> byte code offset #39
    //   Java source line #140	-> byte code offset #43
    //   Java source line #141	-> byte code offset #50
    //   Java source line #142	-> byte code offset #73
    //   Java source line #143	-> byte code offset #80
    //   Java source line #144	-> byte code offset #85
    //   Java source line #145	-> byte code offset #86
    //   Java source line #146	-> byte code offset #94
    //   Java source line #147	-> byte code offset #110
    //   Java source line #148	-> byte code offset #111
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	122	0	this	POP3Message
    //   85	18	1	eex	EOFException
    //   110	8	1	ex	IOException
    //   23	4	2	localObject1	Object
    //   80	4	3	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   4	17	23	finally
    //   18	20	23	finally
    //   23	26	23	finally
    //   43	79	80	finally
    //   80	83	80	finally
    //   0	17	85	java/io/EOFException
    //   18	79	85	java/io/EOFException
    //   80	85	85	java/io/EOFException
    //   0	17	110	java/io/IOException
    //   18	79	110	java/io/IOException
    //   80	85	110	java/io/IOException
  }
  
  private InputStream getRawStream(boolean skipHeader)
    throws MessagingException
  {
    InputStream rawcontent = null;
    try {
      synchronized (this) {
        rawcontent = (InputStream)rawData.get();
        if (rawcontent == null) {
          TempFile cache = folder.getFileCache();
          if (cache != null) {
            if (folder.logger.isLoggable(Level.FINE)) {
              folder.logger.fine("caching message #" + msgnum + " in temp file");
            }
            AppendStream os = cache.getAppendStream();
            BufferedOutputStream bos = new BufferedOutputStream(os);
            try {
              folder.getProtocol().retr(msgnum, bos);
            } finally {
              bos.close();
            }
            rawcontent = os.getInputStream();
          } else {
            rawcontent = folder.getProtocol().retr(msgnum, msgSize > 0 ? msgSize + hdrSize : 0);
          }
          
          if (rawcontent == null) {
            expunged = true;
            throw new MessageRemovedException("can't retrieve message #" + msgnum + " in POP3Message.getContentStream");
          }
          


          if ((headers == null) || 
            (folder.getStore()).forgetTopHeaders)) {
            headers = new InternetHeaders(rawcontent);
            
            hdrSize = ((int)((SharedInputStream)rawcontent).getPosition());





          }
          else
          {





            int offset = 0;
            for (;;) {
              int len = 0;
              int c1;
              while (((c1 = rawcontent.read()) >= 0) && 
                (c1 != 10))
              {
                if (c1 == 13)
                {
                  if (rawcontent.available() <= 0) break;
                  rawcontent.mark(1);
                  if (rawcontent.read() == 10) break;
                  rawcontent.reset(); break;
                }
                



                len++;
              }
              


              if (rawcontent.available() == 0) {
                break;
              }
              
              if (len == 0) {
                break;
              }
            }
            hdrSize = ((int)((SharedInputStream)rawcontent).getPosition());
          }
          

          msgSize = rawcontent.available();
          
          rawData = new SoftReference(rawcontent);
        }
      }
    } catch (EOFException eex) {
      folder.close(false);
      throw new FolderClosedException(folder, eex.toString());
    } catch (IOException ex) {
      throw new MessagingException("error fetching POP3 content", ex);
    }
    





    rawcontent = ((SharedInputStream)rawcontent).newStream(skipHeader ? hdrSize : 0L, -1L);
    
    return rawcontent;
  }
  






  protected synchronized InputStream getContentStream()
    throws MessagingException
  {
    if (contentStream != null) {
      return ((SharedInputStream)contentStream).newStream(0L, -1L);
    }
    InputStream cstream = getRawStream(true);
    




    TempFile cache = folder.getFileCache();
    if ((cache != null) || 
      (folder.getStore()).keepMessageContent))
      contentStream = ((SharedInputStream)cstream).newStream(0L, -1L);
    return cstream;
  }
  





  public InputStream getMimeStream()
    throws MessagingException
  {
    return getRawStream(false);
  }
  







  public synchronized void invalidate(boolean invalidateHeaders)
  {
    content = null;
    InputStream rstream = (InputStream)rawData.get();
    if (rstream != null)
    {
      try
      {
        rstream.close();
      }
      catch (IOException localIOException) {}
      
      rawData = new SoftReference(null);
    }
    if (contentStream != null) {
      try {
        contentStream.close();
      }
      catch (IOException localIOException1) {}
      
      contentStream = null;
    }
    msgSize = -1;
    if (invalidateHeaders) {
      headers = null;
      hdrSize = -1;
    }
  }
  
  /* Error */
  public InputStream top(int n)
    throws MessagingException
  {
    // Byte code:
    //   0: aload_0
    //   1: dup
    //   2: astore_2
    //   3: monitorenter
    //   4: aload_0
    //   5: getfield 14	com/sun/mail/pop3/POP3Message:folder	Lcom/sun/mail/pop3/POP3Folder;
    //   8: invokevirtual 24	com/sun/mail/pop3/POP3Folder:getProtocol	()Lcom/sun/mail/pop3/Protocol;
    //   11: aload_0
    //   12: getfield 25	com/sun/mail/pop3/POP3Message:msgnum	I
    //   15: iload_1
    //   16: invokevirtual 82	com/sun/mail/pop3/Protocol:top	(II)Ljava/io/InputStream;
    //   19: aload_2
    //   20: monitorexit
    //   21: areturn
    //   22: astore_3
    //   23: aload_2
    //   24: monitorexit
    //   25: aload_3
    //   26: athrow
    //   27: astore_2
    //   28: aload_0
    //   29: getfield 14	com/sun/mail/pop3/POP3Message:folder	Lcom/sun/mail/pop3/POP3Folder;
    //   32: iconst_0
    //   33: invokevirtual 28	com/sun/mail/pop3/POP3Folder:close	(Z)V
    //   36: new 29	javax/mail/FolderClosedException
    //   39: dup
    //   40: aload_0
    //   41: getfield 14	com/sun/mail/pop3/POP3Message:folder	Lcom/sun/mail/pop3/POP3Folder;
    //   44: aload_2
    //   45: invokevirtual 30	java/io/EOFException:toString	()Ljava/lang/String;
    //   48: invokespecial 31	javax/mail/FolderClosedException:<init>	(Ljavax/mail/Folder;Ljava/lang/String;)V
    //   51: athrow
    //   52: astore_2
    //   53: new 33	javax/mail/MessagingException
    //   56: dup
    //   57: ldc 34
    //   59: aload_2
    //   60: invokespecial 35	javax/mail/MessagingException:<init>	(Ljava/lang/String;Ljava/lang/Exception;)V
    //   63: athrow
    // Line number table:
    //   Java source line #346	-> byte code offset #0
    //   Java source line #347	-> byte code offset #4
    //   Java source line #348	-> byte code offset #22
    //   Java source line #349	-> byte code offset #27
    //   Java source line #350	-> byte code offset #28
    //   Java source line #351	-> byte code offset #36
    //   Java source line #352	-> byte code offset #52
    //   Java source line #353	-> byte code offset #53
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	64	0	this	POP3Message
    //   0	64	1	n	int
    //   27	18	2	eex	EOFException
    //   52	8	2	ex	IOException
    //   22	4	3	localObject1	Object
    // Exception table:
    //   from	to	target	type
    //   4	21	22	finally
    //   22	25	22	finally
    //   0	21	27	java/io/EOFException
    //   22	27	27	java/io/EOFException
    //   0	21	52	java/io/IOException
    //   22	27	52	java/io/IOException
  }
  
  public String[] getHeader(String name)
    throws MessagingException
  {
    if (headers == null)
      loadHeaders();
    return headers.getHeader(name);
  }
  












  public String getHeader(String name, String delimiter)
    throws MessagingException
  {
    if (headers == null)
      loadHeaders();
    return headers.getHeader(name, delimiter);
  }
  














  public void setHeader(String name, String value)
    throws MessagingException
  {
    throw new IllegalWriteException("POP3 messages are read-only");
  }
  













  public void addHeader(String name, String value)
    throws MessagingException
  {
    throw new IllegalWriteException("POP3 messages are read-only");
  }
  










  public void removeHeader(String name)
    throws MessagingException
  {
    throw new IllegalWriteException("POP3 messages are read-only");
  }
  











  public Enumeration<Header> getAllHeaders()
    throws MessagingException
  {
    if (headers == null)
      loadHeaders();
    return headers.getAllHeaders();
  }
  






  public Enumeration<Header> getMatchingHeaders(String[] names)
    throws MessagingException
  {
    if (headers == null)
      loadHeaders();
    return headers.getMatchingHeaders(names);
  }
  






  public Enumeration<Header> getNonMatchingHeaders(String[] names)
    throws MessagingException
  {
    if (headers == null)
      loadHeaders();
    return headers.getNonMatchingHeaders(names);
  }
  









  public void addHeaderLine(String line)
    throws MessagingException
  {
    throw new IllegalWriteException("POP3 messages are read-only");
  }
  






  public Enumeration<String> getAllHeaderLines()
    throws MessagingException
  {
    if (headers == null)
      loadHeaders();
    return headers.getAllHeaderLines();
  }
  







  public Enumeration<String> getMatchingHeaderLines(String[] names)
    throws MessagingException
  {
    if (headers == null)
      loadHeaders();
    return headers.getMatchingHeaderLines(names);
  }
  







  public Enumeration<String> getNonMatchingHeaderLines(String[] names)
    throws MessagingException
  {
    if (headers == null)
      loadHeaders();
    return headers.getNonMatchingHeaderLines(names);
  }
  







  public void saveChanges()
    throws MessagingException
  {
    throw new IllegalWriteException("POP3 messages are read-only");
  }
  














  public synchronized void writeTo(OutputStream os, String[] ignoreList)
    throws IOException, MessagingException
  {
    InputStream rawcontent = (InputStream)rawData.get();
    if ((rawcontent == null) && (ignoreList == null) && 
      (!folder.getStore()).cacheWriteTo)) {
      if (folder.logger.isLoggable(Level.FINE))
        folder.logger.fine("streaming msg " + msgnum);
      if (!folder.getProtocol().retr(msgnum, os)) {
        expunged = true;
        throw new MessageRemovedException("can't retrieve message #" + msgnum + " in POP3Message.writeTo");
      }
    } else {
      if ((rawcontent != null) && (ignoreList == null))
      {
        InputStream in = ((SharedInputStream)rawcontent).newStream(0L, -1L);
        try {
          byte[] buf = new byte['䀀'];
          int len;
          while ((len = in.read(buf)) > 0)
            os.write(buf, 0, len);
        } finally {
          try {
            if (in != null)
              in.close();
          } catch (IOException localIOException1) {}
        }
      }
      super.writeTo(os, ignoreList);
    }
  }
  

  private void loadHeaders()
    throws MessagingException
  {
    assert (!Thread.holdsLock(this));
    try {
      boolean fetchContent = false;
      synchronized (this) {
        if (headers != null)
          return;
        InputStream hdrs = null;
        if ((folder.getStore()).disableTop) || 
          ((hdrs = folder.getProtocol().top(msgnum, 0)) == null))
        {


          fetchContent = true;
        } else {
          try {
            hdrSize = hdrs.available();
            headers = new InternetHeaders(hdrs);
          } finally {
            hdrs.close();
          }
        }
      }
      










      if (fetchContent) {
        InputStream cs = null;
        try {
          cs = getContentStream();
        } finally {
          if (cs != null)
            cs.close();
        }
      }
    } catch (EOFException eex) {
      folder.close(false);
      throw new FolderClosedException(folder, eex.toString());
    } catch (IOException ex) {
      throw new MessagingException("error loading POP3 headers", ex);
    }
  }
}
