package com.sun.mail.pop3;

import java.io.File;
import java.io.IOException;













































class TempFile
{
  private File file;
  private WritableSharedFile sf;
  
  public TempFile(File dir)
    throws IOException
  {
    file = File.createTempFile("pop3.", ".mbox", dir);
    
    file.deleteOnExit();
    sf = new WritableSharedFile(file);
  }
  

  public AppendStream getAppendStream()
    throws IOException
  {
    return sf.getAppendStream();
  }
  

  public void close()
  {
    try
    {
      sf.close();
    }
    catch (IOException localIOException) {}
    
    file.delete();
  }
  
  protected void finalize() throws Throwable
  {
    try {
      close();
      
      super.finalize(); } finally { super.finalize();
    }
  }
}
