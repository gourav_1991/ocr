package com.sun.mail.pop3;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import javax.mail.util.SharedFileInputStream;









































class WritableSharedFile
  extends SharedFileInputStream
{
  private RandomAccessFile raf;
  private AppendStream af;
  
  public WritableSharedFile(File file)
    throws IOException
  {
    super(file);
    try {
      raf = new RandomAccessFile(file, "rw");
    }
    catch (IOException ex)
    {
      super.close();
    }
  }
  


  public RandomAccessFile getWritableFile()
  {
    return raf;
  }
  

  public void close()
    throws IOException
  {
    try
    {
      super.close();
      
      raf.close(); } finally { raf.close();
    }
  }
  


  synchronized long updateLength()
    throws IOException
  {
    datalen = in.length();
    af = null;
    return datalen;
  }
  

  public synchronized AppendStream getAppendStream()
    throws IOException
  {
    if (af != null) {
      throw new IOException("POP3 file cache only supports single threaded access");
    }
    
    af = new AppendStream(this);
    return af;
  }
}
