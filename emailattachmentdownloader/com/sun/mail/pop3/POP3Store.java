package com.sun.mail.pop3;

import com.sun.mail.util.MailConnectException;
import com.sun.mail.util.MailLogger;
import com.sun.mail.util.PropUtil;
import com.sun.mail.util.SocketConnectException;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.Collections;
import java.util.Map;
import java.util.logging.Level;
import javax.mail.AuthenticationFailedException;
import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.URLName;

















































public class POP3Store
  extends Store
{
  private String name = "pop3";
  private int defaultPort = 110;
  private boolean isSSL = false;
  
  private Protocol port = null;
  private POP3Folder portOwner = null;
  private String host = null;
  private int portNum = -1;
  private String user = null;
  private String passwd = null;
  private boolean useStartTLS = false;
  private boolean requireStartTLS = false;
  private boolean usingSSL = false;
  
  private Map<String, String> capabilities;
  
  private MailLogger logger;
  volatile Constructor<?> messageConstructor = null;
  volatile boolean rsetBeforeQuit = false;
  volatile boolean disableTop = false;
  volatile boolean forgetTopHeaders = false;
  volatile boolean supportsUidl = true;
  volatile boolean cacheWriteTo = false;
  volatile boolean useFileCache = false;
  volatile File fileCacheDir = null;
  volatile boolean keepMessageContent = false;
  volatile boolean finalizeCleanClose = false;
  
  public POP3Store(Session session, URLName url) {
    this(session, url, "pop3", false);
  }
  
  public POP3Store(Session session, URLName url, String name, boolean isSSL)
  {
    super(session, url);
    if (url != null)
      name = url.getProtocol();
    this.name = name;
    logger = new MailLogger(getClass(), "DEBUG POP3", session);
    

    if (!isSSL) {
      isSSL = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".ssl.enable", false);
    }
    if (isSSL) {
      defaultPort = 995;
    } else
      defaultPort = 110;
    this.isSSL = isSSL;
    
    rsetBeforeQuit = getBoolProp("rsetbeforequit");
    disableTop = getBoolProp("disabletop");
    forgetTopHeaders = getBoolProp("forgettopheaders");
    cacheWriteTo = getBoolProp("cachewriteto");
    useFileCache = getBoolProp("filecache.enable");
    String dir = session.getProperty("mail." + name + ".filecache.dir");
    if ((dir != null) && (logger.isLoggable(Level.CONFIG)))
      logger.config("mail." + name + ".filecache.dir: " + dir);
    if (dir != null)
      fileCacheDir = new File(dir);
    keepMessageContent = getBoolProp("keepmessagecontent");
    

    useStartTLS = getBoolProp("starttls.enable");
    

    requireStartTLS = getBoolProp("starttls.required");
    

    finalizeCleanClose = getBoolProp("finalizecleanclose");
    
    String s = session.getProperty("mail." + name + ".message.class");
    if (s != null) {
      logger.log(Level.CONFIG, "message class: {0}", s);
      try {
        ClassLoader cl = getClass().getClassLoader();
        

        Class<?> messageClass = null;
        

        try
        {
          messageClass = Class.forName(s, false, cl);

        }
        catch (ClassNotFoundException ex1)
        {
          messageClass = Class.forName(s);
        }
        
        Class<?>[] c = { Folder.class, Integer.TYPE };
        messageConstructor = messageClass.getConstructor(c);
      } catch (Exception ex) {
        logger.log(Level.CONFIG, "failed to load message class", ex);
      }
    }
  }
  



  private final synchronized boolean getBoolProp(String prop)
  {
    prop = "mail." + name + "." + prop;
    boolean val = PropUtil.getBooleanSessionProperty(session, prop, false);
    if (logger.isLoggable(Level.CONFIG))
      logger.config(prop + ": " + val);
    return val;
  }
  


  synchronized Session getSession()
  {
    return session;
  }
  


  protected synchronized boolean protocolConnect(String host, int portNum, String user, String passwd)
    throws MessagingException
  {
    if ((host == null) || (passwd == null) || (user == null)) {
      return false;
    }
    

    if (portNum == -1) {
      portNum = PropUtil.getIntSessionProperty(session, "mail." + name + ".port", -1);
    }
    
    if (portNum == -1) {
      portNum = defaultPort;
    }
    this.host = host;
    this.portNum = portNum;
    this.user = user;
    this.passwd = passwd;
    try {
      port = getPort(null);
    } catch (EOFException eex) {
      throw new AuthenticationFailedException(eex.getMessage());
    } catch (SocketConnectException scex) {
      throw new MailConnectException(scex);
    } catch (IOException ioex) {
      throw new MessagingException("Connect failed", ioex);
    }
    
    return true;
  }
  












  public synchronized boolean isConnected()
  {
    if (!super.isConnected())
    {

      return false; }
    try {
      if (port == null) {
        port = getPort(null);
      } else if (!port.noop())
        throw new IOException("NOOP failed");
      return true;
    }
    catch (IOException ioex) {
      try {
        super.close();
      }
      catch (MessagingException localMessagingException) {}
    }
    return false;
  }
  


  synchronized Protocol getPort(POP3Folder owner)
    throws IOException
  {
    if ((port != null) && (portOwner == null)) {
      portOwner = owner;
      return port;
    }
    


    Protocol p = new Protocol(host, portNum, logger, session.getProperties(), "mail." + name, isSSL);
    
    if ((useStartTLS) || (requireStartTLS)) {
      if (p.hasCapability("STLS")) {
        if (p.stls())
        {
          p.setCapabilities(p.capa());
        } else if (requireStartTLS) {
          logger.fine("STLS required but failed");
          throw cleanupAndThrow(p, new EOFException("STLS required but failed"));
        }
      }
      else if (requireStartTLS) {
        logger.fine("STLS required but not supported");
        throw cleanupAndThrow(p, new EOFException("STLS required but not supported"));
      }
    }
    

    capabilities = p.getCapabilities();
    usingSSL = p.isSSL();
    






    if ((!disableTop) && (capabilities != null) && 
      (!capabilities.containsKey("TOP"))) {
      disableTop = true;
      logger.fine("server doesn't support TOP, disabling it");
    }
    
    supportsUidl = ((capabilities == null) || (capabilities.containsKey("UIDL")));
    
    String msg = null;
    if ((msg = p.login(user, passwd)) != null) {
      throw cleanupAndThrow(p, new EOFException(msg));
    }
    








    if ((port == null) && (owner != null)) {
      port = p;
      portOwner = owner;
    }
    if (portOwner == null)
      portOwner = owner;
    return p;
  }
  
  private static IOException cleanupAndThrow(Protocol p, IOException ife) {
    try {
      p.quit();
    } catch (Throwable thr) {
      if (isRecoverable(thr)) {
        ife.addSuppressed(thr);
      } else {
        thr.addSuppressed(ife);
        if ((thr instanceof Error)) {
          throw ((Error)thr);
        }
        if ((thr instanceof RuntimeException)) {
          throw ((RuntimeException)thr);
        }
        throw new RuntimeException("unexpected exception", thr);
      }
    }
    return ife;
  }
  
  private static boolean isRecoverable(Throwable t) {
    return ((t instanceof Exception)) || ((t instanceof LinkageError));
  }
  
  synchronized void closePort(POP3Folder owner) {
    if (portOwner == owner) {
      port = null;
      portOwner = null;
    }
  }
  
  public synchronized void close() throws MessagingException
  {
    close(false);
  }
  
  /* Error */
  synchronized void close(boolean force)
    throws MessagingException
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 7	com/sun/mail/pop3/POP3Store:port	Lcom/sun/mail/pop3/Protocol;
    //   4: ifnull +25 -> 29
    //   7: iload_1
    //   8: ifeq +13 -> 21
    //   11: aload_0
    //   12: getfield 7	com/sun/mail/pop3/POP3Store:port	Lcom/sun/mail/pop3/Protocol;
    //   15: invokevirtual 126	com/sun/mail/pop3/Protocol:close	()V
    //   18: goto +11 -> 29
    //   21: aload_0
    //   22: getfield 7	com/sun/mail/pop3/POP3Store:port	Lcom/sun/mail/pop3/Protocol;
    //   25: invokevirtual 115	com/sun/mail/pop3/Protocol:quit	()Z
    //   28: pop
    //   29: aload_0
    //   30: aconst_null
    //   31: putfield 7	com/sun/mail/pop3/POP3Store:port	Lcom/sun/mail/pop3/Protocol;
    //   34: aload_0
    //   35: invokespecial 93	javax/mail/Store:close	()V
    //   38: goto +28 -> 66
    //   41: astore_2
    //   42: aload_0
    //   43: aconst_null
    //   44: putfield 7	com/sun/mail/pop3/POP3Store:port	Lcom/sun/mail/pop3/Protocol;
    //   47: aload_0
    //   48: invokespecial 93	javax/mail/Store:close	()V
    //   51: goto +15 -> 66
    //   54: astore_3
    //   55: aload_0
    //   56: aconst_null
    //   57: putfield 7	com/sun/mail/pop3/POP3Store:port	Lcom/sun/mail/pop3/Protocol;
    //   60: aload_0
    //   61: invokespecial 93	javax/mail/Store:close	()V
    //   64: aload_3
    //   65: athrow
    //   66: return
    // Line number table:
    //   Java source line #366	-> byte code offset #0
    //   Java source line #367	-> byte code offset #7
    //   Java source line #368	-> byte code offset #11
    //   Java source line #370	-> byte code offset #21
    //   Java source line #374	-> byte code offset #29
    //   Java source line #377	-> byte code offset #34
    //   Java source line #378	-> byte code offset #38
    //   Java source line #372	-> byte code offset #41
    //   Java source line #374	-> byte code offset #42
    //   Java source line #377	-> byte code offset #47
    //   Java source line #378	-> byte code offset #51
    //   Java source line #374	-> byte code offset #54
    //   Java source line #377	-> byte code offset #60
    //   Java source line #379	-> byte code offset #66
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	67	0	this	POP3Store
    //   0	67	1	force	boolean
    //   41	1	2	localIOException	IOException
    //   54	11	3	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   0	29	41	java/io/IOException
    //   0	29	54	finally
  }
  
  public Folder getDefaultFolder()
    throws MessagingException
  {
    checkConnected();
    return new DefaultFolder(this);
  }
  


  public Folder getFolder(String name)
    throws MessagingException
  {
    checkConnected();
    return new POP3Folder(this, name);
  }
  
  public Folder getFolder(URLName url) throws MessagingException
  {
    checkConnected();
    return new POP3Folder(this, url.getFile());
  }
  







  public Map<String, String> capabilities()
    throws MessagingException
  {
    Map<String, String> c;
    





    synchronized (this) {
      c = capabilities; }
    Map<String, String> c;
    if (c != null) {
      return Collections.unmodifiableMap(c);
    }
    return Collections.emptyMap();
  }
  





  public synchronized boolean isSSL()
  {
    return usingSSL;
  }
  
  protected void finalize() throws Throwable
  {
    try {
      if (port != null) {
        close(!finalizeCleanClose);
      }
      super.finalize(); } finally { super.finalize();
    }
  }
  
  private void checkConnected() throws MessagingException {
    if (!super.isConnected()) {
      throw new MessagingException("Not connected");
    }
  }
}
