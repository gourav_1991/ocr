package com.sun.mail.iap;

import java.io.ByteArrayInputStream;





















































public class ByteArray
{
  private byte[] bytes;
  private int start;
  private int count;
  
  public ByteArray(byte[] b, int start, int count)
  {
    bytes = b;
    this.start = start;
    this.count = count;
  }
  





  public ByteArray(int size)
  {
    this(new byte[size], 0, size);
  }
  





  public byte[] getBytes()
  {
    return bytes;
  }
  




  public byte[] getNewBytes()
  {
    byte[] b = new byte[count];
    System.arraycopy(bytes, start, b, 0, count);
    return b;
  }
  




  public int getStart()
  {
    return start;
  }
  




  public int getCount()
  {
    return count;
  }
  





  public void setCount(int count)
  {
    this.count = count;
  }
  




  public ByteArrayInputStream toByteArrayInputStream()
  {
    return new ByteArrayInputStream(bytes, start, count);
  }
  





  public void grow(int incr)
  {
    byte[] nbuf = new byte[bytes.length + incr];
    System.arraycopy(bytes, 0, nbuf, 0, bytes.length);
    bytes = nbuf;
  }
}
