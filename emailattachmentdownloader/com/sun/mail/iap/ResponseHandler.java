package com.sun.mail.iap;

public abstract interface ResponseHandler
{
  public abstract void handleResponse(Response paramResponse);
}
