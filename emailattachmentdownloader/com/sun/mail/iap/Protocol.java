package com.sun.mail.iap;

import com.sun.mail.util.MailLogger;
import com.sun.mail.util.PropUtil;
import com.sun.mail.util.SocketFetcher;
import com.sun.mail.util.TraceInputStream;
import com.sun.mail.util.TraceOutputStream;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;
import javax.net.ssl.SSLSocket;












































public class Protocol
{
  protected String host;
  private Socket socket;
  protected boolean quote;
  protected MailLogger logger;
  protected MailLogger traceLogger;
  protected Properties props;
  protected String prefix;
  private TraceInputStream traceInput;
  private volatile ResponseInputStream input;
  private TraceOutputStream traceOutput;
  private volatile DataOutputStream output;
  private int tagCounter = 0;
  
  private String localHostName;
  
  private final List<ResponseHandler> handlers = new CopyOnWriteArrayList();
  

  private volatile long timestamp;
  
  private static final byte[] CRLF = { 13, 10 };
  















  public Protocol(String host, int port, Properties props, String prefix, boolean isSSL, MailLogger logger)
    throws IOException, ProtocolException
  {
    boolean connected = false;
    try {
      this.host = host;
      this.props = props;
      this.prefix = prefix;
      this.logger = logger;
      traceLogger = logger.getSubLogger("protocol", null);
      
      socket = SocketFetcher.getSocket(host, port, props, prefix, isSSL);
      quote = PropUtil.getBooleanProperty(props, "mail.debug.quote", false);
      

      initStreams();
      

      processGreeting(readResponse());
      
      timestamp = System.currentTimeMillis();
      
      connected = true;


    }
    finally
    {


      if (!connected)
        disconnect();
    }
  }
  
  private void initStreams() throws IOException {
    traceInput = new TraceInputStream(socket.getInputStream(), traceLogger);
    traceInput.setQuote(quote);
    input = new ResponseInputStream(traceInput);
    

    traceOutput = new TraceOutputStream(socket.getOutputStream(), traceLogger);
    traceOutput.setQuote(quote);
    output = new DataOutputStream(new BufferedOutputStream(traceOutput));
  }
  








  public Protocol(InputStream in, PrintStream out, Properties props, boolean debug)
    throws IOException
  {
    host = "localhost";
    this.props = props;
    quote = false;
    logger = new MailLogger(getClass(), "DEBUG", debug, System.out);
    traceLogger = logger.getSubLogger("protocol", null);
    

    traceInput = new TraceInputStream(in, traceLogger);
    traceInput.setQuote(quote);
    input = new ResponseInputStream(traceInput);
    
    traceOutput = new TraceOutputStream(out, traceLogger);
    traceOutput.setQuote(quote);
    output = new DataOutputStream(new BufferedOutputStream(traceOutput));
    
    timestamp = System.currentTimeMillis();
  }
  




  public long getTimestamp()
  {
    return timestamp;
  }
  




  public void addResponseHandler(ResponseHandler h)
  {
    handlers.add(h);
  }
  




  public void removeResponseHandler(ResponseHandler h)
  {
    handlers.remove(h);
  }
  




  public void notifyResponseHandlers(Response[] responses)
  {
    if (handlers.isEmpty()) {
      return;
    }
    Response r;
    for (r : responses) {
      if (r != null) {
        for (ResponseHandler rh : handlers) {
          if (rh != null) {
            rh.handleResponse(r);
          }
        }
      }
    }
  }
  
  protected void processGreeting(Response r) throws ProtocolException {
    if (r.isBYE()) {
      throw new ConnectionException(this, r);
    }
  }
  



  protected ResponseInputStream getInputStream()
  {
    return input;
  }
  




  protected OutputStream getOutputStream()
  {
    return output;
  }
  





  protected synchronized boolean supportsNonSyncLiterals()
  {
    return false;
  }
  
  public Response readResponse() throws IOException, ProtocolException
  {
    return new Response(this);
  }
  










  public boolean hasResponse()
  {
    try
    {
      return input.available() > 0;
    }
    catch (IOException localIOException) {}
    return false;
  }
  







  protected ByteArray getResponseBuffer()
  {
    return null;
  }
  

  public String writeCommand(String command, Argument args)
    throws IOException, ProtocolException
  {
    String tag = "A" + Integer.toString(tagCounter++, 10);
    
    output.writeBytes(tag + " " + command);
    
    if (args != null) {
      output.write(32);
      args.write(this);
    }
    
    output.write(CRLF);
    output.flush();
    return tag;
  }
  








  public synchronized Response[] command(String command, Argument args)
  {
    commandStart(command);
    List<Response> v = new ArrayList();
    boolean done = false;
    String tag = null;
    
    try
    {
      tag = writeCommand(command, args);
    } catch (LiteralException lex) {
      v.add(lex.getResponse());
      done = true;
    }
    catch (Exception ex) {
      v.add(Response.byeResponse(ex));
      done = true;
    }
    
    Response byeResp = null;
    while (!done) {
      Response r = null;
      try {
        r = readResponse();
      } catch (IOException ioex) {
        if (byeResp == null) {
          byeResp = Response.byeResponse(ioex);
        }
        break;
      } catch (ProtocolException pex) {
        logger.log(Level.FINE, "ignoring bad response", pex); }
      continue;
      

      if (r.isBYE()) {
        byeResp = r;
      }
      else
      {
        v.add(r);
        

        if ((r.isTagged()) && (r.getTag().equals(tag)))
          done = true;
      }
    }
    if (byeResp != null)
      v.add(byeResp);
    Response[] responses = new Response[v.size()];
    v.toArray(responses);
    timestamp = System.currentTimeMillis();
    commandEnd();
    return responses;
  }
  




  public void handleResult(Response response)
    throws ProtocolException
  {
    if (response.isOK())
      return;
    if (response.isNO())
      throw new CommandFailedException(response);
    if (response.isBAD())
      throw new BadCommandException(response);
    if (response.isBYE()) {
      disconnect();
      throw new ConnectionException(this, response);
    }
  }
  








  public void simpleCommand(String cmd, Argument args)
    throws ProtocolException
  {
    Response[] r = command(cmd, args);
    

    notifyResponseHandlers(r);
    

    handleResult(r[(r.length - 1)]);
  }
  










  public synchronized void startTLS(String cmd)
    throws IOException, ProtocolException
  {
    if ((socket instanceof SSLSocket))
      return;
    simpleCommand(cmd, null);
    socket = SocketFetcher.startTLS(socket, host, props, prefix);
    initStreams();
  }
  












  public synchronized void startCompression(String cmd)
    throws IOException, ProtocolException
  {
    Class<DeflaterOutputStream> dc = DeflaterOutputStream.class;
    Constructor<DeflaterOutputStream> cons = null;
    try {
      cons = dc.getConstructor(new Class[] { OutputStream.class, Deflater.class, Boolean.TYPE });
    }
    catch (NoSuchMethodException ex) {
      logger.fine("Ignoring COMPRESS; missing JDK 1.7 DeflaterOutputStream constructor");
      
      return;
    }
    

    simpleCommand(cmd, null);
    

    Inflater inf = new Inflater(true);
    
    traceInput = new TraceInputStream(new InflaterInputStream(socket.getInputStream(), inf), traceLogger);
    traceInput.setQuote(quote);
    input = new ResponseInputStream(traceInput);
    

    int level = PropUtil.getIntProperty(props, prefix + ".compress.level", -1);
    
    int strategy = PropUtil.getIntProperty(props, prefix + ".compress.strategy", 0);
    

    if (logger.isLoggable(Level.FINE))
      logger.log(Level.FINE, "Creating Deflater with compression level {0} and strategy {1}", new Object[] {
      
        Integer.valueOf(level), Integer.valueOf(strategy) });
    Deflater def = new Deflater(-1, true);
    try {
      def.setLevel(level);
    } catch (IllegalArgumentException ex) {
      logger.log(Level.FINE, "Ignoring bad compression level", ex);
    }
    try {
      def.setStrategy(strategy);
    } catch (IllegalArgumentException ex) {
      logger.log(Level.FINE, "Ignoring bad compression strategy", ex);
    }
    
    try
    {
      traceOutput = new TraceOutputStream((OutputStream)cons.newInstance(new Object[] {socket
        .getOutputStream(), def, Boolean.valueOf(true) }), traceLogger);
    } catch (Exception ex) {
      throw new ProtocolException("can't create deflater", ex);
    }
    traceOutput.setQuote(quote);
    output = new DataOutputStream(new BufferedOutputStream(traceOutput));
  }
  





  public boolean isSSL()
  {
    return socket instanceof SSLSocket;
  }
  





  public InetAddress getInetAddress()
  {
    return socket.getInetAddress();
  }
  





  public SocketChannel getChannel()
  {
    SocketChannel ret = socket.getChannel();
    if (ret != null) {
      return ret;
    }
    

    if ((socket instanceof SSLSocket)) {
      try {
        Field f = socket.getClass().getDeclaredField("socket");
        f.setAccessible(true);
        Socket s = (Socket)f.get(socket);
        ret = s.getChannel();
      }
      catch (Exception localException) {}
    }
    
    return ret;
  }
  






  public boolean supportsUtf8()
  {
    return false;
  }
  


  protected synchronized void disconnect()
  {
    if (socket != null) {
      try {
        socket.close();
      }
      catch (IOException localIOException) {}
      
      socket = null;
    }
  }
  








  protected synchronized String getLocalHost()
  {
    if ((localHostName == null) || (localHostName.length() <= 0))
    {
      localHostName = props.getProperty(prefix + ".localhost"); }
    if ((localHostName == null) || (localHostName.length() <= 0))
    {
      localHostName = props.getProperty(prefix + ".localaddress"); }
    try {
      if ((localHostName == null) || (localHostName.length() <= 0)) {
        InetAddress localHost = InetAddress.getLocalHost();
        localHostName = localHost.getCanonicalHostName();
        
        if (localHostName == null)
        {
          localHostName = ("[" + localHost.getHostAddress() + "]");
        }
      }
    }
    catch (UnknownHostException localUnknownHostException) {}
    
    if (((localHostName == null) || (localHostName.length() <= 0)) && 
      (socket != null) && (socket.isBound())) {
      InetAddress localHost = socket.getLocalAddress();
      localHostName = localHost.getCanonicalHostName();
      
      if (localHostName == null)
      {
        localHostName = ("[" + localHost.getHostAddress() + "]");
      }
    }
    return localHostName;
  }
  




  protected boolean isTracing()
  {
    return traceLogger.isLoggable(Level.FINEST);
  }
  



  protected void suspendTracing()
  {
    if (traceLogger.isLoggable(Level.FINEST)) {
      traceInput.setTrace(false);
      traceOutput.setTrace(false);
    }
  }
  


  protected void resumeTracing()
  {
    if (traceLogger.isLoggable(Level.FINEST)) {
      traceInput.setTrace(true);
      traceOutput.setTrace(true);
    }
  }
  

  protected void finalize()
    throws Throwable
  {
    try
    {
      disconnect();
      
      super.finalize(); } finally { super.finalize();
    }
  }
  
  private void commandStart(String command) {}
  
  private void commandEnd() {}
}
