package com.sun.mail.iap;

import com.sun.mail.util.ASCIIUtility;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;















































public class Response
{
  protected int index;
  protected int pindex;
  protected int size;
  protected byte[] buffer = null;
  protected int type = 0;
  protected String tag = null;
  

  protected Exception ex;
  

  protected boolean utf8;
  
  private static final int increment = 100;
  
  public static final int TAG_MASK = 3;
  
  public static final int CONTINUATION = 1;
  
  public static final int TAGGED = 2;
  
  public static final int UNTAGGED = 3;
  
  public static final int TYPE_MASK = 28;
  
  public static final int OK = 4;
  
  public static final int NO = 8;
  
  public static final int BAD = 12;
  
  public static final int BYE = 16;
  
  public static final int SYNTHETIC = 32;
  
  private static String ATOM_CHAR_DELIM = " (){%*\"\\]";
  





  private static String ASTRING_CHAR_DELIM = " (){%*\"\\";
  
  public Response(String s) {
    this(s, true);
  }
  




  public Response(String s, boolean supportsUtf8)
  {
    if (supportsUtf8) {
      buffer = s.getBytes(StandardCharsets.UTF_8);
    } else
      buffer = s.getBytes(StandardCharsets.US_ASCII);
    size = buffer.length;
    utf8 = supportsUtf8;
    parse();
  }
  






  public Response(Protocol p)
    throws IOException, ProtocolException
  {
    ByteArray ba = p.getResponseBuffer();
    ByteArray response = p.getInputStream().readResponse(ba);
    buffer = response.getBytes();
    size = (response.getCount() - 2);
    utf8 = p.supportsUtf8();
    
    parse();
  }
  




  public Response(Response r)
  {
    index = index;
    pindex = pindex;
    size = size;
    buffer = buffer;
    type = type;
    tag = tag;
    ex = ex;
    utf8 = utf8;
  }
  






  public static Response byeResponse(Exception ex)
  {
    String err = "* BYE JavaMail Exception: " + ex.toString();
    err = err.replace('\r', ' ').replace('\n', ' ');
    Response r = new Response(err);
    type |= 0x20;
    ex = ex;
    return r;
  }
  




  public boolean supportsUtf8()
  {
    return utf8;
  }
  
  private void parse() {
    index = 0;
    
    if (size == 0)
      return;
    if (buffer[index] == 43) {
      type |= 0x1;
      index += 1;
      return; }
    if (buffer[index] == 42) {
      type |= 0x3;
      index += 1;
    } else {
      type |= 0x2;
      tag = readAtom();
      if (tag == null) {
        tag = "";
      }
    }
    int mark = index;
    String s = readAtom();
    if (s == null)
      s = "";
    if (s.equalsIgnoreCase("OK")) {
      type |= 0x4;
    } else if (s.equalsIgnoreCase("NO")) {
      type |= 0x8;
    } else if (s.equalsIgnoreCase("BAD")) {
      type |= 0xC;
    } else if (s.equalsIgnoreCase("BYE")) {
      type |= 0x10;
    } else {
      index = mark;
    }
    pindex = index;
  }
  
  public void skipSpaces()
  {
    while ((index < size) && (buffer[index] == 32)) {
      index += 1;
    }
  }
  



  public boolean isNextNonSpace(char c)
  {
    skipSpaces();
    if ((index < size) && (buffer[index] == (byte)c)) {
      index += 1;
      return true;
    }
    return false;
  }
  


  public void skipToken()
  {
    while ((index < size) && (buffer[index] != 32))
      index += 1;
  }
  
  public void skip(int count) {
    index += count;
  }
  
  public byte peekByte() {
    if (index < size) {
      return buffer[index];
    }
    return 0;
  }
  




  public byte readByte()
  {
    if (index < size) {
      return buffer[(index++)];
    }
    return 0;
  }
  





  public String readAtom()
  {
    return readDelimString(ATOM_CHAR_DELIM);
  }
  



  private String readDelimString(String delim)
  {
    skipSpaces();
    
    if (index >= size) {
      return null;
    }
    
    int start = index;
    int b; while ((index < size) && ((b = buffer[index] & 0xFF) >= 32) && 
      (delim.indexOf((char)b) < 0) && (b != 127)) {
      index += 1;
    }
    return toString(buffer, start, index);
  }
  







  public String readString(char delim)
  {
    skipSpaces();
    
    if (index >= size) {
      return null;
    }
    int start = index;
    while ((index < size) && (buffer[index] != delim)) {
      index += 1;
    }
    return toString(buffer, start, index);
  }
  
  public String[] readStringList() {
    return readStringList(false);
  }
  
  public String[] readAtomStringList() {
    return readStringList(true);
  }
  
  private String[] readStringList(boolean atom) {
    skipSpaces();
    
    if (buffer[index] != 40) {
      return null;
    }
    index += 1;
    


    List<String> result = new ArrayList();
    while (!isNextNonSpace(')')) {
      result.add(atom ? readAtomString() : readString());
    }
    return (String[])result.toArray(new String[result.size()]);
  }
  







  public int readNumber()
  {
    skipSpaces();
    
    int start = index;
    while ((index < size) && (Character.isDigit((char)buffer[index]))) {
      index += 1;
    }
    if (index > start) {
      try {
        return ASCIIUtility.parseInt(buffer, start, index);
      }
      catch (NumberFormatException localNumberFormatException) {}
    }
    return -1;
  }
  







  public long readLong()
  {
    skipSpaces();
    
    int start = index;
    while ((index < size) && (Character.isDigit((char)buffer[index]))) {
      index += 1;
    }
    if (index > start) {
      try {
        return ASCIIUtility.parseLong(buffer, start, index);
      }
      catch (NumberFormatException localNumberFormatException) {}
    }
    return -1L;
  }
  







  public String readString()
  {
    return (String)parseString(false, true);
  }
  







  public ByteArrayInputStream readBytes()
  {
    ByteArray ba = readByteArray();
    if (ba != null) {
      return ba.toByteArrayInputStream();
    }
    return null;
  }
  











  public ByteArray readByteArray()
  {
    if (isContinuation()) {
      skipSpaces();
      return new ByteArray(buffer, index, size - index);
    }
    return (ByteArray)parseString(false, false);
  }
  










  public String readAtomString()
  {
    return (String)parseString(true, true);
  }
  







  private Object parseString(boolean parseAtoms, boolean returnString)
  {
    skipSpaces();
    
    byte b = buffer[index];
    if (b == 34) {
      index += 1;
      int start = index;
      int copyto = index;
      
      while ((index < size) && ((b = buffer[index]) != 34)) {
        if (b == 92)
          index += 1;
        if (index != copyto)
        {

          buffer[copyto] = buffer[index];
        }
        copyto++;
        index += 1;
      }
      if (index >= size)
      {


        return null;
      }
      index += 1;
      
      if (returnString) {
        return toString(buffer, start, copyto);
      }
      return new ByteArray(buffer, start, copyto - start); }
    if (b == 123) {
      int start = ++index;
      
      while (buffer[index] != 125) {
        index += 1;
      }
      int count = 0;
      try {
        count = ASCIIUtility.parseInt(buffer, start, index);
      }
      catch (NumberFormatException nex) {
        return null;
      }
      
      start = index + 3;
      index = (start + count);
      
      if (returnString) {
        return toString(buffer, start, start + count);
      }
      return new ByteArray(buffer, start, count); }
    if (parseAtoms) {
      int start = index;
      
      String s = readDelimString(ASTRING_CHAR_DELIM);
      if (returnString) {
        return s;
      }
      return new ByteArray(buffer, start, index); }
    if ((b == 78) || (b == 110)) {
      index += 3;
      return null;
    }
    return null;
  }
  
  private String toString(byte[] buffer, int start, int end) {
    return utf8 ? new String(buffer, start, end - start, StandardCharsets.UTF_8) : 
    
      ASCIIUtility.toString(buffer, start, end);
  }
  
  public int getType() {
    return type;
  }
  
  public boolean isContinuation() {
    return (type & 0x3) == 1;
  }
  
  public boolean isTagged() {
    return (type & 0x3) == 2;
  }
  
  public boolean isUnTagged() {
    return (type & 0x3) == 3;
  }
  
  public boolean isOK() {
    return (type & 0x1C) == 4;
  }
  
  public boolean isNO() {
    return (type & 0x1C) == 8;
  }
  
  public boolean isBAD() {
    return (type & 0x1C) == 12;
  }
  
  public boolean isBYE() {
    return (type & 0x1C) == 16;
  }
  
  public boolean isSynthetic() {
    return (type & 0x20) == 32;
  }
  




  public String getTag()
  {
    return tag;
  }
  





  public String getRest()
  {
    skipSpaces();
    return toString(buffer, index, size);
  }
  





  public Exception getException()
  {
    return ex;
  }
  


  public void reset()
  {
    index = pindex;
  }
  
  public String toString()
  {
    return toString(buffer, 0, size);
  }
}
