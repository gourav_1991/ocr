package com.sun.mail.imap;

import java.util.ArrayList;
import java.util.List;






































































public class Rights
  implements Cloneable
{
  private boolean[] rights = new boolean[''];
  
  public Rights() {}
  

  public static final class Right
  {
    private static Right[] cache = new Right[''];
    




    public static final Right LOOKUP = getInstance('l');
    




    public static final Right READ = getInstance('r');
    



    public static final Right KEEP_SEEN = getInstance('s');
    



    public static final Right WRITE = getInstance('w');
    



    public static final Right INSERT = getInstance('i');
    




    public static final Right POST = getInstance('p');
    




    public static final Right CREATE = getInstance('c');
    



    public static final Right DELETE = getInstance('d');
    



    public static final Right ADMINISTER = getInstance('a');
    

    char right;
    

    private Right(char right)
    {
      if (right >= '')
        throw new IllegalArgumentException("Right must be ASCII");
      this.right = right;
    }
    






    public static synchronized Right getInstance(char right)
    {
      if (right >= '')
        throw new IllegalArgumentException("Right must be ASCII");
      if (cache[right] == null)
        cache[right] = new Right(right);
      return cache[right];
    }
    
    public String toString()
    {
      return String.valueOf(right);
    }
  }
  










  public Rights(Rights rights)
  {
    System.arraycopy(rights, 0, this.rights, 0, this.rights.length);
  }
  




  public Rights(String rights)
  {
    for (int i = 0; i < rights.length(); i++) {
      add(Right.getInstance(rights.charAt(i)));
    }
  }
  



  public Rights(Right right)
  {
    rights[right] = true;
  }
  




  public void add(Right right)
  {
    rights[right] = true;
  }
  





  public void add(Rights rights)
  {
    for (int i = 0; i < rights.length; i++) {
      if (rights[i] != 0) {
        this.rights[i] = true;
      }
    }
  }
  


  public void remove(Right right)
  {
    rights[right] = false;
  }
  





  public void remove(Rights rights)
  {
    for (int i = 0; i < rights.length; i++) {
      if (rights[i] != 0) {
        this.rights[i] = false;
      }
    }
  }
  



  public boolean contains(Right right)
  {
    return rights[right];
  }
  







  public boolean contains(Rights rights)
  {
    for (int i = 0; i < rights.length; i++) {
      if ((rights[i] != 0) && (this.rights[i] == 0)) {
        return false;
      }
    }
    return true;
  }
  





  public boolean equals(Object obj)
  {
    if (!(obj instanceof Rights)) {
      return false;
    }
    Rights rights = (Rights)obj;
    
    for (int i = 0; i < rights.length; i++) {
      if (rights[i] != this.rights[i])
        return false;
    }
    return true;
  }
  





  public int hashCode()
  {
    int hash = 0;
    for (int i = 0; i < rights.length; i++)
      if (rights[i] != 0)
        hash++;
    return hash;
  }
  





  public Right[] getRights()
  {
    List<Right> v = new ArrayList();
    for (int i = 0; i < rights.length; i++)
      if (rights[i] != 0)
        v.add(Right.getInstance((char)i));
    return (Right[])v.toArray(new Right[v.size()]);
  }
  



  public Object clone()
  {
    Rights r = null;
    try {
      r = (Rights)super.clone();
      rights = new boolean[''];
      System.arraycopy(rights, 0, rights, 0, rights.length);
    }
    catch (CloneNotSupportedException localCloneNotSupportedException) {}
    
    return r;
  }
  
  public String toString()
  {
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < rights.length; i++)
      if (rights[i] != 0)
        sb.append((char)i);
    return sb.toString();
  }
}
