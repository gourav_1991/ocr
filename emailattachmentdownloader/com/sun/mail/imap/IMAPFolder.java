package com.sun.mail.imap;

import com.sun.mail.iap.BadCommandException;
import com.sun.mail.iap.CommandFailedException;
import com.sun.mail.iap.ConnectionException;
import com.sun.mail.iap.ProtocolException;
import com.sun.mail.iap.Response;
import com.sun.mail.iap.ResponseHandler;
import com.sun.mail.imap.protocol.FLAGS;
import com.sun.mail.imap.protocol.FetchItem;
import com.sun.mail.imap.protocol.FetchResponse;
import com.sun.mail.imap.protocol.IMAPProtocol;
import com.sun.mail.imap.protocol.IMAPResponse;
import com.sun.mail.imap.protocol.Item;
import com.sun.mail.imap.protocol.ListInfo;
import com.sun.mail.imap.protocol.MODSEQ;
import com.sun.mail.imap.protocol.MailboxInfo;
import com.sun.mail.imap.protocol.MessageSet;
import com.sun.mail.imap.protocol.Status;
import com.sun.mail.imap.protocol.UID;
import com.sun.mail.imap.protocol.UIDSet;
import com.sun.mail.util.MailLogger;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.SocketTimeoutException;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import javax.mail.FetchProfile;
import javax.mail.FetchProfile.Item;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.FolderClosedException;
import javax.mail.FolderNotFoundException;
import javax.mail.Message;
import javax.mail.MessageRemovedException;
import javax.mail.MessagingException;
import javax.mail.Quota;
import javax.mail.ReadOnlyFolderException;
import javax.mail.StoreClosedException;
import javax.mail.UIDFolder;
import javax.mail.UIDFolder.FetchProfileItem;
import javax.mail.event.MailEvent;
import javax.mail.event.MessageChangedEvent;
import javax.mail.event.MessageCountListener;
import javax.mail.internet.MimeMessage;
import javax.mail.search.SearchException;
import javax.mail.search.SearchTerm;


























































































































public class IMAPFolder
  extends Folder
  implements UIDFolder, ResponseHandler
{
  protected volatile String fullName;
  protected String name;
  protected int type;
  protected char separator;
  protected Flags availableFlags;
  protected Flags permanentFlags;
  protected volatile boolean exists;
  protected boolean isNamespace = false;
  
  protected volatile String[] attributes;
  
  protected volatile IMAPProtocol protocol;
  protected MessageCache messageCache;
  protected final Object messageCacheLock = new Object();
  


  protected Hashtable<Long, IMAPMessage> uidTable;
  


  protected static final char UNKNOWN_SEPARATOR = '￿';
  


  private volatile boolean opened = false;
  










  private boolean reallyClosed = true;
  











  private static final int RUNNING = 0;
  











  private static final int IDLE = 1;
  











  private static final int ABORTING = 2;
  











  private int idleState = 0;
  
  private IdleManager idleManager;
  private volatile int total = -1;
  
  private volatile int recent = -1;
  private int realTotal = -1;
  
  private long uidvalidity = -1L;
  private long uidnext = -1L;
  private boolean uidNotSticky = false;
  private volatile long highestmodseq = -1L;
  private boolean doExpungeNotification = true;
  
  private Status cachedStatus = null;
  private long cachedStatusTime = 0L;
  
  private boolean hasMessageCountListener = false;
  protected MailLogger logger;
  private MailLogger connectionPoolLogger;
  
  public static abstract interface ProtocolCommand
  {
    public abstract Object doCommand(IMAPProtocol paramIMAPProtocol) throws ProtocolException;
  }
  
  public static class FetchProfileItem
    extends FetchProfile.Item
  {
    protected FetchProfileItem(String name)
    {
      super();
    }
    















    public static final FetchProfileItem HEADERS = new FetchProfileItem("HEADERS");
    











    @Deprecated
    public static final FetchProfileItem SIZE = new FetchProfileItem("SIZE");
    






















    public static final FetchProfileItem MESSAGE = new FetchProfileItem("MESSAGE");
    



















    public static final FetchProfileItem INTERNALDATE = new FetchProfileItem("INTERNALDATE");
  }
  










  protected IMAPFolder(String fullName, char separator, IMAPStore store, Boolean isNamespace)
  {
    super(store);
    if (fullName == null)
      throw new NullPointerException("Folder name is null");
    this.fullName = fullName;
    this.separator = separator;
    
    logger = new MailLogger(getClass(), "DEBUG IMAP", store.getSession());
    connectionPoolLogger = store.getConnectionPoolLogger();
    











    this.isNamespace = false;
    if ((separator != 65535) && (separator != 0)) {
      int i = this.fullName.indexOf(separator);
      if ((i > 0) && (i == this.fullName.length() - 1)) {
        this.fullName = this.fullName.substring(0, i);
        this.isNamespace = true;
      }
    }
    

    if (isNamespace != null) {
      this.isNamespace = isNamespace.booleanValue();
    }
  }
  




  protected IMAPFolder(ListInfo li, IMAPStore store)
  {
    this(name, separator, store, null);
    
    if (hasInferiors)
      type |= 0x2;
    if (canOpen)
      type |= 0x1;
    exists = true;
    attributes = attrs;
  }
  






  protected void checkExists()
    throws MessagingException
  {
    if ((!exists) && (!exists())) {
      throw new FolderNotFoundException(this, fullName + " not found");
    }
  }
  



  protected void checkClosed()
  {
    if (opened) {
      throw new IllegalStateException("This operation is not allowed on an open folder");
    }
  }
  



  protected void checkOpened()
    throws FolderClosedException
  {
    assert (Thread.holdsLock(this));
    if (!opened) {
      if (reallyClosed) {
        throw new IllegalStateException("This operation is not allowed on a closed folder");
      }
      

      throw new FolderClosedException(this, "Lost folder connection to server");
    }
  }
  






  protected void checkRange(int msgno)
    throws MessagingException
  {
    if (msgno < 1) {
      throw new IndexOutOfBoundsException("message number < 1");
    }
    if (msgno <= total) {
      return;
    }
    


    synchronized (messageCacheLock) {
      try {
        keepConnectionAlive(false);
      }
      catch (ConnectionException cex) {
        throw new FolderClosedException(this, cex.getMessage());
      } catch (ProtocolException pex) {
        throw new MessagingException(pex.getMessage(), pex);
      }
    }
    
    if (msgno > total) {
      throw new IndexOutOfBoundsException(msgno + " > " + total);
    }
  }
  

  private void checkFlags(Flags flags)
    throws MessagingException
  {
    assert (Thread.holdsLock(this));
    if (mode != 2) {
      throw new IllegalStateException("Cannot change flags on READ_ONLY folder: " + fullName);
    }
  }
  













  public synchronized String getName()
  {
    if (name == null) {
      try {
        name = fullName.substring(fullName
          .lastIndexOf(getSeparator()) + 1);
      }
      catch (MessagingException localMessagingException) {}
    }
    return name;
  }
  



  public String getFullName()
  {
    return fullName;
  }
  


  public synchronized Folder getParent()
    throws MessagingException
  {
    char c = getSeparator();
    int index;
    if ((index = fullName.lastIndexOf(c)) != -1) {
      return ((IMAPStore)store).newIMAPFolder(fullName
        .substring(0, index), c);
    }
    return new DefaultFolder((IMAPStore)store);
  }
  



  public synchronized boolean exists()
    throws MessagingException
  {
    ListInfo[] li = null;
    String lname;
    final String lname; if ((isNamespace) && (separator != 0)) {
      lname = fullName + separator;
    } else {
      lname = fullName;
    }
    li = (ListInfo[])doCommand(new ProtocolCommand()
    {
      public Object doCommand(IMAPProtocol p) throws ProtocolException {
        return p.list("", lname);
      }
    });
    
    if (li != null) {
      int i = findName(li, lname);
      fullName = name;
      separator = separator;
      int len = fullName.length();
      if ((separator != 0) && (len > 0) && 
        (fullName.charAt(len - 1) == separator)) {
        fullName = fullName.substring(0, len - 1);
      }
      type = 0;
      if (hasInferiors)
        type |= 0x2;
      if (canOpen)
        type |= 0x1;
      exists = true;
      attributes = attrs;
    } else {
      exists = opened;
      attributes = null;
    }
    
    return exists;
  }
  






  private int findName(ListInfo[] li, String lname)
  {
    for (int i = 0; i < li.length; i++) {
      if (name.equals(lname))
        break;
    }
    if (i >= li.length)
    {


      i = 0;
    }
    return i;
  }
  


  public Folder[] list(String pattern)
    throws MessagingException
  {
    return doList(pattern, false);
  }
  


  public Folder[] listSubscribed(String pattern)
    throws MessagingException
  {
    return doList(pattern, true);
  }
  
  private synchronized Folder[] doList(final String pattern, final boolean subscribed) throws MessagingException
  {
    checkExists();
    

    if ((attributes != null) && (!isDirectory())) {
      return new Folder[0];
    }
    final char c = getSeparator();
    
    ListInfo[] li = (ListInfo[])doCommandIgnoreFailure(new ProtocolCommand()
    {
      public Object doCommand(IMAPProtocol p)
        throws ProtocolException
      {
        if (subscribed) {
          return p.lsub("", fullName + c + pattern);
        }
        return p.list("", fullName + c + pattern);
      }
    });
    
    if (li == null) {
      return new Folder[0];
    }
    










    int start = 0;
    
    if ((li.length > 0) && (0name.equals(fullName + c))) {
      start = 1;
    }
    IMAPFolder[] folders = new IMAPFolder[li.length - start];
    IMAPStore st = (IMAPStore)store;
    for (int i = start; i < li.length; i++)
      folders[(i - start)] = st.newIMAPFolder(li[i]);
    return folders;
  }
  


  public synchronized char getSeparator()
    throws MessagingException
  {
    if (separator == 65535) {
      ListInfo[] li = null;
      
      li = (ListInfo[])doCommand(new ProtocolCommand()
      {

        public Object doCommand(IMAPProtocol p)
          throws ProtocolException
        {
          if (p.isREV1()) {
            return p.list(fullName, "");
          }
          
          return p.list("", fullName);
        }
      });
      
      if (li != null) {
        separator = 0separator;
      } else
        separator = '/';
    }
    return separator;
  }
  


  public synchronized int getType()
    throws MessagingException
  {
    if (opened)
    {
      if (attributes == null)
        exists();
    } else {
      checkExists();
    }
    return type;
  }
  



  public synchronized boolean isSubscribed()
  {
    ListInfo[] li = null;
    String lname;
    final String lname; if ((isNamespace) && (separator != 0)) {
      lname = fullName + separator;
    } else {
      lname = fullName;
    }
    try {
      li = (ListInfo[])doProtocolCommand(new ProtocolCommand()
      {
        public Object doCommand(IMAPProtocol p) throws ProtocolException
        {
          return p.lsub("", lname);
        }
      });
    }
    catch (ProtocolException localProtocolException) {}
    
    if (li != null) {
      int i = findName(li, lname);
      return canOpen;
    }
    return false;
  }
  



  public synchronized void setSubscribed(final boolean subscribe)
    throws MessagingException
  {
    doCommandIgnoreFailure(new ProtocolCommand()
    {
      public Object doCommand(IMAPProtocol p) throws ProtocolException {
        if (subscribe) {
          p.subscribe(fullName);
        } else
          p.unsubscribe(fullName);
        return null;
      }
    });
  }
  




  public synchronized boolean create(final int type)
    throws MessagingException
  {
    char c = '\000';
    if ((type & 0x1) == 0)
      c = getSeparator();
    final char sep = c;
    Object ret = doCommandIgnoreFailure(new ProtocolCommand()
    {
      public Object doCommand(IMAPProtocol p) throws ProtocolException
      {
        if ((type & 0x1) == 0) {
          p.create(fullName + sep);
        } else {
          p.create(fullName);
          




          if ((type & 0x2) != 0)
          {

            ListInfo[] li = p.list("", fullName);
            if ((li != null) && (!0hasInferiors))
            {

              p.delete(fullName);
              throw new ProtocolException("Unsupported type");
            }
          }
        }
        return Boolean.TRUE;
      }
    });
    
    if (ret == null) {
      return false;
    }
    


    boolean retb = exists();
    if (retb)
      notifyFolderListeners(1);
    return retb;
  }
  


  public synchronized boolean hasNewMessages()
    throws MessagingException
  {
    synchronized (messageCacheLock) {
      if (opened)
      {
        try
        {
          keepConnectionAlive(true);
        } catch (ConnectionException cex) {
          throw new FolderClosedException(this, cex.getMessage());
        } catch (ProtocolException pex) {
          throw new MessagingException(pex.getMessage(), pex);
        }
        return recent > 0;
      }
    }
    



    ListInfo[] li = null;
    String lname;
    final String lname; if ((isNamespace) && (separator != 0)) {
      lname = fullName + separator;
    } else
      lname = fullName;
    li = (ListInfo[])doCommandIgnoreFailure(new ProtocolCommand()
    {
      public Object doCommand(IMAPProtocol p) throws ProtocolException {
        return p.list("", lname);
      }
    });
    

    if (li == null) {
      throw new FolderNotFoundException(this, fullName + " not found");
    }
    int i = findName(li, lname);
    if (changeState == 1)
      return true;
    if (changeState == 2) {
      return false;
    }
    try
    {
      Status status = getStatus();
      if (recent > 0) {
        return true;
      }
      return false;
    }
    catch (BadCommandException bex) {
      return false;
    } catch (ConnectionException cex) {
      throw new StoreClosedException(store, cex.getMessage());
    } catch (ProtocolException pex) {
      throw new MessagingException(pex.getMessage(), pex);
    }
  }
  





  public synchronized Folder getFolder(String name)
    throws MessagingException
  {
    if ((attributes != null) && (!isDirectory())) {
      throw new MessagingException("Cannot contain subfolders");
    }
    char c = getSeparator();
    return ((IMAPStore)store).newIMAPFolder(fullName + c + name, c);
  }
  



  public synchronized boolean delete(boolean recurse)
    throws MessagingException
  {
    checkClosed();
    
    if (recurse)
    {
      Folder[] f = list();
      for (int i = 0; i < f.length; i++) {
        f[i].delete(recurse);
      }
    }
    

    Object ret = doCommandIgnoreFailure(new ProtocolCommand()
    {
      public Object doCommand(IMAPProtocol p) throws ProtocolException {
        p.delete(fullName);
        return Boolean.TRUE;
      }
    });
    
    if (ret == null)
    {
      return false;
    }
    
    exists = false;
    attributes = null;
    

    notifyFolderListeners(2);
    return true;
  }
  



  public synchronized boolean renameTo(final Folder f)
    throws MessagingException
  {
    checkClosed();
    checkExists();
    if (f.getStore() != store) {
      throw new MessagingException("Can't rename across Stores");
    }
    
    Object ret = doCommandIgnoreFailure(new ProtocolCommand()
    {
      public Object doCommand(IMAPProtocol p) throws ProtocolException {
        p.rename(fullName, f.getFullName());
        return Boolean.TRUE;
      }
    });
    
    if (ret == null) {
      return false;
    }
    exists = false;
    attributes = null;
    notifyFolderRenamedListeners(f);
    return true;
  }
  


  public synchronized void open(int mode)
    throws MessagingException
  {
    open(mode, null);
  }
  









  public synchronized List<MailEvent> open(int mode, ResyncData rd)
    throws MessagingException
  {
    checkClosed();
    
    MailboxInfo mi = null;
    
    protocol = ((IMAPStore)store).getProtocol(this);
    
    List<MailEvent> openEvents = null;
    ReadOnlyFolderException ife; synchronized (messageCacheLock)
    {






      protocol.addResponseHandler(this);
      





      try
      {
        if (rd != null) {
          if (rd == ResyncData.CONDSTORE) {
            if ((!protocol.isEnabled("CONDSTORE")) && 
              (!protocol.isEnabled("QRESYNC"))) {
              if (protocol.hasCapability("CONDSTORE")) {
                protocol.enable("CONDSTORE");
              } else {
                protocol.enable("QRESYNC");
              }
            }
          } else if (!protocol.isEnabled("QRESYNC")) {
            protocol.enable("QRESYNC");
          }
        }
        
        if (mode == 1) {
          mi = protocol.examine(fullName, rd);
        } else {
          mi = protocol.select(fullName, rd);
        }
        

      }
      catch (CommandFailedException cex)
      {

        try
        {
          checkExists();
          
          if ((type & 0x1) == 0) {
            throw new MessagingException("folder cannot contain messages");
          }
          throw new MessagingException(cex.getMessage(), cex);
        }
        finally
        {
          exists = false;
          attributes = null;
          type = 0;
          
          releaseProtocol(true);
        }
      }
      catch (ProtocolException pex)
      {
        try {
          throw logoutAndThrow(pex.getMessage(), pex);
        } finally {
          releaseProtocol(false);
        }
      }
      
      if ((mode != mode) && (
        (mode != 2) || (mode != 1) || 
        (!((IMAPStore)store).allowReadOnlySelect())))
      {

        ife = new ReadOnlyFolderException(this, "Cannot open in desired mode");
        
        throw cleanupAndThrow(ife);
      }
      


      opened = true;
      reallyClosed = false;
      this.mode = mode;
      availableFlags = availableFlags;
      permanentFlags = permanentFlags;
      total = (this.realTotal = total);
      recent = recent;
      uidvalidity = uidvalidity;
      uidnext = uidnext;
      uidNotSticky = uidNotSticky;
      highestmodseq = highestmodseq;
      

      messageCache = new MessageCache(this, (IMAPStore)store, total);
      

      if (responses != null) {
        openEvents = new ArrayList();
        for (IMAPResponse ir : responses) {
          if (ir.keyEquals("VANISHED"))
          {
            String[] s = ir.readAtomStringList();
            
            if ((s != null) && (s.length == 1) && 
              (s[0].equalsIgnoreCase("EARLIER")))
            {
              String uids = ir.readAtom();
              UIDSet[] uidset = UIDSet.parseUIDSets(uids);
              long[] luid = UIDSet.toArray(uidset, uidnext);
              if ((luid != null) && (luid.length > 0))
                openEvents.add(new MessageVanishedEvent(this, luid));
            }
          } else if (ir.keyEquals("FETCH")) {
            assert ((ir instanceof FetchResponse)) : "!ir instanceof FetchResponse";
            
            Message msg = processFetchResponse((FetchResponse)ir);
            if (msg != null) {
              openEvents.add(new MessageChangedEvent(this, 1, msg));
            }
          }
        }
      }
    }
    
    exists = true;
    attributes = null;
    type = 1;
    

    notifyConnectionListeners(1);
    
    return openEvents;
  }
  
  private MessagingException cleanupAndThrow(MessagingException ife)
  {
    try {
      try {
        protocol.close();
        releaseProtocol(true);
      }
      catch (ProtocolException pex) {}
      try {
        addSuppressed(ife, logoutAndThrow(pex.getMessage(), pex));
        
        releaseProtocol(false); } finally { releaseProtocol(false);
      }
      



      return ife;
    }
    catch (Throwable thr)
    {
      addSuppressed(ife, thr);
    }
  }
  
  private MessagingException logoutAndThrow(String why, ProtocolException t)
  {
    MessagingException ife = new MessagingException(why, t);
    try {
      protocol.logout();
    } catch (Throwable thr) {
      addSuppressed(ife, thr);
    }
    return ife;
  }
  
  private void addSuppressed(Throwable ife, Throwable thr) {
    if (isRecoverable(thr)) {
      ife.addSuppressed(thr);
    } else {
      thr.addSuppressed(ife);
      if ((thr instanceof Error)) {
        throw ((Error)thr);
      }
      if ((thr instanceof RuntimeException)) {
        throw ((RuntimeException)thr);
      }
      throw new RuntimeException("unexpected exception", thr);
    }
  }
  
  private boolean isRecoverable(Throwable t) {
    return ((t instanceof Exception)) || ((t instanceof LinkageError));
  }
  



  public synchronized void fetch(Message[] msgs, FetchProfile fp)
    throws MessagingException
  {
    FetchItem[] fitems;
    


    synchronized (messageCacheLock) {
      checkOpened();
      boolean isRev1 = protocol.isREV1();
      fitems = protocol.getFetchItems(); }
    FetchItem[] fitems;
    boolean isRev1;
    StringBuffer command = new StringBuffer();
    boolean first = true;
    boolean allHeaders = false;
    
    if (fp.contains(FetchProfile.Item.ENVELOPE)) {
      command.append(getEnvelopeCommand());
      first = false;
    }
    if (fp.contains(FetchProfile.Item.FLAGS)) {
      command.append(first ? "FLAGS" : " FLAGS");
      first = false;
    }
    if (fp.contains(FetchProfile.Item.CONTENT_INFO)) {
      command.append(first ? "BODYSTRUCTURE" : " BODYSTRUCTURE");
      first = false;
    }
    if (fp.contains(UIDFolder.FetchProfileItem.UID)) {
      command.append(first ? "UID" : " UID");
      first = false;
    }
    if (fp.contains(FetchProfileItem.HEADERS)) {
      allHeaders = true;
      if (isRev1) {
        command.append(first ? "BODY.PEEK[HEADER]" : " BODY.PEEK[HEADER]");
      }
      else
        command.append(first ? "RFC822.HEADER" : " RFC822.HEADER");
      first = false;
    }
    if (fp.contains(FetchProfileItem.MESSAGE)) {
      allHeaders = true;
      if (isRev1) {
        command.append(first ? "BODY.PEEK[]" : " BODY.PEEK[]");
      } else
        command.append(first ? "RFC822" : " RFC822");
      first = false;
    }
    if ((fp.contains(FetchProfile.Item.SIZE)) || 
      (fp.contains(FetchProfileItem.SIZE))) {
      command.append(first ? "RFC822.SIZE" : " RFC822.SIZE");
      first = false;
    }
    if (fp.contains(FetchProfileItem.INTERNALDATE)) {
      command.append(first ? "INTERNALDATE" : " INTERNALDATE");
      first = false;
    }
    

    String[] hdrs = null;
    if (!allHeaders) {
      hdrs = fp.getHeaderNames();
      if (hdrs.length > 0) {
        if (!first)
          command.append(" ");
        command.append(createHeaderCommand(hdrs, isRev1));
      }
    }
    



    for (int i = 0; i < fitems.length; i++) {
      if (fp.contains(fitems[i].getFetchProfileItem())) {
        if (command.length() != 0)
          command.append(" ");
        command.append(fitems[i].getName());
      }
    }
    
    Utility.Condition condition = new IMAPMessage.FetchProfileCondition(fp, fitems);
    


    synchronized (messageCacheLock)
    {

      checkOpened();
      


      MessageSet[] msgsets = Utility.toMessageSetSorted(msgs, condition);
      
      if (msgsets == null)
      {
        return;
      }
      Response[] r = null;
      
      List<Response> v = new ArrayList();
      try {
        r = getProtocol().fetch(msgsets, command.toString());
      } catch (ConnectionException cex) {
        throw new FolderClosedException(this, cex.getMessage());
      }
      catch (CommandFailedException localCommandFailedException) {}catch (ProtocolException pex)
      {
        throw new MessagingException(pex.getMessage(), pex);
      }
      
      if (r == null) {
        return;
      }
      for (int i = 0; i < r.length; i++) {
        if (r[i] != null)
        {
          if (!(r[i] instanceof FetchResponse)) {
            v.add(r[i]);

          }
          else
          {
            FetchResponse f = (FetchResponse)r[i];
            
            IMAPMessage msg = getMessageBySeqNumber(f.getNumber());
            
            int count = f.getItemCount();
            boolean unsolicitedFlags = false;
            
            for (int j = 0; j < count; j++) {
              Item item = f.getItem(j);
              
              if (((item instanceof Flags)) && (
                (!fp.contains(FetchProfile.Item.FLAGS)) || (msg == null)))
              {

                unsolicitedFlags = true;
              } else if (msg != null)
                msg.handleFetchItem(item, hdrs, allHeaders);
            }
            if (msg != null) {
              msg.handleExtensionFetchItems(f.getExtensionItems());
            }
            

            if (unsolicitedFlags)
              v.add(f);
          }
        }
      }
      if (!v.isEmpty()) {
        Response[] responses = new Response[v.size()];
        v.toArray(responses);
        handleResponses(responses);
      }
    }
  }
  









  protected String getEnvelopeCommand()
  {
    return "ENVELOPE INTERNALDATE RFC822.SIZE";
  }
  








  protected IMAPMessage newIMAPMessage(int msgnum)
  {
    return new IMAPMessage(this, msgnum);
  }
  

  private String createHeaderCommand(String[] hdrs, boolean isRev1)
  {
    StringBuffer sb;
    
    StringBuffer sb;
    
    if (isRev1) {
      sb = new StringBuffer("BODY.PEEK[HEADER.FIELDS (");
    } else {
      sb = new StringBuffer("RFC822.HEADER.LINES (");
    }
    for (int i = 0; i < hdrs.length; i++) {
      if (i > 0)
        sb.append(" ");
      sb.append(hdrs[i]);
    }
    
    if (isRev1) {
      sb.append(")]");
    } else {
      sb.append(")");
    }
    return sb.toString();
  }
  



  public synchronized void setFlags(Message[] msgs, Flags flag, boolean value)
    throws MessagingException
  {
    checkOpened();
    checkFlags(flag);
    
    if (msgs.length == 0) {
      return;
    }
    synchronized (messageCacheLock) {
      try {
        IMAPProtocol p = getProtocol();
        MessageSet[] ms = Utility.toMessageSetSorted(msgs, null);
        if (ms == null) {
          throw new MessageRemovedException("Messages have been removed");
        }
        p.storeFlags(ms, flag, value);
      } catch (ConnectionException cex) {
        throw new FolderClosedException(this, cex.getMessage());
      } catch (ProtocolException pex) {
        throw new MessagingException(pex.getMessage(), pex);
      }
    }
  }
  



  public synchronized void setFlags(int start, int end, Flags flag, boolean value)
    throws MessagingException
  {
    checkOpened();
    Message[] msgs = new Message[end - start + 1];
    int i = 0;
    for (int n = start; n <= end; n++)
      msgs[(i++)] = getMessage(n);
    setFlags(msgs, flag, value);
  }
  



  public synchronized void setFlags(int[] msgnums, Flags flag, boolean value)
    throws MessagingException
  {
    checkOpened();
    Message[] msgs = new Message[msgnums.length];
    for (int i = 0; i < msgnums.length; i++)
      msgs[i] = getMessage(msgnums[i]);
    setFlags(msgs, flag, value);
  }
  


  public synchronized void close(boolean expunge)
    throws MessagingException
  {
    close(expunge, false);
  }
  



  public synchronized void forceClose()
    throws MessagingException
  {
    close(false, true);
  }
  


  private void close(boolean expunge, boolean force)
    throws MessagingException
  {
    assert (Thread.holdsLock(this));
    synchronized (messageCacheLock)
    {





      if ((!opened) && (reallyClosed)) {
        throw new IllegalStateException("This operation is not allowed on a closed folder");
      }
      

      reallyClosed = true;
      




      if (!opened) {
        return;
      }
      boolean reuseProtocol = true;
      try {
        waitIfIdle();
        if (force) {
          logger.log(Level.FINE, "forcing folder {0} to close", fullName);
          
          if (protocol != null)
            protocol.disconnect();
        } else if (((IMAPStore)store).isConnectionPoolFull())
        {
          logger.fine("pool is full, not adding an Authenticated connection");
          


          if ((expunge) && (protocol != null)) {
            protocol.close();
          }
          if (protocol != null) {
            protocol.logout();
          }
          

        }
        else if ((!expunge) && (mode == 2)) {
          try {
            if ((protocol != null) && 
              (protocol.hasCapability("UNSELECT"))) {
              protocol.unselect();








            }
            else if (protocol != null) {
              boolean selected = true;
              try {
                protocol.examine(fullName);

              }
              catch (CommandFailedException ex)
              {
                selected = false;
              }
              if ((selected) && (protocol != null)) {
                protocol.close();
              }
            }
          } catch (ProtocolException pex2) {
            reuseProtocol = false;
          }
        }
        else if (protocol != null) {
          protocol.close();
        }
      }
      catch (ProtocolException pex) {
        throw new MessagingException(pex.getMessage(), pex);
      }
      finally {
        if (opened) {
          cleanup(reuseProtocol);
        }
      }
    }
  }
  



  private void cleanup(boolean returnToPool)
  {
    assert (Thread.holdsLock(messageCacheLock));
    releaseProtocol(returnToPool);
    messageCache = null;
    uidTable = null;
    exists = false;
    attributes = null;
    opened = false;
    idleState = 0;
    messageCacheLock.notifyAll();
    notifyConnectionListeners(3);
  }
  



  public synchronized boolean isOpen()
  {
    synchronized (messageCacheLock)
    {
      if (opened) {
        try {
          keepConnectionAlive(false);
        }
        catch (ProtocolException localProtocolException) {}
      }
    }
    return opened;
  }
  



  public synchronized Flags getPermanentFlags()
  {
    if (permanentFlags == null)
      return null;
    return (Flags)permanentFlags.clone();
  }
  


  public synchronized int getMessageCount()
    throws MessagingException
  {
    synchronized (messageCacheLock) {
      if (opened)
      {
        try
        {
          keepConnectionAlive(true);
          return total;
        } catch (ConnectionException cex) {
          throw new FolderClosedException(this, cex.getMessage());
        } catch (ProtocolException pex) {
          throw new MessagingException(pex.getMessage(), pex);
        }
      }
    }
    


    checkExists();
    try {
      Status status = getStatus();
      return total;
    }
    catch (BadCommandException bex)
    {
      IMAPProtocol p = null;
      try
      {
        p = getStoreProtocol();
        MailboxInfo minfo = p.examine(fullName);
        p.close();
        return total;
      }
      catch (ProtocolException pex) {
        throw new MessagingException(pex.getMessage(), pex);
      } finally {
        releaseStoreProtocol(p);
      }
    } catch (ConnectionException cex) {
      throw new StoreClosedException(store, cex.getMessage());
    } catch (ProtocolException pex) {
      throw new MessagingException(pex.getMessage(), pex);
    }
  }
  


  public synchronized int getNewMessageCount()
    throws MessagingException
  {
    synchronized (messageCacheLock) {
      if (opened)
      {
        try
        {
          keepConnectionAlive(true);
          return recent;
        } catch (ConnectionException cex) {
          throw new FolderClosedException(this, cex.getMessage());
        } catch (ProtocolException pex) {
          throw new MessagingException(pex.getMessage(), pex);
        }
      }
    }
    


    checkExists();
    try {
      Status status = getStatus();
      return recent;
    }
    catch (BadCommandException bex)
    {
      IMAPProtocol p = null;
      try
      {
        p = getStoreProtocol();
        MailboxInfo minfo = p.examine(fullName);
        p.close();
        return recent;
      }
      catch (ProtocolException pex) {
        throw new MessagingException(pex.getMessage(), pex);
      } finally {
        releaseStoreProtocol(p);
      }
    } catch (ConnectionException cex) {
      throw new StoreClosedException(store, cex.getMessage());
    } catch (ProtocolException pex) {
      throw new MessagingException(pex.getMessage(), pex);
    }
  }
  
  /* Error */
  public synchronized int getUnreadMessageCount()
    throws MessagingException
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 8	com/sun/mail/imap/IMAPFolder:opened	Z
    //   4: ifne +51 -> 55
    //   7: aload_0
    //   8: invokevirtual 103	com/sun/mail/imap/IMAPFolder:checkExists	()V
    //   11: aload_0
    //   12: invokespecial 124	com/sun/mail/imap/IMAPFolder:getStatus	()Lcom/sun/mail/imap/protocol/Status;
    //   15: astore_1
    //   16: aload_1
    //   17: getfield 311	com/sun/mail/imap/protocol/Status:unseen	I
    //   20: ireturn
    //   21: astore_1
    //   22: iconst_m1
    //   23: ireturn
    //   24: astore_1
    //   25: new 127	javax/mail/StoreClosedException
    //   28: dup
    //   29: aload_0
    //   30: getfield 89	com/sun/mail/imap/IMAPFolder:store	Ljavax/mail/Store;
    //   33: aload_1
    //   34: invokevirtual 76	com/sun/mail/iap/ConnectionException:getMessage	()Ljava/lang/String;
    //   37: invokespecial 128	javax/mail/StoreClosedException:<init>	(Ljavax/mail/Store;Ljava/lang/String;)V
    //   40: athrow
    //   41: astore_1
    //   42: new 78	javax/mail/MessagingException
    //   45: dup
    //   46: aload_1
    //   47: invokevirtual 79	com/sun/mail/iap/ProtocolException:getMessage	()Ljava/lang/String;
    //   50: aload_1
    //   51: invokespecial 80	javax/mail/MessagingException:<init>	(Ljava/lang/String;Ljava/lang/Exception;)V
    //   54: athrow
    //   55: new 267	javax/mail/Flags
    //   58: dup
    //   59: invokespecial 312	javax/mail/Flags:<init>	()V
    //   62: astore_1
    //   63: aload_1
    //   64: getstatic 313	javax/mail/Flags$Flag:SEEN	Ljavax/mail/Flags$Flag;
    //   67: invokevirtual 314	javax/mail/Flags:add	(Ljavax/mail/Flags$Flag;)V
    //   70: aload_0
    //   71: getfield 7	com/sun/mail/imap/IMAPFolder:messageCacheLock	Ljava/lang/Object;
    //   74: dup
    //   75: astore_2
    //   76: monitorenter
    //   77: aload_0
    //   78: invokevirtual 260	com/sun/mail/imap/IMAPFolder:getProtocol	()Lcom/sun/mail/imap/protocol/IMAPProtocol;
    //   81: new 315	javax/mail/search/FlagTerm
    //   84: dup
    //   85: aload_1
    //   86: iconst_0
    //   87: invokespecial 316	javax/mail/search/FlagTerm:<init>	(Ljavax/mail/Flags;Z)V
    //   90: invokevirtual 317	com/sun/mail/imap/protocol/IMAPProtocol:search	(Ljavax/mail/search/SearchTerm;)[I
    //   93: astore_3
    //   94: aload_3
    //   95: arraylength
    //   96: aload_2
    //   97: monitorexit
    //   98: ireturn
    //   99: astore 4
    //   101: aload_2
    //   102: monitorexit
    //   103: aload 4
    //   105: athrow
    //   106: astore_2
    //   107: new 68	javax/mail/FolderClosedException
    //   110: dup
    //   111: aload_0
    //   112: aload_2
    //   113: invokevirtual 76	com/sun/mail/iap/ConnectionException:getMessage	()Ljava/lang/String;
    //   116: invokespecial 70	javax/mail/FolderClosedException:<init>	(Ljavax/mail/Folder;Ljava/lang/String;)V
    //   119: athrow
    //   120: astore_2
    //   121: new 78	javax/mail/MessagingException
    //   124: dup
    //   125: aload_2
    //   126: invokevirtual 79	com/sun/mail/iap/ProtocolException:getMessage	()Ljava/lang/String;
    //   129: aload_2
    //   130: invokespecial 80	javax/mail/MessagingException:<init>	(Ljava/lang/String;Ljava/lang/Exception;)V
    //   133: athrow
    // Line number table:
    //   Java source line #1739	-> byte code offset #0
    //   Java source line #1740	-> byte code offset #7
    //   Java source line #1744	-> byte code offset #11
    //   Java source line #1745	-> byte code offset #16
    //   Java source line #1746	-> byte code offset #21
    //   Java source line #1750	-> byte code offset #22
    //   Java source line #1751	-> byte code offset #24
    //   Java source line #1752	-> byte code offset #25
    //   Java source line #1753	-> byte code offset #41
    //   Java source line #1754	-> byte code offset #42
    //   Java source line #1760	-> byte code offset #55
    //   Java source line #1761	-> byte code offset #63
    //   Java source line #1763	-> byte code offset #70
    //   Java source line #1764	-> byte code offset #77
    //   Java source line #1765	-> byte code offset #94
    //   Java source line #1766	-> byte code offset #99
    //   Java source line #1767	-> byte code offset #106
    //   Java source line #1768	-> byte code offset #107
    //   Java source line #1769	-> byte code offset #120
    //   Java source line #1771	-> byte code offset #121
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	134	0	this	IMAPFolder
    //   15	2	1	status	Status
    //   21	2	1	bex	BadCommandException
    //   24	10	1	cex	ConnectionException
    //   41	10	1	pex	ProtocolException
    //   62	24	1	f	Flags
    //   106	7	2	cex	ConnectionException
    //   120	10	2	pex	ProtocolException
    //   93	2	3	matches	int[]
    //   99	5	4	localObject1	Object
    // Exception table:
    //   from	to	target	type
    //   11	20	21	com/sun/mail/iap/BadCommandException
    //   11	20	24	com/sun/mail/iap/ConnectionException
    //   11	20	41	com/sun/mail/iap/ProtocolException
    //   77	98	99	finally
    //   99	103	99	finally
    //   70	98	106	com/sun/mail/iap/ConnectionException
    //   99	106	106	com/sun/mail/iap/ConnectionException
    //   70	98	120	com/sun/mail/iap/ProtocolException
    //   99	106	120	com/sun/mail/iap/ProtocolException
  }
  
  /* Error */
  public synchronized int getDeletedMessageCount()
    throws MessagingException
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 8	com/sun/mail/imap/IMAPFolder:opened	Z
    //   4: ifne +9 -> 13
    //   7: aload_0
    //   8: invokevirtual 103	com/sun/mail/imap/IMAPFolder:checkExists	()V
    //   11: iconst_m1
    //   12: ireturn
    //   13: new 267	javax/mail/Flags
    //   16: dup
    //   17: invokespecial 312	javax/mail/Flags:<init>	()V
    //   20: astore_1
    //   21: aload_1
    //   22: getstatic 318	javax/mail/Flags$Flag:DELETED	Ljavax/mail/Flags$Flag;
    //   25: invokevirtual 314	javax/mail/Flags:add	(Ljavax/mail/Flags$Flag;)V
    //   28: aload_0
    //   29: getfield 7	com/sun/mail/imap/IMAPFolder:messageCacheLock	Ljava/lang/Object;
    //   32: dup
    //   33: astore_2
    //   34: monitorenter
    //   35: aload_0
    //   36: invokevirtual 260	com/sun/mail/imap/IMAPFolder:getProtocol	()Lcom/sun/mail/imap/protocol/IMAPProtocol;
    //   39: new 315	javax/mail/search/FlagTerm
    //   42: dup
    //   43: aload_1
    //   44: iconst_1
    //   45: invokespecial 316	javax/mail/search/FlagTerm:<init>	(Ljavax/mail/Flags;Z)V
    //   48: invokevirtual 317	com/sun/mail/imap/protocol/IMAPProtocol:search	(Ljavax/mail/search/SearchTerm;)[I
    //   51: astore_3
    //   52: aload_3
    //   53: arraylength
    //   54: aload_2
    //   55: monitorexit
    //   56: ireturn
    //   57: astore 4
    //   59: aload_2
    //   60: monitorexit
    //   61: aload 4
    //   63: athrow
    //   64: astore_2
    //   65: new 68	javax/mail/FolderClosedException
    //   68: dup
    //   69: aload_0
    //   70: aload_2
    //   71: invokevirtual 76	com/sun/mail/iap/ConnectionException:getMessage	()Ljava/lang/String;
    //   74: invokespecial 70	javax/mail/FolderClosedException:<init>	(Ljavax/mail/Folder;Ljava/lang/String;)V
    //   77: athrow
    //   78: astore_2
    //   79: new 78	javax/mail/MessagingException
    //   82: dup
    //   83: aload_2
    //   84: invokevirtual 79	com/sun/mail/iap/ProtocolException:getMessage	()Ljava/lang/String;
    //   87: aload_2
    //   88: invokespecial 80	javax/mail/MessagingException:<init>	(Ljava/lang/String;Ljava/lang/Exception;)V
    //   91: athrow
    // Line number table:
    //   Java source line #1781	-> byte code offset #0
    //   Java source line #1782	-> byte code offset #7
    //   Java source line #1784	-> byte code offset #11
    //   Java source line #1789	-> byte code offset #13
    //   Java source line #1790	-> byte code offset #21
    //   Java source line #1792	-> byte code offset #28
    //   Java source line #1793	-> byte code offset #35
    //   Java source line #1794	-> byte code offset #52
    //   Java source line #1795	-> byte code offset #57
    //   Java source line #1796	-> byte code offset #64
    //   Java source line #1797	-> byte code offset #65
    //   Java source line #1798	-> byte code offset #78
    //   Java source line #1800	-> byte code offset #79
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	92	0	this	IMAPFolder
    //   20	24	1	f	Flags
    //   64	7	2	cex	ConnectionException
    //   78	10	2	pex	ProtocolException
    //   51	2	3	matches	int[]
    //   57	5	4	localObject1	Object
    // Exception table:
    //   from	to	target	type
    //   35	56	57	finally
    //   57	61	57	finally
    //   28	56	64	com/sun/mail/iap/ConnectionException
    //   57	64	64	com/sun/mail/iap/ConnectionException
    //   28	56	78	com/sun/mail/iap/ProtocolException
    //   57	64	78	com/sun/mail/iap/ProtocolException
  }
  
  private Status getStatus()
    throws ProtocolException
  {
    int statusCacheTimeout = ((IMAPStore)store).getStatusCacheTimeout();
    

    if ((statusCacheTimeout > 0) && (cachedStatus != null) && 
      (System.currentTimeMillis() - cachedStatusTime < statusCacheTimeout)) {
      return cachedStatus;
    }
    IMAPProtocol p = null;
    try
    {
      p = getStoreProtocol();
      Status s = p.status(fullName, null);
      
      if (statusCacheTimeout > 0) {
        cachedStatus = s;
        cachedStatusTime = System.currentTimeMillis();
      }
      return s;
    } finally {
      releaseStoreProtocol(p);
    }
  }
  



  public synchronized Message getMessage(int msgnum)
    throws MessagingException
  {
    checkOpened();
    checkRange(msgnum);
    
    return messageCache.getMessage(msgnum);
  }
  






  public synchronized Message[] getMessages()
    throws MessagingException
  {
    checkOpened();
    int total = getMessageCount();
    Message[] msgs = new Message[total];
    for (int i = 1; i <= total; i++)
      msgs[(i - 1)] = messageCache.getMessage(i);
    return msgs;
  }
  



  public synchronized void appendMessages(Message[] msgs)
    throws MessagingException
  {
    checkExists();
    





    int maxsize = ((IMAPStore)store).getAppendBufferSize();
    
    for (int i = 0; i < msgs.length; i++) {
      Message m = msgs[i];
      Date d = m.getReceivedDate();
      if (d == null)
        d = m.getSentDate();
      final Date dd = d;
      final Flags f = m.getFlags();
      


      try
      {
        mos = new MessageLiteral(m, m.getSize() > maxsize ? 0 : maxsize);
      } catch (IOException ex) { MessageLiteral mos;
        throw new MessagingException("IOException while appending messages", ex);
      }
      catch (MessageRemovedException mrex) {
        continue;
      }
      final MessageLiteral mos;
      doCommand(new ProtocolCommand()
      {
        public Object doCommand(IMAPProtocol p) throws ProtocolException
        {
          p.append(fullName, f, dd, mos);
          return null;
        }
      });
    }
  }
  

















  public synchronized AppendUID[] appendUIDMessages(Message[] msgs)
    throws MessagingException
  {
    checkExists();
    





    int maxsize = ((IMAPStore)store).getAppendBufferSize();
    
    AppendUID[] uids = new AppendUID[msgs.length];
    for (int i = 0; i < msgs.length; i++) {
      Message m = msgs[i];
      


      try
      {
        mos = new MessageLiteral(m, m.getSize() > maxsize ? 0 : maxsize);
      } catch (IOException ex) { MessageLiteral mos;
        throw new MessagingException("IOException while appending messages", ex);
      }
      catch (MessageRemovedException mrex) {
        continue;
      }
      final MessageLiteral mos;
      Date d = m.getReceivedDate();
      if (d == null)
        d = m.getSentDate();
      final Date dd = d;
      final Flags f = m.getFlags();
      AppendUID auid = (AppendUID)doCommand(new ProtocolCommand()
      {
        public Object doCommand(IMAPProtocol p) throws ProtocolException
        {
          return p.appenduid(fullName, f, dd, mos);
        }
      });
      uids[i] = auid;
    }
    return uids;
  }
  


















  public synchronized Message[] addMessages(Message[] msgs)
    throws MessagingException
  {
    checkOpened();
    Message[] rmsgs = new MimeMessage[msgs.length];
    AppendUID[] uids = appendUIDMessages(msgs);
    for (int i = 0; i < uids.length; i++) {
      AppendUID auid = uids[i];
      if ((auid != null) && 
        (uidvalidity == uidvalidity)) {
        try {
          rmsgs[i] = getMessageByUID(uid);
        }
        catch (MessagingException localMessagingException) {}
      }
    }
    

    return rmsgs;
  }
  




  public synchronized void copyMessages(Message[] msgs, Folder folder)
    throws MessagingException
  {
    copymoveMessages(msgs, folder, false);
  }
  



















  public synchronized AppendUID[] copyUIDMessages(Message[] msgs, Folder folder)
    throws MessagingException
  {
    return copymoveUIDMessages(msgs, folder, false);
  }
  












  public synchronized void moveMessages(Message[] msgs, Folder folder)
    throws MessagingException
  {
    copymoveMessages(msgs, folder, true);
  }
  





















  public synchronized AppendUID[] moveUIDMessages(Message[] msgs, Folder folder)
    throws MessagingException
  {
    return copymoveUIDMessages(msgs, folder, true);
  }
  





  private synchronized void copymoveMessages(Message[] msgs, Folder folder, boolean move)
    throws MessagingException
  {
    checkOpened();
    
    if (msgs.length == 0) {
      return;
    }
    
    if (folder.getStore() == store) {
      synchronized (messageCacheLock) {
        try {
          IMAPProtocol p = getProtocol();
          MessageSet[] ms = Utility.toMessageSet(msgs, null);
          if (ms == null) {
            throw new MessageRemovedException("Messages have been removed");
          }
          if (move) {
            p.move(ms, folder.getFullName());
          } else
            p.copy(ms, folder.getFullName());
        } catch (CommandFailedException cfx) {
          if (cfx.getMessage().indexOf("TRYCREATE") != -1)
          {

            throw new FolderNotFoundException(folder, folder.getFullName() + " does not exist");
          }
          
          throw new MessagingException(cfx.getMessage(), cfx);
        } catch (ConnectionException cex) {
          throw new FolderClosedException(this, cex.getMessage());
        } catch (ProtocolException pex) {
          throw new MessagingException(pex.getMessage(), pex);
        }
      }
    } else {
      if (move) {
        throw new MessagingException("Move between stores not supported");
      }
      
      super.copyMessages(msgs, folder);
    }
  }
  





















  private synchronized AppendUID[] copymoveUIDMessages(Message[] msgs, Folder folder, boolean move)
    throws MessagingException
  {
    checkOpened();
    
    if (msgs.length == 0) {
      return null;
    }
    
    if (folder.getStore() != store) {
      throw new MessagingException(move ? "can't moveUIDMessages to a different store" : "can't copyUIDMessages to a different store");
    }
    




    FetchProfile fp = new FetchProfile();
    fp.add(UIDFolder.FetchProfileItem.UID);
    fetch(msgs, fp);
    

    synchronized (messageCacheLock) {
      try {
        IMAPProtocol p = getProtocol();
        
        MessageSet[] ms = Utility.toMessageSet(msgs, null);
        if (ms == null)
          throw new MessageRemovedException("Messages have been removed");
        CopyUID cuid;
        CopyUID cuid;
        if (move) {
          cuid = p.moveuid(ms, folder.getFullName());
        } else {
          cuid = p.copyuid(ms, folder.getFullName());
        }
        







































        long[] srcuids = UIDSet.toArray(src);
        long[] dstuids = UIDSet.toArray(dst);
        

        Message[] srcmsgs = getMessagesByUID(srcuids);
        AppendUID[] result = new AppendUID[msgs.length];
        for (int i = 0; i < msgs.length; i++) {
          int j = i;
          do {
            if (msgs[i] == srcmsgs[j]) {
              result[i] = new AppendUID(uidvalidity, dstuids[j]);
              
              break;
            }
            j++;
            if (j >= srcmsgs.length)
              j = 0;
          } while (j != i);
        }
        return result;
      } catch (CommandFailedException cfx) {
        if (cfx.getMessage().indexOf("TRYCREATE") != -1)
        {

          throw new FolderNotFoundException(folder, folder.getFullName() + " does not exist");
        }
        
        throw new MessagingException(cfx.getMessage(), cfx);
      } catch (ConnectionException cex) {
        throw new FolderClosedException(this, cex.getMessage());
      } catch (ProtocolException pex) {
        throw new MessagingException(pex.getMessage(), pex);
      }
    }
  }
  


  public synchronized Message[] expunge()
    throws MessagingException
  {
    return expunge(null);
  }
  









  public synchronized Message[] expunge(Message[] msgs)
    throws MessagingException
  {
    checkOpened();
    
    if (msgs != null)
    {
      FetchProfile fp = new FetchProfile();
      fp.add(UIDFolder.FetchProfileItem.UID);
      fetch(msgs, fp);
    }
    

    synchronized (messageCacheLock) {
      doExpungeNotification = false;
      try {
        IMAPProtocol p = getProtocol();
        if (msgs != null) {
          p.uidexpunge(Utility.toUIDSet(msgs));
        } else {
          p.expunge();
        }
      } catch (CommandFailedException cfx) {
        if (mode != 2) {
          throw new IllegalStateException("Cannot expunge READ_ONLY folder: " + fullName);
        }
        
        throw new MessagingException(cfx.getMessage(), cfx);
      } catch (ConnectionException cex) {
        throw new FolderClosedException(this, cex.getMessage());
      }
      catch (ProtocolException pex) {
        throw new MessagingException(pex.getMessage(), pex);
      } finally {
        doExpungeNotification = true;
      }
      IMAPMessage[] rmsgs;
      IMAPMessage[] rmsgs;
      if (msgs != null) {
        rmsgs = messageCache.removeExpungedMessages(msgs);
      } else
        rmsgs = messageCache.removeExpungedMessages();
      if (uidTable != null) {
        for (int i = 0; i < rmsgs.length; i++) {
          IMAPMessage m = rmsgs[i];
          
          long uid = m.getUID();
          if (uid != -1L) {
            uidTable.remove(Long.valueOf(uid));
          }
        }
      }
      
      total = messageCache.size();
    }
    
    IMAPMessage[] rmsgs;
    if (rmsgs.length > 0)
      notifyMessageRemovedListeners(true, rmsgs);
    return rmsgs;
  }
  














  public synchronized Message[] search(SearchTerm term)
    throws MessagingException
  {
    checkOpened();
    try
    {
      Message[] matchMsgs = null;
      
      synchronized (messageCacheLock) {
        int[] matches = getProtocol().search(term);
        if (matches != null)
          matchMsgs = getMessagesBySeqNumbers(matches);
      }
      return matchMsgs;
    }
    catch (CommandFailedException cfx)
    {
      return super.search(term);
    }
    catch (SearchException sex) {
      if (((IMAPStore)store).throwSearchException())
        throw sex;
      return super.search(term);
    } catch (ConnectionException cex) {
      throw new FolderClosedException(this, cex.getMessage());
    }
    catch (ProtocolException pex) {
      throw new MessagingException(pex.getMessage(), pex);
    }
  }
  





  public synchronized Message[] search(SearchTerm term, Message[] msgs)
    throws MessagingException
  {
    checkOpened();
    
    if (msgs.length == 0)
    {
      return msgs;
    }
    try {
      Message[] matchMsgs = null;
      
      synchronized (messageCacheLock) {
        IMAPProtocol p = getProtocol();
        MessageSet[] ms = Utility.toMessageSetSorted(msgs, null);
        if (ms == null) {
          throw new MessageRemovedException("Messages have been removed");
        }
        int[] matches = p.search(ms, term);
        if (matches != null)
          matchMsgs = getMessagesBySeqNumbers(matches);
      }
      return matchMsgs;
    }
    catch (CommandFailedException cfx)
    {
      return super.search(term, msgs);
    }
    catch (SearchException sex) {
      return super.search(term, msgs);
    } catch (ConnectionException cex) {
      throw new FolderClosedException(this, cex.getMessage());
    }
    catch (ProtocolException pex) {
      throw new MessagingException(pex.getMessage(), pex);
    }
  }
  












  public synchronized Message[] getSortedMessages(SortTerm[] term)
    throws MessagingException
  {
    return getSortedMessages(term, null);
  }
  














  public synchronized Message[] getSortedMessages(SortTerm[] term, SearchTerm sterm)
    throws MessagingException
  {
    checkOpened();
    try
    {
      Message[] matchMsgs = null;
      
      synchronized (messageCacheLock) {
        int[] matches = getProtocol().sort(term, sterm);
        if (matches != null)
          matchMsgs = getMessagesBySeqNumbers(matches);
      }
      return matchMsgs;
    }
    catch (CommandFailedException cfx)
    {
      throw new MessagingException(cfx.getMessage(), cfx);
    }
    catch (SearchException sex) {
      throw new MessagingException(sex.getMessage(), sex);
    } catch (ConnectionException cex) {
      throw new FolderClosedException(this, cex.getMessage());
    }
    catch (ProtocolException pex) {
      throw new MessagingException(pex.getMessage(), pex);
    }
  }
  







  public synchronized void addMessageCountListener(MessageCountListener l)
  {
    super.addMessageCountListener(l);
    hasMessageCountListener = true;
  }
  






  public synchronized long getUIDValidity()
    throws MessagingException
  {
    if (opened) {
      return uidvalidity;
    }
    IMAPProtocol p = null;
    Status status = null;
    try
    {
      p = getStoreProtocol();
      String[] item = { "UIDVALIDITY" };
      status = p.status(fullName, item);
    }
    catch (BadCommandException bex) {
      throw new MessagingException("Cannot obtain UIDValidity", bex);
    }
    catch (ConnectionException cex) {
      throwClosedException(cex);
    } catch (ProtocolException pex) {
      throw new MessagingException(pex.getMessage(), pex);
    } finally {
      releaseStoreProtocol(p);
    }
    
    if (status == null)
      throw new MessagingException("Cannot obtain UIDValidity");
    return uidvalidity;
  }
  

















  public synchronized long getUIDNext()
    throws MessagingException
  {
    if (opened) {
      return uidnext;
    }
    IMAPProtocol p = null;
    Status status = null;
    try
    {
      p = getStoreProtocol();
      String[] item = { "UIDNEXT" };
      status = p.status(fullName, item);
    }
    catch (BadCommandException bex) {
      throw new MessagingException("Cannot obtain UIDNext", bex);
    }
    catch (ConnectionException cex) {
      throwClosedException(cex);
    } catch (ProtocolException pex) {
      throw new MessagingException(pex.getMessage(), pex);
    } finally {
      releaseStoreProtocol(p);
    }
    
    if (status == null)
      throw new MessagingException("Cannot obtain UIDNext");
    return uidnext;
  }
  




  public synchronized Message getMessageByUID(long uid)
    throws MessagingException
  {
    checkOpened();
    
    IMAPMessage m = null;
    try
    {
      synchronized (messageCacheLock) {
        Long l = Long.valueOf(uid);
        
        if (uidTable != null)
        {
          m = (IMAPMessage)uidTable.get(l);
          if (m != null)
            return m;
        } else {
          uidTable = new Hashtable();
        }
        

        getProtocol().fetchSequenceNumber(uid);
        
        if (uidTable != null)
        {
          m = (IMAPMessage)uidTable.get(l);
          if (m != null)
            return m;
        }
      }
    } catch (ConnectionException cex) {
      throw new FolderClosedException(this, cex.getMessage());
    } catch (ProtocolException pex) {
      throw new MessagingException(pex.getMessage(), pex);
    }
    
    return m;
  }
  





  public synchronized Message[] getMessagesByUID(long start, long end)
    throws MessagingException
  {
    checkOpened();
    
    try
    {
      Message[] msgs;
      synchronized (messageCacheLock) {
        if (uidTable == null) {
          uidTable = new Hashtable();
        }
        
        long[] ua = getProtocol().fetchSequenceNumbers(start, end);
        
        List<Message> ma = new ArrayList();
        
        for (int i = 0; i < ua.length; i++) {
          Message m = (Message)uidTable.get(Long.valueOf(ua[i]));
          if (m != null)
            ma.add(m);
        }
        msgs = (Message[])ma.toArray(new Message[ma.size()]);
      }
    } catch (ConnectionException cex) { Message[] msgs;
      throw new FolderClosedException(this, cex.getMessage());
    } catch (ProtocolException pex) {
      throw new MessagingException(pex.getMessage(), pex);
    }
    Message[] msgs;
    return msgs;
  }
  
  /* Error */
  public synchronized Message[] getMessagesByUID(long[] uids)
    throws MessagingException
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 216	com/sun/mail/imap/IMAPFolder:checkOpened	()V
    //   4: aload_0
    //   5: getfield 7	com/sun/mail/imap/IMAPFolder:messageCacheLock	Ljava/lang/Object;
    //   8: dup
    //   9: astore_2
    //   10: monitorenter
    //   11: aload_1
    //   12: astore_3
    //   13: aload_0
    //   14: getfield 305	com/sun/mail/imap/IMAPFolder:uidTable	Ljava/util/Hashtable;
    //   17: ifnull +123 -> 140
    //   20: new 178	java/util/ArrayList
    //   23: dup
    //   24: invokespecial 179	java/util/ArrayList:<init>	()V
    //   27: astore 4
    //   29: aload_1
    //   30: astore 5
    //   32: aload 5
    //   34: arraylength
    //   35: istore 6
    //   37: iconst_0
    //   38: istore 7
    //   40: iload 7
    //   42: iload 6
    //   44: if_icmpge +44 -> 88
    //   47: aload 5
    //   49: iload 7
    //   51: laload
    //   52: lstore 8
    //   54: aload_0
    //   55: getfield 305	com/sun/mail/imap/IMAPFolder:uidTable	Ljava/util/Hashtable;
    //   58: lload 8
    //   60: invokestatic 377	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   63: invokevirtual 405	java/util/Hashtable:containsKey	(Ljava/lang/Object;)Z
    //   66: ifne +16 -> 82
    //   69: aload 4
    //   71: lload 8
    //   73: invokestatic 377	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   76: invokeinterface 194 2 0
    //   81: pop
    //   82: iinc 7 1
    //   85: goto -45 -> 40
    //   88: aload 4
    //   90: invokeinterface 272 1 0
    //   95: istore 5
    //   97: iload 5
    //   99: newarray long
    //   101: astore_3
    //   102: iconst_0
    //   103: istore 6
    //   105: iload 6
    //   107: iload 5
    //   109: if_icmpge +28 -> 137
    //   112: aload_3
    //   113: iload 6
    //   115: aload 4
    //   117: iload 6
    //   119: invokeinterface 406 2 0
    //   124: checkcast 407	java/lang/Long
    //   127: invokevirtual 408	java/lang/Long:longValue	()J
    //   130: lastore
    //   131: iinc 6 1
    //   134: goto -29 -> 105
    //   137: goto +14 -> 151
    //   140: aload_0
    //   141: new 400	java/util/Hashtable
    //   144: dup
    //   145: invokespecial 401	java/util/Hashtable:<init>	()V
    //   148: putfield 305	com/sun/mail/imap/IMAPFolder:uidTable	Ljava/util/Hashtable;
    //   151: aload_3
    //   152: arraylength
    //   153: ifle +11 -> 164
    //   156: aload_0
    //   157: invokevirtual 260	com/sun/mail/imap/IMAPFolder:getProtocol	()Lcom/sun/mail/imap/protocol/IMAPProtocol;
    //   160: aload_3
    //   161: invokevirtual 409	com/sun/mail/imap/protocol/IMAPProtocol:fetchSequenceNumbers	([J)V
    //   164: aload_1
    //   165: arraylength
    //   166: anewarray 289	javax/mail/Message
    //   169: astore 4
    //   171: iconst_0
    //   172: istore 5
    //   174: iload 5
    //   176: aload_1
    //   177: arraylength
    //   178: if_icmpge +31 -> 209
    //   181: aload 4
    //   183: iload 5
    //   185: aload_0
    //   186: getfield 305	com/sun/mail/imap/IMAPFolder:uidTable	Ljava/util/Hashtable;
    //   189: aload_1
    //   190: iload 5
    //   192: laload
    //   193: invokestatic 377	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   196: invokevirtual 399	java/util/Hashtable:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   199: checkcast 289	javax/mail/Message
    //   202: aastore
    //   203: iinc 5 1
    //   206: goto -32 -> 174
    //   209: aload 4
    //   211: aload_2
    //   212: monitorexit
    //   213: areturn
    //   214: astore 10
    //   216: aload_2
    //   217: monitorexit
    //   218: aload 10
    //   220: athrow
    //   221: astore_2
    //   222: new 68	javax/mail/FolderClosedException
    //   225: dup
    //   226: aload_0
    //   227: aload_2
    //   228: invokevirtual 76	com/sun/mail/iap/ConnectionException:getMessage	()Ljava/lang/String;
    //   231: invokespecial 70	javax/mail/FolderClosedException:<init>	(Ljavax/mail/Folder;Ljava/lang/String;)V
    //   234: athrow
    //   235: astore_2
    //   236: new 78	javax/mail/MessagingException
    //   239: dup
    //   240: aload_2
    //   241: invokevirtual 79	com/sun/mail/iap/ProtocolException:getMessage	()Ljava/lang/String;
    //   244: aload_2
    //   245: invokespecial 80	javax/mail/MessagingException:<init>	(Ljava/lang/String;Ljava/lang/Exception;)V
    //   248: athrow
    // Line number table:
    //   Java source line #2686	-> byte code offset #0
    //   Java source line #2689	-> byte code offset #4
    //   Java source line #2690	-> byte code offset #11
    //   Java source line #2691	-> byte code offset #13
    //   Java source line #2693	-> byte code offset #20
    //   Java source line #2694	-> byte code offset #29
    //   Java source line #2695	-> byte code offset #54
    //   Java source line #2697	-> byte code offset #69
    //   Java source line #2694	-> byte code offset #82
    //   Java source line #2701	-> byte code offset #88
    //   Java source line #2702	-> byte code offset #97
    //   Java source line #2703	-> byte code offset #102
    //   Java source line #2704	-> byte code offset #112
    //   Java source line #2703	-> byte code offset #131
    //   Java source line #2706	-> byte code offset #137
    //   Java source line #2707	-> byte code offset #140
    //   Java source line #2709	-> byte code offset #151
    //   Java source line #2711	-> byte code offset #156
    //   Java source line #2715	-> byte code offset #164
    //   Java source line #2716	-> byte code offset #171
    //   Java source line #2717	-> byte code offset #181
    //   Java source line #2716	-> byte code offset #203
    //   Java source line #2718	-> byte code offset #209
    //   Java source line #2719	-> byte code offset #214
    //   Java source line #2720	-> byte code offset #221
    //   Java source line #2721	-> byte code offset #222
    //   Java source line #2722	-> byte code offset #235
    //   Java source line #2723	-> byte code offset #236
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	249	0	this	IMAPFolder
    //   0	249	1	uids	long[]
    //   221	7	2	cex	ConnectionException
    //   235	10	2	pex	ProtocolException
    //   12	149	3	unavailUids	long[]
    //   27	89	4	v	List<Long>
    //   169	41	4	msgs	Message[]
    //   30	18	5	arrayOfLong1	long[]
    //   95	15	5	vsize	int
    //   172	32	5	i	int
    //   35	10	6	i	int
    //   103	29	6	i	int
    //   38	45	7	j	int
    //   52	20	8	uid	long
    //   214	5	10	localObject1	Object
    // Exception table:
    //   from	to	target	type
    //   11	213	214	finally
    //   214	218	214	finally
    //   4	213	221	com/sun/mail/iap/ConnectionException
    //   214	221	221	com/sun/mail/iap/ConnectionException
    //   4	213	235	com/sun/mail/iap/ProtocolException
    //   214	221	235	com/sun/mail/iap/ProtocolException
  }
  
  public synchronized long getUID(Message message)
    throws MessagingException
  {
    if (message.getFolder() != this) {
      throw new NoSuchElementException("Message does not belong to this folder");
    }
    
    checkOpened();
    
    if (!(message instanceof IMAPMessage))
      throw new MessagingException("message is not an IMAPMessage");
    IMAPMessage m = (IMAPMessage)message;
    
    long uid;
    if ((uid = m.getUID()) != -1L) {
      return uid;
    }
    synchronized (messageCacheLock) {
      try {
        IMAPProtocol p = getProtocol();
        m.checkExpunged();
        UID u = p.fetchUID(m.getSequenceNumber());
        
        if (u != null) {
          uid = uid;
          m.setUID(uid);
          

          if (uidTable == null)
            uidTable = new Hashtable();
          uidTable.put(Long.valueOf(uid), m);
        }
      } catch (ConnectionException cex) {
        throw new FolderClosedException(this, cex.getMessage());
      } catch (ProtocolException pex) {
        throw new MessagingException(pex.getMessage(), pex);
      }
    }
    
    return uid;
  }
  











  public synchronized boolean getUIDNotSticky()
    throws MessagingException
  {
    checkOpened();
    return uidNotSticky;
  }
  


  private Message[] createMessagesForUIDs(long[] uids)
  {
    IMAPMessage[] msgs = new IMAPMessage[uids.length];
    for (int i = 0; i < uids.length; i++) {
      IMAPMessage m = null;
      if (uidTable != null)
        m = (IMAPMessage)uidTable.get(Long.valueOf(uids[i]));
      if (m == null)
      {
        m = newIMAPMessage(-1);
        m.setUID(uids[i]);
        m.setExpunged(true);
      }
      msgs[(i++)] = m;
    }
    return msgs;
  }
  






  public synchronized long getHighestModSeq()
    throws MessagingException
  {
    if (opened) {
      return highestmodseq;
    }
    IMAPProtocol p = null;
    Status status = null;
    try
    {
      p = getStoreProtocol();
      if (!p.hasCapability("CONDSTORE"))
        throw new BadCommandException("CONDSTORE not supported");
      String[] item = { "HIGHESTMODSEQ" };
      status = p.status(fullName, item);
    }
    catch (BadCommandException bex) {
      throw new MessagingException("Cannot obtain HIGHESTMODSEQ", bex);
    }
    catch (ConnectionException cex) {
      throwClosedException(cex);
    } catch (ProtocolException pex) {
      throw new MessagingException(pex.getMessage(), pex);
    } finally {
      releaseStoreProtocol(p);
    }
    
    if (status == null)
      throw new MessagingException("Cannot obtain HIGHESTMODSEQ");
    return highestmodseq;
  }
  
  /* Error */
  public synchronized Message[] getMessagesByUIDChangedSince(long start, long end, long modseq)
    throws MessagingException
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 216	com/sun/mail/imap/IMAPFolder:checkOpened	()V
    //   4: aload_0
    //   5: getfield 7	com/sun/mail/imap/IMAPFolder:messageCacheLock	Ljava/lang/Object;
    //   8: dup
    //   9: astore 7
    //   11: monitorenter
    //   12: aload_0
    //   13: invokevirtual 260	com/sun/mail/imap/IMAPFolder:getProtocol	()Lcom/sun/mail/imap/protocol/IMAPProtocol;
    //   16: astore 8
    //   18: aload 8
    //   20: ldc -110
    //   22: invokevirtual 149	com/sun/mail/imap/protocol/IMAPProtocol:hasCapability	(Ljava/lang/String;)Z
    //   25: ifne +14 -> 39
    //   28: new 126	com/sun/mail/iap/BadCommandException
    //   31: dup
    //   32: ldc_w 423
    //   35: invokespecial 424	com/sun/mail/iap/BadCommandException:<init>	(Ljava/lang/String;)V
    //   38: athrow
    //   39: aload 8
    //   41: lload_1
    //   42: lload_3
    //   43: lload 5
    //   45: invokevirtual 428	com/sun/mail/imap/protocol/IMAPProtocol:uidfetchChangedSince	(JJJ)[I
    //   48: astore 9
    //   50: aload_0
    //   51: aload 9
    //   53: invokevirtual 381	com/sun/mail/imap/IMAPFolder:getMessagesBySeqNumbers	([I)[Lcom/sun/mail/imap/IMAPMessage;
    //   56: aload 7
    //   58: monitorexit
    //   59: areturn
    //   60: astore 10
    //   62: aload 7
    //   64: monitorexit
    //   65: aload 10
    //   67: athrow
    //   68: astore 7
    //   70: new 68	javax/mail/FolderClosedException
    //   73: dup
    //   74: aload_0
    //   75: aload 7
    //   77: invokevirtual 76	com/sun/mail/iap/ConnectionException:getMessage	()Ljava/lang/String;
    //   80: invokespecial 70	javax/mail/FolderClosedException:<init>	(Ljavax/mail/Folder;Ljava/lang/String;)V
    //   83: athrow
    //   84: astore 7
    //   86: new 78	javax/mail/MessagingException
    //   89: dup
    //   90: aload 7
    //   92: invokevirtual 79	com/sun/mail/iap/ProtocolException:getMessage	()Ljava/lang/String;
    //   95: aload 7
    //   97: invokespecial 80	javax/mail/MessagingException:<init>	(Ljava/lang/String;Ljava/lang/Exception;)V
    //   100: athrow
    // Line number table:
    //   Java source line #2865	-> byte code offset #0
    //   Java source line #2868	-> byte code offset #4
    //   Java source line #2869	-> byte code offset #12
    //   Java source line #2870	-> byte code offset #18
    //   Java source line #2871	-> byte code offset #28
    //   Java source line #2874	-> byte code offset #39
    //   Java source line #2875	-> byte code offset #50
    //   Java source line #2876	-> byte code offset #60
    //   Java source line #2877	-> byte code offset #68
    //   Java source line #2878	-> byte code offset #70
    //   Java source line #2879	-> byte code offset #84
    //   Java source line #2880	-> byte code offset #86
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	101	0	this	IMAPFolder
    //   0	101	1	start	long
    //   0	101	3	end	long
    //   0	101	5	modseq	long
    //   68	8	7	cex	ConnectionException
    //   84	12	7	pex	ProtocolException
    //   16	24	8	p	IMAPProtocol
    //   48	4	9	nums	int[]
    //   60	6	10	localObject1	Object
    // Exception table:
    //   from	to	target	type
    //   12	59	60	finally
    //   60	65	60	finally
    //   4	59	68	com/sun/mail/iap/ConnectionException
    //   60	68	68	com/sun/mail/iap/ConnectionException
    //   4	59	84	com/sun/mail/iap/ProtocolException
    //   60	68	84	com/sun/mail/iap/ProtocolException
  }
  
  public Quota[] getQuota()
    throws MessagingException
  {
    (Quota[])doOptionalCommand("QUOTA not supported", new ProtocolCommand()
    {
      public Object doCommand(IMAPProtocol p)
        throws ProtocolException
      {
        return p.getQuotaRoot(fullName);
      }
    });
  }
  








  public void setQuota(final Quota quota)
    throws MessagingException
  {
    doOptionalCommand("QUOTA not supported", new ProtocolCommand()
    {
      public Object doCommand(IMAPProtocol p)
        throws ProtocolException
      {
        p.setQuota(quota);
        return null;
      }
    });
  }
  





  public ACL[] getACL()
    throws MessagingException
  {
    (ACL[])doOptionalCommand("ACL not supported", new ProtocolCommand()
    {
      public Object doCommand(IMAPProtocol p)
        throws ProtocolException
      {
        return p.getACL(fullName);
      }
    });
  }
  






  public void addACL(ACL acl)
    throws MessagingException
  {
    setACL(acl, '\000');
  }
  






  public void removeACL(final String name)
    throws MessagingException
  {
    doOptionalCommand("ACL not supported", new ProtocolCommand()
    {
      public Object doCommand(IMAPProtocol p)
        throws ProtocolException
      {
        p.deleteACL(fullName, name);
        return null;
      }
    });
  }
  







  public void addRights(ACL acl)
    throws MessagingException
  {
    setACL(acl, '+');
  }
  






  public void removeRights(ACL acl)
    throws MessagingException
  {
    setACL(acl, '-');
  }
  

















  public Rights[] listRights(final String name)
    throws MessagingException
  {
    (Rights[])doOptionalCommand("ACL not supported", new ProtocolCommand()
    {
      public Object doCommand(IMAPProtocol p)
        throws ProtocolException
      {
        return p.listRights(fullName, name);
      }
    });
  }
  





  public Rights myRights()
    throws MessagingException
  {
    (Rights)doOptionalCommand("ACL not supported", new ProtocolCommand()
    {
      public Object doCommand(IMAPProtocol p)
        throws ProtocolException
      {
        return p.myRights(fullName);
      }
    });
  }
  
  private void setACL(final ACL acl, final char mod) throws MessagingException
  {
    doOptionalCommand("ACL not supported", new ProtocolCommand()
    {
      public Object doCommand(IMAPProtocol p)
        throws ProtocolException
      {
        p.setACL(fullName, mod, acl);
        return null;
      }
    });
  }
  






  public synchronized String[] getAttributes()
    throws MessagingException
  {
    checkExists();
    if (attributes == null)
      exists();
    return attributes == null ? new String[0] : (String[])attributes.clone();
  }
  





















  public void idle()
    throws MessagingException
  {
    idle(false);
  }
  










  public void idle(boolean once)
    throws MessagingException
  {
    synchronized (this)
    {







      if ((protocol != null) && (protocol.getChannel() != null)) {
        throw new MessagingException("idle method not supported with SocketChannels");
      }
    }
    if (!startIdle(null)) {
      return;
    }
    













    for (;;)
    {
      if (!handleIdle(once)) {
        break;
      }
    }
    




    int minidle = ((IMAPStore)store).getMinIdleTime();
    if (minidle > 0) {
      try {
        Thread.sleep(minidle);
      }
      catch (InterruptedException ex) {
        Thread.currentThread().interrupt();
      }
    }
  }
  











  boolean startIdle(final IdleManager im)
    throws MessagingException
  {
    assert (!Thread.holdsLock(this));
    synchronized (this) {
      checkOpened();
      if ((im != null) && (idleManager != null) && (im != idleManager)) {
        throw new MessagingException("Folder already being watched by another IdleManager");
      }
      Boolean started = (Boolean)doOptionalCommand("IDLE not supported", new ProtocolCommand()
      {

        public Object doCommand(IMAPProtocol p)
          throws ProtocolException
        {

          if ((idleState == 1) && (im != null) && 
            (im == idleManager))
            return Boolean.TRUE;
          if (idleState == 0) {
            p.idleStart();
            logger.finest("startIdle: set to IDLE");
            idleState = 1;
            idleManager = im;
            return Boolean.TRUE;
          }
          


          try
          {
            messageCacheLock.wait();
          }
          catch (InterruptedException ex)
          {
            Thread.currentThread().interrupt();
          }
          return Boolean.FALSE;
        }
        
      });
      logger.log(Level.FINEST, "startIdle: return {0}", started);
      return started.booleanValue();
    }
  }
  










  boolean handleIdle(boolean once)
    throws MessagingException
  {
    Response r = null;
    do {
      r = protocol.readIdleResponse();
      try {
        synchronized (messageCacheLock) { IdleManager im;
          if ((r.isBYE()) && (r.isSynthetic()) && (idleState == 1))
          {






            Exception ex = r.getException();
            if (((ex instanceof InterruptedIOException)) && (bytesTransferred == 0))
            {

              if ((ex instanceof SocketTimeoutException)) {
                logger.finest("handleIdle: ignoring socket timeout");
                
                r = null;
              } else {
                logger.finest("handleIdle: interrupting IDLE");
                im = idleManager;
                if (im != null) {
                  logger.finest("handleIdle: request IdleManager to abort");
                  
                  im.requestAbort(this);
                } else {
                  logger.finest("handleIdle: abort IDLE");
                  protocol.idleAbort();
                  idleState = 2;
                }
              }
              
              continue;
            }
          }
          boolean done = true;
          try {
            if ((protocol == null) || 
              (!protocol.processIdleResponse(r))) {
              im = 0;
              

              if (done) {
                logger.finest("handleIdle: set to RUNNING");
                idleState = 0;
                idleManager = null;
                messageCacheLock.notifyAll();
              }
              return im; }
            done = false;
          } finally {
            if (done) {
              logger.finest("handleIdle: set to RUNNING");
              idleState = 0;
              idleManager = null;
              messageCacheLock.notifyAll();
            }
          }
          if ((once) && 
            (idleState == 1)) {
            try {
              protocol.idleAbort();
            }
            catch (Exception localException1) {}
            


            idleState = 2;
          }
        }
      }
      catch (ConnectionException cex)
      {
        throw new FolderClosedException(this, cex.getMessage());
      } catch (ProtocolException pex) {
        throw new MessagingException(pex.getMessage(), pex);
      }
      
    } while ((r == null) || (protocol.hasResponse()));
    return true;
  }
  



  void waitIfIdle()
    throws ProtocolException
  {
    assert (Thread.holdsLock(messageCacheLock));
    while (idleState != 0) {
      if (idleState == 1) {
        IdleManager im = idleManager;
        if (im != null) {
          logger.finest("waitIfIdle: request IdleManager to abort");
          im.requestAbort(this);
        } else {
          logger.finest("waitIfIdle: abort IDLE");
          protocol.idleAbort();
          idleState = 2;
        }
      } else {
        logger.log(Level.FINEST, "waitIfIdle: idleState {0}", Integer.valueOf(idleState));
      }
      try {
        if (logger.isLoggable(Level.FINEST))
          logger.finest("waitIfIdle: wait to be not idle: " + 
            Thread.currentThread());
        messageCacheLock.wait();
        if (logger.isLoggable(Level.FINEST)) {
          logger.finest("waitIfIdle: wait done, idleState " + idleState + ": " + 
            Thread.currentThread());
        }
      } catch (InterruptedException ex) {
        Thread.currentThread().interrupt();
        




        throw new ProtocolException("Interrupted waitIfIdle", ex);
      }
    }
  }
  


  void idleAbort()
  {
    synchronized (messageCacheLock) {
      if ((idleState == 1) && (protocol != null)) {
        protocol.idleAbort();
        idleState = 2;
      }
    }
  }
  



  void idleAbortWait()
  {
    synchronized (messageCacheLock) {
      if ((idleState == 1) && (protocol != null)) {
        protocol.idleAbort();
        idleState = 2;
        try
        {
          for (;;)
          {
            if (!handleIdle(false)) {
              break;
            }
          }
        } catch (Exception ex) {
          logger.log(Level.FINEST, "Exception in idleAbortWait", ex);
        }
        logger.finest("IDLE aborted");
      }
    }
  }
  



  SocketChannel getChannel()
  {
    return protocol != null ? protocol.getChannel() : null;
  }
  













  public Map<String, String> id(final Map<String, String> clientParams)
    throws MessagingException
  {
    checkOpened();
    (Map)doOptionalCommand("ID not supported", new ProtocolCommand()
    {
      public Object doCommand(IMAPProtocol p)
        throws ProtocolException
      {
        return p.id(clientParams);
      }
    });
  }
  











  public synchronized long getStatusItem(String item)
    throws MessagingException
  {
    if (!opened) {
      checkExists();
      
      IMAPProtocol p = null;
      Status status = null;
      try {
        p = getStoreProtocol();
        String[] items = { item };
        status = p.status(fullName, items);
        return status != null ? status.getItem(item) : -1L;
      }
      catch (BadCommandException bex)
      {
        long l;
        return -1L;
      } catch (ConnectionException cex) {
        throw new StoreClosedException(store, cex.getMessage());
      } catch (ProtocolException pex) {
        throw new MessagingException(pex.getMessage(), pex);
      } finally {
        releaseStoreProtocol(p);
      }
    }
    return -1L;
  }
  












  public void handleResponse(Response r)
  {
    assert (Thread.holdsLock(messageCacheLock));
    



    if ((r.isOK()) || (r.isNO()) || (r.isBAD()) || (r.isBYE())) {
      ((IMAPStore)store).handleResponseCode(r);
    }
    



    if (r.isBYE()) {
      if (opened)
        cleanup(false);
      return; }
    if (r.isOK())
    {
      r.skipSpaces();
      if (r.readByte() == 91) {
        String s = r.readAtom();
        if (s.equalsIgnoreCase("HIGHESTMODSEQ"))
          highestmodseq = r.readLong();
      }
      r.reset();
      return; }
    if (!r.isUnTagged()) {
      return;
    }
    

    if (!(r instanceof IMAPResponse))
    {

      logger.fine("UNEXPECTED RESPONSE : " + r.toString());
      return;
    }
    
    IMAPResponse ir = (IMAPResponse)r;
    
    if (ir.keyEquals("EXISTS")) {
      int exists = ir.getNumber();
      if (exists <= realTotal)
      {
        return;
      }
      int count = exists - realTotal;
      Message[] msgs = new Message[count];
      

      messageCache.addMessages(count, realTotal + 1);
      int oldtotal = total;
      realTotal += count;
      total += count;
      

      if (hasMessageCountListener) {
        for (int i = 0; i < count; i++) {
          msgs[i] = messageCache.getMessage(++oldtotal);
        }
        
        notifyMessageAddedListeners(msgs);
      }
    }
    else if (ir.keyEquals("EXPUNGE"))
    {

      int seqnum = ir.getNumber();
      Message[] msgs = null;
      if ((doExpungeNotification) && (hasMessageCountListener))
      {

        msgs = new Message[] { getMessageBySeqNumber(seqnum) };
        if (msgs[0] == null) {
          msgs = null;
        }
      }
      messageCache.expungeMessage(seqnum);
      

      realTotal -= 1;
      
      if (msgs != null) {
        notifyMessageRemovedListeners(false, msgs);
      }
    } else if (ir.keyEquals("VANISHED"))
    {




      String[] s = ir.readAtomStringList();
      if (s == null) {
        String uids = ir.readAtom();
        UIDSet[] uidset = UIDSet.parseUIDSets(uids);
        
        realTotal = ((int)(realTotal - UIDSet.size(uidset)));
        long[] luid = UIDSet.toArray(uidset);
        Message[] msgs = createMessagesForUIDs(luid);
        for (Message m : msgs) {
          if (m.getMessageNumber() > 0)
            messageCache.expungeMessage(m.getMessageNumber());
        }
        if ((doExpungeNotification) && (hasMessageCountListener)) {
          notifyMessageRemovedListeners(true, msgs);
        }
      }
    }
    else if (ir.keyEquals("FETCH")) {
      assert ((ir instanceof FetchResponse)) : "!ir instanceof FetchResponse";
      Message msg = processFetchResponse((FetchResponse)ir);
      if (msg != null) {
        notifyMessageChangedListeners(1, msg);
      }
    }
    else if (ir.keyEquals("RECENT"))
    {
      recent = ir.getNumber();
    }
  }
  





  private Message processFetchResponse(FetchResponse fr)
  {
    IMAPMessage msg = getMessageBySeqNumber(fr.getNumber());
    if (msg != null) {
      boolean notify = false;
      
      UID uid = (UID)fr.getItem(UID.class);
      if ((uid != null) && (msg.getUID() != uid)) {
        msg.setUID(uid);
        if (uidTable == null)
          uidTable = new Hashtable();
        uidTable.put(Long.valueOf(uid), msg);
        notify = true;
      }
      
      MODSEQ modseq = (MODSEQ)fr.getItem(MODSEQ.class);
      if ((modseq != null) && (msg._getModSeq() != modseq)) {
        msg.setModSeq(modseq);
        





        notify = true;
      }
      

      FLAGS flags = (FLAGS)fr.getItem(FLAGS.class);
      if (flags != null) {
        msg._setFlags(flags);
        notify = true;
      }
      


      msg.handleExtensionFetchItems(fr.getExtensionItems());
      
      if (!notify)
        msg = null;
    }
    return msg;
  }
  





  void handleResponses(Response[] r)
  {
    for (int i = 0; i < r.length; i++) {
      if (r[i] != null) {
        handleResponse(r[i]);
      }
    }
  }
  





















  protected synchronized IMAPProtocol getStoreProtocol()
    throws ProtocolException
  {
    connectionPoolLogger.fine("getStoreProtocol() borrowing a connection");
    return ((IMAPStore)store).getFolderStoreProtocol();
  }
  













  protected synchronized void throwClosedException(ConnectionException cex)
    throws FolderClosedException, StoreClosedException
  {
    if (((protocol != null) && (cex.getProtocol() == protocol)) || ((protocol == null) && (!reallyClosed)))
    {
      throw new FolderClosedException(this, cex.getMessage());
    }
    throw new StoreClosedException(store, cex.getMessage());
  }
  







  protected IMAPProtocol getProtocol()
    throws ProtocolException
  {
    assert (Thread.holdsLock(messageCacheLock));
    waitIfIdle();
    


    if (protocol == null)
      throw new ConnectionException("Connection closed");
    return protocol;
  }
  







































































































  public Object doCommand(ProtocolCommand cmd)
    throws MessagingException
  {
    try
    {
      return doProtocolCommand(cmd);
    }
    catch (ConnectionException cex) {
      throwClosedException(cex);
    } catch (ProtocolException pex) {
      throw new MessagingException(pex.getMessage(), pex);
    }
    return null;
  }
  
  public Object doOptionalCommand(String err, ProtocolCommand cmd) throws MessagingException
  {
    try {
      return doProtocolCommand(cmd);
    } catch (BadCommandException bex) {
      throw new MessagingException(err, bex);
    }
    catch (ConnectionException cex) {
      throwClosedException(cex);
    } catch (ProtocolException pex) {
      throw new MessagingException(pex.getMessage(), pex);
    }
    return null;
  }
  
  public Object doCommandIgnoreFailure(ProtocolCommand cmd) throws MessagingException
  {
    try {
      return doProtocolCommand(cmd);
    } catch (CommandFailedException cfx) {
      return null;
    }
    catch (ConnectionException cex) {
      throwClosedException(cex);
    } catch (ProtocolException pex) {
      throw new MessagingException(pex.getMessage(), pex);
    }
    return null;
  }
  




  protected synchronized Object doProtocolCommand(ProtocolCommand cmd)
    throws ProtocolException
  {
    if (protocol != null) {
      synchronized (messageCacheLock) {
        return cmd.doCommand(getProtocol());
      }
    }
    

    IMAPProtocol p = null;
    try
    {
      p = getStoreProtocol();
      return cmd.doCommand(p);
    } finally {
      releaseStoreProtocol(p);
    }
  }
  








  protected synchronized void releaseStoreProtocol(IMAPProtocol p)
  {
    if (p != protocol) {
      ((IMAPStore)store).releaseFolderStoreProtocol(p);
    }
    else {
      logger.fine("releasing our protocol as store protocol?");
    }
  }
  







  protected void releaseProtocol(boolean returnToPool)
  {
    if (protocol != null) {
      protocol.removeResponseHandler(this);
      
      if (returnToPool) {
        ((IMAPStore)store).releaseProtocol(this, protocol);
      } else {
        protocol.disconnect();
        ((IMAPStore)store).releaseProtocol(this, null);
      }
      protocol = null;
    }
  }
  











  protected void keepConnectionAlive(boolean keepStoreAlive)
    throws ProtocolException
  {
    assert (Thread.holdsLock(messageCacheLock));
    if (protocol == null)
      return;
    if (System.currentTimeMillis() - protocol.getTimestamp() > 1000L) {
      waitIfIdle();
      if (protocol != null) {
        protocol.noop();
      }
    }
    if ((keepStoreAlive) && (((IMAPStore)store).hasSeparateStoreConnection())) {
      IMAPProtocol p = null;
      try {
        p = ((IMAPStore)store).getFolderStoreProtocol();
        if (System.currentTimeMillis() - p.getTimestamp() > 1000L) {
          p.noop();
        }
        ((IMAPStore)store).releaseFolderStoreProtocol(p); } finally { ((IMAPStore)store).releaseFolderStoreProtocol(p);
      }
    }
  }
  









  protected IMAPMessage getMessageBySeqNumber(int seqnum)
  {
    if (seqnum > messageCache.size())
    {



      if (logger.isLoggable(Level.FINE))
        logger.fine("ignoring message number " + seqnum + " outside range " + messageCache
          .size());
      return null;
    }
    return messageCache.getMessageBySeqnum(seqnum);
  }
  









  protected IMAPMessage[] getMessagesBySeqNumbers(int[] seqnums)
  {
    IMAPMessage[] msgs = new IMAPMessage[seqnums.length];
    int nulls = 0;
    
    for (int i = 0; i < seqnums.length; i++) {
      msgs[i] = getMessageBySeqNumber(seqnums[i]);
      if (msgs[i] == null)
        nulls++;
    }
    if (nulls > 0) {
      IMAPMessage[] nmsgs = new IMAPMessage[seqnums.length - nulls];
      int i = 0; for (int j = 0; i < msgs.length; i++) {
        if (msgs[i] != null)
          nmsgs[(j++)] = msgs[i];
      }
      msgs = nmsgs;
    }
    return msgs;
  }
  
  private boolean isDirectory() {
    return (type & 0x2) != 0;
  }
}
