package com.sun.mail.imap;

import com.sun.mail.iap.BadCommandException;
import com.sun.mail.iap.CommandFailedException;
import com.sun.mail.iap.ConnectionException;
import com.sun.mail.iap.ProtocolException;
import com.sun.mail.iap.Response;
import com.sun.mail.iap.ResponseHandler;
import com.sun.mail.imap.protocol.IMAPProtocol;
import com.sun.mail.imap.protocol.IMAPReferralException;
import com.sun.mail.imap.protocol.ListInfo;
import com.sun.mail.imap.protocol.Namespaces;
import com.sun.mail.imap.protocol.Namespaces.Namespace;
import com.sun.mail.util.MailConnectException;
import com.sun.mail.util.MailLogger;
import com.sun.mail.util.PropUtil;
import com.sun.mail.util.SocketConnectException;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.logging.Level;
import javax.mail.AuthenticationFailedException;
import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Quota;
import javax.mail.QuotaAwareStore;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.StoreClosedException;
import javax.mail.URLName;









































































































































public class IMAPStore
  extends Store
  implements QuotaAwareStore, ResponseHandler
{
  public static final int RESPONSE = 1000;
  public static final String ID_NAME = "name";
  public static final String ID_VERSION = "version";
  public static final String ID_OS = "os";
  public static final String ID_OS_VERSION = "os-version";
  public static final String ID_VENDOR = "vendor";
  public static final String ID_SUPPORT_URL = "support-url";
  public static final String ID_ADDRESS = "address";
  public static final String ID_DATE = "date";
  public static final String ID_COMMAND = "command";
  public static final String ID_ARGUMENTS = "arguments";
  public static final String ID_ENVIRONMENT = "environment";
  protected final String name;
  protected final int defaultPort;
  protected final boolean isSSL;
  private final int blksize;
  private boolean ignoreSize;
  private final int statusCacheTimeout;
  private final int appendBufferSize;
  private final int minIdleTime;
  private volatile int port = -1;
  
  protected String host;
  
  protected String user;
  
  protected String password;
  
  protected String proxyAuthUser;
  protected String authorizationID;
  protected String saslRealm;
  private Namespaces namespaces;
  private boolean enableStartTLS = false;
  private boolean requireStartTLS = false;
  private boolean usingSSL = false;
  private boolean enableSASL = false;
  private String[] saslMechanisms;
  private boolean forcePasswordRefresh = false;
  
  private boolean enableResponseEvents = false;
  
  private boolean enableImapEvents = false;
  private String guid;
  private boolean throwSearchException = false;
  private boolean peek = false;
  private boolean closeFoldersOnStoreFailure = true;
  private boolean enableCompress = false;
  private boolean finalizeCleanClose = false;
  









  private volatile boolean connectionFailed = false;
  private volatile boolean forceClose = false;
  private final Object connectionFailedLock = new Object();
  
  private boolean debugusername;
  
  private boolean debugpassword;
  
  protected MailLogger logger;
  
  private boolean messageCacheDebug;
  private volatile Constructor<?> folderConstructor = null;
  private volatile Constructor<?> folderConstructorLI = null;
  
  private final ConnectionPool pool;
  

  static class ConnectionPool
  {
    private Vector<IMAPProtocol> authenticatedConnections = new Vector();
    


    private Vector<IMAPFolder> folders;
    

    private boolean storeConnectionInUse = false;
    





    private long lastTimePruned;
    





    private final boolean separateStoreConnection;
    





    private final long clientTimeoutInterval;
    




    private final long serverTimeoutInterval;
    




    private final int poolSize;
    




    private final long pruningInterval;
    




    private final MailLogger logger;
    




    private static final int RUNNING = 0;
    




    private static final int IDLE = 1;
    




    private static final int ABORTING = 2;
    




    private int idleState = 0;
    private IMAPProtocol idleProtocol;
    
    ConnectionPool(String name, MailLogger plogger, Session session) {
      lastTimePruned = System.currentTimeMillis();
      
      boolean debug = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".connectionpool.debug", false);
      
      logger = plogger.getSubLogger("connectionpool", "DEBUG IMAP CP", debug);
      


      int size = PropUtil.getIntSessionProperty(session, "mail." + name + ".connectionpoolsize", -1);
      
      if (size > 0) {
        poolSize = size;
        if (logger.isLoggable(Level.CONFIG))
          logger.config("mail.imap.connectionpoolsize: " + poolSize);
      } else {
        poolSize = 1;
      }
      
      int connectionPoolTimeout = PropUtil.getIntSessionProperty(session, "mail." + name + ".connectionpooltimeout", -1);
      
      if (connectionPoolTimeout > 0) {
        clientTimeoutInterval = connectionPoolTimeout;
        if (logger.isLoggable(Level.CONFIG)) {
          logger.config("mail.imap.connectionpooltimeout: " + clientTimeoutInterval);
        }
      } else {
        clientTimeoutInterval = 45000L;
      }
      
      int serverTimeout = PropUtil.getIntSessionProperty(session, "mail." + name + ".servertimeout", -1);
      
      if (serverTimeout > 0) {
        serverTimeoutInterval = serverTimeout;
        if (logger.isLoggable(Level.CONFIG)) {
          logger.config("mail.imap.servertimeout: " + serverTimeoutInterval);
        }
      } else {
        serverTimeoutInterval = 1800000L;
      }
      
      int pruning = PropUtil.getIntSessionProperty(session, "mail." + name + ".pruninginterval", -1);
      
      if (pruning > 0) {
        pruningInterval = pruning;
        if (logger.isLoggable(Level.CONFIG)) {
          logger.config("mail.imap.pruninginterval: " + pruningInterval);
        }
      } else {
        pruningInterval = 60000L;
      }
      


      separateStoreConnection = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".separatestoreconnection", false);
      
      if (separateStoreConnection) {
        logger.config("dedicate a store connection");
      }
    }
  }
  









  private ResponseHandler nonStoreResponseHandler = new ResponseHandler()
  {
    public void handleResponse(Response r)
    {
      if ((r.isOK()) || (r.isNO()) || (r.isBAD()) || (r.isBYE()))
        handleResponseCode(r);
      if (r.isBYE()) {
        logger.fine("IMAPStore non-store connection dead");
      }
    }
  };
  





  public IMAPStore(Session session, URLName url)
  {
    this(session, url, "imap", false);
  }
  








  protected IMAPStore(Session session, URLName url, String name, boolean isSSL)
  {
    super(session, url);
    if (url != null)
      name = url.getProtocol();
    this.name = name;
    if (!isSSL) {
      isSSL = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".ssl.enable", false);
    }
    if (isSSL) {
      defaultPort = 993;
    } else
      defaultPort = 143;
    this.isSSL = isSSL;
    
    debug = session.getDebug();
    debugusername = PropUtil.getBooleanSessionProperty(session, "mail.debug.auth.username", true);
    
    debugpassword = PropUtil.getBooleanSessionProperty(session, "mail.debug.auth.password", false);
    

    logger = new MailLogger(getClass(), "DEBUG " + name.toUpperCase(Locale.ENGLISH), session);
    
    boolean partialFetch = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".partialfetch", true);
    
    if (!partialFetch) {
      blksize = -1;
      logger.config("mail.imap.partialfetch: false");
    } else {
      blksize = PropUtil.getIntSessionProperty(session, "mail." + name + ".fetchsize", 16384);
      
      if (logger.isLoggable(Level.CONFIG)) {
        logger.config("mail.imap.fetchsize: " + blksize);
      }
    }
    ignoreSize = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".ignorebodystructuresize", false);
    
    if (logger.isLoggable(Level.CONFIG)) {
      logger.config("mail.imap.ignorebodystructuresize: " + ignoreSize);
    }
    statusCacheTimeout = PropUtil.getIntSessionProperty(session, "mail." + name + ".statuscachetimeout", 1000);
    
    if (logger.isLoggable(Level.CONFIG)) {
      logger.config("mail.imap.statuscachetimeout: " + statusCacheTimeout);
    }
    
    appendBufferSize = PropUtil.getIntSessionProperty(session, "mail." + name + ".appendbuffersize", -1);
    
    if (logger.isLoggable(Level.CONFIG)) {
      logger.config("mail.imap.appendbuffersize: " + appendBufferSize);
    }
    minIdleTime = PropUtil.getIntSessionProperty(session, "mail." + name + ".minidletime", 10);
    
    if (logger.isLoggable(Level.CONFIG)) {
      logger.config("mail.imap.minidletime: " + minIdleTime);
    }
    
    String s = session.getProperty("mail." + name + ".proxyauth.user");
    if (s != null) {
      proxyAuthUser = s;
      if (logger.isLoggable(Level.CONFIG)) {
        logger.config("mail.imap.proxyauth.user: " + proxyAuthUser);
      }
    }
    
    enableStartTLS = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".starttls.enable", false);
    
    if (enableStartTLS) {
      logger.config("enable STARTTLS");
    }
    
    requireStartTLS = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".starttls.required", false);
    
    if (requireStartTLS) {
      logger.config("require STARTTLS");
    }
    
    enableSASL = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".sasl.enable", false);
    
    if (enableSASL) {
      logger.config("enable SASL");
    }
    
    if (enableSASL) {
      s = session.getProperty("mail." + name + ".sasl.mechanisms");
      if ((s != null) && (s.length() > 0)) {
        if (logger.isLoggable(Level.CONFIG))
          logger.config("SASL mechanisms allowed: " + s);
        List<String> v = new ArrayList(5);
        StringTokenizer st = new StringTokenizer(s, " ,");
        while (st.hasMoreTokens()) {
          String m = st.nextToken();
          if (m.length() > 0)
            v.add(m);
        }
        saslMechanisms = new String[v.size()];
        v.toArray(saslMechanisms);
      }
    }
    

    s = session.getProperty("mail." + name + ".sasl.authorizationid");
    if (s != null) {
      authorizationID = s;
      logger.log(Level.CONFIG, "mail.imap.sasl.authorizationid: {0}", authorizationID);
    }
    


    s = session.getProperty("mail." + name + ".sasl.realm");
    if (s != null) {
      saslRealm = s;
      logger.log(Level.CONFIG, "mail.imap.sasl.realm: {0}", saslRealm);
    }
    

    forcePasswordRefresh = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".forcepasswordrefresh", false);
    
    if (forcePasswordRefresh) {
      logger.config("enable forcePasswordRefresh");
    }
    
    enableResponseEvents = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".enableresponseevents", false);
    
    if (enableResponseEvents) {
      logger.config("enable IMAP response events");
    }
    
    enableImapEvents = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".enableimapevents", false);
    
    if (enableImapEvents) {
      logger.config("enable IMAP IDLE events");
    }
    
    messageCacheDebug = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".messagecache.debug", false);
    

    guid = session.getProperty("mail." + name + ".yahoo.guid");
    if (guid != null) {
      logger.log(Level.CONFIG, "mail.imap.yahoo.guid: {0}", guid);
    }
    
    throwSearchException = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".throwsearchexception", false);
    
    if (throwSearchException) {
      logger.config("throw SearchException");
    }
    
    peek = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".peek", false);
    
    if (peek) {
      logger.config("peek");
    }
    
    closeFoldersOnStoreFailure = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".closefoldersonstorefailure", true);
    
    if (closeFoldersOnStoreFailure) {
      logger.config("closeFoldersOnStoreFailure");
    }
    
    enableCompress = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".compress.enable", false);
    
    if (enableCompress) {
      logger.config("enable COMPRESS");
    }
    
    finalizeCleanClose = PropUtil.getBooleanSessionProperty(session, "mail." + name + ".finalizecleanclose", false);
    
    if (finalizeCleanClose) {
      logger.config("close connection cleanly in finalize");
    }
    s = session.getProperty("mail." + name + ".folder.class");
    if (s != null) {
      logger.log(Level.CONFIG, "IMAP: folder class: {0}", s);
      try {
        ClassLoader cl = getClass().getClassLoader();
        

        Class<?> folderClass = null;
        

        try
        {
          folderClass = Class.forName(s, false, cl);

        }
        catch (ClassNotFoundException ex1)
        {
          folderClass = Class.forName(s);
        }
        
        Class<?>[] c = { String.class, Character.TYPE, IMAPStore.class, Boolean.class };
        
        folderConstructor = folderClass.getConstructor(c);
        Class<?>[] c2 = { ListInfo.class, IMAPStore.class };
        folderConstructorLI = folderClass.getConstructor(c2);
      } catch (Exception ex) {
        logger.log(Level.CONFIG, "IMAP: failed to load folder class", ex);
      }
    }
    

    pool = new ConnectionPool(name, logger, session);
  }
  












  protected synchronized boolean protocolConnect(String host, int pport, String user, String password)
    throws MessagingException
  {
    IMAPProtocol protocol = null;
    

    if ((host == null) || (password == null) || (user == null)) {
      if (logger.isLoggable(Level.FINE))
        logger.fine("protocolConnect returning false, host=" + host + ", user=" + 
        
          traceUser(user) + ", password=" + 
          tracePassword(password));
      return false;
    }
    

    if (pport != -1) {
      port = pport;
    } else {
      port = PropUtil.getIntSessionProperty(session, "mail." + name + ".port", port);
    }
    


    if (port == -1) {
      port = defaultPort;
    }
    try
    {
      boolean poolEmpty;
      synchronized (pool) {
        poolEmpty = pool.authenticatedConnections.isEmpty();
      }
      boolean poolEmpty;
      if (poolEmpty) {
        if (logger.isLoggable(Level.FINE)) {
          logger.fine("trying to connect to host \"" + host + "\", port " + port + ", isSSL " + isSSL);
        }
        protocol = newIMAPProtocol(host, port);
        if (logger.isLoggable(Level.FINE))
          logger.fine("protocolConnect login, host=" + host + ", user=" + 
          
            traceUser(user) + ", password=" + 
            tracePassword(password));
        protocol.addResponseHandler(nonStoreResponseHandler);
        login(protocol, user, password);
        protocol.removeResponseHandler(nonStoreResponseHandler);
        protocol.addResponseHandler(this);
        
        usingSSL = protocol.isSSL();
        
        this.host = host;
        this.user = user;
        this.password = password;
        
        synchronized (pool) {
          pool.authenticatedConnections.addElement(protocol);
        }
      }
    }
    catch (IMAPReferralException ex) {
      if (protocol != null)
        protocol.disconnect();
      protocol = null;
      throw new ReferralException(ex.getUrl(), ex.getMessage());
    }
    catch (CommandFailedException cex) {
      if (protocol != null)
        protocol.disconnect();
      protocol = null;
      Response r = cex.getResponse();
      
      throw new AuthenticationFailedException(r != null ? r.getRest() : cex.getMessage());
    }
    catch (ProtocolException pex) {
      if (protocol != null)
        protocol.disconnect();
      protocol = null;
      throw new MessagingException(pex.getMessage(), pex);
    } catch (SocketConnectException scex) {
      throw new MailConnectException(scex);
    } catch (IOException ioex) {
      throw new MessagingException(ioex.getMessage(), ioex);
    }
    
    return true;
  }
  











  protected IMAPProtocol newIMAPProtocol(String host, int port)
    throws IOException, ProtocolException
  {
    return new IMAPProtocol(name, host, port, session
      .getProperties(), isSSL, logger);
  }
  




  private void login(IMAPProtocol p, String u, String pw)
    throws ProtocolException
  {
    if (((enableStartTLS) || (requireStartTLS)) && (!p.isSSL())) {
      if (p.hasCapability("STARTTLS")) {
        p.startTLS();
        
        p.capability();
      } else if (requireStartTLS) {
        logger.fine("STARTTLS required but not supported by server");
        throw new ProtocolException("STARTTLS required but not supported by server");
      }
    }
    
    if (p.isAuthenticated()) {
      return;
    }
    
    preLogin(p);
    


    if (guid != null) {
      Map<String, String> gmap = new HashMap();
      gmap.put("GUID", guid);
      p.id(gmap);
    }
    





    p.getCapabilities().put("__PRELOGIN__", "");
    String authzid;
    String authzid; if (authorizationID != null) {
      authzid = authorizationID; } else { String authzid;
      if (proxyAuthUser != null) {
        authzid = proxyAuthUser;
      } else
        authzid = null;
    }
    if (enableSASL) {
      try {
        p.sasllogin(saslMechanisms, saslRealm, authzid, u, pw);
        if (!p.isAuthenticated()) {
          throw new CommandFailedException("SASL authentication failed");
        }
      }
      catch (UnsupportedOperationException localUnsupportedOperationException) {}
    }
    

    if (!p.isAuthenticated()) {
      authenticate(p, authzid, u, pw);
    }
    if (proxyAuthUser != null) {
      p.proxyauth(proxyAuthUser);
    }
    



    if (p.hasCapability("__PRELOGIN__")) {
      try {
        p.capability();
      } catch (ConnectionException cex) {
        throw cex;
      }
      catch (ProtocolException localProtocolException) {}
    }
    


    if ((enableCompress) && 
      (p.hasCapability("COMPRESS=DEFLATE"))) {
      p.compress();
    }
    



    if ((p.hasCapability("UTF8=ACCEPT")) || (p.hasCapability("UTF8=ONLY"))) {
      p.enable("UTF8=ACCEPT");
    }
  }
  









  private void authenticate(IMAPProtocol p, String authzid, String user, String password)
    throws ProtocolException
  {
    String defaultAuthenticationMechanisms = "PLAIN LOGIN NTLM XOAUTH2";
    



    String mechs = session.getProperty("mail." + name + ".auth.mechanisms");
    
    if (mechs == null) {
      mechs = defaultAuthenticationMechanisms;
    }
    





    StringTokenizer st = new StringTokenizer(mechs);
    while (st.hasMoreTokens()) {
      String m = st.nextToken();
      m = m.toUpperCase(Locale.ENGLISH);
      



      if (mechs == defaultAuthenticationMechanisms)
      {
        String dprop = "mail." + name + ".auth." + m.toLowerCase(Locale.ENGLISH) + ".disable";
        boolean disabled = PropUtil.getBooleanSessionProperty(session, dprop, m
          .equals("XOAUTH2"));
        if (disabled) {
          if (!logger.isLoggable(Level.FINE)) continue;
          logger.fine("mechanism " + m + " disabled by property: " + dprop); continue;
        }
      }
      


      if ((!p.hasCapability("AUTH=" + m)) && (
        (!m.equals("LOGIN")) || (!p.hasCapability("AUTH-LOGIN")))) {
        logger.log(Level.FINE, "mechanism {0} not supported by server", m);

      }
      else
      {
        if (m.equals("PLAIN")) {
          p.authplain(authzid, user, password);
        } else if (m.equals("LOGIN")) {
          p.authlogin(user, password);
        } else if (m.equals("NTLM")) {
          p.authntlm(authzid, user, password);
        } else if (m.equals("XOAUTH2")) {
          p.authoauth2(user, password);
        } else {
          logger.log(Level.FINE, "no authenticator for mechanism {0}", m);
          continue;
        }
        return;
      }
    }
    if (!p.hasCapability("LOGINDISABLED")) {
      p.login(user, password);
      return;
    }
    
    throw new ProtocolException("No login methods supported!");
  }
  









  protected void preLogin(IMAPProtocol p)
    throws ProtocolException
  {}
  









  public synchronized boolean isSSL()
  {
    return usingSSL;
  }
  















  public synchronized void setUsername(String user)
  {
    this.user = user;
  }
  










  public synchronized void setPassword(String password)
  {
    this.password = password;
  }
  




  IMAPProtocol getProtocol(IMAPFolder folder)
    throws MessagingException
  {
    IMAPProtocol p = null;
    

    while (p == null)
    {






      synchronized (pool)
      {


        if ((pool.authenticatedConnections.isEmpty()) || (
          (pool.authenticatedConnections.size() == 1) && (
          (pool.separateStoreConnection) || (pool.storeConnectionInUse))))
        {
          logger.fine("no connections in the pool, creating a new one");
          try {
            if (forcePasswordRefresh) {
              refreshPassword();
            }
            p = newIMAPProtocol(host, port);
            p.addResponseHandler(nonStoreResponseHandler);
            
            login(p, user, password);
            p.removeResponseHandler(nonStoreResponseHandler);
          } catch (Exception ex1) {
            if (p != null)
              try {
                p.disconnect();
              } catch (Exception localException1) {}
            p = null;
          }
          
          if (p == null)
            throw new MessagingException("connection failure");
        } else {
          if (logger.isLoggable(Level.FINE)) {
            logger.fine("connection available -- size: " + 
              pool.authenticatedConnections.size());
          }
          
          p = (IMAPProtocol)pool.authenticatedConnections.lastElement();
          pool.authenticatedConnections.removeElement(p);
          

          long lastUsed = System.currentTimeMillis() - p.getTimestamp();
          if (lastUsed > pool.serverTimeoutInterval)
          {


            try
            {

              p.removeResponseHandler(this);
              p.addResponseHandler(nonStoreResponseHandler);
              p.noop();
              p.removeResponseHandler(nonStoreResponseHandler);
              p.addResponseHandler(this);
            } catch (ProtocolException pex) {
              try {
                p.removeResponseHandler(nonStoreResponseHandler);
                p.disconnect();
              }
              catch (RuntimeException localRuntimeException) {}
              
              p = null; }
            continue;
          }
          


          if ((proxyAuthUser != null) && 
            (!proxyAuthUser.equals(p.getProxyAuthUser())) && 
            (p.hasCapability("X-UNAUTHENTICATE")))
          {


            try
            {

              p.removeResponseHandler(this);
              p.addResponseHandler(nonStoreResponseHandler);
              p.unauthenticate();
              login(p, user, password);
              p.removeResponseHandler(nonStoreResponseHandler);
              p.addResponseHandler(this);
            } catch (ProtocolException pex) {
              try {
                p.removeResponseHandler(nonStoreResponseHandler);
                p.disconnect();
              }
              catch (RuntimeException localRuntimeException1) {}
              
              p = null; }
            continue;
          }
          


          p.removeResponseHandler(this);
        }
        

        timeoutConnections();
        

        if (folder != null) {
          if (pool.folders == null)
            pool.folders = new Vector();
          pool.folders.addElement(folder);
        }
      }
    }
    

    return p;
  }
  
















  private IMAPProtocol getStoreProtocol()
    throws ProtocolException
  {
    IMAPProtocol p = null;
    
    while (p == null) {
      synchronized (pool) {
        waitIfIdle();
        


        if (pool.authenticatedConnections.isEmpty()) {
          pool.logger.fine("getStoreProtocol() - no connections in the pool, creating a new one");
          try
          {
            if (forcePasswordRefresh) {
              refreshPassword();
            }
            p = newIMAPProtocol(host, port);
            
            login(p, user, password);
          } catch (Exception ex1) {
            if (p != null)
              try {
                p.logout();
              } catch (Exception localException1) {}
            p = null;
          }
          
          if (p == null) {
            throw new ConnectionException("failed to create new store connection");
          }
          
          p.addResponseHandler(this);
          pool.authenticatedConnections.addElement(p);
        }
        else
        {
          if (pool.logger.isLoggable(Level.FINE))
            pool.logger.fine("getStoreProtocol() - connection available -- size: " + 
            
              pool.authenticatedConnections.size());
          p = (IMAPProtocol)pool.authenticatedConnections.firstElement();
          

          if ((proxyAuthUser != null) && 
            (!proxyAuthUser.equals(p.getProxyAuthUser())) && 
            (p.hasCapability("X-UNAUTHENTICATE"))) {
            p.unauthenticate();
            login(p, user, password);
          }
        }
        
        if (pool.storeConnectionInUse)
        {
          try
          {
            p = null;
            pool.wait();
          }
          catch (InterruptedException ex)
          {
            Thread.currentThread().interrupt();
            

            throw new ProtocolException("Interrupted getStoreProtocol", ex);
          }
        }
        else {
          pool.storeConnectionInUse = true;
          
          pool.logger.fine("getStoreProtocol() -- storeConnectionInUse");
        }
        
        timeoutConnections();
      }
    }
    return p;
  }
  

  IMAPProtocol getFolderStoreProtocol()
    throws ProtocolException
  {
    IMAPProtocol p = getStoreProtocol();
    p.removeResponseHandler(this);
    p.addResponseHandler(nonStoreResponseHandler);
    return p;
  }
  







  private void refreshPassword()
  {
    if (logger.isLoggable(Level.FINE))
      logger.fine("refresh password, user: " + traceUser(user));
    InetAddress addr;
    try {
      addr = InetAddress.getByName(host);
    } catch (UnknownHostException e) { InetAddress addr;
      addr = null;
    }
    
    PasswordAuthentication pa = session.requestPasswordAuthentication(addr, port, name, null, user);
    
    if (pa != null) {
      user = pa.getUserName();
      password = pa.getPassword();
    }
  }
  




  boolean allowReadOnlySelect()
  {
    return PropUtil.getBooleanSessionProperty(session, "mail." + name + ".allowreadonlyselect", false);
  }
  



  boolean hasSeparateStoreConnection()
  {
    return pool.separateStoreConnection;
  }
  


  MailLogger getConnectionPoolLogger()
  {
    return pool.logger;
  }
  


  boolean getMessageCacheDebug()
  {
    return messageCacheDebug;
  }
  



  boolean isConnectionPoolFull()
  {
    synchronized (pool) {
      if (pool.logger.isLoggable(Level.FINE)) {
        pool.logger.fine("connection pool current size: " + 
          pool.authenticatedConnections.size() + "   pool size: " + 
          pool.poolSize);
      }
      return pool.authenticatedConnections.size() >= pool.poolSize;
    }
  }
  




  void releaseProtocol(IMAPFolder folder, IMAPProtocol protocol)
  {
    synchronized (pool) {
      if (protocol != null)
      {

        if (!isConnectionPoolFull()) {
          protocol.addResponseHandler(this);
          pool.authenticatedConnections.addElement(protocol);
          
          if (logger.isLoggable(Level.FINE))
            logger.fine("added an Authenticated connection -- size: " + 
            
              pool.authenticatedConnections.size());
        } else {
          logger.fine("pool is full, not adding an Authenticated connection");
          try
          {
            protocol.logout();
          }
          catch (ProtocolException localProtocolException) {}
        }
      }
      if (pool.folders != null) {
        pool.folders.removeElement(folder);
      }
      timeoutConnections();
    }
  }
  






  private void releaseStoreProtocol(IMAPProtocol protocol)
  {
    if (protocol == null) {
      cleanup();
      return;
    }
    





    synchronized (connectionFailedLock) {
      boolean failed = connectionFailed;
      connectionFailed = false;
    }
    
    boolean failed;
    synchronized (pool) {
      pool.storeConnectionInUse = false;
      pool.notifyAll();
      
      pool.logger.fine("releaseStoreProtocol()");
      
      timeoutConnections();
    }
    





    assert (!Thread.holdsLock(pool));
    if (failed) {
      cleanup();
    }
  }
  

  void releaseFolderStoreProtocol(IMAPProtocol protocol)
  {
    if (protocol == null)
      return;
    protocol.removeResponseHandler(nonStoreResponseHandler);
    protocol.addResponseHandler(this);
    synchronized (pool) {
      pool.storeConnectionInUse = false;
      pool.notifyAll();
      
      pool.logger.fine("releaseFolderStoreProtocol()");
      
      timeoutConnections();
    }
  }
  



  private void emptyConnectionPool(boolean force)
  {
    synchronized (pool) {
      for (int index = pool.authenticatedConnections.size() - 1; 
          index >= 0; index--) {
        try
        {
          IMAPProtocol p = (IMAPProtocol)pool.authenticatedConnections.elementAt(index);
          p.removeResponseHandler(this);
          if (force) {
            p.disconnect();
          } else {
            p.logout();
          }
        } catch (ProtocolException localProtocolException) {}
      }
      pool.authenticatedConnections.removeAllElements();
    }
    
    pool.logger.fine("removed all authenticated connections from pool");
  }
  



  private void timeoutConnections()
  {
    synchronized (pool)
    {



      if ((System.currentTimeMillis() - pool.lastTimePruned > pool.pruningInterval) && 
        (pool.authenticatedConnections.size() > 1))
      {
        if (pool.logger.isLoggable(Level.FINE)) {
          pool.logger.fine("checking for connections to prune: " + (
            System.currentTimeMillis() - pool.lastTimePruned));
          pool.logger.fine("clientTimeoutInterval: " + 
            pool.clientTimeoutInterval);
        }
        





        for (int index = pool.authenticatedConnections.size() - 1; 
            index > 0; index--)
        {
          IMAPProtocol p = (IMAPProtocol)pool.authenticatedConnections.elementAt(index);
          if (pool.logger.isLoggable(Level.FINE)) {
            pool.logger.fine("protocol last used: " + (
              System.currentTimeMillis() - p.getTimestamp()));
          }
          if (System.currentTimeMillis() - p.getTimestamp() > pool.clientTimeoutInterval)
          {
            pool.logger.fine("authenticated connection timed out, logging out the connection");
            


            p.removeResponseHandler(this);
            pool.authenticatedConnections.removeElementAt(index);
            try
            {
              p.logout();
            } catch (ProtocolException localProtocolException) {}
          }
        }
        pool.lastTimePruned = System.currentTimeMillis();
      }
    }
  }
  


  int getFetchBlockSize()
  {
    return blksize;
  }
  


  boolean ignoreBodyStructureSize()
  {
    return ignoreSize;
  }
  


  Session getSession()
  {
    return session;
  }
  


  int getStatusCacheTimeout()
  {
    return statusCacheTimeout;
  }
  


  int getAppendBufferSize()
  {
    return appendBufferSize;
  }
  


  int getMinIdleTime()
  {
    return minIdleTime;
  }
  


  boolean throwSearchException()
  {
    return throwSearchException;
  }
  


  boolean getPeek()
  {
    return peek;
  }
  








  public synchronized boolean hasCapability(String capability)
    throws MessagingException
  {
    IMAPProtocol p = null;
    try {
      p = getStoreProtocol();
      return p.hasCapability(capability);
    } catch (ProtocolException pex) {
      throw new MessagingException(pex.getMessage(), pex);
    } finally {
      releaseStoreProtocol(p);
    }
  }
  








  public void setProxyAuthUser(String user)
  {
    proxyAuthUser = user;
  }
  





  public String getProxyAuthUser()
  {
    return proxyAuthUser;
  }
  
  /* Error */
  public synchronized boolean isConnected()
  {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial 320	javax/mail/Store:isConnected	()Z
    //   4: ifne +5 -> 9
    //   7: iconst_0
    //   8: ireturn
    //   9: aconst_null
    //   10: astore_1
    //   11: aload_0
    //   12: invokespecial 284	com/sun/mail/imap/IMAPStore:getStoreProtocol	()Lcom/sun/mail/imap/protocol/IMAPProtocol;
    //   15: astore_1
    //   16: aload_1
    //   17: invokevirtual 258	com/sun/mail/imap/protocol/IMAPProtocol:noop	()V
    //   20: aload_0
    //   21: aload_1
    //   22: invokespecial 319	com/sun/mail/imap/IMAPStore:releaseStoreProtocol	(Lcom/sun/mail/imap/protocol/IMAPProtocol;)V
    //   25: goto +20 -> 45
    //   28: astore_2
    //   29: aload_0
    //   30: aload_1
    //   31: invokespecial 319	com/sun/mail/imap/IMAPStore:releaseStoreProtocol	(Lcom/sun/mail/imap/protocol/IMAPProtocol;)V
    //   34: goto +11 -> 45
    //   37: astore_3
    //   38: aload_0
    //   39: aload_1
    //   40: invokespecial 319	com/sun/mail/imap/IMAPStore:releaseStoreProtocol	(Lcom/sun/mail/imap/protocol/IMAPProtocol;)V
    //   43: aload_3
    //   44: athrow
    //   45: aload_0
    //   46: invokespecial 320	javax/mail/Store:isConnected	()Z
    //   49: ireturn
    // Line number table:
    //   Java source line #1579	-> byte code offset #0
    //   Java source line #1582	-> byte code offset #7
    //   Java source line #1601	-> byte code offset #9
    //   Java source line #1603	-> byte code offset #11
    //   Java source line #1604	-> byte code offset #16
    //   Java source line #1608	-> byte code offset #20
    //   Java source line #1609	-> byte code offset #25
    //   Java source line #1605	-> byte code offset #28
    //   Java source line #1608	-> byte code offset #29
    //   Java source line #1609	-> byte code offset #34
    //   Java source line #1608	-> byte code offset #37
    //   Java source line #1612	-> byte code offset #45
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	50	0	this	IMAPStore
    //   10	30	1	p	IMAPProtocol
    //   28	1	2	localProtocolException	ProtocolException
    //   37	7	3	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   11	20	28	com/sun/mail/iap/ProtocolException
    //   11	20	37	finally
  }
  
  public synchronized void close()
    throws MessagingException
  {
    if (!super.isConnected()) {
      return;
    }
    IMAPProtocol protocol = null;
    try {
      boolean isEmpty;
      synchronized (pool)
      {

        isEmpty = pool.authenticatedConnections.isEmpty();
      }
      



      boolean isEmpty;
      



      if (isEmpty) {
        pool.logger.fine("close() - no connections ");
        cleanup();
        return;
      }
      
      protocol = getStoreProtocol();
      





      synchronized (pool) {
        pool.authenticatedConnections.removeElement(protocol);
      }
      















      protocol.logout();
    }
    catch (ProtocolException pex) {
      throw new MessagingException(pex.getMessage(), pex);
    } finally {
      releaseStoreProtocol(protocol);
    }
  }
  
  protected void finalize() throws Throwable
  {
    if (!finalizeCleanClose)
    {
      synchronized (connectionFailedLock) {
        connectionFailed = true;
        forceClose = true;
      }
      closeFoldersOnStoreFailure = true;
    }
    try {
      close();
      
      super.finalize(); } finally { super.finalize();
    }
  }
  



  private synchronized void cleanup()
  {
    if (!super.isConnected()) {
      logger.fine("IMAPStore cleanup, not connected");
      return;
    }
    







    synchronized (connectionFailedLock) {
      boolean force = forceClose;
      forceClose = false;
      connectionFailed = false; }
    boolean force;
    if (logger.isLoggable(Level.FINE)) {
      logger.fine("IMAPStore cleanup, force " + force);
    }
    if ((!force) || (closeFoldersOnStoreFailure)) {
      List<IMAPFolder> foldersCopy = null;
      boolean done = true;
      








      for (;;)
      {
        synchronized (pool) {
          if (pool.folders != null) {
            done = false;
            foldersCopy = pool.folders;
            pool.folders = null;
          } else {
            done = true;
          }
        }
        if (done) {
          break;
        }
        
        int i = 0; for (int fsize = foldersCopy.size(); i < fsize; i++) {
          IMAPFolder f = (IMAPFolder)foldersCopy.get(i);
          try
          {
            if (force) {
              logger.fine("force folder to close");
              


              f.forceClose();
            } else {
              logger.fine("close folder");
              f.close(false);
            }
          }
          catch (MessagingException localMessagingException) {}catch (IllegalStateException localIllegalStateException) {}
        }
      }
    }
    




    synchronized (pool) {
      emptyConnectionPool(force);
    }
    
    try
    {
      super.close();
    }
    catch (MessagingException localMessagingException1) {}
    
    logger.fine("IMAPStore cleanup done");
  }
  



  public synchronized Folder getDefaultFolder()
    throws MessagingException
  {
    checkConnected();
    return new DefaultFolder(this);
  }
  



  public synchronized Folder getFolder(String name)
    throws MessagingException
  {
    checkConnected();
    return newIMAPFolder(name, 65535);
  }
  



  public synchronized Folder getFolder(URLName url)
    throws MessagingException
  {
    checkConnected();
    return newIMAPFolder(url.getFile(), 65535);
  }
  









  protected IMAPFolder newIMAPFolder(String fullName, char separator, Boolean isNamespace)
  {
    IMAPFolder f = null;
    if (folderConstructor != null) {
      try
      {
        Object[] o = { fullName, Character.valueOf(separator), this, isNamespace };
        f = (IMAPFolder)folderConstructor.newInstance(o);
      } catch (Exception ex) {
        logger.log(Level.FINE, "exception creating IMAPFolder class", ex);
      }
    }
    
    if (f == null)
      f = new IMAPFolder(fullName, separator, this, isNamespace);
    return f;
  }
  







  protected IMAPFolder newIMAPFolder(String fullName, char separator)
  {
    return newIMAPFolder(fullName, separator, null);
  }
  






  protected IMAPFolder newIMAPFolder(ListInfo li)
  {
    IMAPFolder f = null;
    if (folderConstructorLI != null) {
      try {
        Object[] o = { li, this };
        f = (IMAPFolder)folderConstructorLI.newInstance(o);
      } catch (Exception ex) {
        logger.log(Level.FINE, "exception creating IMAPFolder class LI", ex);
      }
    }
    
    if (f == null)
      f = new IMAPFolder(li, this);
    return f;
  }
  



  public Folder[] getPersonalNamespaces()
    throws MessagingException
  {
    Namespaces ns = getNamespaces();
    if ((ns == null) || (personal == null))
      return super.getPersonalNamespaces();
    return namespaceToFolders(personal, null);
  }
  




  public Folder[] getUserNamespaces(String user)
    throws MessagingException
  {
    Namespaces ns = getNamespaces();
    if ((ns == null) || (otherUsers == null))
      return super.getUserNamespaces(user);
    return namespaceToFolders(otherUsers, user);
  }
  



  public Folder[] getSharedNamespaces()
    throws MessagingException
  {
    Namespaces ns = getNamespaces();
    if ((ns == null) || (shared == null))
      return super.getSharedNamespaces();
    return namespaceToFolders(shared, null);
  }
  
  private synchronized Namespaces getNamespaces() throws MessagingException {
    checkConnected();
    
    IMAPProtocol p = null;
    
    if (namespaces == null) {
      try {
        p = getStoreProtocol();
        namespaces = p.namespace();
      }
      catch (BadCommandException localBadCommandException) {}catch (ConnectionException cex)
      {
        throw new StoreClosedException(this, cex.getMessage());
      } catch (ProtocolException pex) {
        throw new MessagingException(pex.getMessage(), pex);
      } finally {
        releaseStoreProtocol(p);
      }
    }
    return namespaces;
  }
  
  private Folder[] namespaceToFolders(Namespaces.Namespace[] ns, String user)
  {
    Folder[] fa = new Folder[ns.length];
    for (int i = 0; i < fa.length; i++) {
      String name = prefix;
      if (user == null)
      {
        int len = name.length();
        if ((len > 0) && (name.charAt(len - 1) == delimiter)) {
          name = name.substring(0, len - 1);
        }
      } else {
        name = name + user;
      }
      fa[i] = newIMAPFolder(name, delimiter, 
        Boolean.valueOf(user == null ? 1 : false));
    }
    return fa;
  }
  
















  public synchronized Quota[] getQuota(String root)
    throws MessagingException
  {
    checkConnected();
    Quota[] qa = null;
    
    IMAPProtocol p = null;
    try {
      p = getStoreProtocol();
      qa = p.getQuotaRoot(root);
    } catch (BadCommandException bex) {
      throw new MessagingException("QUOTA not supported", bex);
    } catch (ConnectionException cex) {
      throw new StoreClosedException(this, cex.getMessage());
    } catch (ProtocolException pex) {
      throw new MessagingException(pex.getMessage(), pex);
    } finally {
      releaseStoreProtocol(p);
    }
    return qa;
  }
  








  public synchronized void setQuota(Quota quota)
    throws MessagingException
  {
    checkConnected();
    IMAPProtocol p = null;
    try {
      p = getStoreProtocol();
      p.setQuota(quota);
    } catch (BadCommandException bex) {
      throw new MessagingException("QUOTA not supported", bex);
    } catch (ConnectionException cex) {
      throw new StoreClosedException(this, cex.getMessage());
    } catch (ProtocolException pex) {
      throw new MessagingException(pex.getMessage(), pex);
    } finally {
      releaseStoreProtocol(p);
    }
  }
  
  private void checkConnected() {
    assert (Thread.holdsLock(this));
    if (!super.isConnected()) {
      throw new IllegalStateException("Not connected");
    }
  }
  



  public void handleResponse(Response r)
  {
    if ((r.isOK()) || (r.isNO()) || (r.isBAD()) || (r.isBYE()))
      handleResponseCode(r);
    if (r.isBYE()) {
      logger.fine("IMAPStore connection dead");
      

      synchronized (connectionFailedLock) {
        connectionFailed = true;
        if (r.isSynthetic())
          forceClose = true;
      }
      return;
    }
  }
  


































  public void idle()
    throws MessagingException
  {
    IMAPProtocol p = null;
    

    assert (!Thread.holdsLock(pool));
    synchronized (this) {
      checkConnected();
    }
    boolean needNotification = false;
    try {
      synchronized (pool) {
        p = getStoreProtocol();
        if (pool.idleState != 0)
        {

          try
          {

            pool.wait();
          }
          catch (InterruptedException ex)
          {
            Thread.currentThread().interrupt();
            
            throw new MessagingException("idle interrupted", ex);
          }
          return;
        }
        p.idleStart();
        needNotification = true;
        pool.idleState = 1;
        pool.idleProtocol = p;
      }
      












      for (;;)
      {
        Response r = p.readIdleResponse();
        synchronized (pool) {
          if ((r == null) || (!p.processIdleResponse(r))) {
            pool.idleState = 0;
            pool.idleProtocol = null;
            pool.notifyAll();
            needNotification = false;
            break;
          }
        }
        if ((enableImapEvents) && (r.isUnTagged())) {
          notifyStoreListeners(1000, r.toString());
        }
      }
      





      int minidle = getMinIdleTime();
      if (minidle > 0) {
        try {
          Thread.sleep(minidle);
        }
        catch (InterruptedException ex)
        {
          Thread.currentThread().interrupt();
        }
      }
    }
    catch (BadCommandException bex) {
      throw new MessagingException("IDLE not supported", bex);
    } catch (ConnectionException cex) {
      throw new StoreClosedException(this, cex.getMessage());
    } catch (ProtocolException pex) {
      throw new MessagingException(pex.getMessage(), pex);
    } finally {
      if (needNotification) {
        synchronized (pool) {
          pool.idleState = 0;
          pool.idleProtocol = null;
          pool.notifyAll();
        }
      }
      releaseStoreProtocol(p);
    }
  }
  



  private void waitIfIdle()
    throws ProtocolException
  {
    assert (Thread.holdsLock(pool));
    while (pool.idleState != 0) {
      if (pool.idleState == 1) {
        pool.idleProtocol.idleAbort();
        pool.idleState = 2;
      }
      try
      {
        pool.wait();


      }
      catch (InterruptedException ex)
      {

        throw new ProtocolException("Interrupted waitIfIdle", ex);
      }
    }
  }
  












  public synchronized Map<String, String> id(Map<String, String> clientParams)
    throws MessagingException
  {
    checkConnected();
    Map<String, String> serverParams = null;
    
    IMAPProtocol p = null;
    try {
      p = getStoreProtocol();
      serverParams = p.id(clientParams);
    } catch (BadCommandException bex) {
      throw new MessagingException("ID not supported", bex);
    } catch (ConnectionException cex) {
      throw new StoreClosedException(this, cex.getMessage());
    } catch (ProtocolException pex) {
      throw new MessagingException(pex.getMessage(), pex);
    } finally {
      releaseStoreProtocol(p);
    }
    return serverParams;
  }
  



  void handleResponseCode(Response r)
  {
    if (enableResponseEvents)
      notifyStoreListeners(1000, r.toString());
    String s = r.getRest();
    boolean isAlert = false;
    if (s.startsWith("[")) {
      int i = s.indexOf(']');
      
      if ((i > 0) && (s.substring(0, i + 1).equalsIgnoreCase("[ALERT]"))) {
        isAlert = true;
      }
      s = s.substring(i + 1).trim();
    }
    if (isAlert) {
      notifyStoreListeners(1, s);
    } else if ((r.isUnTagged()) && (s.length() > 0))
    {


      notifyStoreListeners(2, s); }
  }
  
  private String traceUser(String user) {
    return debugusername ? user : "<user name suppressed>";
  }
  
  private String tracePassword(String password) {
    return password == null ? "<null>" : debugpassword ? password : "<non-null>";
  }
}
