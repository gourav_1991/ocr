package com.sun.mail.imap;

import com.sun.mail.iap.ConnectionException;
import com.sun.mail.iap.ProtocolException;
import com.sun.mail.iap.Response;
import com.sun.mail.imap.protocol.BODY;
import com.sun.mail.imap.protocol.BODYSTRUCTURE;
import com.sun.mail.imap.protocol.ENVELOPE;
import com.sun.mail.imap.protocol.FetchItem;
import com.sun.mail.imap.protocol.FetchResponse;
import com.sun.mail.imap.protocol.IMAPProtocol;
import com.sun.mail.imap.protocol.INTERNALDATE;
import com.sun.mail.imap.protocol.Item;
import com.sun.mail.imap.protocol.MODSEQ;
import com.sun.mail.imap.protocol.RFC822DATA;
import com.sun.mail.imap.protocol.RFC822SIZE;
import com.sun.mail.imap.protocol.UID;
import com.sun.mail.util.ReadableMime;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import javax.activation.DataHandler;
import javax.mail.Address;
import javax.mail.FetchProfile;
import javax.mail.FetchProfile.Item;
import javax.mail.Flags;
import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.FolderClosedException;
import javax.mail.Header;
import javax.mail.IllegalWriteException;
import javax.mail.Message.RecipientType;
import javax.mail.MessageRemovedException;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.UIDFolder.FetchProfileItem;
import javax.mail.internet.ContentType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import javax.mail.internet.ParameterList;
































public class IMAPMessage
  extends MimeMessage
  implements ReadableMime
{
  protected BODYSTRUCTURE bs;
  protected ENVELOPE envelope;
  protected Map<String, Object> items;
  private Date receivedDate;
  private long size = -1L;
  

  private Boolean peek;
  
  private volatile long uid = -1L;
  

  private volatile long modseq = -1L;
  

  protected String sectionId;
  

  private String type;
  
  private String subject;
  
  private String description;
  
  private volatile boolean headersLoaded = false;
  

  private volatile boolean bodyLoaded = false;
  








  private Hashtable<String, String> loadedHeaders = new Hashtable(1);
  



  static final String EnvelopeCmd = "ENVELOPE INTERNALDATE RFC822.SIZE";
  




  protected IMAPMessage(IMAPFolder folder, int msgnum)
  {
    super(folder, msgnum);
    flags = null;
  }
  




  protected IMAPMessage(Session session)
  {
    super(session);
  }
  










  protected IMAPProtocol getProtocol()
    throws ProtocolException, FolderClosedException
  {
    ((IMAPFolder)folder).waitIfIdle();
    IMAPProtocol p = folder).protocol;
    if (p == null) {
      throw new FolderClosedException(folder);
    }
    return p;
  }
  



  protected boolean isREV1()
    throws FolderClosedException
  {
    IMAPProtocol p = folder).protocol;
    if (p == null) {
      throw new FolderClosedException(folder);
    }
    return p.isREV1();
  }
  





  protected Object getMessageCacheLock()
  {
    return folder).messageCacheLock;
  }
  







  protected int getSequenceNumber()
  {
    return folder).messageCache.seqnumOf(getMessageNumber());
  }
  




  protected void setMessageNumber(int msgnum)
  {
    super.setMessageNumber(msgnum);
  }
  






  protected long getUID()
  {
    return uid;
  }
  
  protected void setUID(long uid) {
    this.uid = uid;
  }
  







  public synchronized long getModSeq()
    throws MessagingException
  {
    if (modseq != -1L) {
      return modseq;
    }
    synchronized (getMessageCacheLock()) {
      try {
        IMAPProtocol p = getProtocol();
        checkExpunged();
        MODSEQ ms = p.fetchMODSEQ(getSequenceNumber());
        
        if (ms != null)
          modseq = modseq;
      } catch (ConnectionException cex) {
        throw new FolderClosedException(folder, cex.getMessage());
      } catch (ProtocolException pex) {
        throw new MessagingException(pex.getMessage(), pex);
      }
    }
    return modseq;
  }
  
  long _getModSeq() {
    return modseq;
  }
  
  void setModSeq(long modseq) {
    this.modseq = modseq;
  }
  

  protected void setExpunged(boolean set)
  {
    super.setExpunged(set);
  }
  
  protected void checkExpunged() throws MessageRemovedException
  {
    if (expunged) {
      throw new MessageRemovedException();
    }
  }
  





  protected void forceCheckExpunged()
    throws MessageRemovedException, FolderClosedException
  {
    synchronized (getMessageCacheLock()) {
      try {
        getProtocol().noop();
      } catch (ConnectionException cex) {
        throw new FolderClosedException(folder, cex.getMessage());
      }
      catch (ProtocolException localProtocolException) {}
    }
    
    if (expunged) {
      throw new MessageRemovedException();
    }
  }
  
  protected int getFetchBlockSize()
  {
    return ((IMAPStore)folder.getStore()).getFetchBlockSize();
  }
  

  protected boolean ignoreBodyStructureSize()
  {
    return ((IMAPStore)folder.getStore()).ignoreBodyStructureSize();
  }
  


  public Address[] getFrom()
    throws MessagingException
  {
    checkExpunged();
    if (bodyLoaded)
      return super.getFrom();
    loadEnvelope();
    InternetAddress[] a = envelope.from;
    







    if ((a == null) || (a.length == 0))
      a = envelope.sender;
    return aaclone(a);
  }
  
  public void setFrom(Address address) throws MessagingException
  {
    throw new IllegalWriteException("IMAPMessage is read-only");
  }
  
  public void addFrom(Address[] addresses) throws MessagingException
  {
    throw new IllegalWriteException("IMAPMessage is read-only");
  }
  


  public Address getSender()
    throws MessagingException
  {
    checkExpunged();
    if (bodyLoaded)
      return super.getSender();
    loadEnvelope();
    if ((envelope.sender != null) && (envelope.sender.length > 0)) {
      return envelope.sender[0];
    }
    return null;
  }
  
  public void setSender(Address address)
    throws MessagingException
  {
    throw new IllegalWriteException("IMAPMessage is read-only");
  }
  



  public Address[] getRecipients(Message.RecipientType type)
    throws MessagingException
  {
    checkExpunged();
    if (bodyLoaded)
      return super.getRecipients(type);
    loadEnvelope();
    
    if (type == Message.RecipientType.TO)
      return aaclone(envelope.to);
    if (type == Message.RecipientType.CC)
      return aaclone(envelope.cc);
    if (type == Message.RecipientType.BCC) {
      return aaclone(envelope.bcc);
    }
    return super.getRecipients(type);
  }
  
  public void setRecipients(Message.RecipientType type, Address[] addresses)
    throws MessagingException
  {
    throw new IllegalWriteException("IMAPMessage is read-only");
  }
  
  public void addRecipients(Message.RecipientType type, Address[] addresses)
    throws MessagingException
  {
    throw new IllegalWriteException("IMAPMessage is read-only");
  }
  


  public Address[] getReplyTo()
    throws MessagingException
  {
    checkExpunged();
    if (bodyLoaded)
      return super.getReplyTo();
    loadEnvelope();
    





    if ((envelope.replyTo == null) || (envelope.replyTo.length == 0))
      return getFrom();
    return aaclone(envelope.replyTo);
  }
  
  public void setReplyTo(Address[] addresses) throws MessagingException
  {
    throw new IllegalWriteException("IMAPMessage is read-only");
  }
  


  public String getSubject()
    throws MessagingException
  {
    checkExpunged();
    if (bodyLoaded) {
      return super.getSubject();
    }
    if (subject != null) {
      return subject;
    }
    loadEnvelope();
    if (envelope.subject == null) {
      return null;
    }
    


    try
    {
      subject = MimeUtility.decodeText(MimeUtility.unfold(envelope.subject));
    } catch (UnsupportedEncodingException ex) {
      subject = envelope.subject;
    }
    
    return subject;
  }
  
  public void setSubject(String subject, String charset)
    throws MessagingException
  {
    throw new IllegalWriteException("IMAPMessage is read-only");
  }
  


  public Date getSentDate()
    throws MessagingException
  {
    checkExpunged();
    if (bodyLoaded)
      return super.getSentDate();
    loadEnvelope();
    if (envelope.date == null) {
      return null;
    }
    return new Date(envelope.date.getTime());
  }
  
  public void setSentDate(Date d) throws MessagingException
  {
    throw new IllegalWriteException("IMAPMessage is read-only");
  }
  


  public Date getReceivedDate()
    throws MessagingException
  {
    checkExpunged();
    if (receivedDate == null)
      loadEnvelope();
    if (receivedDate == null) {
      return null;
    }
    return new Date(receivedDate.getTime());
  }
  







  public int getSize()
    throws MessagingException
  {
    checkExpunged();
    
    if (size == -1L)
      loadEnvelope();
    if (size > 2147483647L) {
      return Integer.MAX_VALUE;
    }
    return (int)size;
  }
  






  public long getSizeLong()
    throws MessagingException
  {
    checkExpunged();
    
    if (size == -1L)
      loadEnvelope();
    return size;
  }
  






  public int getLineCount()
    throws MessagingException
  {
    checkExpunged();
    
    loadBODYSTRUCTURE();
    return bs.lines;
  }
  


  public String[] getContentLanguage()
    throws MessagingException
  {
    checkExpunged();
    if (bodyLoaded)
      return super.getContentLanguage();
    loadBODYSTRUCTURE();
    if (bs.language != null) {
      return (String[])bs.language.clone();
    }
    return null;
  }
  
  public void setContentLanguage(String[] languages)
    throws MessagingException
  {
    throw new IllegalWriteException("IMAPMessage is read-only");
  }
  





  public String getInReplyTo()
    throws MessagingException
  {
    checkExpunged();
    if (bodyLoaded)
      return super.getHeader("In-Reply-To", " ");
    loadEnvelope();
    return envelope.inReplyTo;
  }
  





  public synchronized String getContentType()
    throws MessagingException
  {
    checkExpunged();
    if (bodyLoaded) {
      return super.getContentType();
    }
    
    if (type == null) {
      loadBODYSTRUCTURE();
      
      ContentType ct = new ContentType(bs.type, bs.subtype, bs.cParams);
      type = ct.toString();
    }
    return type;
  }
  


  public String getDisposition()
    throws MessagingException
  {
    checkExpunged();
    if (bodyLoaded)
      return super.getDisposition();
    loadBODYSTRUCTURE();
    return bs.disposition;
  }
  
  public void setDisposition(String disposition) throws MessagingException
  {
    throw new IllegalWriteException("IMAPMessage is read-only");
  }
  


  public String getEncoding()
    throws MessagingException
  {
    checkExpunged();
    if (bodyLoaded)
      return super.getEncoding();
    loadBODYSTRUCTURE();
    return bs.encoding;
  }
  


  public String getContentID()
    throws MessagingException
  {
    checkExpunged();
    if (bodyLoaded)
      return super.getContentID();
    loadBODYSTRUCTURE();
    return bs.id;
  }
  
  public void setContentID(String cid) throws MessagingException
  {
    throw new IllegalWriteException("IMAPMessage is read-only");
  }
  


  public String getContentMD5()
    throws MessagingException
  {
    checkExpunged();
    if (bodyLoaded)
      return super.getContentMD5();
    loadBODYSTRUCTURE();
    return bs.md5;
  }
  
  public void setContentMD5(String md5) throws MessagingException
  {
    throw new IllegalWriteException("IMAPMessage is read-only");
  }
  


  public String getDescription()
    throws MessagingException
  {
    checkExpunged();
    if (bodyLoaded) {
      return super.getDescription();
    }
    if (description != null) {
      return description;
    }
    loadBODYSTRUCTURE();
    if (bs.description == null) {
      return null;
    }
    try {
      description = MimeUtility.decodeText(bs.description);
    } catch (UnsupportedEncodingException ex) {
      description = bs.description;
    }
    
    return description;
  }
  
  public void setDescription(String description, String charset)
    throws MessagingException
  {
    throw new IllegalWriteException("IMAPMessage is read-only");
  }
  


  public String getMessageID()
    throws MessagingException
  {
    checkExpunged();
    if (bodyLoaded)
      return super.getMessageID();
    loadEnvelope();
    return envelope.messageId;
  }
  




  public String getFileName()
    throws MessagingException
  {
    checkExpunged();
    if (bodyLoaded) {
      return super.getFileName();
    }
    String filename = null;
    loadBODYSTRUCTURE();
    
    if (bs.dParams != null)
      filename = bs.dParams.get("filename");
    if ((filename == null) && (bs.cParams != null))
      filename = bs.cParams.get("name");
    return filename;
  }
  
  public void setFileName(String filename) throws MessagingException
  {
    throw new IllegalWriteException("IMAPMessage is read-only");
  }
  






  protected InputStream getContentStream()
    throws MessagingException
  {
    if (bodyLoaded)
      return super.getContentStream();
    InputStream is = null;
    boolean pk = getPeek();
    

    synchronized (getMessageCacheLock()) {
      try {
        IMAPProtocol p = getProtocol();
        


        checkExpunged();
        
        if ((p.isREV1()) && (getFetchBlockSize() != -1)) {
          return new IMAPInputStream(this, toSection("TEXT"), (bs != null) && 
            (!ignoreBodyStructureSize()) ? bs.size : -1, pk);
        }
        
        if (p.isREV1()) { BODY b;
          BODY b;
          if (pk) {
            b = p.peekBody(getSequenceNumber(), toSection("TEXT"));
          } else
            b = p.fetchBody(getSequenceNumber(), toSection("TEXT"));
          if (b != null)
            is = b.getByteArrayInputStream();
        } else {
          RFC822DATA rd = p.fetchRFC822(getSequenceNumber(), "TEXT");
          if (rd != null)
            is = rd.getByteArrayInputStream();
        }
      } catch (ConnectionException cex) {
        throw new FolderClosedException(folder, cex.getMessage());
      } catch (ProtocolException pex) {
        forceCheckExpunged();
        throw new MessagingException(pex.getMessage(), pex);
      }
    }
    
    if (is == null) {
      forceCheckExpunged();
      



      is = new ByteArrayInputStream(new byte[0]);
    }
    return is;
  }
  



  public synchronized DataHandler getDataHandler()
    throws MessagingException
  {
    checkExpunged();
    
    if ((dh == null) && (!bodyLoaded)) {
      loadBODYSTRUCTURE();
      if (type == null)
      {
        ContentType ct = new ContentType(bs.type, bs.subtype, bs.cParams);
        
        type = ct.toString();
      }
      



      if (bs.isMulti()) {
        dh = new DataHandler(new IMAPMultipartDataSource(this, bs.bodies, sectionId, this));


      }
      else if ((bs.isNested()) && (isREV1()) && (bs.envelope != null))
      {



        dh = new DataHandler(new IMAPNestedMessage(this, bs.bodies[0], bs.envelope, sectionId + ".1"), type);
      }
    }
    





    return super.getDataHandler();
  }
  
  public void setDataHandler(DataHandler content)
    throws MessagingException
  {
    throw new IllegalWriteException("IMAPMessage is read-only");
  }
  






  public InputStream getMimeStream()
    throws MessagingException
  {
    InputStream is = null;
    boolean pk = getPeek();
    

    synchronized (getMessageCacheLock()) {
      try {
        IMAPProtocol p = getProtocol();
        
        checkExpunged();
        
        if ((p.isREV1()) && (getFetchBlockSize() != -1)) {
          return new IMAPInputStream(this, sectionId, -1, pk);
        }
        if (p.isREV1()) { BODY b;
          BODY b;
          if (pk) {
            b = p.peekBody(getSequenceNumber(), sectionId);
          } else
            b = p.fetchBody(getSequenceNumber(), sectionId);
          if (b != null)
            is = b.getByteArrayInputStream();
        } else {
          RFC822DATA rd = p.fetchRFC822(getSequenceNumber(), null);
          if (rd != null)
            is = rd.getByteArrayInputStream();
        }
      } catch (ConnectionException cex) {
        throw new FolderClosedException(folder, cex.getMessage());
      } catch (ProtocolException pex) {
        forceCheckExpunged();
        throw new MessagingException(pex.getMessage(), pex);
      }
    }
    
    if (is == null) {
      forceCheckExpunged();
      



      is = new ByteArrayInputStream(new byte[0]);
    }
    return is;
  }
  



  public void writeTo(OutputStream os)
    throws IOException, MessagingException
  {
    if (bodyLoaded) {
      super.writeTo(os);
      return;
    }
    InputStream is = getMimeStream();
    try
    {
      byte[] bytes = new byte['䀀'];
      int count;
      while ((count = is.read(bytes)) != -1)
        os.write(bytes, 0, count);
    } finally {
      is.close();
    }
  }
  


  public String[] getHeader(String name)
    throws MessagingException
  {
    checkExpunged();
    
    if (isHeaderLoaded(name)) {
      return headers.getHeader(name);
    }
    
    InputStream is = null;
    

    synchronized (getMessageCacheLock()) {
      try {
        IMAPProtocol p = getProtocol();
        


        checkExpunged();
        
        if (p.isREV1()) {
          BODY b = p.peekBody(getSequenceNumber(), 
            toSection("HEADER.FIELDS (" + name + ")"));
          
          if (b != null)
            is = b.getByteArrayInputStream();
        } else {
          RFC822DATA rd = p.fetchRFC822(getSequenceNumber(), "HEADER.LINES (" + name + ")");
          
          if (rd != null)
            is = rd.getByteArrayInputStream();
        }
      } catch (ConnectionException cex) {
        throw new FolderClosedException(folder, cex.getMessage());
      } catch (ProtocolException pex) {
        forceCheckExpunged();
        throw new MessagingException(pex.getMessage(), pex);
      }
    }
    


    if (is == null) {
      return null;
    }
    if (headers == null)
      headers = new InternetHeaders();
    headers.load(is);
    setHeaderLoaded(name);
    
    return headers.getHeader(name);
  }
  



  public String getHeader(String name, String delimiter)
    throws MessagingException
  {
    checkExpunged();
    

    if (getHeader(name) == null)
      return null;
    return headers.getHeader(name, delimiter);
  }
  
  public void setHeader(String name, String value)
    throws MessagingException
  {
    throw new IllegalWriteException("IMAPMessage is read-only");
  }
  
  public void addHeader(String name, String value)
    throws MessagingException
  {
    throw new IllegalWriteException("IMAPMessage is read-only");
  }
  
  public void removeHeader(String name)
    throws MessagingException
  {
    throw new IllegalWriteException("IMAPMessage is read-only");
  }
  


  public Enumeration<Header> getAllHeaders()
    throws MessagingException
  {
    checkExpunged();
    loadHeaders();
    return super.getAllHeaders();
  }
  



  public Enumeration<Header> getMatchingHeaders(String[] names)
    throws MessagingException
  {
    checkExpunged();
    loadHeaders();
    return super.getMatchingHeaders(names);
  }
  



  public Enumeration<Header> getNonMatchingHeaders(String[] names)
    throws MessagingException
  {
    checkExpunged();
    loadHeaders();
    return super.getNonMatchingHeaders(names);
  }
  
  public void addHeaderLine(String line) throws MessagingException
  {
    throw new IllegalWriteException("IMAPMessage is read-only");
  }
  


  public Enumeration<String> getAllHeaderLines()
    throws MessagingException
  {
    checkExpunged();
    loadHeaders();
    return super.getAllHeaderLines();
  }
  



  public Enumeration<String> getMatchingHeaderLines(String[] names)
    throws MessagingException
  {
    checkExpunged();
    loadHeaders();
    return super.getMatchingHeaderLines(names);
  }
  



  public Enumeration<String> getNonMatchingHeaderLines(String[] names)
    throws MessagingException
  {
    checkExpunged();
    loadHeaders();
    return super.getNonMatchingHeaderLines(names);
  }
  


  public synchronized Flags getFlags()
    throws MessagingException
  {
    checkExpunged();
    loadFlags();
    return super.getFlags();
  }
  



  public synchronized boolean isSet(Flags.Flag flag)
    throws MessagingException
  {
    checkExpunged();
    loadFlags();
    return super.isSet(flag);
  }
  




  public synchronized void setFlags(Flags flag, boolean set)
    throws MessagingException
  {
    synchronized (getMessageCacheLock()) {
      try {
        IMAPProtocol p = getProtocol();
        checkExpunged();
        p.storeFlags(getSequenceNumber(), flag, set);
      } catch (ConnectionException cex) {
        throw new FolderClosedException(folder, cex.getMessage());
      } catch (ProtocolException pex) {
        throw new MessagingException(pex.getMessage(), pex);
      }
    }
  }
  







  public synchronized void setPeek(boolean peek)
  {
    this.peek = Boolean.valueOf(peek);
  }
  






  public synchronized boolean getPeek()
  {
    if (peek == null) {
      return ((IMAPStore)folder.getStore()).getPeek();
    }
    return peek.booleanValue();
  }
  






  public synchronized void invalidateHeaders()
  {
    headersLoaded = false;
    loadedHeaders.clear();
    headers = null;
    envelope = null;
    bs = null;
    receivedDate = null;
    size = -1L;
    type = null;
    subject = null;
    description = null;
    flags = null;
    content = null;
    contentStream = null;
    bodyLoaded = false;
  }
  





  public static class FetchProfileCondition
    implements Utility.Condition
  {
    private boolean needEnvelope = false;
    private boolean needFlags = false;
    private boolean needBodyStructure = false;
    private boolean needUID = false;
    private boolean needHeaders = false;
    private boolean needSize = false;
    private boolean needMessage = false;
    private boolean needRDate = false;
    private String[] hdrs = null;
    private Set<FetchItem> need = new HashSet();
    







    public FetchProfileCondition(FetchProfile fp, FetchItem[] fitems)
    {
      if (fp.contains(FetchProfile.Item.ENVELOPE))
        needEnvelope = true;
      if (fp.contains(FetchProfile.Item.FLAGS))
        needFlags = true;
      if (fp.contains(FetchProfile.Item.CONTENT_INFO))
        needBodyStructure = true;
      if (fp.contains(FetchProfile.Item.SIZE))
        needSize = true;
      if (fp.contains(UIDFolder.FetchProfileItem.UID))
        needUID = true;
      if (fp.contains(IMAPFolder.FetchProfileItem.HEADERS))
        needHeaders = true;
      if (fp.contains(IMAPFolder.FetchProfileItem.SIZE))
        needSize = true;
      if (fp.contains(IMAPFolder.FetchProfileItem.MESSAGE))
        needMessage = true;
      if (fp.contains(IMAPFolder.FetchProfileItem.INTERNALDATE))
        needRDate = true;
      hdrs = fp.getHeaderNames();
      for (int i = 0; i < fitems.length; i++) {
        if (fp.contains(fitems[i].getFetchProfileItem())) {
          need.add(fitems[i]);
        }
      }
    }
    



    public boolean test(IMAPMessage m)
    {
      if ((needEnvelope) && (m._getEnvelope() == null) && (!bodyLoaded))
        return true;
      if ((needFlags) && (m._getFlags() == null))
        return true;
      if ((needBodyStructure) && (m._getBodyStructure() == null) && 
        (!bodyLoaded))
        return true;
      if ((needUID) && (m.getUID() == -1L))
        return true;
      if ((needHeaders) && (!m.areHeadersLoaded()))
        return true;
      if ((needSize) && (size == -1L) && (!bodyLoaded))
        return true;
      if ((needMessage) && (!bodyLoaded))
        return true;
      if ((needRDate) && (receivedDate == null)) {
        return true;
      }
      
      for (int i = 0; i < hdrs.length; i++) {
        if (!m.isHeaderLoaded(hdrs[i]))
          return true;
      }
      Iterator<FetchItem> it = need.iterator();
      while (it.hasNext()) {
        FetchItem fitem = (FetchItem)it.next();
        if ((items == null) || (items.get(fitem.getName()) == null)) {
          return true;
        }
      }
      return false;
    }
  }
  













  protected boolean handleFetchItem(Item item, String[] hdrs, boolean allHeaders)
    throws MessagingException
  {
    if ((item instanceof Flags)) {
      flags = ((Flags)item);
    }
    else if ((item instanceof ENVELOPE)) {
      envelope = ((ENVELOPE)item);
    } else if ((item instanceof INTERNALDATE)) {
      receivedDate = ((INTERNALDATE)item).getDate();
    } else if ((item instanceof RFC822SIZE)) {
      size = size;
    } else if ((item instanceof MODSEQ)) {
      modseq = modseq;

    }
    else if ((item instanceof BODYSTRUCTURE)) {
      bs = ((BODYSTRUCTURE)item);
    }
    else if ((item instanceof UID)) {
      UID u = (UID)item;
      uid = uid;
      
      if (folder).uidTable == null) {
        folder).uidTable = new Hashtable();
      }
      folder).uidTable.put(Long.valueOf(uid), this);


    }
    else if (((item instanceof RFC822DATA)) || ((item instanceof BODY))) {
      boolean isHeader;
      InputStream headerStream;
      boolean isHeader;
      if ((item instanceof RFC822DATA))
      {
        InputStream headerStream = ((RFC822DATA)item).getByteArrayInputStream();
        isHeader = ((RFC822DATA)item).isHeader();
      }
      else {
        headerStream = ((BODY)item).getByteArrayInputStream();
        isHeader = ((BODY)item).isHeader();
      }
      
      if (!isHeader)
      {

        try
        {
          size = headerStream.available();
        }
        catch (IOException localIOException) {}
        
        parse(headerStream);
        bodyLoaded = true;
        setHeadersLoaded(true);
      }
      else {
        InternetHeaders h = new InternetHeaders();
        


        if (headerStream != null)
          h.load(headerStream);
        if ((headers == null) || (allHeaders)) {
          headers = h;






        }
        else
        {






          Enumeration<Header> e = h.getAllHeaders();
          while (e.hasMoreElements()) {
            Header he = (Header)e.nextElement();
            if (!isHeaderLoaded(he.getName())) {
              headers.addHeader(he
                .getName(), he.getValue());
            }
          }
        }
        
        if (allHeaders) {
          setHeadersLoaded(true);
        }
        else {
          for (int k = 0; k < hdrs.length; k++)
            setHeaderLoaded(hdrs[k]);
        }
      }
    } else {
      return false; }
    return true;
  }
  











  protected void handleExtensionFetchItems(Map<String, Object> extensionItems)
  {
    if ((extensionItems == null) || (extensionItems.isEmpty()))
      return;
    if (items == null)
      items = new HashMap();
    items.putAll(extensionItems);
  }
  












  protected Object fetchItem(FetchItem fitem)
    throws MessagingException
  {
    synchronized (getMessageCacheLock()) {
      Object robj = null;
      try
      {
        IMAPProtocol p = getProtocol();
        
        checkExpunged();
        
        int seqnum = getSequenceNumber();
        Response[] r = p.fetch(seqnum, fitem.getName());
        
        for (int i = 0; i < r.length; i++)
        {

          if ((r[i] != null) && ((r[i] instanceof FetchResponse)))
          {
            if (((FetchResponse)r[i]).getNumber() == seqnum)
            {

              FetchResponse f = (FetchResponse)r[i];
              handleExtensionFetchItems(f.getExtensionItems());
              if (items != null) {
                Object o = items.get(fitem.getName());
                if (o != null)
                  robj = o;
              }
            }
          }
        }
        p.notifyResponseHandlers(r);
        p.handleResult(r[(r.length - 1)]);
      } catch (ConnectionException cex) {
        throw new FolderClosedException(folder, cex.getMessage());
      } catch (ProtocolException pex) {
        forceCheckExpunged();
        throw new MessagingException(pex.getMessage(), pex);
      }
      return robj;
    }
  }
  











  public synchronized Object getItem(FetchItem fitem)
    throws MessagingException
  {
    Object item = items == null ? null : items.get(fitem.getName());
    if (item == null)
      item = fetchItem(fitem);
    return item;
  }
  

  private synchronized void loadEnvelope()
    throws MessagingException
  {
    if (envelope != null) {
      return;
    }
    Response[] r = null;
    

    synchronized (getMessageCacheLock()) {
      try {
        IMAPProtocol p = getProtocol();
        
        checkExpunged();
        
        int seqnum = getSequenceNumber();
        r = p.fetch(seqnum, "ENVELOPE INTERNALDATE RFC822.SIZE");
        
        for (int i = 0; i < r.length; i++)
        {

          if ((r[i] != null) && ((r[i] instanceof FetchResponse)))
          {
            if (((FetchResponse)r[i]).getNumber() == seqnum)
            {

              FetchResponse f = (FetchResponse)r[i];
              

              int count = f.getItemCount();
              for (int j = 0; j < count; j++) {
                Item item = f.getItem(j);
                
                if ((item instanceof ENVELOPE)) {
                  envelope = ((ENVELOPE)item);
                } else if ((item instanceof INTERNALDATE)) {
                  receivedDate = ((INTERNALDATE)item).getDate();
                } else if ((item instanceof RFC822SIZE))
                  size = size;
              }
            }
          }
        }
        p.notifyResponseHandlers(r);
        p.handleResult(r[(r.length - 1)]);
      } catch (ConnectionException cex) {
        throw new FolderClosedException(folder, cex.getMessage());
      } catch (ProtocolException pex) {
        forceCheckExpunged();
        throw new MessagingException(pex.getMessage(), pex);
      }
    }
    

    if (envelope == null) {
      throw new MessagingException("Failed to load IMAP envelope");
    }
  }
  

  private synchronized void loadBODYSTRUCTURE()
    throws MessagingException
  {
    if (bs != null) {
      return;
    }
    
    synchronized (getMessageCacheLock()) {
      try {
        IMAPProtocol p = getProtocol();
        


        checkExpunged();
        
        bs = p.fetchBodyStructure(getSequenceNumber());
      } catch (ConnectionException cex) {
        throw new FolderClosedException(folder, cex.getMessage());
      } catch (ProtocolException pex) {
        forceCheckExpunged();
        throw new MessagingException(pex.getMessage(), pex);
      }
      if (bs == null)
      {


        forceCheckExpunged();
        throw new MessagingException("Unable to load BODYSTRUCTURE");
      }
    }
  }
  

  private synchronized void loadHeaders()
    throws MessagingException
  {
    if (headersLoaded) {
      return;
    }
    InputStream is = null;
    

    synchronized (getMessageCacheLock()) {
      try {
        IMAPProtocol p = getProtocol();
        


        checkExpunged();
        
        if (p.isREV1()) {
          BODY b = p.peekBody(getSequenceNumber(), 
            toSection("HEADER"));
          if (b != null)
            is = b.getByteArrayInputStream();
        } else {
          RFC822DATA rd = p.fetchRFC822(getSequenceNumber(), "HEADER");
          
          if (rd != null)
            is = rd.getByteArrayInputStream();
        }
      } catch (ConnectionException cex) {
        throw new FolderClosedException(folder, cex.getMessage());
      } catch (ProtocolException pex) {
        forceCheckExpunged();
        throw new MessagingException(pex.getMessage(), pex);
      }
    }
    
    if (is == null)
      throw new MessagingException("Cannot load header");
    headers = new InternetHeaders(is);
    headersLoaded = true;
  }
  

  private synchronized void loadFlags()
    throws MessagingException
  {
    if (flags != null) {
      return;
    }
    
    synchronized (getMessageCacheLock()) {
      try {
        IMAPProtocol p = getProtocol();
        


        checkExpunged();
        
        flags = p.fetchFlags(getSequenceNumber());
        
        if (flags == null)
          flags = new Flags();
      } catch (ConnectionException cex) {
        throw new FolderClosedException(folder, cex.getMessage());
      } catch (ProtocolException pex) {
        forceCheckExpunged();
        throw new MessagingException(pex.getMessage(), pex);
      }
    }
  }
  


  private boolean areHeadersLoaded()
  {
    return headersLoaded;
  }
  


  private void setHeadersLoaded(boolean loaded)
  {
    headersLoaded = loaded;
  }
  


  private boolean isHeaderLoaded(String name)
  {
    if (headersLoaded) {
      return true;
    }
    return loadedHeaders.containsKey(name.toUpperCase(Locale.ENGLISH));
  }
  


  private void setHeaderLoaded(String name)
  {
    loadedHeaders.put(name.toUpperCase(Locale.ENGLISH), name);
  }
  



  private String toSection(String what)
  {
    if (sectionId == null) {
      return what;
    }
    return sectionId + "." + what;
  }
  


  private InternetAddress[] aaclone(InternetAddress[] aa)
  {
    if (aa == null) {
      return null;
    }
    return (InternetAddress[])aa.clone();
  }
  
  private Flags _getFlags() {
    return flags;
  }
  
  private ENVELOPE _getEnvelope() {
    return envelope;
  }
  
  private BODYSTRUCTURE _getBodyStructure() {
    return bs;
  }
  








  void _setFlags(Flags flags)
  {
    this.flags = flags;
  }
  


  Session _getSession()
  {
    return session;
  }
}
