package com.sun.mail.imap;

import java.util.Date;
import javax.mail.Message;
import javax.mail.search.SearchTerm;





















































public final class OlderTerm
  extends SearchTerm
{
  private int interval;
  private static final long serialVersionUID = 3951078948727995682L;
  
  public OlderTerm(int interval)
  {
    this.interval = interval;
  }
  




  public int getInterval()
  {
    return interval;
  }
  








  public boolean match(Message msg)
  {
    try
    {
      d = msg.getReceivedDate();
    } catch (Exception e) { Date d;
      return false;
    }
    Date d;
    if (d == null) {
      return false;
    }
    return 
      d.getTime() <= System.currentTimeMillis() - interval * 1000L;
  }
  



  public boolean equals(Object obj)
  {
    if (!(obj instanceof OlderTerm))
      return false;
    return interval == interval;
  }
  



  public int hashCode()
  {
    return interval;
  }
}
