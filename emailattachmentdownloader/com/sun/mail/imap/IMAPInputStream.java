package com.sun.mail.imap;

import com.sun.mail.iap.ByteArray;
import com.sun.mail.iap.ConnectionException;
import com.sun.mail.iap.ProtocolException;
import com.sun.mail.imap.protocol.BODY;
import com.sun.mail.imap.protocol.IMAPProtocol;
import com.sun.mail.util.FolderClosedIOException;
import com.sun.mail.util.MessageRemovedIOException;
import java.io.IOException;
import java.io.InputStream;
import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.FolderClosedException;
import javax.mail.MessagingException;



















































public class IMAPInputStream
  extends InputStream
{
  private IMAPMessage msg;
  private String section;
  private int pos;
  private int blksize;
  private int max;
  private byte[] buf;
  private int bufcount;
  private int bufpos;
  private boolean lastBuffer;
  private boolean peek;
  private ByteArray readbuf;
  private static final int slop = 64;
  
  public IMAPInputStream(IMAPMessage msg, String section, int max, boolean peek)
  {
    this.msg = msg;
    this.section = section;
    this.max = max;
    this.peek = peek;
    pos = 0;
    blksize = msg.getFetchBlockSize();
  }
  



  private void forceCheckExpunged()
    throws MessageRemovedIOException, FolderClosedIOException
  {
    synchronized (msg.getMessageCacheLock()) {
      try {
        msg.getProtocol().noop();
      }
      catch (ConnectionException cex) {
        throw new FolderClosedIOException(msg.getFolder(), cex.getMessage());
      }
      catch (FolderClosedException fex) {
        throw new FolderClosedIOException(fex.getFolder(), fex.getMessage());
      }
      catch (ProtocolException localProtocolException) {}
    }
    
    if (msg.isExpunged()) {
      throw new MessageRemovedIOException();
    }
  }
  






  private void fill()
    throws IOException
  {
    if ((lastBuffer) || ((max != -1) && (pos >= max))) {
      if (pos == 0)
        checkSeen();
      readbuf = null;
      return;
    }
    
    BODY b = null;
    if (readbuf == null) {
      readbuf = new ByteArray(blksize + 64);
    }
    
    ByteArray ba;
    
    synchronized (msg.getMessageCacheLock()) {
      try {
        IMAPProtocol p = msg.getProtocol();
        

        if (msg.isExpunged()) {
          throw new MessageRemovedIOException("No content for expunged message");
        }
        
        int seqnum = msg.getSequenceNumber();
        int cnt = blksize;
        if ((max != -1) && (pos + blksize > max))
          cnt = max - pos;
        if (peek) {
          b = p.peekBody(seqnum, section, pos, cnt, readbuf);
        } else
          b = p.fetchBody(seqnum, section, pos, cnt, readbuf);
      } catch (ProtocolException pex) {
        forceCheckExpunged();
        throw new IOException(pex.getMessage());
      }
      catch (FolderClosedException fex) {
        throw new FolderClosedIOException(fex.getFolder(), fex.getMessage()); }
      int cnt;
      ByteArray ba;
      if ((b == null) || ((ba = b.getByteArray()) == null)) {
        forceCheckExpunged();
        



        ba = new ByteArray(0);
      }
    }
    int cnt;
    ByteArray ba;
    if (pos == 0) {
      checkSeen();
    }
    
    buf = ba.getBytes();
    bufpos = ba.getStart();
    int n = ba.getCount();
    

    lastBuffer = (n < cnt);
    bufcount = (bufpos + n);
    pos += n;
  }
  



  public synchronized int read()
    throws IOException
  {
    if (bufpos >= bufcount) {
      fill();
      if (bufpos >= bufcount)
        return -1;
    }
    return buf[(bufpos++)] & 0xFF;
  }
  















  public synchronized int read(byte[] b, int off, int len)
    throws IOException
  {
    int avail = bufcount - bufpos;
    if (avail <= 0) {
      fill();
      avail = bufcount - bufpos;
      if (avail <= 0)
        return -1;
    }
    int cnt = avail < len ? avail : len;
    System.arraycopy(buf, bufpos, b, off, cnt);
    bufpos += cnt;
    return cnt;
  }
  













  public int read(byte[] b)
    throws IOException
  {
    return read(b, 0, b.length);
  }
  



  public synchronized int available()
    throws IOException
  {
    return bufcount - bufpos;
  }
  






  private void checkSeen()
  {
    if (peek)
      return;
    try {
      Folder f = msg.getFolder();
      if ((f != null) && (f.getMode() != 1) && 
        (!msg.isSet(Flags.Flag.SEEN))) {
        msg.setFlag(Flags.Flag.SEEN, true);
      }
    }
    catch (MessagingException localMessagingException) {}
  }
}
