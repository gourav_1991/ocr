package com.sun.mail.imap;

import com.sun.mail.util.MailLogger;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.Socket;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;
import java.util.logging.Level;
import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.URLName;

















































































































public class IdleManager
{
  private Executor es;
  private Selector selector;
  private MailLogger logger;
  private volatile boolean die = false;
  private volatile boolean running;
  private Queue<IMAPFolder> toWatch = new ConcurrentLinkedQueue();
  private Queue<IMAPFolder> toAbort = new ConcurrentLinkedQueue();
  







  public IdleManager(Session session, Executor es)
    throws IOException
  {
    this.es = es;
    logger = new MailLogger(getClass(), "DEBUG IMAP", session);
    selector = Selector.open();
    es.execute(new Runnable()
    {
      public void run() {
        logger.fine("IdleManager select starting");
        try {
          running = true;
          IdleManager.this.select();
          
          running = false;
          logger.fine("IdleManager select terminating");
        }
        finally
        {
          running = false;
          logger.fine("IdleManager select terminating");
        }
      }
    });
  }
  









  public boolean isRunning()
  {
    return running;
  }
  






  public void watch(Folder folder)
    throws MessagingException
  {
    if (die)
      throw new MessagingException("IdleManager is not running");
    if (!(folder instanceof IMAPFolder))
      throw new MessagingException("Can only watch IMAP folders");
    IMAPFolder ifolder = (IMAPFolder)folder;
    SocketChannel sc = ifolder.getChannel();
    if (sc == null) {
      if (folder.isOpen()) {
        throw new MessagingException("Folder is not using SocketChannels");
      }
      
      throw new MessagingException("Folder is not open");
    }
    if (logger.isLoggable(Level.FINEST)) {
      logger.log(Level.FINEST, "IdleManager watching {0}", 
        folderName(ifolder));
    }
    
    int tries = 0;
    while (!ifolder.startIdle(this)) {
      if (logger.isLoggable(Level.FINEST))
        logger.log(Level.FINEST, "IdleManager.watch startIdle failed for {0}", 
        
          folderName(ifolder));
      tries++;
    }
    if (logger.isLoggable(Level.FINEST)) {
      if (tries > 0) {
        logger.log(Level.FINEST, "IdleManager.watch startIdle succeeded for {0} after " + tries + " tries", 
        

          folderName(ifolder));
      } else
        logger.log(Level.FINEST, "IdleManager.watch startIdle succeeded for {0}", 
        
          folderName(ifolder));
    }
    synchronized (this) {
      toWatch.add(ifolder);
      selector.wakeup();
    }
  }
  







  void requestAbort(IMAPFolder folder)
  {
    toAbort.add(folder);
    selector.wakeup();
  }
  



  private void select()
  {
    die = false;
    try {
      while (!die) {
        watchAll();
        logger.finest("IdleManager waiting...");
        int ns = selector.select();
        if (logger.isLoggable(Level.FINEST))
          logger.log(Level.FINEST, "IdleManager selected {0} channels", 
            Integer.valueOf(ns));
        if ((die) || (Thread.currentThread().isInterrupted())) {
          break;
        }
        














        do
        {
          processKeys();
        } while ((selector.selectNow() > 0) || (!toAbort.isEmpty()));
      }
    } catch (InterruptedIOException ex) {
      logger.log(Level.FINEST, "IdleManager interrupted", ex);
    } catch (IOException ex) {
      logger.log(Level.FINEST, "IdleManager got I/O exception", ex);
    } catch (Exception ex) {
      logger.log(Level.FINEST, "IdleManager got exception", ex);
    } finally {
      die = true;
      logger.finest("IdleManager unwatchAll");
      try {
        unwatchAll();
        selector.close();
      }
      catch (IOException ex2) {
        logger.log(Level.FINEST, "IdleManager unwatch exception", ex2);
      }
      logger.fine("IdleManager exiting");
    }
  }
  




  private void watchAll()
  {
    IMAPFolder folder;
    


    while ((folder = (IMAPFolder)toWatch.poll()) != null) {
      if (logger.isLoggable(Level.FINEST))
        logger.log(Level.FINEST, "IdleManager adding {0} to selector", 
          folderName(folder));
      try {
        SocketChannel sc = folder.getChannel();
        if (sc != null)
        {

          sc.configureBlocking(false);
          sc.register(selector, 1, folder);
        }
      } catch (IOException ex) {
        logger.log(Level.FINEST, "IdleManager can't register folder", ex);
      }
      catch (CancelledKeyException ex)
      {
        logger.log(Level.FINEST, "IdleManager can't register folder", ex);
      }
    }
  }
  







  private void processKeys()
    throws IOException
  {
    Set<SelectionKey> selectedKeys = selector.selectedKeys();
    








    Iterator<SelectionKey> it = selectedKeys.iterator();
    while (it.hasNext()) {
      SelectionKey sk = (SelectionKey)it.next();
      it.remove();
      
      sk.cancel();
      IMAPFolder folder = (IMAPFolder)sk.attachment();
      if (logger.isLoggable(Level.FINEST))
        logger.log(Level.FINEST, "IdleManager selected folder: {0}", 
          folderName(folder));
      SelectableChannel sc = sk.channel();
      
      sc.configureBlocking(true);
      try {
        if (folder.handleIdle(false)) {
          if (logger.isLoggable(Level.FINEST)) {
            logger.log(Level.FINEST, "IdleManager continue watching folder {0}", 
            
              folderName(folder));
          }
          toWatch.add(folder);

        }
        else if (logger.isLoggable(Level.FINEST)) {
          logger.log(Level.FINEST, "IdleManager done watching folder {0}", 
          
            folderName(folder));
        }
      }
      catch (MessagingException ex) {
        logger.log(Level.FINEST, "IdleManager got exception for folder: " + 
        
          folderName(folder), ex);
      }
    }
    

    IMAPFolder folder;
    
    while ((folder = (IMAPFolder)toAbort.poll()) != null) {
      if (logger.isLoggable(Level.FINEST))
        logger.log(Level.FINEST, "IdleManager aborting IDLE for folder: {0}", 
        
          folderName(folder));
      SocketChannel sc = folder.getChannel();
      if (sc != null)
      {
        SelectionKey sk = sc.keyFor(selector);
        
        if (sk != null) {
          sk.cancel();
        }
        sc.configureBlocking(true);
        

        Socket sock = sc.socket();
        if ((sock != null) && (sock.getSoTimeout() > 0)) {
          logger.finest("IdleManager requesting DONE with timeout");
          toWatch.remove(folder);
          final IMAPFolder folder0 = folder;
          es.execute(new Runnable()
          {
            public void run()
            {
              folder0.idleAbortWait();
            }
          });
        } else {
          folder.idleAbort();
          

          toWatch.add(folder);
        }
      }
    }
  }
  





  private void unwatchAll()
  {
    Set<SelectionKey> keys = selector.keys();
    for (SelectionKey sk : keys)
    {
      sk.cancel();
      IMAPFolder folder = (IMAPFolder)sk.attachment();
      if (logger.isLoggable(Level.FINEST))
        logger.log(Level.FINEST, "IdleManager no longer watching folder: {0}", 
        
          folderName(folder));
      SelectableChannel sc = sk.channel();
      try
      {
        sc.configureBlocking(true);
        folder.idleAbortWait();
      }
      catch (IOException ex) {
        logger.log(Level.FINEST, "IdleManager exception while aborting idle for folder: " + 
        
          folderName(folder), ex);
      }
    }
    

    IMAPFolder folder;
    
    while ((folder = (IMAPFolder)toWatch.poll()) != null) {
      if (logger.isLoggable(Level.FINEST))
        logger.log(Level.FINEST, "IdleManager aborting IDLE for unwatched folder: {0}", 
        
          folderName(folder));
      SocketChannel sc = folder.getChannel();
      if (sc != null)
      {
        try
        {
          sc.configureBlocking(true);
          folder.idleAbortWait();
        }
        catch (IOException ex) {
          logger.log(Level.FINEST, "IdleManager exception while aborting idle for folder: " + 
          
            folderName(folder), ex);
        }
      }
    }
  }
  

  public synchronized void stop()
  {
    die = true;
    logger.fine("IdleManager stopping");
    selector.wakeup();
  }
  



  private static String folderName(Folder folder)
  {
    try
    {
      return folder.getURLName().toString();
    }
    catch (MessagingException mex) {}
    return folder.getStore().toString() + "/" + folder.toString();
  }
}
