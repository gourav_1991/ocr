package com.sun.mail.imap;

import com.sun.mail.iap.Literal;
import com.sun.mail.util.CRLFOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.mail.Message;
import javax.mail.MessagingException;
















































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































class MessageLiteral
  implements Literal
{
  private Message msg;
  private int msgSize = -1;
  private byte[] buf;
  
  public MessageLiteral(Message msg, int maxsize) throws MessagingException, IOException
  {
    this.msg = msg;
    
    LengthCounter lc = new LengthCounter(maxsize);
    OutputStream os = new CRLFOutputStream(lc);
    msg.writeTo(os);
    os.flush();
    msgSize = lc.getSize();
    buf = lc.getBytes();
  }
  
  public int size()
  {
    return msgSize;
  }
  
  public void writeTo(OutputStream os) throws IOException
  {
    try
    {
      if (buf != null) {
        os.write(buf, 0, msgSize);
      } else {
        os = new CRLFOutputStream(os);
        msg.writeTo(os);
      }
    }
    catch (MessagingException mex) {
      throw new IOException("MessagingException while appending message: " + mex);
    }
  }
}
