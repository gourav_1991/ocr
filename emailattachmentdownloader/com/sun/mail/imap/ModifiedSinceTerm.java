package com.sun.mail.imap;

import javax.mail.Message;
import javax.mail.search.SearchTerm;





















































public final class ModifiedSinceTerm
  extends SearchTerm
{
  private long modseq;
  private static final long serialVersionUID = 5151457469634727992L;
  
  public ModifiedSinceTerm(long modseq)
  {
    this.modseq = modseq;
  }
  




  public long getModSeq()
  {
    return modseq;
  }
  




  public boolean match(Message msg)
  {
    try
    {
      long m;
      


      if ((msg instanceof IMAPMessage)) {
        m = ((IMAPMessage)msg).getModSeq();
      } else
        return false;
    } catch (Exception e) { long m;
      return false;
    }
    long m;
    return m >= modseq;
  }
  



  public boolean equals(Object obj)
  {
    if (!(obj instanceof ModifiedSinceTerm))
      return false;
    return modseq == modseq;
  }
  



  public int hashCode()
  {
    return (int)modseq;
  }
}
