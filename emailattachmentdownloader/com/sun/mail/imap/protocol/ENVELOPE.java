package com.sun.mail.imap.protocol;

import com.sun.mail.iap.ParsingException;
import com.sun.mail.iap.Response;
import com.sun.mail.util.PropUtil;
import java.io.PrintStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MailDateFormat;

















































public class ENVELOPE
  implements Item
{
  static final char[] name = { 'E', 'N', 'V', 'E', 'L', 'O', 'P', 'E' };
  
  public int msgno;
  public Date date = null;
  
  public String subject;
  
  public InternetAddress[] from;
  public InternetAddress[] sender;
  public InternetAddress[] replyTo;
  public InternetAddress[] to;
  public InternetAddress[] cc;
  public InternetAddress[] bcc;
  public String inReplyTo;
  public String messageId;
  private static final MailDateFormat mailDateFormat = new MailDateFormat();
  


  private static final boolean parseDebug = PropUtil.getBooleanSystemProperty("mail.imap.parse.debug", false);
  
  public ENVELOPE(FetchResponse r) throws ParsingException {
    if (parseDebug)
      System.out.println("parse ENVELOPE");
    msgno = r.getNumber();
    
    r.skipSpaces();
    
    if (r.readByte() != 40) {
      throw new ParsingException("ENVELOPE parse error");
    }
    String s = r.readString();
    if (s != null) {
      try {
        synchronized (mailDateFormat) {
          date = mailDateFormat.parse(s);
        }
      }
      catch (ParseException localParseException) {}
    }
    if (parseDebug) {
      System.out.println("  Date: " + date);
    }
    subject = r.readString();
    if (parseDebug)
      System.out.println("  Subject: " + subject);
    if (parseDebug)
      System.out.println("  From addresses:");
    from = parseAddressList(r);
    if (parseDebug)
      System.out.println("  Sender addresses:");
    sender = parseAddressList(r);
    if (parseDebug)
      System.out.println("  Reply-To addresses:");
    replyTo = parseAddressList(r);
    if (parseDebug)
      System.out.println("  To addresses:");
    to = parseAddressList(r);
    if (parseDebug)
      System.out.println("  Cc addresses:");
    cc = parseAddressList(r);
    if (parseDebug)
      System.out.println("  Bcc addresses:");
    bcc = parseAddressList(r);
    inReplyTo = r.readString();
    if (parseDebug)
      System.out.println("  In-Reply-To: " + inReplyTo);
    messageId = r.readString();
    if (parseDebug) {
      System.out.println("  Message-ID: " + messageId);
    }
    if (!r.isNextNonSpace(')')) {
      throw new ParsingException("ENVELOPE parse error");
    }
  }
  
  private InternetAddress[] parseAddressList(Response r) throws ParsingException {
    r.skipSpaces();
    
    byte b = r.readByte();
    if (b == 40)
    {




      if (r.isNextNonSpace(')')) {
        return null;
      }
      List<InternetAddress> v = new ArrayList();
      do
      {
        IMAPAddress a = new IMAPAddress(r);
        if (parseDebug) {
          System.out.println("    Address: " + a);
        }
        if (!a.isEndOfGroup())
          v.add(a);
      } while (!r.isNextNonSpace(')'));
      
      return (InternetAddress[])v.toArray(new InternetAddress[v.size()]); }
    if ((b == 78) || (b == 110)) {
      r.skip(2);
      return null;
    }
    throw new ParsingException("ADDRESS parse error");
  }
}
