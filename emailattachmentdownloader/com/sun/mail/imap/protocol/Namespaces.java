package com.sun.mail.imap.protocol;

import com.sun.mail.iap.ProtocolException;
import com.sun.mail.iap.Response;
import java.util.ArrayList;
import java.util.List;



























































public class Namespaces
{
  public Namespace[] personal;
  public Namespace[] otherUsers;
  public Namespace[] shared;
  
  public static class Namespace
  {
    public String prefix;
    public char delimiter;
    
    public Namespace(Response r)
      throws ProtocolException
    {
      if (!r.isNextNonSpace('(')) {
        throw new ProtocolException("Missing '(' at start of Namespace");
      }
      
      prefix = r.readString();
      if (!r.supportsUtf8())
        prefix = BASE64MailboxDecoder.decode(prefix);
      r.skipSpaces();
      
      if (r.peekByte() == 34) {
        r.readByte();
        delimiter = ((char)r.readByte());
        if (delimiter == '\\')
          delimiter = ((char)r.readByte());
        if (r.readByte() != 34) {
          throw new ProtocolException("Missing '\"' at end of QUOTED_CHAR");
        }
      } else {
        String s = r.readAtom();
        if (s == null)
          throw new ProtocolException("Expected NIL, got null");
        if (!s.equalsIgnoreCase("NIL"))
          throw new ProtocolException("Expected NIL, got " + s);
        delimiter = '\000';
      }
      
      if (r.isNextNonSpace(')')) {
        return;
      }
      


      r.readString();
      r.skipSpaces();
      r.readStringList();
      if (!r.isNextNonSpace(')')) {
        throw new ProtocolException("Missing ')' at end of Namespace");
      }
    }
  }
  





















  public Namespaces(Response r)
    throws ProtocolException
  {
    personal = getNamespaces(r);
    otherUsers = getNamespaces(r);
    shared = getNamespaces(r);
  }
  


  private Namespace[] getNamespaces(Response r)
    throws ProtocolException
  {
    if (r.isNextNonSpace('(')) {
      List<Namespace> v = new ArrayList();
      do {
        Namespace ns = new Namespace(r);
        v.add(ns);
      } while (!r.isNextNonSpace(')'));
      return (Namespace[])v.toArray(new Namespace[v.size()]);
    }
    String s = r.readAtom();
    if (s == null)
      throw new ProtocolException("Expected NIL, got null");
    if (!s.equalsIgnoreCase("NIL"))
      throw new ProtocolException("Expected NIL, got " + s);
    return null;
  }
}
