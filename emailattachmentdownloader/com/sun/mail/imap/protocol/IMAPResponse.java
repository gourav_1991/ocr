package com.sun.mail.imap.protocol;

import com.sun.mail.iap.Protocol;
import com.sun.mail.iap.ProtocolException;
import com.sun.mail.iap.Response;
import com.sun.mail.util.ASCIIUtility;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;









































public class IMAPResponse
  extends Response
{
  private String key;
  private int number;
  
  public IMAPResponse(Protocol c)
    throws IOException, ProtocolException
  {
    super(c);
    init();
  }
  
  private void init() throws IOException, ProtocolException
  {
    if ((isUnTagged()) && (!isOK()) && (!isNO()) && (!isBAD()) && (!isBYE())) {
      key = readAtom();
      
      try
      {
        number = Integer.parseInt(key);
        key = readAtom();
      }
      catch (NumberFormatException localNumberFormatException) {}
    }
  }
  



  public IMAPResponse(IMAPResponse r)
  {
    super(r);
    key = key;
    number = number;
  }
  





  public IMAPResponse(String r)
    throws IOException, ProtocolException
  {
    this(r, true);
  }
  








  public IMAPResponse(String r, boolean utf8)
    throws IOException, ProtocolException
  {
    super(r, utf8);
    init();
  }
  









  public String[] readSimpleList()
  {
    skipSpaces();
    
    if (buffer[index] != 40)
      return null;
    index += 1;
    
    List<String> v = new ArrayList();
    
    for (int start = index; buffer[index] != 41; index += 1) {
      if (buffer[index] == 32) {
        v.add(ASCIIUtility.toString(buffer, start, index));
        start = index + 1;
      }
    }
    if (index > start)
      v.add(ASCIIUtility.toString(buffer, start, index));
    index += 1;
    
    int size = v.size();
    if (size > 0) {
      return (String[])v.toArray(new String[size]);
    }
    return null;
  }
  
  public String getKey() {
    return key;
  }
  
  public boolean keyEquals(String k) {
    if ((key != null) && (key.equalsIgnoreCase(k))) {
      return true;
    }
    return false;
  }
  
  public int getNumber() {
    return number;
  }
}
