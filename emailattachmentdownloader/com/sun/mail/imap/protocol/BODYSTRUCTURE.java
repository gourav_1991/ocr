package com.sun.mail.imap.protocol;

import com.sun.mail.iap.ParsingException;
import com.sun.mail.iap.Response;
import com.sun.mail.util.PropUtil;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import javax.mail.internet.ParameterList;













































public class BODYSTRUCTURE
  implements Item
{
  static final char[] name = { 'B', 'O', 'D', 'Y', 'S', 'T', 'R', 'U', 'C', 'T', 'U', 'R', 'E' };
  
  public int msgno;
  
  public String type;
  public String subtype;
  public String encoding;
  public int lines = -1;
  public int size = -1;
  
  public String disposition;
  
  public String id;
  public String description;
  public String md5;
  public String attachment;
  public ParameterList cParams;
  public ParameterList dParams;
  public String[] language;
  public BODYSTRUCTURE[] bodies;
  public ENVELOPE envelope;
  private static int SINGLE = 1;
  private static int MULTI = 2;
  private static int NESTED = 3;
  

  private int processedType;
  
  private static final boolean parseDebug = PropUtil.getBooleanSystemProperty("mail.imap.parse.debug", false);
  
  public BODYSTRUCTURE(FetchResponse r) throws ParsingException
  {
    if (parseDebug)
      System.out.println("DEBUG IMAP: parsing BODYSTRUCTURE");
    msgno = r.getNumber();
    if (parseDebug) {
      System.out.println("DEBUG IMAP: msgno " + msgno);
    }
    r.skipSpaces();
    
    if (r.readByte() != 40) {
      throw new ParsingException("BODYSTRUCTURE parse error: missing ``('' at start");
    }
    
    if (r.peekByte() == 40) {
      if (parseDebug)
        System.out.println("DEBUG IMAP: parsing multipart");
      type = "multipart";
      processedType = MULTI;
      List<BODYSTRUCTURE> v = new ArrayList(1);
      int i = 1;
      do {
        v.add(new BODYSTRUCTURE(r));
        





        r.skipSpaces();
      } while (r.peekByte() == 40);
      

      bodies = ((BODYSTRUCTURE[])v.toArray(new BODYSTRUCTURE[v.size()]));
      
      subtype = r.readString();
      if (parseDebug) {
        System.out.println("DEBUG IMAP: subtype " + subtype);
      }
      if (r.isNextNonSpace(')')) {
        if (parseDebug)
          System.out.println("DEBUG IMAP: parse DONE");
        return;
      }
      


      if (parseDebug) {
        System.out.println("DEBUG IMAP: parsing extension data");
      }
      cParams = parseParameters(r);
      if (r.isNextNonSpace(')')) {
        if (parseDebug)
          System.out.println("DEBUG IMAP: body parameters DONE");
        return;
      }
      

      byte b = r.readByte();
      if (b == 40) {
        if (parseDebug)
          System.out.println("DEBUG IMAP: parse disposition");
        disposition = r.readString();
        if (parseDebug) {
          System.out.println("DEBUG IMAP: disposition " + disposition);
        }
        dParams = parseParameters(r);
        if (!r.isNextNonSpace(')')) {
          throw new ParsingException("BODYSTRUCTURE parse error: missing ``)'' at end of disposition in multipart");
        }
        
        if (parseDebug)
          System.out.println("DEBUG IMAP: disposition DONE");
      } else if ((b == 78) || (b == 110)) {
        if (parseDebug)
          System.out.println("DEBUG IMAP: disposition NIL");
        r.skip(2);
      } else {
        throw new ParsingException("BODYSTRUCTURE parse error: " + type + "/" + subtype + ": bad multipart disposition, b " + b);
      }
      





      if (r.isNextNonSpace(')')) {
        if (parseDebug)
          System.out.println("DEBUG IMAP: no body-fld-lang");
        return;
      }
      

      if (r.peekByte() == 40) {
        language = r.readStringList();
        if (parseDebug) {
          System.out.println("DEBUG IMAP: language len " + language.length);
        }
      } else {
        String l = r.readString();
        if (l != null) {
          String[] la = { l };
          language = la;
          if (parseDebug) {
            System.out.println("DEBUG IMAP: language " + l);
          }
        }
      }
      



      while (r.readByte() == 32)
        parseBodyExtension(r);
    } else { if (r.peekByte() == 41)
      {

















        throw new ParsingException("BODYSTRUCTURE parse error: missing body content");
      }
      
      if (parseDebug)
        System.out.println("DEBUG IMAP: single part");
      type = r.readString();
      if (parseDebug)
        System.out.println("DEBUG IMAP: type " + type);
      processedType = SINGLE;
      subtype = r.readString();
      if (parseDebug) {
        System.out.println("DEBUG IMAP: subtype " + subtype);
      }
      
      if (type == null) {
        type = "application";
        subtype = "octet-stream";
      }
      cParams = parseParameters(r);
      if (parseDebug)
        System.out.println("DEBUG IMAP: cParams " + cParams);
      id = r.readString();
      if (parseDebug)
        System.out.println("DEBUG IMAP: id " + id);
      description = r.readString();
      if (parseDebug) {
        System.out.println("DEBUG IMAP: description " + description);
      }
      


      encoding = r.readAtomString();
      if ((encoding != null) && (encoding.equalsIgnoreCase("NIL")))
        encoding = null;
      if (parseDebug)
        System.out.println("DEBUG IMAP: encoding " + encoding);
      size = r.readNumber();
      if (parseDebug)
        System.out.println("DEBUG IMAP: size " + size);
      if (size < 0) {
        throw new ParsingException("BODYSTRUCTURE parse error: bad ``size'' element");
      }
      

      if (type.equalsIgnoreCase("text")) {
        lines = r.readNumber();
        if (parseDebug)
          System.out.println("DEBUG IMAP: lines " + lines);
        if (lines < 0) {
          throw new ParsingException("BODYSTRUCTURE parse error: bad ``lines'' element");
        }
      } else if ((type.equalsIgnoreCase("message")) && 
        (subtype.equalsIgnoreCase("rfc822")))
      {
        processedType = NESTED;
        



        r.skipSpaces();
        if (r.peekByte() == 40) {
          envelope = new ENVELOPE(r);
          if (parseDebug) {
            System.out.println("DEBUG IMAP: got envelope of nested message");
          }
          BODYSTRUCTURE[] bs = { new BODYSTRUCTURE(r) };
          bodies = bs;
          lines = r.readNumber();
          if (parseDebug)
            System.out.println("DEBUG IMAP: lines " + lines);
          if (lines < 0) {
            throw new ParsingException("BODYSTRUCTURE parse error: bad ``lines'' element");
          }
        }
        else if (parseDebug) {
          System.out.println("DEBUG IMAP: missing envelope and body of nested message");
        }
      }
      else
      {
        r.skipSpaces();
        byte bn = r.peekByte();
        if (Character.isDigit((char)bn)) {
          throw new ParsingException("BODYSTRUCTURE parse error: server erroneously included ``lines'' element with type " + type + "/" + subtype);
        }
      }
      


      if (r.isNextNonSpace(')')) {
        if (parseDebug)
          System.out.println("DEBUG IMAP: parse DONE");
        return;
      }
      



      md5 = r.readString();
      if (r.isNextNonSpace(')')) {
        if (parseDebug)
          System.out.println("DEBUG IMAP: no MD5 DONE");
        return;
      }
      

      byte b = r.readByte();
      if (b == 40) {
        disposition = r.readString();
        if (parseDebug) {
          System.out.println("DEBUG IMAP: disposition " + disposition);
        }
        dParams = parseParameters(r);
        if (parseDebug)
          System.out.println("DEBUG IMAP: dParams " + dParams);
        if (!r.isNextNonSpace(')')) {
          throw new ParsingException("BODYSTRUCTURE parse error: missing ``)'' at end of disposition");
        }
      }
      else if ((b == 78) || (b == 110)) {
        if (parseDebug)
          System.out.println("DEBUG IMAP: disposition NIL");
        r.skip(2);
      } else {
        throw new ParsingException("BODYSTRUCTURE parse error: " + type + "/" + subtype + ": bad single part disposition, b " + b);
      }
      



      if (r.isNextNonSpace(')')) {
        if (parseDebug)
          System.out.println("DEBUG IMAP: disposition DONE");
        return;
      }
      

      if (r.peekByte() == 40) {
        language = r.readStringList();
        if (parseDebug) {
          System.out.println("DEBUG IMAP: language len " + language.length);
        }
      } else {
        String l = r.readString();
        if (l != null) {
          String[] la = { l };
          language = la;
          if (parseDebug) {
            System.out.println("DEBUG IMAP: language " + l);
          }
        }
      }
      



      while (r.readByte() == 32)
        parseBodyExtension(r);
      if (parseDebug)
        System.out.println("DEBUG IMAP: all DONE");
    }
  }
  
  public boolean isMulti() {
    return processedType == MULTI;
  }
  
  public boolean isSingle() {
    return processedType == SINGLE;
  }
  
  public boolean isNested() {
    return processedType == NESTED;
  }
  
  private ParameterList parseParameters(Response r) throws ParsingException
  {
    r.skipSpaces();
    
    ParameterList list = null;
    byte b = r.readByte();
    if (b == 40) {
      list = new ParameterList();
      do {
        String name = r.readString();
        if (parseDebug)
          System.out.println("DEBUG IMAP: parameter name " + name);
        if (name == null) {
          throw new ParsingException("BODYSTRUCTURE parse error: " + type + "/" + subtype + ": null name in parameter list");
        }
        

        String value = r.readString();
        if (parseDebug)
          System.out.println("DEBUG IMAP: parameter value " + value);
        if (value == null)
          value = "";
        list.set(name, value);
      } while (!r.isNextNonSpace(')'));
      list.combineSegments();
    } else if ((b == 78) || (b == 110)) {
      if (parseDebug)
        System.out.println("DEBUG IMAP: parameter list NIL");
      r.skip(2);
    } else {
      throw new ParsingException("Parameter list parse error");
    }
    return list;
  }
  
  private void parseBodyExtension(Response r) throws ParsingException {
    r.skipSpaces();
    
    byte b = r.peekByte();
    if (b == 40) {
      r.skip(1);
      do {
        parseBodyExtension(r);
      } while (!r.isNextNonSpace(')'));
    } else if (Character.isDigit((char)b)) {
      r.readNumber();
    } else {
      r.readString();
    }
  }
}
