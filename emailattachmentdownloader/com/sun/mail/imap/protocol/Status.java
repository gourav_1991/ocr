package com.sun.mail.imap.protocol;

import com.sun.mail.iap.ParsingException;
import com.sun.mail.iap.Response;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;















































public class Status
{
  public String mbox = null;
  public int total = -1;
  public int recent = -1;
  public long uidnext = -1L;
  public long uidvalidity = -1L;
  public int unseen = -1;
  public long highestmodseq = -1L;
  
  public Map<String, Long> items;
  static final String[] standardItems = { "MESSAGES", "RECENT", "UNSEEN", "UIDNEXT", "UIDVALIDITY" };
  
  public Status(Response r)
    throws ParsingException
  {
    mbox = r.readAtomString();
    if (!r.supportsUtf8()) {
      mbox = BASE64MailboxDecoder.decode(mbox);
    }
    

    StringBuffer buffer = new StringBuffer();
    boolean onlySpaces = true;
    
    while ((r.peekByte() != 40) && (r.peekByte() != 0)) {
      char next = (char)r.readByte();
      
      buffer.append(next);
      
      if (next != ' ') {
        onlySpaces = false;
      }
    }
    
    if (!onlySpaces) {
      mbox = (mbox + buffer).trim();
    }
    
    if (r.readByte() != 40) {
      throw new ParsingException("parse error in STATUS");
    }
    do {
      String attr = r.readAtom();
      if (attr == null)
        throw new ParsingException("parse error in STATUS");
      if (attr.equalsIgnoreCase("MESSAGES")) {
        total = r.readNumber();
      } else if (attr.equalsIgnoreCase("RECENT")) {
        recent = r.readNumber();
      } else if (attr.equalsIgnoreCase("UIDNEXT")) {
        uidnext = r.readLong();
      } else if (attr.equalsIgnoreCase("UIDVALIDITY")) {
        uidvalidity = r.readLong();
      } else if (attr.equalsIgnoreCase("UNSEEN")) {
        unseen = r.readNumber();
      } else if (attr.equalsIgnoreCase("HIGHESTMODSEQ")) {
        highestmodseq = r.readLong();
      } else {
        if (items == null)
          items = new HashMap();
        items.put(attr.toUpperCase(Locale.ENGLISH), 
          Long.valueOf(r.readLong()));
      }
    } while (!r.isNextNonSpace(')'));
  }
  






  public long getItem(String item)
  {
    item = item.toUpperCase(Locale.ENGLISH);
    
    long ret = -1L;
    Long v; if ((items != null) && ((v = (Long)items.get(item)) != null)) {
      ret = v.longValue();
    } else if (item.equals("MESSAGES")) {
      ret = total;
    } else if (item.equals("RECENT")) {
      ret = recent;
    } else if (item.equals("UIDNEXT")) {
      ret = uidnext;
    } else if (item.equals("UIDVALIDITY")) {
      ret = uidvalidity;
    } else if (item.equals("UNSEEN")) {
      ret = unseen;
    } else if (item.equals("HIGHESTMODSEQ"))
      ret = highestmodseq;
    return ret;
  }
  
  public static void add(Status s1, Status s2) {
    if (total != -1)
      total = total;
    if (recent != -1)
      recent = recent;
    if (uidnext != -1L)
      uidnext = uidnext;
    if (uidvalidity != -1L)
      uidvalidity = uidvalidity;
    if (unseen != -1)
      unseen = unseen;
    if (highestmodseq != -1L)
      highestmodseq = highestmodseq;
    if (items == null) {
      items = items;
    } else if (items != null) {
      items.putAll(items);
    }
  }
}
