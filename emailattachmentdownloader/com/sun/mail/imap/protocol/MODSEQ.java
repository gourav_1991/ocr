package com.sun.mail.imap.protocol;

import com.sun.mail.iap.ParsingException;















































public class MODSEQ
  implements Item
{
  static final char[] name = { 'M', 'O', 'D', 'S', 'E', 'Q' };
  

  public int seqnum;
  

  public long modseq;
  

  public MODSEQ(FetchResponse r)
    throws ParsingException
  {
    seqnum = r.getNumber();
    r.skipSpaces();
    
    if (r.readByte() != 40) {
      throw new ParsingException("MODSEQ parse error");
    }
    modseq = r.readLong();
    
    if (!r.isNextNonSpace(')')) {
      throw new ParsingException("MODSEQ parse error");
    }
  }
}
