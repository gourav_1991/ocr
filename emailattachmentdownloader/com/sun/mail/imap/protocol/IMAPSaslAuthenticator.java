package com.sun.mail.imap.protocol;

import com.sun.mail.auth.OAuth2SaslClientFactory;
import com.sun.mail.iap.Argument;
import com.sun.mail.iap.ByteArray;
import com.sun.mail.iap.ProtocolException;
import com.sun.mail.iap.Response;
import com.sun.mail.util.ASCIIUtility;
import com.sun.mail.util.BASE64DecoderStream;
import com.sun.mail.util.BASE64EncoderStream;
import com.sun.mail.util.MailLogger;
import com.sun.mail.util.PropUtil;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.sasl.RealmCallback;
import javax.security.sasl.RealmChoiceCallback;
import javax.security.sasl.Sasl;
import javax.security.sasl.SaslClient;
import javax.security.sasl.SaslException;









































public class IMAPSaslAuthenticator
  implements SaslAuthenticator
{
  private IMAPProtocol pr;
  private String name;
  private Properties props;
  private MailLogger logger;
  private String host;
  
  public IMAPSaslAuthenticator(IMAPProtocol pr, String name, Properties props, MailLogger logger, String host)
  {
    this.pr = pr;
    this.name = name;
    this.props = props;
    this.logger = logger;
    this.host = host;
  }
  


  public boolean authenticate(String[] mechs, final String realm, String authzid, final String u, final String p)
    throws ProtocolException
  {
    synchronized (pr) {
      List<Response> v = new ArrayList();
      String tag = null;
      Response r = null;
      boolean done = false;
      if (logger.isLoggable(Level.FINE)) {
        logger.fine("SASL Mechanisms:");
        for (int i = 0; i < mechs.length; i++)
          logger.fine(" " + mechs[i]);
        logger.fine("");
      }
      

      CallbackHandler cbh = new CallbackHandler()
      {
        public void handle(Callback[] callbacks) {
          if (logger.isLoggable(Level.FINE))
            logger.fine("SASL callback length: " + callbacks.length);
          for (int i = 0; i < callbacks.length; i++) {
            if (logger.isLoggable(Level.FINE))
              logger.fine("SASL callback " + i + ": " + callbacks[i]);
            if ((callbacks[i] instanceof NameCallback)) {
              NameCallback ncb = (NameCallback)callbacks[i];
              ncb.setName(u);
            } else if ((callbacks[i] instanceof PasswordCallback)) {
              PasswordCallback pcb = (PasswordCallback)callbacks[i];
              pcb.setPassword(p.toCharArray());
            } else if ((callbacks[i] instanceof RealmCallback)) {
              RealmCallback rcb = (RealmCallback)callbacks[i];
              rcb.setText(realm != null ? realm : rcb
                .getDefaultText());
            } else if ((callbacks[i] instanceof RealmChoiceCallback)) {
              RealmChoiceCallback rcb = (RealmChoiceCallback)callbacks[i];
              
              if (realm == null) {
                rcb.setSelectedIndex(rcb.getDefaultChoice());
              }
              else {
                String[] choices = rcb.getChoices();
                for (int k = 0; k < choices.length; k++) {
                  if (choices[k].equals(realm)) {
                    rcb.setSelectedIndex(k);
                    break;
                  }
                }
              }
            }
          }
        }
      };
      
      try
      {
        Map<String, ?> propsMap = props;
        sc = Sasl.createSaslClient(mechs, authzid, name, host, propsMap, cbh);
      } catch (SaslException sex) {
        SaslClient sc;
        logger.log(Level.FINE, "Failed to create SASL client", sex);
        throw new UnsupportedOperationException(sex.getMessage(), sex); }
      SaslClient sc;
      if (sc == null) {
        logger.fine("No SASL support");
        throw new UnsupportedOperationException("No SASL support");
      }
      if (logger.isLoggable(Level.FINE)) {
        logger.fine("SASL client " + sc.getMechanismName());
      }
      try {
        Argument args = new Argument();
        args.writeAtom(sc.getMechanismName());
        if ((pr.hasCapability("SASL-IR")) && (sc.hasInitialResponse()))
        {
          byte[] ba = sc.evaluateChallenge(new byte[0]);
          String irs; String irs; if (ba.length > 0) {
            ba = BASE64EncoderStream.encode(ba);
            irs = ASCIIUtility.toString(ba, 0, ba.length);
          } else {
            irs = "="; }
          args.writeAtom(irs);
        }
        tag = pr.writeCommand("AUTHENTICATE", args);
      } catch (Exception ex) {
        logger.log(Level.FINE, "SASL AUTHENTICATE Exception", ex);
        return false;
      }
      
      OutputStream os = pr.getIMAPOutputStream();
      













      ByteArrayOutputStream bos = new ByteArrayOutputStream();
      byte[] CRLF = { 13, 10 };
      





      boolean isXGWTRUSTEDAPP = (sc.getMechanismName().equals("XGWTRUSTEDAPP")) && (PropUtil.getBooleanProperty(props, "mail." + name + ".sasl.xgwtrustedapphack.enable", true));
      
      while (!done) {
        try {
          r = pr.readResponse();
          if (r.isContinuation()) {
            byte[] ba = null;
            if (!sc.isComplete()) {
              ba = r.readByteArray().getNewBytes();
              if (ba.length > 0)
                ba = BASE64DecoderStream.decode(ba);
              if (logger.isLoggable(Level.FINE))
                logger.fine("SASL challenge: " + 
                  ASCIIUtility.toString(ba, 0, ba.length) + " :");
              ba = sc.evaluateChallenge(ba);
            }
            if (ba == null) {
              logger.fine("SASL no response");
              os.write(CRLF);
              os.flush();
              bos.reset();
            } else {
              if (logger.isLoggable(Level.FINE))
                logger.fine("SASL response: " + 
                  ASCIIUtility.toString(ba, 0, ba.length) + " :");
              ba = BASE64EncoderStream.encode(ba);
              if (isXGWTRUSTEDAPP)
                bos.write(ASCIIUtility.getBytes("XGWTRUSTEDAPP "));
              bos.write(ba);
              
              bos.write(CRLF);
              os.write(bos.toByteArray());
              os.flush();
              bos.reset();
            }
          } else if ((r.isTagged()) && (r.getTag().equals(tag)))
          {
            done = true;
          } else if (r.isBYE()) {
            done = true;
          } else {
            v.add(r);
          }
        } catch (Exception ioex) { logger.log(Level.FINE, "SASL Exception", ioex);
          
          r = Response.byeResponse(ioex);
          done = true;
        }
      }
      

      if (sc.isComplete()) {
        String qop = (String)sc.getNegotiatedProperty("javax.security.sasl.qop");
        if ((qop != null) && ((qop.equalsIgnoreCase("auth-int")) || 
          (qop.equalsIgnoreCase("auth-conf"))))
        {
          logger.fine("SASL Mechanism requires integrity or confidentiality");
          
          return false;
        }
      }
      
      Response[] responses = (Response[])v.toArray(new Response[v.size()]);
      

      pr.handleCapabilityResponse(responses);
      







      pr.notifyResponseHandlers(responses);
      

      pr.handleLoginResult(r);
      pr.setCapabilities(r);
      





      if ((isXGWTRUSTEDAPP) && (authzid != null)) {
        Argument args = new Argument();
        args.writeString(authzid);
        
        responses = pr.command("LOGIN", args);
        

        pr.notifyResponseHandlers(responses);
        

        pr.handleResult(responses[(responses.length - 1)]);
        
        pr.setCapabilities(responses[(responses.length - 1)]);
      }
      return true;
    }
  }
  
  static
  {
    try
    {
      
    }
    catch (Throwable localThrowable) {}
  }
}
