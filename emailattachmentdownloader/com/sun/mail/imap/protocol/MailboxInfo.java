package com.sun.mail.imap.protocol;

import com.sun.mail.iap.ParsingException;
import com.sun.mail.iap.Response;
import java.util.ArrayList;
import java.util.List;
import javax.mail.Flags;

















































public class MailboxInfo
{
  public Flags availableFlags = null;
  
  public Flags permanentFlags = null;
  
  public int total = -1;
  
  public int recent = -1;
  
  public int first = -1;
  
  public long uidvalidity = -1L;
  
  public long uidnext = -1L;
  
  public boolean uidNotSticky = false;
  
  public long highestmodseq = -1L;
  


  public int mode;
  

  public List<IMAPResponse> responses;
  


  public MailboxInfo(Response[] r)
    throws ParsingException
  {
    for (int i = 0; i < r.length; i++) {
      if ((r[i] != null) && ((r[i] instanceof IMAPResponse)))
      {

        IMAPResponse ir = (IMAPResponse)r[i];
        
        if (ir.keyEquals("EXISTS")) {
          total = ir.getNumber();
          r[i] = null;
        } else if (ir.keyEquals("RECENT")) {
          recent = ir.getNumber();
          r[i] = null;
        } else if (ir.keyEquals("FLAGS")) {
          availableFlags = new FLAGS(ir);
          r[i] = null;
        } else if (ir.keyEquals("VANISHED")) {
          if (responses == null)
            responses = new ArrayList();
          responses.add(ir);
          r[i] = null;
        } else if (ir.keyEquals("FETCH")) {
          if (responses == null)
            responses = new ArrayList();
          responses.add(ir);
          r[i] = null;
        } else if ((ir.isUnTagged()) && (ir.isOK()))
        {







          ir.skipSpaces();
          
          if (ir.readByte() != 91) {
            ir.reset();
          }
          else
          {
            boolean handled = true;
            String s = ir.readAtom();
            if (s.equalsIgnoreCase("UNSEEN")) {
              first = ir.readNumber();
            } else if (s.equalsIgnoreCase("UIDVALIDITY")) {
              uidvalidity = ir.readLong();
            } else if (s.equalsIgnoreCase("PERMANENTFLAGS")) {
              permanentFlags = new FLAGS(ir);
            } else if (s.equalsIgnoreCase("UIDNEXT")) {
              uidnext = ir.readLong();
            } else if (s.equalsIgnoreCase("HIGHESTMODSEQ")) {
              highestmodseq = ir.readLong();
            } else {
              handled = false;
            }
            if (handled) {
              r[i] = null;
            } else
              ir.reset();
          } } else if ((ir.isUnTagged()) && (ir.isNO()))
        {



          ir.skipSpaces();
          
          if (ir.readByte() != 91) {
            ir.reset();
          }
          else
          {
            boolean handled = true;
            String s = ir.readAtom();
            if (s.equalsIgnoreCase("UIDNOTSTICKY")) {
              uidNotSticky = true;
            } else {
              handled = false;
            }
            if (handled) {
              r[i] = null;
            } else {
              ir.reset();
            }
          }
        }
      }
    }
    


    if (permanentFlags == null) {
      if (availableFlags != null) {
        permanentFlags = new Flags(availableFlags);
      } else {
        permanentFlags = new Flags();
      }
    }
  }
}
