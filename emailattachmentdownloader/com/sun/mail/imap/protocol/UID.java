package com.sun.mail.imap.protocol;

import com.sun.mail.iap.ParsingException;














































public class UID
  implements Item
{
  static final char[] name = { 'U', 'I', 'D' };
  

  public int seqnum;
  

  public long uid;
  

  public UID(FetchResponse r)
    throws ParsingException
  {
    seqnum = r.getNumber();
    r.skipSpaces();
    uid = r.readLong();
  }
}
