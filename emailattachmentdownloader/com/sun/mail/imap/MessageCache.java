package com.sun.mail.imap;

import com.sun.mail.util.MailLogger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import javax.mail.Message;
















































































public class MessageCache
{
  private IMAPMessage[] messages;
  private int[] seqnums;
  private int size;
  private IMAPFolder folder;
  private MailLogger logger;
  private static final int SLOP = 64;
  
  MessageCache(IMAPFolder folder, IMAPStore store, int size)
  {
    this.folder = folder;
    logger = logger.getSubLogger("messagecache", "DEBUG IMAP MC", store
      .getMessageCacheDebug());
    if (logger.isLoggable(Level.CONFIG))
      logger.config("create cache of size " + size);
    ensureCapacity(size, 1);
  }
  


  MessageCache(int size, boolean debug)
  {
    folder = null;
    
    logger = new MailLogger(getClass(), "messagecache", "DEBUG IMAP MC", debug, System.out);
    
    if (logger.isLoggable(Level.CONFIG))
      logger.config("create DEBUG cache of size " + size);
    ensureCapacity(size, 1);
  }
  




  public int size()
  {
    return size;
  }
  







  public IMAPMessage getMessage(int msgnum)
  {
    if ((msgnum < 1) || (msgnum > size)) {
      throw new ArrayIndexOutOfBoundsException("message number (" + msgnum + ") out of bounds (" + size + ")");
    }
    IMAPMessage msg = messages[(msgnum - 1)];
    if (msg == null) {
      if (logger.isLoggable(Level.FINE))
        logger.fine("create message number " + msgnum);
      msg = folder.newIMAPMessage(msgnum);
      messages[(msgnum - 1)] = msg;
      
      if (seqnumOf(msgnum) <= 0) {
        logger.fine("it's expunged!");
        msg.setExpunged(true);
      }
    }
    return msg;
  }
  







  public IMAPMessage getMessageBySeqnum(int seqnum)
  {
    int msgnum = msgnumOf(seqnum);
    if (msgnum < 0) {
      if (logger.isLoggable(Level.FINE))
        logger.fine("no message seqnum " + seqnum);
      return null;
    }
    return getMessage(msgnum);
  }
  




  public void expungeMessage(int seqnum)
  {
    int msgnum = msgnumOf(seqnum);
    if (msgnum < 0) {
      if (logger.isLoggable(Level.FINE))
        logger.fine("expunge no seqnum " + seqnum);
      return;
    }
    IMAPMessage msg = messages[(msgnum - 1)];
    if (msg != null) {
      if (logger.isLoggable(Level.FINE))
        logger.fine("expunge existing " + msgnum);
      msg.setExpunged(true);
    }
    if (seqnums == null) {
      logger.fine("create seqnums array");
      seqnums = new int[messages.length];
      for (int i = 1; i < msgnum; i++)
        seqnums[(i - 1)] = i;
      seqnums[(msgnum - 1)] = 0;
      for (int i = msgnum + 1; i <= seqnums.length; i++)
        seqnums[(i - 1)] = (i - 1);
    } else {
      seqnums[(msgnum - 1)] = 0;
      for (int i = msgnum + 1; i <= seqnums.length; i++) {
        assert (seqnums[(i - 1)] != 1);
        if (seqnums[(i - 1)] > 0) {
          seqnums[(i - 1)] -= 1;
        }
      }
    }
  }
  




  public IMAPMessage[] removeExpungedMessages()
  {
    logger.fine("remove expunged messages");
    
    List<IMAPMessage> mlist = new ArrayList();
    








    int oldnum = 1;
    int newnum = 1;
    while (oldnum <= size)
    {
      if (seqnumOf(oldnum) <= 0) {
        IMAPMessage m = getMessage(oldnum);
        mlist.add(m);
      }
      else {
        if (newnum != oldnum)
        {
          messages[(newnum - 1)] = messages[(oldnum - 1)];
          if (messages[(newnum - 1)] != null)
            messages[(newnum - 1)].setMessageNumber(newnum);
        }
        newnum++;
      }
      oldnum++;
    }
    seqnums = null;
    shrink(newnum, oldnum);
    
    IMAPMessage[] rmsgs = new IMAPMessage[mlist.size()];
    if (logger.isLoggable(Level.FINE))
      logger.fine("return " + rmsgs.length);
    mlist.toArray(rmsgs);
    return rmsgs;
  }
  








  public IMAPMessage[] removeExpungedMessages(Message[] msgs)
  {
    logger.fine("remove expunged messages");
    
    List<IMAPMessage> mlist = new ArrayList();
    





    int[] mnum = new int[msgs.length];
    for (int i = 0; i < msgs.length; i++)
      mnum[i] = msgs[i].getMessageNumber();
    Arrays.sort(mnum);
    














    int oldnum = 1;
    int newnum = 1;
    int mnumi = 0;
    boolean keepSeqnums = false;
    while (oldnum <= size)
    {




      if ((mnumi < mnum.length) && (oldnum == mnum[mnumi]))
      {
        if (seqnumOf(oldnum) <= 0) {
          IMAPMessage m = getMessage(oldnum);
          mlist.add(m);
          




          while ((mnumi < mnum.length) && (mnum[mnumi] <= oldnum))
            mnumi++;
          break label244;
        } }
      if (newnum != oldnum)
      {
        messages[(newnum - 1)] = messages[(oldnum - 1)];
        if (messages[(newnum - 1)] != null)
          messages[(newnum - 1)].setMessageNumber(newnum);
        if (seqnums != null)
          seqnums[(newnum - 1)] = seqnums[(oldnum - 1)];
      }
      if ((seqnums != null) && (seqnums[(newnum - 1)] != newnum))
        keepSeqnums = true;
      newnum++;
      label244:
      oldnum++;
    }
    
    if (!keepSeqnums)
      seqnums = null;
    shrink(newnum, oldnum);
    
    IMAPMessage[] rmsgs = new IMAPMessage[mlist.size()];
    if (logger.isLoggable(Level.FINE))
      logger.fine("return " + rmsgs.length);
    mlist.toArray(rmsgs);
    return rmsgs;
  }
  



  private void shrink(int newend, int oldend)
  {
    size = (newend - 1);
    if (logger.isLoggable(Level.FINE))
      logger.fine("size now " + size);
    if (size == 0) {
      messages = null;
      seqnums = null;
    } else if ((size > 64) && (size < messages.length / 2))
    {
      logger.fine("reallocate array");
      IMAPMessage[] newm = new IMAPMessage[size + 64];
      System.arraycopy(messages, 0, newm, 0, size);
      messages = newm;
      if (seqnums != null) {
        int[] news = new int[size + 64];
        System.arraycopy(seqnums, 0, news, 0, size);
        seqnums = news;
      }
    } else {
      if (logger.isLoggable(Level.FINE)) {
        logger.fine("clean " + newend + " to " + oldend);
      }
      for (int msgnum = newend; msgnum < oldend; msgnum++) {
        messages[(msgnum - 1)] = null;
        if (seqnums != null) {
          seqnums[(msgnum - 1)] = 0;
        }
      }
    }
  }
  





  public void addMessages(int count, int newSeqNum)
  {
    if (logger.isLoggable(Level.FINE)) {
      logger.fine("add " + count + " messages");
    }
    ensureCapacity(size + count, newSeqNum);
  }
  



  private void ensureCapacity(int newsize, int newSeqNum)
  {
    if (messages == null) {
      messages = new IMAPMessage[newsize + 64];
    } else if (messages.length < newsize) {
      if (logger.isLoggable(Level.FINE))
        logger.fine("expand capacity to " + newsize);
      IMAPMessage[] newm = new IMAPMessage[newsize + 64];
      System.arraycopy(messages, 0, newm, 0, messages.length);
      messages = newm;
      if (seqnums != null) {
        int[] news = new int[newsize + 64];
        System.arraycopy(seqnums, 0, news, 0, seqnums.length);
        for (int i = size; i < news.length; i++)
          news[i] = (newSeqNum++);
        seqnums = news;
        if (logger.isLoggable(Level.FINE)) {
          logger.fine("message " + newsize + " has sequence number " + seqnums[(newsize - 1)]);
        }
      }
    } else if (newsize < size)
    {
      if (logger.isLoggable(Level.FINE))
        logger.fine("shrink capacity to " + newsize);
      for (int msgnum = newsize + 1; msgnum <= size; msgnum++) {
        messages[(msgnum - 1)] = null;
        if (seqnums != null)
          seqnums[(msgnum - 1)] = -1;
      }
    }
    size = newsize;
  }
  





  public int seqnumOf(int msgnum)
  {
    if (seqnums == null) {
      return msgnum;
    }
    if (logger.isLoggable(Level.FINE)) {
      logger.fine("msgnum " + msgnum + " is seqnum " + seqnums[(msgnum - 1)]);
    }
    return seqnums[(msgnum - 1)];
  }
  



  private int msgnumOf(int seqnum)
  {
    if (seqnums == null)
      return seqnum;
    if (seqnum < 1) {
      if (logger.isLoggable(Level.FINE))
        logger.fine("bad seqnum " + seqnum);
      return -1;
    }
    for (int msgnum = seqnum; msgnum <= size; msgnum++) {
      if (seqnums[(msgnum - 1)] == seqnum)
        return msgnum;
      if (seqnums[(msgnum - 1)] > seqnum)
        break;
    }
    return -1;
  }
}
