package com.sun.mail.util.logging;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.ErrorManager;
import java.util.logging.Filter;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileTypeMap;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessageContext;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.SendFailedException;
import javax.mail.Service;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.URLName;
import javax.mail.internet.AddressException;
import javax.mail.internet.ContentType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimePart;
import javax.mail.internet.MimeUtility;
import javax.mail.util.ByteArrayDataSource;













































































































































































































































































































public class MailHandler
  extends Handler
{
  private static final Filter[] EMPTY_FILTERS = new Filter[0];
  


  private static final Formatter[] EMPTY_FORMATTERS = new Formatter[0];
  


  private static final int MIN_HEADER_SIZE = 1024;
  


  private static final int offValue = Level.OFF.intValue();
  






  private static final PrivilegedAction<Object> MAILHANDLER_LOADER = new GetAndSetContext(MailHandler.class);
  












  private static final ThreadLocal<Integer> MUTEX = new ThreadLocal();
  



  private static final Integer MUTEX_PUBLISH = Integer.valueOf(-2);
  



  private static final Integer MUTEX_REPORT = Integer.valueOf(-4);
  



  private static final Integer MUTEX_LINKAGE = Integer.valueOf(-8);
  




  private volatile boolean sealed;
  




  private boolean isWriting;
  




  private Properties mailProps;
  




  private Authenticator auth;
  




  private Session session;
  



  private int[] matched;
  



  private LogRecord[] data;
  



  private int size;
  



  private int capacity;
  



  private Comparator<? super LogRecord> comparator;
  



  private Formatter subjectFormatter;
  



  private Level pushLevel;
  



  private Filter pushFilter;
  



  private volatile Filter filter;
  



  private volatile Level logLevel = Level.ALL;
  




  private volatile Filter[] attachmentFilters;
  




  private String encoding;
  




  private Formatter formatter;
  




  private Formatter[] attachmentFormatters;
  




  private Formatter[] attachmentNames;
  




  private FileTypeMap contentTypes;
  




  private volatile ErrorManager errorManager = defaultErrorManager();
  





  public MailHandler()
  {
    init((Properties)null);
    sealed = true;
    checkAccess();
  }
  








  public MailHandler(int capacity)
  {
    init((Properties)null);
    sealed = true;
    setCapacity0(capacity);
  }
  









  public MailHandler(Properties props)
  {
    if (props == null) {
      throw new NullPointerException();
    }
    init(props);
    sealed = true;
    setMailProperties0(props);
  }
  












  public boolean isLoggable(LogRecord record)
  {
    int levelValue = getLevel().intValue();
    if ((record.getLevel().intValue() < levelValue) || (levelValue == offValue)) {
      return false;
    }
    
    Filter body = getFilter();
    if ((body == null) || (body.isLoggable(record))) {
      setMatchedPart(-1);
      return true;
    }
    
    return isAttachmentLoggable(record);
  }
  





















  public void publish(LogRecord record)
  {
    if (tryMutex()) {
      try {
        if (isLoggable(record)) {
          record.getSourceMethodName();
          publish0(record);
        }
      } catch (LinkageError JDK8152515) {
        reportLinkageError(JDK8152515, 1);
      } finally {
        releaseMutex();
      }
    } else {
      reportUnPublishedError(record);
    }
  }
  



  private void publish0(LogRecord record)
  {
    Message msg;
    

    synchronized (this) {
      if ((size == data.length) && (size < capacity)) {
        grow();
      }
      Message msg;
      if (size < data.length)
      {
        matched[size] = getMatchedPart();
        data[size] = record;
        size += 1;
        boolean priority = isPushable(record);
        Message msg; if ((priority) || (size >= capacity)) {
          msg = writeLogRecords(1);
        } else {
          msg = null;
        }
      } else {
        boolean priority = false;
        msg = null;
      } }
    boolean priority;
    Message msg;
    if (msg != null) {
      send(msg, priority, 1);
    }
  }
  







  private void reportUnPublishedError(LogRecord record)
  {
    Integer idx = (Integer)MUTEX.get();
    if ((idx == null) || (idx.intValue() > MUTEX_REPORT.intValue())) {
      MUTEX.set(MUTEX_REPORT);
      try { String msg;
        String msg;
        if (record != null) {
          Formatter f = createSimpleFormatter();
          

          msg = "Log record " + record.getSequenceNumber() + " was not published. " + head(f) + format(f, record) + tail(f, "");
        } else {
          msg = null;
        }
        

        Exception e = new IllegalStateException("Recursive publish detected by thread " + Thread.currentThread());
        reportError(msg, e, 1);
      } finally {
        if (idx != null) {
          MUTEX.set(idx);
        } else {
          MUTEX.remove();
        }
      }
    }
  }
  






  private boolean tryMutex()
  {
    if (MUTEX.get() == null) {
      MUTEX.set(MUTEX_PUBLISH);
      return true;
    }
    return false;
  }
  





  private void releaseMutex()
  {
    MUTEX.remove();
  }
  








  private int getMatchedPart()
  {
    Integer idx = (Integer)MUTEX.get();
    if ((idx == null) || (idx.intValue() >= readOnlyAttachmentFilters().length)) {
      idx = MUTEX_PUBLISH;
    }
    return idx.intValue();
  }
  






  private void setMatchedPart(int index)
  {
    if (MUTEX_PUBLISH.equals(MUTEX.get())) {
      MUTEX.set(Integer.valueOf(index));
    }
  }
  





  private void clearMatches(int index)
  {
    assert (Thread.holdsLock(this));
    for (int r = 0; r < size; r++) {
      if (matched[r] >= index) {
        matched[r] = MUTEX_PUBLISH.intValue();
      }
    }
  }
  













  public void postConstruct() {}
  












  public void preDestroy()
  {
    push(false, 3);
  }
  





  public void push()
  {
    push(true, 2);
  }
  






  public void flush()
  {
    push(false, 2);
  }
  
  /* Error */
  public void close()
  {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial 9	com/sun/mail/util/logging/MailHandler:checkAccess	()V
    //   4: aconst_null
    //   5: astore_1
    //   6: aload_0
    //   7: dup
    //   8: astore_2
    //   9: monitorenter
    //   10: aload_0
    //   11: iconst_3
    //   12: invokespecial 38	com/sun/mail/util/logging/MailHandler:writeLogRecords	(I)Ljavax/mail/Message;
    //   15: astore_1
    //   16: aload_0
    //   17: getstatic 75	java/util/logging/Level:OFF	Ljava/util/logging/Level;
    //   20: putfield 3	com/sun/mail/util/logging/MailHandler:logLevel	Ljava/util/logging/Level;
    //   23: aload_0
    //   24: getfield 33	com/sun/mail/util/logging/MailHandler:capacity	I
    //   27: ifle +12 -> 39
    //   30: aload_0
    //   31: aload_0
    //   32: getfield 33	com/sun/mail/util/logging/MailHandler:capacity	I
    //   35: ineg
    //   36: putfield 33	com/sun/mail/util/logging/MailHandler:capacity	I
    //   39: aload_0
    //   40: getfield 31	com/sun/mail/util/logging/MailHandler:size	I
    //   43: ifne +95 -> 138
    //   46: aload_0
    //   47: getfield 32	com/sun/mail/util/logging/MailHandler:data	[Ljava/util/logging/LogRecord;
    //   50: arraylength
    //   51: iconst_1
    //   52: if_icmpeq +86 -> 138
    //   55: aload_0
    //   56: iconst_1
    //   57: anewarray 76	java/util/logging/LogRecord
    //   60: putfield 32	com/sun/mail/util/logging/MailHandler:data	[Ljava/util/logging/LogRecord;
    //   63: aload_0
    //   64: aload_0
    //   65: getfield 32	com/sun/mail/util/logging/MailHandler:data	[Ljava/util/logging/LogRecord;
    //   68: arraylength
    //   69: newarray int
    //   71: putfield 35	com/sun/mail/util/logging/MailHandler:matched	[I
    //   74: goto +64 -> 138
    //   77: astore_3
    //   78: aload_0
    //   79: getstatic 75	java/util/logging/Level:OFF	Ljava/util/logging/Level;
    //   82: putfield 3	com/sun/mail/util/logging/MailHandler:logLevel	Ljava/util/logging/Level;
    //   85: aload_0
    //   86: getfield 33	com/sun/mail/util/logging/MailHandler:capacity	I
    //   89: ifle +12 -> 101
    //   92: aload_0
    //   93: aload_0
    //   94: getfield 33	com/sun/mail/util/logging/MailHandler:capacity	I
    //   97: ineg
    //   98: putfield 33	com/sun/mail/util/logging/MailHandler:capacity	I
    //   101: aload_0
    //   102: getfield 31	com/sun/mail/util/logging/MailHandler:size	I
    //   105: ifne +31 -> 136
    //   108: aload_0
    //   109: getfield 32	com/sun/mail/util/logging/MailHandler:data	[Ljava/util/logging/LogRecord;
    //   112: arraylength
    //   113: iconst_1
    //   114: if_icmpeq +22 -> 136
    //   117: aload_0
    //   118: iconst_1
    //   119: anewarray 76	java/util/logging/LogRecord
    //   122: putfield 32	com/sun/mail/util/logging/MailHandler:data	[Ljava/util/logging/LogRecord;
    //   125: aload_0
    //   126: aload_0
    //   127: getfield 32	com/sun/mail/util/logging/MailHandler:data	[Ljava/util/logging/LogRecord;
    //   130: arraylength
    //   131: newarray int
    //   133: putfield 35	com/sun/mail/util/logging/MailHandler:matched	[I
    //   136: aload_3
    //   137: athrow
    //   138: aload_2
    //   139: monitorexit
    //   140: goto +10 -> 150
    //   143: astore 4
    //   145: aload_2
    //   146: monitorexit
    //   147: aload 4
    //   149: athrow
    //   150: aload_1
    //   151: ifnull +10 -> 161
    //   154: aload_0
    //   155: aload_1
    //   156: iconst_0
    //   157: iconst_3
    //   158: invokespecial 39	com/sun/mail/util/logging/MailHandler:send	(Ljavax/mail/Message;ZI)V
    //   161: goto +10 -> 171
    //   164: astore_1
    //   165: aload_0
    //   166: aload_1
    //   167: iconst_3
    //   168: invokespecial 29	com/sun/mail/util/logging/MailHandler:reportLinkageError	(Ljava/lang/Throwable;I)V
    //   171: return
    // Line number table:
    //   Java source line #860	-> byte code offset #0
    //   Java source line #861	-> byte code offset #4
    //   Java source line #862	-> byte code offset #6
    //   Java source line #864	-> byte code offset #10
    //   Java source line #866	-> byte code offset #16
    //   Java source line #873	-> byte code offset #23
    //   Java source line #874	-> byte code offset #30
    //   Java source line #878	-> byte code offset #39
    //   Java source line #879	-> byte code offset #55
    //   Java source line #880	-> byte code offset #63
    //   Java source line #866	-> byte code offset #77
    //   Java source line #873	-> byte code offset #85
    //   Java source line #874	-> byte code offset #92
    //   Java source line #878	-> byte code offset #101
    //   Java source line #879	-> byte code offset #117
    //   Java source line #880	-> byte code offset #125
    //   Java source line #883	-> byte code offset #138
    //   Java source line #885	-> byte code offset #150
    //   Java source line #886	-> byte code offset #154
    //   Java source line #890	-> byte code offset #161
    //   Java source line #888	-> byte code offset #164
    //   Java source line #889	-> byte code offset #165
    //   Java source line #891	-> byte code offset #171
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	172	0	this	MailHandler
    //   5	151	1	msg	Message
    //   164	3	1	JDK8152515	LinkageError
    //   8	138	2	Ljava/lang/Object;	Object
    //   77	60	3	localObject1	Object
    //   143	5	4	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   10	16	77	finally
    //   10	140	143	finally
    //   143	147	143	finally
    //   0	161	164	java/lang/LinkageError
  }
  
  public void setLevel(Level newLevel)
  {
    if (newLevel == null) {
      throw new NullPointerException();
    }
    checkAccess();
    

    synchronized (this) {
      if (capacity > 0) {
        logLevel = newLevel;
      }
    }
  }
  







  public Level getLevel()
  {
    return logLevel;
  }
  







  public ErrorManager getErrorManager()
  {
    checkAccess();
    return errorManager;
  }
  











  public void setErrorManager(ErrorManager em)
  {
    checkAccess();
    setErrorManager0(em);
  }
  








  private void setErrorManager0(ErrorManager em)
  {
    if (em == null) {
      throw new NullPointerException();
    }
    try {
      synchronized (this) {
        errorManager = em;
        super.setErrorManager(em);
      }
    }
    catch (RuntimeException|LinkageError localRuntimeException) {}
  }
  





  public Filter getFilter()
  {
    return filter;
  }
  











  public void setFilter(Filter newFilter)
  {
    checkAccess();
    synchronized (this) {
      if (newFilter != filter) {
        clearMatches(-1);
      }
      filter = newFilter;
    }
  }
  






  public synchronized String getEncoding()
  {
    return encoding;
  }
  












  public void setEncoding(String encoding)
    throws UnsupportedEncodingException
  {
    checkAccess();
    setEncoding0(encoding);
  }
  





  private void setEncoding0(String e)
    throws UnsupportedEncodingException
  {
    if (e != null) {
      try {
        if (!Charset.isSupported(e)) {
          throw new UnsupportedEncodingException(e);
        }
      } catch (IllegalCharsetNameException icne) {
        throw new UnsupportedEncodingException(e);
      }
    }
    
    synchronized (this) {
      encoding = e;
    }
  }
  





  public synchronized Formatter getFormatter()
  {
    return formatter;
  }
  











  public synchronized void setFormatter(Formatter newFormatter)
    throws SecurityException
  {
    checkAccess();
    if (newFormatter == null) {
      throw new NullPointerException();
    }
    formatter = newFormatter;
  }
  




  public final synchronized Level getPushLevel()
  {
    return pushLevel;
  }
  










  public final synchronized void setPushLevel(Level level)
  {
    checkAccess();
    if (level == null) {
      throw new NullPointerException();
    }
    
    if (isWriting) {
      throw new IllegalStateException();
    }
    pushLevel = level;
  }
  



  public final synchronized Filter getPushFilter()
  {
    return pushFilter;
  }
  










  public final synchronized void setPushFilter(Filter filter)
  {
    checkAccess();
    if (isWriting) {
      throw new IllegalStateException();
    }
    pushFilter = filter;
  }
  




  public final synchronized Comparator<? super LogRecord> getComparator()
  {
    return comparator;
  }
  







  public final synchronized void setComparator(Comparator<? super LogRecord> c)
  {
    checkAccess();
    if (isWriting) {
      throw new IllegalStateException();
    }
    comparator = c;
  }
  





  public final synchronized int getCapacity()
  {
    assert ((capacity != Integer.MIN_VALUE) && (capacity != 0)) : capacity;
    return Math.abs(capacity);
  }
  





  public final synchronized Authenticator getAuthenticator()
  {
    checkAccess();
    return auth;
  }
  






  public final void setAuthenticator(Authenticator auth)
  {
    setAuthenticator0(auth);
  }
  










  public final void setAuthenticator(char... password)
  {
    if (password == null) {
      setAuthenticator0((Authenticator)null);
    } else {
      setAuthenticator0(DefaultAuthenticator.of(new String(password)));
    }
  }
  






  private void setAuthenticator0(Authenticator auth)
  {
    checkAccess();
    
    Session settings;
    synchronized (this) {
      if (isWriting) {
        throw new IllegalStateException();
      }
      this.auth = auth;
      settings = updateSession(); }
    Session settings;
    verifySettings(settings);
  }
  










  public final void setMailProperties(Properties props)
  {
    setMailProperties0(props);
  }
  




  private void setMailProperties0(Properties props)
  {
    checkAccess();
    props = (Properties)props.clone();
    Session settings;
    synchronized (this) {
      if (isWriting) {
        throw new IllegalStateException();
      }
      mailProps = props;
      settings = updateSession(); }
    Session settings;
    verifySettings(settings);
  }
  





  public final Properties getMailProperties()
  {
    checkAccess();
    Properties props;
    synchronized (this) {
      props = mailProps; }
    Properties props;
    return (Properties)props.clone();
  }
  





  public final Filter[] getAttachmentFilters()
  {
    return (Filter[])readOnlyAttachmentFilters().clone();
  }
  











  public final void setAttachmentFilters(Filter... filters)
  {
    checkAccess();
    if (filters.length == 0) {
      filters = emptyFilterArray();
    } else {
      filters = (Filter[])Arrays.copyOf(filters, filters.length, [Ljava.util.logging.Filter.class);
    }
    synchronized (this) {
      if (attachmentFormatters.length != filters.length) {
        throw attachmentMismatch(attachmentFormatters.length, filters.length);
      }
      
      if (isWriting) {
        throw new IllegalStateException();
      }
      
      if (size != 0) {
        for (int i = 0; i < filters.length; i++) {
          if (filters[i] != attachmentFilters[i]) {
            clearMatches(i);
            break;
          }
        }
      }
      attachmentFilters = filters;
    }
  }
  


  public final Formatter[] getAttachmentFormatters()
  {
    Formatter[] formatters;
    

    synchronized (this) {
      formatters = attachmentFormatters; }
    Formatter[] formatters;
    return (Formatter[])formatters.clone();
  }
  











  public final void setAttachmentFormatters(Formatter... formatters)
  {
    checkAccess();
    if (formatters.length == 0) {
      formatters = emptyFormatterArray();
    } else {
      formatters = (Formatter[])Arrays.copyOf(formatters, formatters.length, [Ljava.util.logging.Formatter.class);
      
      for (int i = 0; i < formatters.length; i++) {
        if (formatters[i] == null) {
          throw new NullPointerException(atIndexMsg(i));
        }
      }
    }
    
    synchronized (this) {
      if (isWriting) {
        throw new IllegalStateException();
      }
      
      attachmentFormatters = formatters;
      alignAttachmentFilters();
      alignAttachmentNames();
    }
  }
  



  public final Formatter[] getAttachmentNames()
  {
    Formatter[] formatters;
    


    synchronized (this) {
      formatters = attachmentNames; }
    Formatter[] formatters;
    return (Formatter[])formatters.clone();
  }
  














  public final void setAttachmentNames(String... names)
  {
    checkAccess();
    Formatter[] formatters;
    Formatter[] formatters;
    if (names.length == 0) {
      formatters = emptyFormatterArray();
    } else {
      formatters = new Formatter[names.length];
    }
    
    for (int i = 0; i < names.length; i++) {
      String name = names[i];
      if (name != null) {
        if (name.length() > 0) {
          formatters[i] = TailNameFormatter.of(name);
        } else {
          throw new IllegalArgumentException(atIndexMsg(i));
        }
      } else {
        throw new NullPointerException(atIndexMsg(i));
      }
    }
    
    synchronized (this) {
      if (attachmentFormatters.length != names.length) {
        throw attachmentMismatch(attachmentFormatters.length, names.length);
      }
      
      if (isWriting) {
        throw new IllegalStateException();
      }
      attachmentNames = formatters;
    }
  }
  




















  public final void setAttachmentNames(Formatter... formatters)
  {
    checkAccess();
    
    if (formatters.length == 0) {
      formatters = emptyFormatterArray();
    } else {
      formatters = (Formatter[])Arrays.copyOf(formatters, formatters.length, [Ljava.util.logging.Formatter.class);
    }
    

    for (int i = 0; i < formatters.length; i++) {
      if (formatters[i] == null) {
        throw new NullPointerException(atIndexMsg(i));
      }
    }
    
    synchronized (this) {
      if (attachmentFormatters.length != formatters.length) {
        throw attachmentMismatch(attachmentFormatters.length, formatters.length);
      }
      

      if (isWriting) {
        throw new IllegalStateException();
      }
      
      attachmentNames = formatters;
    }
  }
  





  public final synchronized Formatter getSubject()
  {
    return subjectFormatter;
  }
  










  public final void setSubject(String subject)
  {
    if (subject != null) {
      setSubject(TailNameFormatter.of(subject));
    } else {
      checkAccess();
      throw new NullPointerException();
    }
  }
  


















  public final void setSubject(Formatter format)
  {
    checkAccess();
    if (format == null) {
      throw new NullPointerException();
    }
    
    synchronized (this) {
      if (isWriting) {
        throw new IllegalStateException();
      }
      subjectFormatter = format;
    }
  }
  









  protected void reportError(String msg, Exception ex, int code)
  {
    try
    {
      if (msg != null) {
        errorManager.error(Level.SEVERE.getName()
          .concat(": ").concat(msg), ex, code);
      } else {
        errorManager.error(null, ex, code);
      }
    } catch (RuntimeException|LinkageError GLASSFISH_21258) {
      reportLinkageError(GLASSFISH_21258, code);
    }
  }
  


  private void checkAccess()
  {
    if (sealed) {
      LogManagerProperties.checkLogManagerAccess();
    }
  }
  








  final String contentTypeOf(CharSequence chunk)
  {
    if (!isEmpty(chunk)) {
      int MAX_CHARS = 25;
      if (chunk.length() > 25) {
        chunk = chunk.subSequence(0, 25);
      }
      try {
        String charset = getEncodingName();
        byte[] b = chunk.toString().getBytes(charset);
        ByteArrayInputStream in = new ByteArrayInputStream(b);
        assert (in.markSupported()) : in.getClass().getName();
        return URLConnection.guessContentTypeFromStream(in);
      } catch (IOException IOE) {
        reportError(IOE.getMessage(), IOE, 5);
      }
    }
    return null;
  }
  










  final String contentTypeOf(Formatter f)
  {
    assert (Thread.holdsLock(this));
    if (f != null) {
      String type = getContentType(f.getClass().getName());
      if (type != null) {
        return type;
      }
      
      for (Class<?> k = f.getClass(); k != Formatter.class; 
          k = k.getSuperclass())
      {
        try {
          name = k.getSimpleName();
        } catch (InternalError JDK8057919) { String name;
          name = k.getName();
        }
        String name = name.toLowerCase(Locale.ENGLISH);
        for (int idx = name.indexOf('$') + 1; 
            (idx = name.indexOf("ml", idx)) > -1; idx += 2) {
          if (idx > 0) {
            if (name.charAt(idx - 1) == 'x') {
              return "application/xml";
            }
            if ((idx > 1) && (name.charAt(idx - 2) == 'h') && 
              (name.charAt(idx - 1) == 't')) {
              return "text/html";
            }
          }
        }
      }
    }
    return null;
  }
  












  final boolean isMissingContent(Message msg, Throwable t)
  {
    Object ccl = getAndSetContextClassLoader(MAILHANDLER_LOADER);
    try {
      msg.writeTo(new ByteArrayOutputStream(1024));
    } catch (RuntimeException RE) {
      throw RE;
    } catch (Exception noContent) {
      String txt = noContent.getMessage();
      if (!isEmpty(txt)) {
        int limit = 0;
        for (; t != null; 
            














            limit == 65536)
        {
          if ((noContent.getClass() == t.getClass()) && 
            (txt.equals(t.getMessage()))) {
            return true;
          }
          


          Throwable cause = t.getCause();
          if ((cause == null) && ((t instanceof MessagingException))) {
            t = ((MessagingException)t).getNextException();
          } else {
            t = cause;
          }
          

          limit++;
        }
      }
    }
    finally
    {
      getAndSetContextClassLoader(ccl);
    }
    return false;
  }
  






  private void reportError(Message msg, Exception ex, int code)
  {
    try
    {
      try
      {
        errorManager.error(toRawString(msg), ex, code);
      } catch (RuntimeException re) {
        reportError(toMsgString(re), ex, code);
      } catch (Exception e) {
        reportError(toMsgString(e), ex, code);
      }
    } catch (LinkageError GLASSFISH_21258) {
      reportLinkageError(GLASSFISH_21258, code);
    }
  }
  










  private void reportLinkageError(Throwable le, int code)
  {
    if (le == null) {
      throw new NullPointerException(String.valueOf(code));
    }
    
    Integer idx = (Integer)MUTEX.get();
    if ((idx == null) || (idx.intValue() > MUTEX_LINKAGE.intValue())) {
      MUTEX.set(MUTEX_LINKAGE);
      try
      {
        Thread.currentThread().getUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), le);
      }
      catch (RuntimeException|LinkageError localRuntimeException) {}finally {
        if (idx != null) {
          MUTEX.set(idx);
        } else {
          MUTEX.remove();
        }
      }
    }
  }
  





  private String getContentType(String name)
  {
    assert (Thread.holdsLock(this));
    String type = contentTypes.getContentType(name);
    if ("application/octet-stream".equalsIgnoreCase(type)) {
      return null;
    }
    return type;
  }
  




  private String getEncodingName()
  {
    String charset = getEncoding();
    if (charset == null) {
      charset = MimeUtility.getDefaultJavaCharset();
    }
    return charset;
  }
  





  private void setContent(MimePart part, CharSequence buf, String type)
    throws MessagingException
  {
    String charset = getEncodingName();
    if ((type != null) && (!"text/plain".equalsIgnoreCase(type))) {
      type = contentWithEncoding(type, charset);
      try {
        DataSource source = new ByteArrayDataSource(buf.toString(), type);
        part.setDataHandler(new DataHandler(source));
      } catch (IOException IOE) {
        reportError(IOE.getMessage(), IOE, 5);
        part.setText(buf.toString(), charset);
      }
    } else {
      part.setText(buf.toString(), MimeUtility.mimeCharset(charset));
    }
  }
  





  private String contentWithEncoding(String type, String encoding)
  {
    assert (encoding != null);
    try {
      ContentType ct = new ContentType(type);
      ct.setParameter("charset", MimeUtility.mimeCharset(encoding));
      encoding = ct.toString();
      if (!isEmpty(encoding)) {
        type = encoding;
      }
    } catch (MessagingException ME) {
      reportError(type, ME, 5);
    }
    return type;
  }
  









  private synchronized void setCapacity0(int newCapacity)
  {
    checkAccess();
    if (newCapacity <= 0) {
      throw new IllegalArgumentException("Capacity must be greater than zero.");
    }
    
    if (isWriting) {
      throw new IllegalStateException();
    }
    
    if (capacity < 0) {
      capacity = (-newCapacity);
    } else {
      capacity = newCapacity;
    }
  }
  






  private Filter[] readOnlyAttachmentFilters()
  {
    return attachmentFilters;
  }
  



  private static Formatter[] emptyFormatterArray()
  {
    return EMPTY_FORMATTERS;
  }
  



  private static Filter[] emptyFilterArray()
  {
    return EMPTY_FILTERS;
  }
  




  private boolean alignAttachmentNames()
  {
    assert (Thread.holdsLock(this));
    boolean fixed = false;
    int expect = attachmentFormatters.length;
    int current = attachmentNames.length;
    if (current != expect) {
      attachmentNames = ((Formatter[])Arrays.copyOf(attachmentNames, expect, [Ljava.util.logging.Formatter.class));
      
      fixed = current != 0;
    }
    

    if (expect == 0) {
      attachmentNames = emptyFormatterArray();
      if ((!$assertionsDisabled) && (attachmentNames.length != 0)) throw new AssertionError();
    } else {
      for (int i = 0; i < expect; i++) {
        if (attachmentNames[i] == null) {
          attachmentNames[i] = TailNameFormatter.of(
            toString(attachmentFormatters[i]));
        }
      }
    }
    return fixed;
  }
  



  private boolean alignAttachmentFilters()
  {
    assert (Thread.holdsLock(this));
    
    boolean fixed = false;
    int expect = attachmentFormatters.length;
    int current = attachmentFilters.length;
    if (current != expect) {
      attachmentFilters = ((Filter[])Arrays.copyOf(attachmentFilters, expect, [Ljava.util.logging.Filter.class));
      
      clearMatches(current);
      fixed = current != 0;
      


      Filter body = filter;
      if (body != null) {
        for (int i = current; i < expect; i++) {
          attachmentFilters[i] = body;
        }
      }
    }
    

    if (expect == 0) {
      attachmentFilters = emptyFilterArray();
      assert (attachmentFilters.length == 0);
    }
    return fixed;
  }
  


  private void reset()
  {
    assert (Thread.holdsLock(this));
    if (size < data.length) {
      Arrays.fill(data, 0, size, null);
    } else {
      Arrays.fill(data, null);
    }
    size = 0;
  }
  


  private void grow()
  {
    assert (Thread.holdsLock(this));
    int len = data.length;
    int newCapacity = len + (len >> 1) + 1;
    if ((newCapacity > capacity) || (newCapacity < len)) {
      newCapacity = capacity;
    }
    assert (len != capacity) : len;
    data = ((LogRecord[])Arrays.copyOf(data, newCapacity, [Ljava.util.logging.LogRecord.class));
    matched = Arrays.copyOf(matched, newCapacity);
  }
  






  private synchronized void init(Properties props)
  {
    assert (errorManager != null);
    String p = getClass().getName();
    mailProps = new Properties();
    Object ccl = getAndSetContextClassLoader(MAILHANDLER_LOADER);
    try {
      contentTypes = FileTypeMap.getDefaultFileTypeMap();
    } finally {
      getAndSetContextClassLoader(ccl);
    }
    

    initErrorManager(p);
    
    initLevel(p);
    initFilter(p);
    initCapacity(p);
    initAuthenticator(p);
    
    initEncoding(p);
    initFormatter(p);
    initComparator(p);
    initPushLevel(p);
    initPushFilter(p);
    
    initSubject(p);
    
    initAttachmentFormaters(p);
    initAttachmentFilters(p);
    initAttachmentNames(p);
    
    if ((props == null) && (LogManagerProperties.fromLogManager(p.concat(".verify")) != null)) {
      verifySettings(initSession());
    }
    intern();
  }
  






  private void intern()
  {
    assert (Thread.holdsLock(this));
    
    try
    {
      Map<Object, Object> seen = new HashMap();
      try {
        intern(seen, errorManager);
      } catch (SecurityException se) {
        reportError(se.getMessage(), se, 4);
      }
      try
      {
        Object canidate = filter;
        Object result = intern(seen, canidate);
        if ((result != canidate) && ((result instanceof Filter))) {
          filter = ((Filter)result);
        }
        
        canidate = formatter;
        result = intern(seen, canidate);
        if ((result != canidate) && ((result instanceof Formatter))) {
          formatter = ((Formatter)result);
        }
      } catch (SecurityException se) {
        reportError(se.getMessage(), se, 4);
      }
      
      Object canidate = subjectFormatter;
      Object result = intern(seen, canidate);
      if ((result != canidate) && ((result instanceof Formatter))) {
        subjectFormatter = ((Formatter)result);
      }
      
      canidate = pushFilter;
      result = intern(seen, canidate);
      if ((result != canidate) && ((result instanceof Filter))) {
        pushFilter = ((Filter)result);
      }
      
      for (int i = 0; i < attachmentFormatters.length; i++) {
        canidate = attachmentFormatters[i];
        result = intern(seen, canidate);
        if ((result != canidate) && ((result instanceof Formatter))) {
          attachmentFormatters[i] = ((Formatter)result);
        }
        
        canidate = attachmentFilters[i];
        result = intern(seen, canidate);
        if ((result != canidate) && ((result instanceof Filter))) {
          attachmentFilters[i] = ((Filter)result);
        }
        
        canidate = attachmentNames[i];
        result = intern(seen, canidate);
        if ((result != canidate) && ((result instanceof Formatter))) {
          attachmentNames[i] = ((Formatter)result);
        }
      }
    } catch (Exception skip) {
      reportError(skip.getMessage(), skip, 4);
    } catch (LinkageError skip) {
      reportError(skip.getMessage(), new InvocationTargetException(skip), 4);
    }
  }
  










  private Object intern(Map<Object, Object> m, Object o)
    throws Exception
  {
    if (o == null) {
      return null;
    }
    




    Object key;
    




    Object key;
    




    if (o.getClass().getName().equals(TailNameFormatter.class.getName())) {
      key = o;
    }
    else
    {
      key = o.getClass().getConstructor(new Class[0]).newInstance(new Object[0]);
    }
    
    Object use;
    Object use;
    if (key.getClass() == o.getClass()) {
      Object found = m.get(key);
      Object use; if (found == null)
      {
        boolean right = key.equals(o);
        boolean left = o.equals(key);
        if ((right) && (left))
        {
          found = m.put(o, o);
          if (found != null) {
            reportNonDiscriminating(key, found);
            found = m.remove(key);
            if (found != o) {
              reportNonDiscriminating(key, found);
              m.clear();
            }
          }
        }
        else if (right != left) {
          reportNonSymmetric(o, key);
        }
        
        use = o;
      } else {
        Object use;
        if (o.getClass() == found.getClass()) {
          use = found;
        } else {
          reportNonDiscriminating(o, found);
          use = o;
        }
      }
    } else {
      use = o;
    }
    return use;
  }
  





  private static Formatter createSimpleFormatter()
  {
    return (Formatter)Formatter.class.cast(new SimpleFormatter());
  }
  




  private static boolean isEmpty(CharSequence s)
  {
    return (s == null) || (s.length() == 0);
  }
  




  private static boolean hasValue(String name)
  {
    return (!isEmpty(name)) && (!"null".equalsIgnoreCase(name));
  }
  





  private void initAttachmentFilters(String p)
  {
    assert (Thread.holdsLock(this));
    assert (attachmentFormatters != null);
    String list = LogManagerProperties.fromLogManager(p.concat(".attachment.filters"));
    if (!isEmpty(list)) {
      String[] names = list.split(",");
      Filter[] a = new Filter[names.length];
      for (int i = 0; i < a.length; i++) {
        names[i] = names[i].trim();
        if (!"null".equalsIgnoreCase(names[i])) {
          try {
            a[i] = LogManagerProperties.newFilter(names[i]);
          } catch (SecurityException SE) {
            throw SE;
          } catch (Exception E) {
            reportError(E.getMessage(), E, 4);
          }
        }
      }
      
      attachmentFilters = a;
      if (alignAttachmentFilters()) {
        reportError("Attachment filters.", 
          attachmentMismatch("Length mismatch."), 4);
      }
    } else {
      attachmentFilters = emptyFilterArray();
      alignAttachmentFilters();
    }
  }
  





  private void initAttachmentFormaters(String p)
  {
    assert (Thread.holdsLock(this));
    String list = LogManagerProperties.fromLogManager(p.concat(".attachment.formatters"));
    if (!isEmpty(list))
    {
      String[] names = list.split(",");
      Formatter[] a; Formatter[] a; if (names.length == 0) {
        a = emptyFormatterArray();
      } else {
        a = new Formatter[names.length];
      }
      
      for (int i = 0; i < a.length; i++) {
        names[i] = names[i].trim();
        if (!"null".equalsIgnoreCase(names[i])) {
          try {
            a[i] = LogManagerProperties.newFormatter(names[i]);
            if ((a[i] instanceof TailNameFormatter)) {
              Exception CNFE = new ClassNotFoundException(a[i].toString());
              reportError("Attachment formatter.", CNFE, 4);
              a[i] = createSimpleFormatter();
            }
          } catch (SecurityException SE) {
            throw SE;
          } catch (Exception E) {
            reportError(E.getMessage(), E, 4);
            a[i] = createSimpleFormatter();
          }
        } else {
          Exception NPE = new NullPointerException(atIndexMsg(i));
          reportError("Attachment formatter.", NPE, 4);
          a[i] = createSimpleFormatter();
        }
      }
      
      attachmentFormatters = a;
    } else {
      attachmentFormatters = emptyFormatterArray();
    }
  }
  





  private void initAttachmentNames(String p)
  {
    assert (Thread.holdsLock(this));
    assert (attachmentFormatters != null);
    
    String list = LogManagerProperties.fromLogManager(p.concat(".attachment.names"));
    if (!isEmpty(list)) {
      String[] names = list.split(",");
      Formatter[] a = new Formatter[names.length];
      for (int i = 0; i < a.length; i++) {
        names[i] = names[i].trim();
        if (!"null".equalsIgnoreCase(names[i])) {
          try {
            try {
              a[i] = LogManagerProperties.newFormatter(names[i]);
            }
            catch (ClassNotFoundException|ClassCastException literal) {
              a[i] = TailNameFormatter.of(names[i]);
            }
          } catch (SecurityException SE) {
            throw SE;
          } catch (Exception E) {
            reportError(E.getMessage(), E, 4);
          }
        } else {
          Exception NPE = new NullPointerException(atIndexMsg(i));
          reportError("Attachment names.", NPE, 4);
        }
      }
      
      attachmentNames = a;
      if (alignAttachmentNames()) {
        reportError("Attachment names.", 
          attachmentMismatch("Length mismatch."), 4);
      }
    } else {
      attachmentNames = emptyFormatterArray();
      alignAttachmentNames();
    }
  }
  





  private void initAuthenticator(String p)
  {
    assert (Thread.holdsLock(this));
    String name = LogManagerProperties.fromLogManager(p.concat(".authenticator"));
    if ((name != null) && (!"null".equalsIgnoreCase(name))) {
      if (name.length() != 0) {
        try
        {
          auth = ((Authenticator)LogManagerProperties.newObjectFrom(name, Authenticator.class));
        } catch (SecurityException SE) {
          throw SE;
        }
        catch (ClassNotFoundException|ClassCastException literalAuth) {
          auth = DefaultAuthenticator.of(name);
        } catch (Exception E) {
          reportError(E.getMessage(), E, 4);
        }
      } else {
        auth = DefaultAuthenticator.of(name);
      }
    }
  }
  





  private void initLevel(String p)
  {
    assert (Thread.holdsLock(this));
    try {
      String val = LogManagerProperties.fromLogManager(p.concat(".level"));
      if (val != null) {
        logLevel = Level.parse(val);
      } else {
        logLevel = Level.WARNING;
      }
    } catch (SecurityException SE) {
      throw SE;
    } catch (RuntimeException RE) {
      reportError(RE.getMessage(), RE, 4);
      logLevel = Level.WARNING;
    }
  }
  





  private void initFilter(String p)
  {
    assert (Thread.holdsLock(this));
    try {
      String name = LogManagerProperties.fromLogManager(p.concat(".filter"));
      if (hasValue(name)) {
        filter = LogManagerProperties.newFilter(name);
      }
    } catch (SecurityException SE) {
      throw SE;
    } catch (Exception E) {
      reportError(E.getMessage(), E, 4);
    }
  }
  





  private void initCapacity(String p)
  {
    assert (Thread.holdsLock(this));
    int DEFAULT_CAPACITY = 1000;
    try {
      String value = LogManagerProperties.fromLogManager(p.concat(".capacity"));
      if (value != null) {
        setCapacity0(Integer.parseInt(value));
      } else {
        setCapacity0(1000);
      }
    } catch (SecurityException SE) {
      throw SE;
    } catch (RuntimeException RE) {
      reportError(RE.getMessage(), RE, 4);
    }
    
    if (capacity <= 0) {
      capacity = 1000;
    }
    
    data = new LogRecord[1];
    matched = new int[data.length];
  }
  





  private void initEncoding(String p)
  {
    assert (Thread.holdsLock(this));
    try {
      String e = LogManagerProperties.fromLogManager(p.concat(".encoding"));
      if (e != null) {
        setEncoding0(e);
      }
    } catch (SecurityException SE) {
      throw SE;
    } catch (UnsupportedEncodingException|RuntimeException UEE) {
      reportError(UEE.getMessage(), UEE, 4);
    }
  }
  


  private ErrorManager defaultErrorManager()
  {
    ErrorManager em;
    
    try
    {
      em = super.getErrorManager();
    } catch (RuntimeException|LinkageError ignore) { ErrorManager em;
      em = null;
    }
    

    if (em == null) {
      em = new ErrorManager();
    }
    return em;
  }
  





  private void initErrorManager(String p)
  {
    assert (Thread.holdsLock(this));
    try {
      String name = LogManagerProperties.fromLogManager(p.concat(".errorManager"));
      if (name != null) {
        setErrorManager0(LogManagerProperties.newErrorManager(name));
      }
    } catch (SecurityException SE) {
      throw SE;
    } catch (Exception E) {
      reportError(E.getMessage(), E, 4);
    }
  }
  





  private void initFormatter(String p)
  {
    assert (Thread.holdsLock(this));
    try {
      String name = LogManagerProperties.fromLogManager(p.concat(".formatter"));
      if (hasValue(name))
      {
        Formatter f = LogManagerProperties.newFormatter(name);
        assert (f != null);
        if (!(f instanceof TailNameFormatter)) {
          formatter = f;
        } else {
          formatter = createSimpleFormatter();
        }
      } else {
        formatter = createSimpleFormatter();
      }
    } catch (SecurityException SE) {
      throw SE;
    } catch (Exception E) {
      reportError(E.getMessage(), E, 4);
      formatter = createSimpleFormatter();
    }
  }
  





  private void initComparator(String p)
  {
    assert (Thread.holdsLock(this));
    try {
      String name = LogManagerProperties.fromLogManager(p.concat(".comparator"));
      String reverse = LogManagerProperties.fromLogManager(p.concat(".comparator.reverse"));
      if (hasValue(name)) {
        comparator = LogManagerProperties.newComparator(name);
        if (Boolean.parseBoolean(reverse)) {
          assert (comparator != null) : "null";
          comparator = LogManagerProperties.reverseOrder(comparator);
        }
      }
      else if (!isEmpty(reverse)) {
        throw new IllegalArgumentException("No comparator to reverse.");
      }
    }
    catch (SecurityException SE)
    {
      throw SE;
    } catch (Exception E) {
      reportError(E.getMessage(), E, 4);
    }
  }
  





  private void initPushLevel(String p)
  {
    assert (Thread.holdsLock(this));
    try {
      String val = LogManagerProperties.fromLogManager(p.concat(".pushLevel"));
      if (val != null) {
        pushLevel = Level.parse(val);
      }
    } catch (RuntimeException RE) {
      reportError(RE.getMessage(), RE, 4);
    }
    
    if (pushLevel == null) {
      pushLevel = Level.OFF;
    }
  }
  





  private void initPushFilter(String p)
  {
    assert (Thread.holdsLock(this));
    try {
      String name = LogManagerProperties.fromLogManager(p.concat(".pushFilter"));
      if (hasValue(name)) {
        pushFilter = LogManagerProperties.newFilter(name);
      }
    } catch (SecurityException SE) {
      throw SE;
    } catch (Exception E) {
      reportError(E.getMessage(), E, 4);
    }
  }
  





  private void initSubject(String p)
  {
    assert (Thread.holdsLock(this));
    String name = LogManagerProperties.fromLogManager(p.concat(".subject"));
    if (name == null) {
      name = "com.sun.mail.util.logging.CollectorFormatter";
    }
    
    if (hasValue(name)) {
      try {
        subjectFormatter = LogManagerProperties.newFormatter(name);
      } catch (SecurityException SE) {
        throw SE;
      }
      catch (ClassNotFoundException|ClassCastException literalSubject) {
        subjectFormatter = TailNameFormatter.of(name);
      } catch (Exception E) {
        subjectFormatter = TailNameFormatter.of(name);
        reportError(E.getMessage(), E, 4);
      }
    } else {
      subjectFormatter = TailNameFormatter.of(name);
    }
  }
  






  private boolean isAttachmentLoggable(LogRecord record)
  {
    Filter[] filters = readOnlyAttachmentFilters();
    for (int i = 0; i < filters.length; i++) {
      Filter f = filters[i];
      if ((f == null) || (f.isLoggable(record))) {
        setMatchedPart(i);
        return true;
      }
    }
    return false;
  }
  






  private boolean isPushable(LogRecord record)
  {
    assert (Thread.holdsLock(this));
    int value = getPushLevel().intValue();
    if ((value == offValue) || (record.getLevel().intValue() < value)) {
      return false;
    }
    
    Filter push = getPushFilter();
    if (push == null) {
      return true;
    }
    
    int match = getMatchedPart();
    if (((match == -1) && (getFilter() == push)) || ((match >= 0) && (attachmentFilters[match] == push)))
    {
      return true;
    }
    return push.isLoggable(record);
  }
  





  private void push(boolean priority, int code)
  {
    if (tryMutex()) {
      try {
        Message msg = writeLogRecords(code);
        if (msg != null) {
          send(msg, priority, code);
        }
      } catch (LinkageError JDK8152515) {
        reportLinkageError(JDK8152515, code);
      } finally {
        releaseMutex();
      }
    } else {
      reportUnPublishedError(null);
    }
  }
  








  private void send(Message msg, boolean priority, int code)
  {
    try
    {
      envelopeFor(msg, priority);
      Object ccl = getAndSetContextClassLoader(MAILHANDLER_LOADER);
      try {
        Transport.send(msg);
      } finally {
        getAndSetContextClassLoader(ccl);
      }
    } catch (RuntimeException re) {
      reportError(msg, re, code);
    } catch (Exception e) {
      reportError(msg, e, code);
    }
  }
  



  private void sort()
  {
    assert (Thread.holdsLock(this));
    if (comparator != null) {
      try {
        if (size != 1) {
          Arrays.sort(data, 0, size, comparator);
        }
        else if (comparator.compare(data[0], data[0]) != 0)
        {
          throw new IllegalArgumentException(comparator.getClass().getName());
        }
      }
      catch (RuntimeException RE) {
        reportError(RE.getMessage(), RE, 5);
      }
    }
  }
  







  private Message writeLogRecords(int code)
  {
    try
    {
      synchronized (this) {
        if ((size > 0) && (!isWriting)) {
          isWriting = true;
          try {
            Message localMessage = writeLogRecords0();
            
            isWriting = false;
            if (size > 0) {
              reset();
            }
            return localMessage;
          } finally {
            isWriting = false;
            if (size > 0) {
              reset();
            }
          }
        }
      }
    } catch (RuntimeException re) {
      reportError(re.getMessage(), re, code);
    } catch (Exception e) {
      reportError(e.getMessage(), e, code);
    }
    return null;
  }
  










  private Message writeLogRecords0()
    throws Exception
  {
    assert (Thread.holdsLock(this));
    sort();
    if (session == null) {
      initSession();
    }
    MimeMessage msg = new MimeMessage(session);
    





    MimeBodyPart[] parts = new MimeBodyPart[attachmentFormatters.length];
    



    StringBuilder[] buffers = new StringBuilder[parts.length];
    StringBuilder buf = null;
    MimePart body;
    MimePart body; if (parts.length == 0) {
      msg.setDescription(descriptionFrom(
        getFormatter(), getFilter(), subjectFormatter));
      body = msg;
    } else {
      msg.setDescription(descriptionFrom(comparator, pushLevel, pushFilter));
      
      body = createBodyPart();
    }
    
    appendSubject(msg, head(subjectFormatter));
    Formatter bodyFormat = getFormatter();
    Filter bodyFilter = getFilter();
    
    Locale lastLocale = null;
    for (int ix = 0; ix < size; ix++) {
      boolean formatted = false;
      int match = matched[ix];
      LogRecord r = data[ix];
      data[ix] = null;
      
      Locale locale = localeFor(r);
      appendSubject(msg, format(subjectFormatter, r));
      Filter lmf = null;
      if ((bodyFilter == null) || (match == -1) || (parts.length == 0) || ((match < -1) && 
        (bodyFilter.isLoggable(r)))) {
        lmf = bodyFilter;
        if (buf == null) {
          buf = new StringBuilder();
          buf.append(head(bodyFormat));
        }
        formatted = true;
        buf.append(format(bodyFormat, r));
        if ((locale != null) && (!locale.equals(lastLocale))) {
          appendContentLang(body, locale);
        }
      }
      
      for (int i = 0; i < parts.length; i++)
      {

        Filter af = attachmentFilters[i];
        if ((af == null) || (lmf == af) || (match == i) || ((match < i) && 
          (af.isLoggable(r)))) {
          if ((lmf == null) && (af != null)) {
            lmf = af;
          }
          if (parts[i] == null) {
            parts[i] = createBodyPart(i);
            buffers[i] = new StringBuilder();
            buffers[i].append(head(attachmentFormatters[i]));
            appendFileName(parts[i], head(attachmentNames[i]));
          }
          formatted = true;
          appendFileName(parts[i], format(attachmentNames[i], r));
          buffers[i].append(format(attachmentFormatters[i], r));
          if ((locale != null) && (!locale.equals(lastLocale))) {
            appendContentLang(parts[i], locale);
          }
        }
      }
      
      if (formatted) {
        if ((body != msg) && (locale != null) && 
          (!locale.equals(lastLocale))) {
          appendContentLang(msg, locale);
        }
      } else {
        reportFilterError(r);
      }
      lastLocale = locale;
    }
    size = 0;
    
    for (int i = parts.length - 1; i >= 0; i--) {
      if (parts[i] != null) {
        appendFileName(parts[i], tail(attachmentNames[i], "err"));
        buffers[i].append(tail(attachmentFormatters[i], ""));
        
        if (buffers[i].length() > 0) {
          String name = parts[i].getFileName();
          if (isEmpty(name)) {
            name = toString(attachmentFormatters[i]);
            parts[i].setFileName(name);
          }
          setContent(parts[i], buffers[i], getContentType(name));
        } else {
          setIncompleteCopy(msg);
          parts[i] = null;
        }
        buffers[i] = null;
      }
    }
    
    if (buf != null) {
      buf.append(tail(bodyFormat, ""));
    }
    else
    {
      buf = new StringBuilder(0);
    }
    
    appendSubject(msg, tail(subjectFormatter, ""));
    
    String contentType = contentTypeOf(buf);
    String altType = contentTypeOf(bodyFormat);
    setContent(body, buf, altType == null ? contentType : altType);
    if (body != msg) {
      MimeMultipart multipart = new MimeMultipart();
      
      multipart.addBodyPart((BodyPart)body);
      
      for (int i = 0; i < parts.length; i++) {
        if (parts[i] != null) {
          multipart.addBodyPart(parts[i]);
        }
      }
      msg.setContent(multipart);
    }
    
    return msg;
  }
  







  private void verifySettings(Session session)
  {
    try
    {
      if (session != null) {
        Properties props = session.getProperties();
        Object check = props.put("verify", "");
        if ((check instanceof String)) {
          String value = (String)check;
          
          if (hasValue(value)) {
            verifySettings0(session, value);
          }
        }
        else if (check != null) {
          verifySettings0(session, check.getClass().toString());
        }
      }
    }
    catch (LinkageError JDK8152515) {
      reportLinkageError(JDK8152515, 4);
    }
  }
  










  private void verifySettings0(Session session, String verify)
  {
    assert (verify != null) : ((String)null);
    if ((!"local".equals(verify)) && (!"remote".equals(verify)) && 
      (!"limited".equals(verify)) && (!"resolve".equals(verify)) && 
      (!"login".equals(verify))) {
      reportError("Verify must be 'limited', local', 'resolve', 'login', or 'remote'.", new IllegalArgumentException(verify), 4);
      


      return;
    }
    
    MimeMessage abort = new MimeMessage(session);
    String msg;
    if (!"limited".equals(verify))
    {
      String msg = "Local address is " + InternetAddress.getLocalAddress(session) + '.';
      try
      {
        Charset.forName(getEncodingName());
      }
      catch (RuntimeException RE) {
        UnsupportedEncodingException UEE = new UnsupportedEncodingException(RE.toString());
        UEE.initCause(RE);
        reportError(msg, UEE, 5);
      }
    } else {
      msg = "Skipping local address check.";
    }
    


    synchronized (this) {
      appendSubject(abort, head(subjectFormatter));
      appendSubject(abort, tail(subjectFormatter, ""));
      String[] atn = new String[attachmentNames.length];
      for (int i = 0; i < atn.length; i++) {
        atn[i] = head(attachmentNames[i]);
        if (atn[i].length() == 0) {
          atn[i] = tail(attachmentNames[i], "");
        } else {
          atn[i] = atn[i].concat(tail(attachmentNames[i], ""));
        }
      }
    }
    String[] atn;
    setIncompleteCopy(abort);
    envelopeFor(abort, true);
    saveChangesNoContent(abort, msg);
    try
    {
      Address[] all = abort.getAllRecipients();
      if (all == null) {
        all = new InternetAddress[0];
      }
      try
      {
        Address[] any = all.length != 0 ? all : abort.getFrom();
        if ((any != null) && (any.length != 0)) {
          Transport t = session.getTransport(any[0]);
          session.getProperty("mail.transport.protocol");
        } else {
          MessagingException me = new MessagingException("No recipient or from address.");
          
          reportError(msg, me, 4);
          throw me;
        }
      } catch (MessagingException protocol) {
        Transport t;
        Object ccl = getAndSetContextClassLoader(MAILHANDLER_LOADER);
        try {
          t = session.getTransport();
        } catch (MessagingException fail) { Transport t;
          throw attach(protocol, fail);
        } finally {
          getAndSetContextClassLoader(ccl);
        }
      }
      Transport t;
      String local = null;
      if (("remote".equals(verify)) || ("login".equals(verify))) {
        MessagingException closed = null;
        t.connect();
        try
        {
          try {
            local = getLocalHost(t);
            



            if ("remote".equals(verify)) {
              t.sendMessage(abort, all);
            }
            try
            {
              t.close();
            } catch (MessagingException ME) {
              closed = ME;
            }
            

            if (!"remote".equals(verify)) {
              break label611;
            }
          }
          finally
          {
            try
            {
              t.close();
            } catch (MessagingException ME) {
              closed = ME;
            }
          }
          

          reportUnexpectedSend(abort, verify, null);
          break label627;
          label611: String protocol = t.getURLName().getProtocol();
          verifyProperties(session, protocol);
        }
        catch (SendFailedException sfe) {
          Address[] recip = sfe.getInvalidAddresses();
          if ((recip != null) && (recip.length != 0)) {
            setErrorContent(abort, verify, sfe);
            reportError(abort, sfe, 4);
          }
          
          recip = sfe.getValidSentAddresses();
          if ((recip != null) && (recip.length != 0))
            reportUnexpectedSend(abort, verify, sfe);
        } catch (MessagingException ME) {
          label627:
          if (!isMissingContent(abort, ME)) {
            setErrorContent(abort, verify, ME);
            reportError(abort, ME, 4);
          }
        }
        
        if (closed != null) {
          setErrorContent(abort, verify, closed);
          reportError(abort, closed, 3);
        }
      }
      else {
        String protocol = t.getURLName().getProtocol();
        verifyProperties(session, protocol);
        String mailHost = session.getProperty("mail." + protocol + ".host");
        
        if (isEmpty(mailHost)) {
          mailHost = session.getProperty("mail.host");
        } else {
          session.getProperty("mail.host");
        }
        
        local = session.getProperty("mail." + protocol + ".localhost");
        if (isEmpty(local)) {
          local = session.getProperty("mail." + protocol + ".localaddress");
        }
        else {
          session.getProperty("mail." + protocol + ".localaddress");
        }
        
        if ("resolve".equals(verify)) {
          try {
            String transportHost = t.getURLName().getHost();
            if (!isEmpty(transportHost)) {
              verifyHost(transportHost);
              if (!transportHost.equalsIgnoreCase(mailHost)) {
                verifyHost(mailHost);
              }
            } else {
              verifyHost(mailHost);
            }
          } catch (RuntimeException|IOException IOE) {
            MessagingException ME = new MessagingException(msg, IOE);
            
            setErrorContent(abort, verify, ME);
            reportError(abort, ME, 4);
          }
        }
      }
      
      if (!"limited".equals(verify)) {
        try {
          if ((!"remote".equals(verify)) && (!"login".equals(verify))) {
            local = getLocalHost(t);
          }
          verifyHost(local);
        } catch (RuntimeException|IOException IOE) {
          MessagingException ME = new MessagingException(msg, IOE);
          setErrorContent(abort, verify, ME);
          reportError(abort, ME, 4);
        }
        try
        {
          Object ccl = getAndSetContextClassLoader(MAILHANDLER_LOADER);
          try
          {
            MimeMultipart multipart = new MimeMultipart();
            MimeBodyPart[] ambp = new MimeBodyPart[atn.length];
            

            synchronized (this) {
              String bodyContentType = contentTypeOf(getFormatter());
              MimeBodyPart body = createBodyPart();
              for (int i = 0; i < atn.length; i++) {
                ambp[i] = createBodyPart(i);
                ambp[i].setFileName(atn[i]);
                
                atn[i] = getContentType(atn[i]);
              } }
            String bodyContentType;
            MimeBodyPart body;
            body.setDescription(verify);
            setContent(body, "", bodyContentType);
            multipart.addBodyPart(body);
            for (int i = 0; i < ambp.length; i++) {
              ambp[i].setDescription(verify);
              setContent(ambp[i], "", atn[i]);
            }
            
            abort.setContent(multipart);
            abort.saveChanges();
            abort.writeTo(new ByteArrayOutputStream(1024));
          } finally {
            getAndSetContextClassLoader(ccl);
          }
        } catch (IOException IOE) {
          MessagingException ME = new MessagingException(msg, IOE);
          setErrorContent(abort, verify, ME);
          reportError(abort, ME, 5);
        }
      }
      

      if (all.length != 0) {
        verifyAddresses(all);
      } else {
        throw new MessagingException("No recipient addresses.");
      }
      

      Address[] from = abort.getFrom();
      Address sender = abort.getSender();
      if ((sender instanceof InternetAddress)) {
        ((InternetAddress)sender).validate();
      }
      

      if ((abort.getHeader("From", ",") != null) && (from.length != 0)) {
        verifyAddresses(from);
        for (int i = 0; i < from.length; i++) {
          if (from[i].equals(sender)) {
            MessagingException ME = new MessagingException("Sender address '" + sender + "' equals from address.");
            

            throw new MessagingException(msg, ME);
          }
        }
      }
      else if (sender == null) {
        MessagingException ME = new MessagingException("No from or sender address.");
        
        throw new MessagingException(msg, ME);
      }
      


      verifyAddresses(abort.getReplyTo());
    } catch (RuntimeException RE) {
      setErrorContent(abort, verify, RE);
      reportError(abort, RE, 4);
    } catch (Exception ME) {
      setErrorContent(abort, verify, ME);
      reportError(abort, ME, 4);
    }
  }
  







  private void saveChangesNoContent(Message abort, String msg)
  {
    if (abort != null) {
      try {
        try {
          abort.saveChanges();
        }
        catch (NullPointerException xferEncoding)
        {
          try {
            String cte = "Content-Transfer-Encoding";
            if (abort.getHeader(cte) == null) {
              abort.setHeader(cte, "base64");
              abort.saveChanges();
            } else {
              throw xferEncoding;
            }
          } catch (RuntimeException|MessagingException e) {
            if (e != xferEncoding) {
              e.addSuppressed(xferEncoding);
            }
            throw e;
          }
        }
      } catch (RuntimeException|MessagingException ME) {
        reportError(msg, ME, 5);
      }
    }
  }
  








  private static void verifyProperties(Session session, String protocol)
  {
    session.getProperty("mail.from");
    session.getProperty("mail." + protocol + ".from");
    session.getProperty("mail.dsn.ret");
    session.getProperty("mail." + protocol + ".dsn.ret");
    session.getProperty("mail.dsn.notify");
    session.getProperty("mail." + protocol + ".dsn.notify");
    session.getProperty("mail." + protocol + ".port");
    session.getProperty("mail.user");
    session.getProperty("mail." + protocol + ".user");
    session.getProperty("mail." + protocol + ".localport");
  }
  


  private static InetAddress verifyHost(String host)
    throws IOException
  {
    InetAddress a;
    

    InetAddress a;
    

    if (isEmpty(host)) {
      a = InetAddress.getLocalHost();
    } else {
      a = InetAddress.getByName(host);
    }
    if (a.getCanonicalHostName().length() == 0) {
      throw new UnknownHostException();
    }
    return a;
  }
  






  private static void verifyAddresses(Address[] all)
    throws AddressException
  {
    if (all != null) {
      for (int i = 0; i < all.length; i++) {
        Address a = all[i];
        if ((a instanceof InternetAddress)) {
          ((InternetAddress)a).validate();
        }
      }
    }
  }
  






  private void reportUnexpectedSend(MimeMessage msg, String verify, Exception cause)
  {
    MessagingException write = new MessagingException("An empty message was sent.", cause);
    
    setErrorContent(msg, verify, write);
    reportError(msg, write, 4);
  }
  





  private void setErrorContent(MimeMessage msg, String verify, Throwable t)
  {
    try
    {
      String subjectType;
      



      synchronized (this) {
        MimeBodyPart body = createBodyPart();
        String msgDesc = descriptionFrom(comparator, pushLevel, pushFilter);
        subjectType = getClassId(subjectFormatter); }
      String msgDesc;
      String subjectType;
      MimeBodyPart body; body.setDescription("Formatted using " + (t == null ? Throwable.class
        .getName() : t
        .getClass().getName()) + ", filtered with " + verify + ", and named by " + subjectType + '.');
      

      setContent(body, toMsgString(t), "text/plain");
      MimeMultipart multipart = new MimeMultipart();
      multipart.addBodyPart(body);
      msg.setContent(multipart);
      msg.setDescription(msgDesc);
      setAcceptLang(msg);
      msg.saveChanges();
    } catch (MessagingException|RuntimeException ME) {
      reportError("Unable to create body.", ME, 4);
    }
  }
  




  private Session updateSession()
  {
    assert (Thread.holdsLock(this));
    Session settings;
    if (mailProps.getProperty("verify") != null) {
      Session settings = initSession();
      if ((!$assertionsDisabled) && (settings != session)) throw new AssertionError(session);
    } else {
      session = null;
      settings = null;
    }
    return settings;
  }
  



  private Session initSession()
  {
    assert (Thread.holdsLock(this));
    String p = getClass().getName();
    LogManagerProperties proxy = new LogManagerProperties(mailProps, p);
    session = Session.getInstance(proxy, auth);
    return session;
  }
  






  private void envelopeFor(Message msg, boolean priority)
  {
    setAcceptLang(msg);
    setFrom(msg);
    if (!setRecipient(msg, "mail.to", Message.RecipientType.TO)) {
      setDefaultRecipient(msg, Message.RecipientType.TO);
    }
    setRecipient(msg, "mail.cc", Message.RecipientType.CC);
    setRecipient(msg, "mail.bcc", Message.RecipientType.BCC);
    setReplyTo(msg);
    setSender(msg);
    setMailer(msg);
    setAutoSubmitted(msg);
    if (priority) {
      setPriority(msg);
    }
    try
    {
      msg.setSentDate(new Date());
    } catch (MessagingException ME) {
      reportError(ME.getMessage(), ME, 5);
    }
  }
  



  private MimeBodyPart createBodyPart()
    throws MessagingException
  {
    assert (Thread.holdsLock(this));
    MimeBodyPart part = new MimeBodyPart();
    part.setDisposition("inline");
    part.setDescription(descriptionFrom(getFormatter(), 
      getFilter(), subjectFormatter));
    setAcceptLang(part);
    return part;
  }
  






  private MimeBodyPart createBodyPart(int index)
    throws MessagingException
  {
    assert (Thread.holdsLock(this));
    MimeBodyPart part = new MimeBodyPart();
    part.setDisposition("attachment");
    part.setDescription(descriptionFrom(attachmentFormatters[index], attachmentFilters[index], attachmentNames[index]));
    


    setAcceptLang(part);
    return part;
  }
  










  private String descriptionFrom(Comparator<?> c, Level l, Filter f)
  {
    return 
    

      "Sorted using " + (c == null ? "no comparator" : c.getClass().getName()) + ", pushed when " + l.getName() + ", and " + (f == null ? "no push filter" : f.getClass().getName()) + '.';
  }
  






  private String descriptionFrom(Formatter f, Filter filter, Formatter name)
  {
    return 
    

      "Formatted using " + getClassId(f) + ", filtered with " + (filter == null ? "no filter" : filter.getClass().getName()) + ", and named by " + getClassId(name) + '.';
  }
  







  private String getClassId(Formatter f)
  {
    if ((f instanceof TailNameFormatter)) {
      return String.class.getName();
    }
    return f.getClass().getName();
  }
  






  private String toString(Formatter f)
  {
    String name = f.toString();
    if (!isEmpty(name)) {
      return name;
    }
    return getClassId(f);
  }
  






  private void appendFileName(Part part, String chunk)
  {
    if (chunk != null) {
      if (chunk.length() > 0) {
        appendFileName0(part, chunk);
      }
    } else {
      reportNullError(5);
    }
  }
  





  private void appendFileName0(Part part, String chunk)
  {
    try
    {
      chunk = chunk.replaceAll("[\\x00-\\x1F\\x7F]+", "");
      String old = part.getFileName();
      part.setFileName(old != null ? old.concat(chunk) : chunk);
    } catch (MessagingException ME) {
      reportError(ME.getMessage(), ME, 5);
    }
  }
  




  private void appendSubject(Message msg, String chunk)
  {
    if (chunk != null) {
      if (chunk.length() > 0) {
        appendSubject0(msg, chunk);
      }
    } else {
      reportNullError(5);
    }
  }
  





  private void appendSubject0(Message msg, String chunk)
  {
    try
    {
      chunk = chunk.replaceAll("[\\x00-\\x1F\\x7F]+", "");
      String charset = getEncodingName();
      String old = msg.getSubject();
      assert ((msg instanceof MimeMessage)) : msg;
      ((MimeMessage)msg).setSubject(old != null ? old.concat(chunk) : chunk, 
        MimeUtility.mimeCharset(charset));
    } catch (MessagingException ME) {
      reportError(ME.getMessage(), ME, 5);
    }
  }
  








  private Locale localeFor(LogRecord r)
  {
    ResourceBundle rb = r.getResourceBundle();
    Locale l; if (rb != null) {
      Locale l = rb.getLocale();
      if ((l == null) || (isEmpty(l.getLanguage())))
      {




        l = Locale.getDefault();
      }
    } else {
      l = null;
    }
    return l;
  }
  








  private void appendContentLang(MimePart p, Locale l)
  {
    try
    {
      String lang = LogManagerProperties.toLanguageTag(l);
      if (lang.length() != 0) {
        String header = p.getHeader("Content-Language", null);
        if (isEmpty(header)) {
          p.setHeader("Content-Language", lang);
        } else if (!header.equalsIgnoreCase(lang)) {
          lang = ",".concat(lang);
          int idx = 0;
          while ((idx = header.indexOf(lang, idx)) > -1) {
            idx += lang.length();
            if (idx != header.length()) {
              if (header.charAt(idx) == ',') {
                break;
              }
            }
          }
          if (idx < 0) {
            int len = header.lastIndexOf("\r\n\t");
            if (len < 0) {
              len = 20 + header.length();
            } else {
              len = header.length() - len + 8;
            }
            

            if (len + lang.length() > 76) {
              header = header.concat("\r\n\t".concat(lang));
            } else {
              header = header.concat(lang);
            }
            p.setHeader("Content-Language", header);
          }
        }
      }
    } catch (MessagingException ME) {
      reportError(ME.getMessage(), ME, 5);
    }
  }
  





  private void setAcceptLang(Part p)
  {
    try
    {
      String lang = LogManagerProperties.toLanguageTag(Locale.getDefault());
      if (lang.length() != 0) {
        p.setHeader("Accept-Language", lang);
      }
    } catch (MessagingException ME) {
      reportError(ME.getMessage(), ME, 5);
    }
  }
  







  private void reportFilterError(LogRecord record)
  {
    assert (Thread.holdsLock(this));
    Formatter f = createSimpleFormatter();
    

    String msg = "Log record " + record.getSequenceNumber() + " was filtered from all message parts.  " + head(f) + format(f, record) + tail(f, "");
    
    String txt = getFilter() + ", " + Arrays.asList(readOnlyAttachmentFilters());
    reportError(msg, new IllegalArgumentException(txt), 5);
  }
  







  private void reportNonSymmetric(Object o, Object found)
  {
    reportError("Non symmetric equals implementation.", new IllegalArgumentException(o
      .getClass().getName() + " is not equal to " + found
      .getClass().getName()), 4);
  }
  








  private void reportNonDiscriminating(Object o, Object found)
  {
    reportError("Non discriminating equals implementation.", new IllegalArgumentException(o
      .getClass().getName() + " should not be equal to " + found
      .getClass().getName()), 4);
  }
  





  private void reportNullError(int code)
  {
    reportError("null", new NullPointerException(), code);
  }
  



  private String head(Formatter f)
  {
    try
    {
      return f.getHead(this);
    } catch (RuntimeException RE) {
      reportError(RE.getMessage(), RE, 5); }
    return "";
  }
  





  private String format(Formatter f, LogRecord r)
  {
    try
    {
      return f.format(r);
    } catch (RuntimeException RE) {
      reportError(RE.getMessage(), RE, 5); }
    return "";
  }
  





  private String tail(Formatter f, String def)
  {
    try
    {
      return f.getTail(this);
    } catch (RuntimeException RE) {
      reportError(RE.getMessage(), RE, 5); }
    return def;
  }
  



  private void setMailer(Message msg)
  {
    try
    {
      Class<?> mail = MailHandler.class;
      Class<?> k = getClass();
      String value;
      String value; if (k == mail) {
        value = mail.getName();
      } else {
        try {
          value = MimeUtility.encodeText(k.getName());
        } catch (UnsupportedEncodingException E) { String value;
          reportError(E.getMessage(), E, 5);
          value = k.getName().replaceAll("[^\\x00-\\x7F]", "\032");
        }
        value = MimeUtility.fold(10, mail.getName() + " using the " + value + " extension.");
      }
      
      msg.setHeader("X-Mailer", value);
    } catch (MessagingException ME) {
      reportError(ME.getMessage(), ME, 5);
    }
  }
  


  private void setPriority(Message msg)
  {
    try
    {
      msg.setHeader("Importance", "High");
      msg.setHeader("Priority", "urgent");
      msg.setHeader("X-Priority", "2");
    } catch (MessagingException ME) {
      reportError(ME.getMessage(), ME, 5);
    }
  }
  








  private void setIncompleteCopy(Message msg)
  {
    try
    {
      msg.setHeader("Incomplete-Copy", "");
    } catch (MessagingException ME) {
      reportError(ME.getMessage(), ME, 5);
    }
  }
  





  private void setAutoSubmitted(Message msg)
  {
    if (allowRestrictedHeaders()) {
      try {
        msg.setHeader("auto-submitted", "auto-generated");
      } catch (MessagingException ME) {
        reportError(ME.getMessage(), ME, 5);
      }
    }
  }
  



  private void setFrom(Message msg)
  {
    String from = getSession(msg).getProperty("mail.from");
    if (from != null) {
      try {
        Address[] address = InternetAddress.parse(from, false);
        if (address.length > 0) {
          if (address.length == 1) {
            msg.setFrom(address[0]);
          } else {
            msg.addFrom(address);
          }
          
        }
        
      }
      catch (MessagingException ME)
      {
        reportError(ME.getMessage(), ME, 5);
        setDefaultFrom(msg);
      }
    } else {
      setDefaultFrom(msg);
    }
  }
  


  private void setDefaultFrom(Message msg)
  {
    try
    {
      msg.setFrom();
    } catch (MessagingException ME) {
      reportError(ME.getMessage(), ME, 5);
    }
  }
  






  private void setDefaultRecipient(Message msg, Message.RecipientType type)
  {
    try
    {
      Address a = InternetAddress.getLocalAddress(getSession(msg));
      if (a != null) {
        msg.setRecipient(type, a);
      } else {
        MimeMessage m = new MimeMessage(getSession(msg));
        m.setFrom();
        Address[] from = m.getFrom();
        if (from.length > 0) {
          msg.setRecipients(type, from);
        } else {
          throw new MessagingException("No local address.");
        }
      }
    } catch (MessagingException|RuntimeException ME) {
      reportError("Unable to compute a default recipient.", ME, 5);
    }
  }
  




  private void setReplyTo(Message msg)
  {
    String reply = getSession(msg).getProperty("mail.reply.to");
    if (!isEmpty(reply)) {
      try {
        Address[] address = InternetAddress.parse(reply, false);
        if (address.length > 0) {
          msg.setReplyTo(address);
        }
      } catch (MessagingException ME) {
        reportError(ME.getMessage(), ME, 5);
      }
    }
  }
  



  private void setSender(Message msg)
  {
    assert ((msg instanceof MimeMessage)) : msg;
    String sender = getSession(msg).getProperty("mail.sender");
    if (!isEmpty(sender)) {
      try
      {
        InternetAddress[] address = InternetAddress.parse(sender, false);
        if (address.length > 0) {
          ((MimeMessage)msg).setSender(address[0]);
          if (address.length > 1) {
            reportError("Ignoring other senders.", 
              tooManyAddresses(address, 1), 5);
          }
        }
      }
      catch (MessagingException ME) {
        reportError(ME.getMessage(), ME, 5);
      }
    }
  }
  





  private AddressException tooManyAddresses(Address[] address, int offset)
  {
    Object l = Arrays.asList(address).subList(offset, address.length);
    return new AddressException(l.toString());
  }
  








  private boolean setRecipient(Message msg, String key, Message.RecipientType type)
  {
    String value = getSession(msg).getProperty(key);
    boolean containsKey = value != null;
    if (!isEmpty(value)) {
      try {
        Address[] address = InternetAddress.parse(value, false);
        if (address.length > 0) {
          msg.setRecipients(type, address);
        }
      } catch (MessagingException ME) {
        reportError(ME.getMessage(), ME, 5);
      }
    }
    return containsKey;
  }
  







  private String toRawString(Message msg)
    throws MessagingException, IOException
  {
    if (msg != null) {
      Object ccl = getAndSetContextClassLoader(MAILHANDLER_LOADER);
      try {
        int nbytes = Math.max(msg.getSize() + 1024, 1024);
        ByteArrayOutputStream out = new ByteArrayOutputStream(nbytes);
        msg.writeTo(out);
        return out.toString("UTF-8");
      } finally {
        getAndSetContextClassLoader(ccl);
      }
    }
    return null;
  }
  





  private String toMsgString(Throwable t)
  {
    if (t == null) {
      return "null";
    }
    
    String charset = getEncodingName();
    try {
      ByteArrayOutputStream out = new ByteArrayOutputStream(1024);
      


      OutputStreamWriter ows = new OutputStreamWriter(out, charset);Throwable localThrowable6 = null;
      try { PrintWriter pw = new PrintWriter(ows);Throwable localThrowable7 = null;
        try { pw.println(t.getMessage());
          t.printStackTrace(pw);
          pw.flush();
        }
        catch (Throwable localThrowable1)
        {
          localThrowable7 = localThrowable1;throw localThrowable1; } finally {} } catch (Throwable localThrowable4) { localThrowable6 = localThrowable4;throw localThrowable4;

      }
      finally
      {
        if (ows != null) if (localThrowable6 != null) try { ows.close(); } catch (Throwable localThrowable5) { localThrowable6.addSuppressed(localThrowable5); } else ows.close(); }
      return out.toString(charset);
    } catch (RuntimeException unexpected) {
      return t.toString() + ' ' + unexpected.toString();
    } catch (Exception badMimeCharset) {
      return t.toString() + ' ' + badMimeCharset.toString();
    }
  }
  







  private Object getAndSetContextClassLoader(Object ccl)
  {
    if (ccl != GetAndSetContext.NOT_MODIFIED) {
      try { PrivilegedAction<?> pa;
        PrivilegedAction<?> pa;
        if ((ccl instanceof PrivilegedAction)) {
          pa = (PrivilegedAction)ccl;
        } else {
          pa = new GetAndSetContext(ccl);
        }
        return AccessController.doPrivileged(pa);
      }
      catch (SecurityException localSecurityException) {}
    }
    return GetAndSetContext.NOT_MODIFIED;
  }
  




  private static RuntimeException attachmentMismatch(String msg)
  {
    return new IndexOutOfBoundsException(msg);
  }
  





  private static RuntimeException attachmentMismatch(int expected, int found)
  {
    return attachmentMismatch("Attachments mismatched, expected " + expected + " but given " + found + '.');
  }
  








  private static MessagingException attach(MessagingException required, Exception optional)
  {
    if ((optional != null) && (!required.setNextException(optional))) {
      if ((optional instanceof MessagingException)) {
        MessagingException head = (MessagingException)optional;
        if (head.setNextException(required)) {
          return head;
        }
      }
      
      if (optional != required) {
        required.addSuppressed(optional);
      }
    }
    return required;
  }
  




  private String getLocalHost(Service s)
  {
    try
    {
      return LogManagerProperties.getLocalHost(s);
    }
    catch (SecurityException|NoSuchMethodException|LinkageError localSecurityException) {}catch (Exception ex)
    {
      reportError(s.toString(), ex, 4);
    }
    return null;
  }
  






  private Session getSession(Message msg)
  {
    if (msg == null) {
      throw new NullPointerException();
    }
    return new MessageContext(msg).getSession();
  }
  








  private boolean allowRestrictedHeaders()
  {
    return LogManagerProperties.hasLogManager();
  }
  




  private static String atIndexMsg(int i)
  {
    return "At index: " + i + '.';
  }
  





  private static final class DefaultAuthenticator
    extends Authenticator
  {
    private final String pass;
    





    static Authenticator of(String pass)
    {
      return new DefaultAuthenticator(pass);
    }
    








    private DefaultAuthenticator(String pass)
    {
      assert (pass != null);
      this.pass = pass;
    }
    
    protected final PasswordAuthentication getPasswordAuthentication()
    {
      return new PasswordAuthentication(getDefaultUserName(), pass);
    }
  }
  







  private static final class GetAndSetContext
    implements PrivilegedAction<Object>
  {
    public static final Object NOT_MODIFIED = GetAndSetContext.class;
    



    private final Object source;
    



    GetAndSetContext(Object source)
    {
      this.source = source;
    }
    






    public final Object run()
    {
      Thread current = Thread.currentThread();
      ClassLoader ccl = current.getContextClassLoader();
      ClassLoader loader;
      ClassLoader loader; if (source == null) {
        loader = null; } else { ClassLoader loader;
        if ((source instanceof ClassLoader)) {
          loader = (ClassLoader)source; } else { ClassLoader loader;
          if ((source instanceof Class)) {
            loader = ((Class)source).getClassLoader(); } else { ClassLoader loader;
            if ((source instanceof Thread)) {
              loader = ((Thread)source).getContextClassLoader();
            } else {
              assert (!(source instanceof Class)) : source;
              loader = source.getClass().getClassLoader();
            }
          } } }
      if (ccl != loader) {
        current.setContextClassLoader(loader);
        return ccl;
      }
      return NOT_MODIFIED;
    }
  }
  





  private static final class TailNameFormatter
    extends Formatter
  {
    private final String name;
    





    static Formatter of(String name)
    {
      return new TailNameFormatter(name);
    }
    








    private TailNameFormatter(String name)
    {
      assert (name != null);
      this.name = name;
    }
    
    public final String format(LogRecord record)
    {
      return "";
    }
    
    public final String getTail(Handler h)
    {
      return name;
    }
    






    public final boolean equals(Object o)
    {
      if ((o instanceof TailNameFormatter)) {
        return name.equals(name);
      }
      return false;
    }
    





    public final int hashCode()
    {
      return getClass().hashCode() + name.hashCode();
    }
    
    public final String toString()
    {
      return name;
    }
  }
}
