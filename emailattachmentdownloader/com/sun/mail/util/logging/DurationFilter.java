package com.sun.mail.util.logging;

import java.util.logging.Filter;
import java.util.logging.LogRecord;










































































































public class DurationFilter
  implements Filter
{
  private final long records;
  private final long duration;
  private long count;
  private long peak;
  private long start;
  
  public DurationFilter()
  {
    records = checkRecords(initLong(".records"));
    duration = checkDuration(initLong(".duration"));
  }
  







  public DurationFilter(long records, long duration)
  {
    this.records = checkRecords(records);
    this.duration = checkDuration(duration);
  }
  






  public boolean equals(Object obj)
  {
    if (this == obj) {
      return true;
    }
    
    if ((obj == null) || (getClass() != obj.getClass())) {
      return false;
    }
    
    DurationFilter other = (DurationFilter)obj;
    if (records != records) {
      return false;
    }
    
    if (duration != duration) {
      return false;
    }
    

    long s;
    
    synchronized (this) {
      long c = count;
      long p = peak;
      s = start;
    }
    
    synchronized (other) { long s;
      long p; long c; if ((c != count) || (p != peak) || (s != start)) {
        return false;
      }
    }
    return true;
  }
  







  public boolean isIdle()
  {
    return test(0L, System.currentTimeMillis());
  }
  





  public int hashCode()
  {
    int hash = 3;
    hash = 89 * hash + (int)(records ^ records >>> 32);
    hash = 89 * hash + (int)(duration ^ duration >>> 32);
    return hash;
  }
  








  public boolean isLoggable(LogRecord record)
  {
    return accept(record.getMillis());
  }
  







  public boolean isLoggable()
  {
    return test(records, System.currentTimeMillis());
  }
  




  public String toString()
  {
    boolean loggable;
    



    synchronized (this) {
      long millis = System.currentTimeMillis();
      boolean idle = test(0L, millis);
      loggable = test(records, millis); }
    boolean loggable;
    boolean idle;
    return getClass().getName() + "{records=" + records + ", duration=" + duration + ", idle=" + idle + ", loggable=" + loggable + '}';
  }
  











  protected DurationFilter clone()
    throws CloneNotSupportedException
  {
    DurationFilter clone = (DurationFilter)super.clone();
    count = 0L;
    peak = 0L;
    start = 0L;
    return clone;
  }
  






  private boolean test(long limit, long millis)
  {
    assert (limit >= 0L) : limit;
    
    long s;
    synchronized (this) {
      long c = count;
      s = start; }
    long s;
    long c;
    if (c > 0L) {
      if ((millis - s >= duration) || (c < limit)) {
        return true;
      }
    }
    else if ((millis - s >= 0L) || (c == 0L)) {
      return true;
    }
    
    return false;
  }
  


  private synchronized boolean accept(long millis)
  {
    boolean allow;
    

    boolean allow;
    
    if (count > 0L) {
      if (millis - peak > 0L) {
        peak = millis;
      }
      
      boolean allow;
      if (count != records) {
        count += 1L;
        allow = true;
      } else { boolean allow;
        if (peak - start >= duration) {
          count = 1L;
          start = peak;
          allow = true;
        } else {
          count = -1L;
          start = (peak + duration);
          allow = false;
        }
      }
    }
    else {
      boolean allow;
      if ((millis - start >= 0L) || (count == 0L)) {
        count = 1L;
        start = millis;
        peak = millis;
        allow = true;
      } else {
        allow = false;
      }
    }
    return allow;
  }
  








  private long initLong(String suffix)
  {
    long result = 0L;
    String p = getClass().getName();
    String value = LogManagerProperties.fromLogManager(p.concat(suffix));
    if ((value != null) && (value.length() != 0)) {
      value = value.trim();
      if (isTimeEntry(suffix, value)) {
        try {
          result = LogManagerProperties.parseDurationToMillis(value);
        }
        catch (RuntimeException localRuntimeException1) {}catch (Exception localException) {}catch (LinkageError localLinkageError) {}
      }
      


      if (result == 0L) {
        try {
          result = 1L;
          for (String s : tokenizeLongs(value)) {
            if ((s.endsWith("L")) || (s.endsWith("l"))) {
              s = s.substring(0, s.length() - 1);
            }
            result = multiplyExact(result, Long.parseLong(s));
          }
        } catch (RuntimeException ignore) {
          result = Long.MIN_VALUE;
        }
      }
    } else {
      result = Long.MIN_VALUE;
    }
    return result;
  }
  









  private boolean isTimeEntry(String suffix, String value)
  {
    return ((value.charAt(0) == 'P') || (value.charAt(0) == 'p')) && 
      (suffix.equals(".duration"));
  }
  








  private static String[] tokenizeLongs(String value)
  {
    int i = value.indexOf('*');
    String[] e; String[] e; if ((i > -1) && ((e = value.split("\\s*\\*\\s*")).length != 0)) {
      if ((i == 0) || (value.charAt(value.length() - 1) == '*')) {
        throw new NumberFormatException(value);
      }
      
      if (e.length == 1) {
        throw new NumberFormatException(e[0]);
      }
    } else {
      e = new String[] { value };
    }
    return e;
  }
  








  private static long multiplyExact(long x, long y)
  {
    long r = x * y;
    if (((Math.abs(x) | Math.abs(y)) >>> 31 != 0L) && (
      ((y != 0L) && (r / y != x)) || ((x == Long.MIN_VALUE) && (y == -1L))))
    {
      throw new ArithmeticException();
    }
    
    return r;
  }
  






  private static long checkRecords(long records)
  {
    return records > 0L ? records : 1000L;
  }
  






  private static long checkDuration(long duration)
  {
    return duration > 0L ? duration : 900000L;
  }
}
