package com.sun.mail.util.logging;

import java.lang.reflect.UndeclaredThrowableException;
import java.text.MessageFormat;
import java.util.Comparator;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.LogRecord;


















































































public class CollectorFormatter
  extends Formatter
{
  private static final long INIT_TIME = System.currentTimeMillis();
  



  private final String fmt;
  



  private final Formatter formatter;
  


  private final Comparator<? super LogRecord> comparator;
  


  private LogRecord last;
  


  private long count;
  


  private long generation = 1L;
  


  private long thrown;
  


  private long minMillis = INIT_TIME;
  


  private long maxMillis = Long.MIN_VALUE;
  







  public CollectorFormatter()
  {
    String p = getClass().getName();
    fmt = initFormat(p);
    formatter = initFormatter(p);
    comparator = initComparator(p);
  }
  








  public CollectorFormatter(String format)
  {
    String p = getClass().getName();
    fmt = (format == null ? initFormat(p) : format);
    formatter = initFormatter(p);
    comparator = initComparator(p);
  }
  













  public CollectorFormatter(String format, Formatter f, Comparator<? super LogRecord> c)
  {
    String p = getClass().getName();
    fmt = (format == null ? initFormat(p) : format);
    formatter = f;
    comparator = c;
  }
  









  public String format(LogRecord record)
  {
    if (record == null) {
      throw new NullPointerException();
    }
    boolean accepted;
    do
    {
      LogRecord peek = peek();
      
      LogRecord update = apply(peek != null ? peek : record, record);
      boolean accepted; if (peek != update) {
        update.getSourceMethodName();
        accepted = acceptAndUpdate(peek, update);
      } else {
        accepted = accept(peek, record);
      }
    } while (!accepted);
    return "";
  }
  










































































































  public String getTail(Handler h)
  {
    super.getTail(h);
    return formatRecord(h, true);
  }
  



  public String toString()
  {
    String result;
    


    try
    {
      result = formatRecord((Handler)null, false);
    } catch (RuntimeException ignore) { String result;
      result = super.toString();
    }
    return result;
  }
  








  protected LogRecord apply(LogRecord t, LogRecord u)
  {
    if ((t == null) || (u == null)) {
      throw new NullPointerException();
    }
    
    if (comparator != null) {
      return comparator.compare(t, u) >= 0 ? t : u;
    }
    return u;
  }
  














  private synchronized boolean accept(LogRecord e, LogRecord u)
  {
    long millis = u.getMillis();
    Throwable ex = u.getThrown();
    if (last == e) {
      if (++count != 1L) {
        minMillis = Math.min(minMillis, millis);
      } else {
        minMillis = millis;
      }
      maxMillis = Math.max(maxMillis, millis);
      
      if (ex != null) {
        thrown += 1L;
      }
      return true;
    }
    return false;
  }
  




  private synchronized void reset(long min)
  {
    if (last != null) {
      last = null;
      generation += 1L;
    }
    
    count = 0L;
    thrown = 0L;
    minMillis = min;
    maxMillis = Long.MIN_VALUE;
  }
  















  private String formatRecord(Handler h, boolean reset)
  {
    synchronized (this) {
      LogRecord record = last;
      long c = count;
      long g = generation;
      long t = thrown;
      long msl = minMillis;
      long msh = maxMillis;
      long now = System.currentTimeMillis();
      if (c == 0L) {
        msh = now;
      }
      
      if (reset)
        reset(msh); }
    long now;
    long msh;
    long msl;
    long g;
    long t;
    long c;
    LogRecord record; Formatter f = formatter;
    String tail; String msg; String head; String head; String msg; String tail; if (f != null) { String tail;
      synchronized (f) {
        String head = f.getHead(h);
        String msg = record != null ? f.format(record) : "";
        tail = f.getTail(h);
      }
    } else {
      head = "";
      msg = record != null ? formatMessage(record) : "";
      tail = "";
    }
    
    Locale l = null;
    if (record != null) {
      ResourceBundle rb = record.getResourceBundle();
      l = rb == null ? null : rb.getLocale();
    }
    MessageFormat mf;
    MessageFormat mf;
    if (l == null) {
      mf = new MessageFormat(fmt);
    } else {
      mf = new MessageFormat(fmt, l);
    }
    



    return mf.format(new Object[] { finish(head), finish(msg), finish(tail), 
      Long.valueOf(c), Long.valueOf(c - 1L), Long.valueOf(t), Long.valueOf(c - t), Long.valueOf(msl), Long.valueOf(msh), Long.valueOf(msh - msl), Long.valueOf(INIT_TIME), Long.valueOf(now), 
      Long.valueOf(now - INIT_TIME), Long.valueOf(g) });
  }
  







  protected String finish(String s)
  {
    return s.trim();
  }
  




  private synchronized LogRecord peek()
  {
    return last;
  }
  








  private synchronized boolean acceptAndUpdate(LogRecord e, LogRecord u)
  {
    if (accept(e, u)) {
      last = u;
      return true;
    }
    return false;
  }
  








  private String initFormat(String p)
  {
    String v = LogManagerProperties.fromLogManager(p.concat(".format"));
    if ((v == null) || (v.length() == 0)) {
      v = "{0}{1}{2}{4,choice,-1#|0#|0<... {4,number,integer} more}\n";
    }
    return v;
  }
  









  private Formatter initFormatter(String p)
  {
    String v = LogManagerProperties.fromLogManager(p.concat(".formatter"));
    Formatter f; Formatter f; if ((v != null) && (v.length() != 0)) {
      if (!"null".equalsIgnoreCase(v)) {
        try {
          f = LogManagerProperties.newFormatter(v);
        } catch (RuntimeException re) { Formatter f;
          throw re;
        } catch (Exception e) {
          throw new UndeclaredThrowableException(e);
        }
      } else {
        f = null;
      }
    }
    else {
      f = (Formatter)Formatter.class.cast(new CompactFormatter());
    }
    return f;
  }
  













  private Comparator<? super LogRecord> initComparator(String p)
  {
    String name = LogManagerProperties.fromLogManager(p.concat(".comparator"));
    String reverse = LogManagerProperties.fromLogManager(p.concat(".comparator.reverse"));
    try { Comparator<? super LogRecord> c;
      if ((name != null) && (name.length() != 0)) {
        if (!"null".equalsIgnoreCase(name)) {
          Comparator<? super LogRecord> c = LogManagerProperties.newComparator(name);
          if (Boolean.parseBoolean(reverse)) {
            assert (c != null);
            c = LogManagerProperties.reverseOrder(c);
          }
        } else {
          if (reverse != null) {
            throw new IllegalArgumentException("No comparator to reverse.");
          }
          
          c = null;
        }
      }
      else {
        if (reverse != null) {
          throw new IllegalArgumentException("No comparator to reverse.");
        }
        

        c = (Comparator)Comparator.class.cast(SeverityComparator.getInstance());
      }
    } catch (RuntimeException re) {
      Comparator<? super LogRecord> c;
      throw re;
    } catch (Exception e) {
      throw new UndeclaredThrowableException(e); }
    Comparator<? super LogRecord> c;
    return c;
  }
}
