package com.sun.mail.util;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.Socket;












































class SocksSupport
{
  SocksSupport() {}
  
  public static Socket getSocket(String host, int port)
  {
    if ((host == null) || (host.length() == 0)) {
      return new Socket(Proxy.NO_PROXY);
    }
    return new Socket(new Proxy(Proxy.Type.SOCKS, new InetSocketAddress(host, port)));
  }
}
