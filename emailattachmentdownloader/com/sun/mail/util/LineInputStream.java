package com.sun.mail.util;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.nio.charset.StandardCharsets;



















































public class LineInputStream
  extends FilterInputStream
{
  private boolean allowutf8;
  private byte[] lineBuffer = null;
  private static int MAX_INCR = 1048576;
  
  public LineInputStream(InputStream in) {
    this(in, false);
  }
  


  public LineInputStream(InputStream in, boolean allowutf8)
  {
    super(in);
    this.allowutf8 = allowutf8;
  }
  














  public String readLine()
    throws IOException
  {
    byte[] buf = lineBuffer;
    
    if (buf == null) {
      buf = this.lineBuffer = new byte[''];
    }
    
    int room = buf.length;
    int offset = 0;
    int c1;
    while (((c1 = in.read()) != -1) && 
      (c1 != 10))
    {
      if (c1 == 13)
      {
        boolean twoCRs = false;
        if (in.markSupported())
          in.mark(2);
        int c2 = in.read();
        if (c2 == 13) {
          twoCRs = true;
          c2 = in.read();
        }
        if (c2 == 10) {
          break;
        }
        








        if (in.markSupported()) {
          in.reset(); break;
        }
        if (!(in instanceof PushbackInputStream))
          in = new PushbackInputStream(in, 2);
        if (c2 != -1)
          ((PushbackInputStream)in).unread(c2);
        if (!twoCRs) break;
        ((PushbackInputStream)in).unread(13); break;
      }
      





      room--; if (room < 0) {
        if (buf.length < MAX_INCR) {
          buf = new byte[buf.length * 2];
        } else
          buf = new byte[buf.length + MAX_INCR];
        room = buf.length - offset - 1;
        System.arraycopy(lineBuffer, 0, buf, 0, offset);
        lineBuffer = buf;
      }
      buf[(offset++)] = ((byte)c1);
    }
    
    if ((c1 == -1) && (offset == 0)) {
      return null;
    }
    if (allowutf8) {
      return new String(buf, 0, offset, StandardCharsets.UTF_8);
    }
    return new String(buf, 0, 0, offset);
  }
}
