package com.sun.mail.util;

import java.io.InputStream;
import javax.mail.MessagingException;

public abstract interface ReadableMime
{
  public abstract InputStream getMimeStream()
    throws MessagingException;
}
