package com.sun.mail.util;

import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.nio.channels.SocketChannel;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;








































public class WriteTimeoutSocket
  extends Socket
{
  private final Socket socket;
  private final ScheduledExecutorService ses;
  private final int timeout;
  
  public WriteTimeoutSocket(Socket socket, int timeout)
    throws IOException
  {
    this.socket = socket;
    
    ses = Executors.newScheduledThreadPool(1);
    this.timeout = timeout;
  }
  
  public WriteTimeoutSocket(int timeout) throws IOException {
    this(new Socket(), timeout);
  }
  
  public WriteTimeoutSocket(InetAddress address, int port, int timeout) throws IOException
  {
    this(timeout);
    socket.connect(new InetSocketAddress(address, port));
  }
  
  public WriteTimeoutSocket(InetAddress address, int port, InetAddress localAddress, int localPort, int timeout)
    throws IOException
  {
    this(timeout);
    socket.bind(new InetSocketAddress(localAddress, localPort));
    socket.connect(new InetSocketAddress(address, port));
  }
  
  public WriteTimeoutSocket(String host, int port, int timeout) throws IOException
  {
    this(timeout);
    socket.connect(new InetSocketAddress(host, port));
  }
  
  public WriteTimeoutSocket(String host, int port, InetAddress localAddress, int localPort, int timeout)
    throws IOException
  {
    this(timeout);
    socket.bind(new InetSocketAddress(localAddress, localPort));
    socket.connect(new InetSocketAddress(host, port));
  }
  

  public void connect(SocketAddress remote)
    throws IOException
  {
    socket.connect(remote, 0);
  }
  
  public void connect(SocketAddress remote, int timeout) throws IOException
  {
    socket.connect(remote, timeout);
  }
  
  public void bind(SocketAddress local) throws IOException
  {
    socket.bind(local);
  }
  
  public SocketAddress getRemoteSocketAddress()
  {
    return socket.getRemoteSocketAddress();
  }
  
  public SocketAddress getLocalSocketAddress()
  {
    return socket.getLocalSocketAddress();
  }
  

  public void setPerformancePreferences(int connectionTime, int latency, int bandwidth)
  {
    socket.setPerformancePreferences(connectionTime, latency, bandwidth);
  }
  
  public SocketChannel getChannel()
  {
    return socket.getChannel();
  }
  
  public InetAddress getInetAddress()
  {
    return socket.getInetAddress();
  }
  
  public InetAddress getLocalAddress()
  {
    return socket.getLocalAddress();
  }
  
  public int getPort()
  {
    return socket.getPort();
  }
  
  public int getLocalPort()
  {
    return socket.getLocalPort();
  }
  
  public InputStream getInputStream() throws IOException
  {
    return socket.getInputStream();
  }
  
  public synchronized OutputStream getOutputStream()
    throws IOException
  {
    return new TimeoutOutputStream(socket.getOutputStream(), ses, timeout);
  }
  
  public void setTcpNoDelay(boolean on) throws SocketException
  {
    socket.setTcpNoDelay(on);
  }
  
  public boolean getTcpNoDelay() throws SocketException
  {
    return socket.getTcpNoDelay();
  }
  
  public void setSoLinger(boolean on, int linger) throws SocketException
  {
    socket.setSoLinger(on, linger);
  }
  
  public int getSoLinger() throws SocketException
  {
    return socket.getSoLinger();
  }
  
  public void sendUrgentData(int data) throws IOException
  {
    socket.sendUrgentData(data);
  }
  
  public void setOOBInline(boolean on) throws SocketException
  {
    socket.setOOBInline(on);
  }
  
  public boolean getOOBInline() throws SocketException
  {
    return socket.getOOBInline();
  }
  
  public void setSoTimeout(int timeout) throws SocketException
  {
    socket.setSoTimeout(timeout);
  }
  
  public int getSoTimeout() throws SocketException
  {
    return socket.getSoTimeout();
  }
  
  public void setSendBufferSize(int size) throws SocketException
  {
    socket.setSendBufferSize(size);
  }
  
  public int getSendBufferSize() throws SocketException
  {
    return socket.getSendBufferSize();
  }
  
  public void setReceiveBufferSize(int size) throws SocketException
  {
    socket.setReceiveBufferSize(size);
  }
  
  public int getReceiveBufferSize() throws SocketException
  {
    return socket.getReceiveBufferSize();
  }
  
  public void setKeepAlive(boolean on) throws SocketException
  {
    socket.setKeepAlive(on);
  }
  
  public boolean getKeepAlive() throws SocketException
  {
    return socket.getKeepAlive();
  }
  
  public void setTrafficClass(int tc) throws SocketException
  {
    socket.setTrafficClass(tc);
  }
  
  public int getTrafficClass() throws SocketException
  {
    return socket.getTrafficClass();
  }
  
  public void setReuseAddress(boolean on) throws SocketException
  {
    socket.setReuseAddress(on);
  }
  
  public boolean getReuseAddress() throws SocketException
  {
    return socket.getReuseAddress();
  }
  
  public void close() throws IOException
  {
    try {
      socket.close();
      
      ses.shutdownNow(); } finally { ses.shutdownNow();
    }
  }
  
  public void shutdownInput() throws IOException
  {
    socket.shutdownInput();
  }
  
  public void shutdownOutput() throws IOException
  {
    socket.shutdownOutput();
  }
  
  public String toString()
  {
    return socket.toString();
  }
  
  public boolean isConnected()
  {
    return socket.isConnected();
  }
  
  public boolean isBound()
  {
    return socket.isBound();
  }
  
  public boolean isClosed()
  {
    return socket.isClosed();
  }
  
  public boolean isInputShutdown()
  {
    return socket.isInputShutdown();
  }
  
  public boolean isOutputShutdown()
  {
    return socket.isOutputShutdown();
  }
  

  public FileDescriptor getFileDescriptor$()
  {
    try
    {
      Method m = Socket.class.getDeclaredMethod("getFileDescriptor$", new Class[0]);
      return (FileDescriptor)m.invoke(socket, new Object[0]);
    } catch (Exception ex) {}
    return null;
  }
}
