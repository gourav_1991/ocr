package com.sun.mail.util;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;















































public class BASE64EncoderStream
  extends FilterOutputStream
{
  private byte[] buffer;
  private int bufsize = 0;
  private byte[] outbuf;
  private int count = 0;
  private int bytesPerLine;
  private int lineLimit;
  private boolean noCRLF = false;
  
  private static byte[] newline = { 13, 10 };
  









  public BASE64EncoderStream(OutputStream out, int bytesPerLine)
  {
    super(out);
    buffer = new byte[3];
    if ((bytesPerLine == Integer.MAX_VALUE) || (bytesPerLine < 4)) {
      noCRLF = true;
      bytesPerLine = 76;
    }
    bytesPerLine = bytesPerLine / 4 * 4;
    this.bytesPerLine = bytesPerLine;
    lineLimit = (bytesPerLine / 4 * 3);
    
    if (noCRLF) {
      outbuf = new byte[bytesPerLine];
    } else {
      outbuf = new byte[bytesPerLine + 2];
      outbuf[bytesPerLine] = 13;
      outbuf[(bytesPerLine + 1)] = 10;
    }
  }
  





  public BASE64EncoderStream(OutputStream out)
  {
    this(out, 76);
  }
  










  public synchronized void write(byte[] b, int off, int len)
    throws IOException
  {
    int end = off + len;
    

    while ((bufsize != 0) && (off < end)) {
      write(b[(off++)]);
    }
    
    int blen = (bytesPerLine - count) / 4 * 3;
    if (off + blen <= end)
    {
      int outlen = encodedSize(blen);
      if (!noCRLF) {
        outbuf[(outlen++)] = 13;
        outbuf[(outlen++)] = 10;
      }
      out.write(encode(b, off, blen, outbuf), 0, outlen);
      off += blen;
      count = 0;
    }
    for (; 
        
        off + lineLimit <= end; off += lineLimit) {
      out.write(encode(b, off, lineLimit, outbuf));
    }
    
    if (off + 3 <= end) {
      blen = end - off;
      blen = blen / 3 * 3;
      
      int outlen = encodedSize(blen);
      out.write(encode(b, off, blen, outbuf), 0, outlen);
      off += blen;
      count += outlen;
    }
    for (; 
        
        off < end; off++) {
      write(b[off]);
    }
  }
  




  public void write(byte[] b)
    throws IOException
  {
    write(b, 0, b.length);
  }
  





  public synchronized void write(int c)
    throws IOException
  {
    buffer[(bufsize++)] = ((byte)c);
    if (bufsize == 3) {
      encode();
      bufsize = 0;
    }
  }
  





  public synchronized void flush()
    throws IOException
  {
    if (bufsize > 0) {
      encode();
      bufsize = 0;
    }
    out.flush();
  }
  



  public synchronized void close()
    throws IOException
  {
    flush();
    if ((count > 0) && (!noCRLF)) {
      out.write(newline);
      out.flush();
    }
    out.close();
  }
  

  private static final char[] pem_array = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/' };
  














  private void encode()
    throws IOException
  {
    int osize = encodedSize(bufsize);
    out.write(encode(buffer, 0, bufsize, outbuf), 0, osize);
    
    count += osize;
    

    if (count >= bytesPerLine) {
      if (!noCRLF)
        out.write(newline);
      count = 0;
    }
  }
  








  public static byte[] encode(byte[] inbuf)
  {
    if (inbuf.length == 0)
      return inbuf;
    return encode(inbuf, 0, inbuf.length, null);
  }
  





  private static byte[] encode(byte[] inbuf, int off, int size, byte[] outbuf)
  {
    if (outbuf == null) {
      outbuf = new byte[encodedSize(size)];
    }
    
    int inpos = off; for (int outpos = 0; size >= 3; outpos += 4) {
      int val = inbuf[(inpos++)] & 0xFF;
      val <<= 8;
      val |= inbuf[(inpos++)] & 0xFF;
      val <<= 8;
      val |= inbuf[(inpos++)] & 0xFF;
      outbuf[(outpos + 3)] = ((byte)pem_array[(val & 0x3F)]);
      val >>= 6;
      outbuf[(outpos + 2)] = ((byte)pem_array[(val & 0x3F)]);
      val >>= 6;
      outbuf[(outpos + 1)] = ((byte)pem_array[(val & 0x3F)]);
      val >>= 6;
      outbuf[(outpos + 0)] = ((byte)pem_array[(val & 0x3F)]);size -= 3;
    }
    











    if (size == 1) {
      int val = inbuf[(inpos++)] & 0xFF;
      val <<= 4;
      outbuf[(outpos + 3)] = 61;
      outbuf[(outpos + 2)] = 61;
      outbuf[(outpos + 1)] = ((byte)pem_array[(val & 0x3F)]);
      val >>= 6;
      outbuf[(outpos + 0)] = ((byte)pem_array[(val & 0x3F)]);
    } else if (size == 2) {
      int val = inbuf[(inpos++)] & 0xFF;
      val <<= 8;
      val |= inbuf[(inpos++)] & 0xFF;
      val <<= 2;
      outbuf[(outpos + 3)] = 61;
      outbuf[(outpos + 2)] = ((byte)pem_array[(val & 0x3F)]);
      val >>= 6;
      outbuf[(outpos + 1)] = ((byte)pem_array[(val & 0x3F)]);
      val >>= 6;
      outbuf[(outpos + 0)] = ((byte)pem_array[(val & 0x3F)]);
    }
    return outbuf;
  }
  



  private static int encodedSize(int size)
  {
    return (size + 2) / 3 * 4;
  }
}
