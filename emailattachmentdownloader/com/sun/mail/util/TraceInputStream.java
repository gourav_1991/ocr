package com.sun.mail.util;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;













































public class TraceInputStream
  extends FilterInputStream
{
  private boolean trace = false;
  private boolean quote = false;
  


  private OutputStream traceOut;
  



  public TraceInputStream(InputStream in, MailLogger logger)
  {
    super(in);
    trace = logger.isLoggable(Level.FINEST);
    traceOut = new LogOutputStream(logger);
  }
  






  public TraceInputStream(InputStream in, OutputStream traceOut)
  {
    super(in);
    this.traceOut = traceOut;
  }
  



  public void setTrace(boolean trace)
  {
    this.trace = trace;
  }
  



  public void setQuote(boolean quote)
  {
    this.quote = quote;
  }
  




  public int read()
    throws IOException
  {
    int b = in.read();
    if ((trace) && (b != -1)) {
      if (quote) {
        writeByte(b);
      } else
        traceOut.write(b);
    }
    return b;
  }
  





  public int read(byte[] b, int off, int len)
    throws IOException
  {
    int count = in.read(b, off, len);
    if ((trace) && (count != -1)) {
      if (quote) {
        for (int i = 0; i < count; i++)
          writeByte(b[(off + i)]);
      } else
        traceOut.write(b, off, count);
    }
    return count;
  }
  

  private final void writeByte(int b)
    throws IOException
  {
    b &= 0xFF;
    if (b > 127) {
      traceOut.write(77);
      traceOut.write(45);
      b &= 0x7F;
    }
    if (b == 13) {
      traceOut.write(92);
      traceOut.write(114);
    } else if (b == 10) {
      traceOut.write(92);
      traceOut.write(110);
      traceOut.write(10);
    } else if (b == 9) {
      traceOut.write(92);
      traceOut.write(116);
    } else if (b < 32) {
      traceOut.write(94);
      traceOut.write(64 + b);
    } else {
      traceOut.write(b);
    }
  }
}
