package com.sun.mail.util;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;












































public class CRLFOutputStream
  extends FilterOutputStream
{
  protected int lastb = -1;
  protected boolean atBOL = true;
  private static final byte[] newline = { 13, 10 };
  
  public CRLFOutputStream(OutputStream os) {
    super(os);
  }
  
  public void write(int b) throws IOException
  {
    if (b == 13) {
      writeln();
    } else if (b == 10) {
      if (lastb != 13)
        writeln();
    } else {
      out.write(b);
      atBOL = false;
    }
    lastb = b;
  }
  
  public void write(byte[] b) throws IOException
  {
    write(b, 0, b.length);
  }
  
  public void write(byte[] b, int off, int len) throws IOException
  {
    int start = off;
    
    len += off;
    for (int i = start; i < len; i++) {
      if (b[i] == 13) {
        out.write(b, start, i - start);
        writeln();
        start = i + 1;
      } else if (b[i] == 10) {
        if (lastb != 13) {
          out.write(b, start, i - start);
          writeln();
        }
        start = i + 1;
      }
      lastb = b[i];
    }
    if (len - start > 0) {
      out.write(b, start, len - start);
      atBOL = false;
    }
  }
  

  public void writeln()
    throws IOException
  {
    out.write(newline);
    atBOL = true;
  }
}
