package com.sun.mail.util;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
















































public class UUDecoderStream
  extends FilterInputStream
{
  private String name;
  private int mode;
  private byte[] buffer = new byte[45];
  private int bufsize = 0;
  private int index = 0;
  private boolean gotPrefix = false;
  private boolean gotEnd = false;
  

  private LineInputStream lin;
  

  private boolean ignoreErrors;
  

  private boolean ignoreMissingBeginEnd;
  

  private String readAhead;
  

  public UUDecoderStream(InputStream in)
  {
    super(in);
    lin = new LineInputStream(in);
    
    ignoreErrors = PropUtil.getBooleanSystemProperty("mail.mime.uudecode.ignoreerrors", false);
    

    ignoreMissingBeginEnd = PropUtil.getBooleanSystemProperty("mail.mime.uudecode.ignoremissingbeginend", false);
  }
  







  public UUDecoderStream(InputStream in, boolean ignoreErrors, boolean ignoreMissingBeginEnd)
  {
    super(in);
    lin = new LineInputStream(in);
    this.ignoreErrors = ignoreErrors;
    this.ignoreMissingBeginEnd = ignoreMissingBeginEnd;
  }
  












  public int read()
    throws IOException
  {
    if (index >= bufsize) {
      readPrefix();
      if (!decode())
        return -1;
      index = 0;
    }
    return buffer[(index++)] & 0xFF;
  }
  
  public int read(byte[] buf, int off, int len)
    throws IOException
  {
    for (int i = 0; i < len; i++) { int c;
      if ((c = read()) == -1) {
        if (i != 0) break;
        i = -1; break;
      }
      
      buf[(off + i)] = ((byte)c);
    }
    return i;
  }
  
  public boolean markSupported()
  {
    return false;
  }
  

  public int available()
    throws IOException
  {
    return in.available() * 3 / 4 + (bufsize - index);
  }
  





  public String getName()
    throws IOException
  {
    readPrefix();
    return name;
  }
  





  public int getMode()
    throws IOException
  {
    readPrefix();
    return mode;
  }
  



  private void readPrefix()
    throws IOException
  {
    if (gotPrefix) {
      return;
    }
    mode = 438;
    name = "encoder.buf";
    
    for (;;)
    {
      String line = lin.readLine();
      if (line == null) {
        if (!ignoreMissingBeginEnd) {
          throw new DecodingException("UUDecoder: Missing begin");
        }
        gotPrefix = true;
        gotEnd = true;
        break;
      }
      if (line.regionMatches(false, 0, "begin", 0, 5)) {
        try {
          mode = Integer.parseInt(line.substring(6, 9));
        } catch (NumberFormatException ex) {
          if (!ignoreErrors)
          {
            throw new DecodingException("UUDecoder: Error in mode: " + ex.toString()); }
        }
        if (line.length() > 10) {
          name = line.substring(10);
        }
        else if (!ignoreErrors) {
          throw new DecodingException("UUDecoder: Missing name: " + line);
        }
        
        gotPrefix = true;
        break; }
      if ((ignoreMissingBeginEnd) && (line.length() != 0)) {
        int count = line.charAt(0);
        count = count - 32 & 0x3F;
        int need = (count * 8 + 5) / 6;
        if ((need == 0) || (line.length() >= need + 1))
        {





          readAhead = line;
          gotPrefix = true;
          break;
        }
      }
    }
  }
  
  private boolean decode() throws IOException
  {
    if (gotEnd)
      return false;
    bufsize = 0;
    int count = 0;
    String line;
    do
    {
      do
      {
        do {
          if (readAhead != null) {
            String line = readAhead;
            readAhead = null;
          } else {
            line = lin.readLine();
          }
          




          if (line == null) {
            if (!ignoreMissingBeginEnd) {
              throw new DecodingException("UUDecoder: Missing end at EOF");
            }
            gotEnd = true;
            return false;
          }
          if (line.equals("end")) {
            gotEnd = true;
            return false;
          }
        } while (line.length() == 0);
        
        count = line.charAt(0);
        if (count >= 32) break;
      } while (ignoreErrors);
      throw new DecodingException("UUDecoder: Buffer format error");
      









      count = count - 32 & 0x3F;
      
      if (count == 0) {
        line = lin.readLine();
        if (((line == null) || (!line.equals("end"))) && 
          (!ignoreMissingBeginEnd)) {
          throw new DecodingException("UUDecoder: Missing End after count 0 line");
        }
        
        gotEnd = true;
        return false;
      }
      
      int need = (count * 8 + 5) / 6;
      
      if (line.length() >= need + 1) break;
    } while (ignoreErrors);
    throw new DecodingException("UUDecoder: Short buffer error");
    







    int i = 1;
    






    while (bufsize < count)
    {
      byte a = (byte)(line.charAt(i++) - ' ' & 0x3F);
      byte b = (byte)(line.charAt(i++) - ' ' & 0x3F);
      buffer[(bufsize++)] = ((byte)(a << 2 & 0xFC | b >>> 4 & 0x3));
      
      if (bufsize < count) {
        a = b;
        b = (byte)(line.charAt(i++) - ' ' & 0x3F);
        buffer[(bufsize++)] = ((byte)(a << 4 & 0xF0 | b >>> 2 & 0xF));
      }
      

      if (bufsize < count) {
        a = b;
        b = (byte)(line.charAt(i++) - ' ' & 0x3F);
        buffer[(bufsize++)] = ((byte)(a << 6 & 0xC0 | b & 0x3F));
      }
    }
    return true;
  }
}
