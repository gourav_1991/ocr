package com.sun.mail.util;

import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;











































public class LogOutputStream
  extends OutputStream
{
  protected MailLogger logger;
  protected Level level;
  private int lastb = -1;
  private byte[] buf = new byte[80];
  private int pos = 0;
  




  public LogOutputStream(MailLogger logger)
  {
    this.logger = logger;
    level = Level.FINEST;
  }
  
  public void write(int b) throws IOException
  {
    if (!logger.isLoggable(level)) {
      return;
    }
    if (b == 13) {
      logBuf();
    } else if (b == 10) {
      if (lastb != 13)
        logBuf();
    } else {
      expandCapacity(1);
      buf[(pos++)] = ((byte)b);
    }
    lastb = b;
  }
  
  public void write(byte[] b) throws IOException
  {
    write(b, 0, b.length);
  }
  
  public void write(byte[] b, int off, int len) throws IOException
  {
    int start = off;
    
    if (!logger.isLoggable(level))
      return;
    len += off;
    for (int i = start; i < len; i++) {
      if (b[i] == 13) {
        expandCapacity(i - start);
        System.arraycopy(b, start, buf, pos, i - start);
        pos += i - start;
        logBuf();
        start = i + 1;
      } else if (b[i] == 10) {
        if (lastb != 13) {
          expandCapacity(i - start);
          System.arraycopy(b, start, buf, pos, i - start);
          pos += i - start;
          logBuf();
        }
        start = i + 1;
      }
      lastb = b[i];
    }
    if (len - start > 0) {
      expandCapacity(len - start);
      System.arraycopy(b, start, buf, pos, len - start);
      pos += len - start;
    }
  }
  





  protected void log(String msg)
  {
    logger.log(level, msg);
  }
  


  private void logBuf()
  {
    String msg = new String(buf, 0, pos);
    pos = 0;
    log(msg);
  }
  



  private void expandCapacity(int len)
  {
    while (pos + len > buf.length) {
      byte[] nb = new byte[buf.length * 2];
      System.arraycopy(buf, 0, nb, 0, pos);
      buf = nb;
    }
  }
}
