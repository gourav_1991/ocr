package com.sun.mail.util;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;













































public class UUEncoderStream
  extends FilterOutputStream
{
  private byte[] buffer;
  private int bufsize = 0;
  private boolean wrotePrefix = false;
  private boolean wroteSuffix = false;
  

  private String name;
  
  private int mode;
  

  public UUEncoderStream(OutputStream out)
  {
    this(out, "encoder.buf", 420);
  }
  




  public UUEncoderStream(OutputStream out, String name)
  {
    this(out, name, 420);
  }
  





  public UUEncoderStream(OutputStream out, String name, int mode)
  {
    super(out);
    this.name = name;
    this.mode = mode;
    buffer = new byte[45];
  }
  







  public void setNameMode(String name, int mode)
  {
    this.name = name;
    this.mode = mode;
  }
  
  public void write(byte[] b, int off, int len) throws IOException
  {
    for (int i = 0; i < len; i++) {
      write(b[(off + i)]);
    }
  }
  
  public void write(byte[] data) throws IOException {
    write(data, 0, data.length);
  }
  



  public void write(int c)
    throws IOException
  {
    buffer[(bufsize++)] = ((byte)c);
    if (bufsize == 45) {
      writePrefix();
      encode();
      bufsize = 0;
    }
  }
  
  public void flush() throws IOException
  {
    if (bufsize > 0) {
      writePrefix();
      encode();
      bufsize = 0;
    }
    writeSuffix();
    out.flush();
  }
  
  public void close() throws IOException
  {
    flush();
    out.close();
  }
  

  private void writePrefix()
    throws IOException
  {
    if (!wrotePrefix)
    {
      PrintStream ps = new PrintStream(out, false, "utf-8");
      ps.format("begin %o %s%n", new Object[] { Integer.valueOf(mode), name });
      ps.flush();
      wrotePrefix = true;
    }
  }
  


  private void writeSuffix()
    throws IOException
  {
    if (!wroteSuffix) {
      PrintStream ps = new PrintStream(out, false, "us-ascii");
      ps.println(" \nend");
      ps.flush();
      wroteSuffix = true;
    }
  }
  









  private void encode()
    throws IOException
  {
    int i = 0;
    

    out.write((bufsize & 0x3F) + 32);
    
    while (i < bufsize) {
      byte a = buffer[(i++)];
      byte c; byte b; byte c; if (i < bufsize) {
        byte b = buffer[(i++)];
        byte c; if (i < bufsize) {
          c = buffer[(i++)];
        } else {
          c = 1;
        }
      } else {
        b = 1;
        c = 1;
      }
      
      int c1 = a >>> 2 & 0x3F;
      int c2 = a << 4 & 0x30 | b >>> 4 & 0xF;
      int c3 = b << 2 & 0x3C | c >>> 6 & 0x3;
      int c4 = c & 0x3F;
      out.write(c1 + 32);
      out.write(c2 + 32);
      out.write(c3 + 32);
      out.write(c4 + 32);
    }
    
    out.write(10);
  }
}
