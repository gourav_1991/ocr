package com.sun.mail.util;

import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.Callable;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;



































































































































































































































































































































class TimeoutOutputStream
  extends OutputStream
{
  private final OutputStream os;
  private final ScheduledExecutorService ses;
  private final Callable<Object> timeoutTask;
  private final int timeout;
  private byte[] b1;
  
  public TimeoutOutputStream(OutputStream os0, ScheduledExecutorService ses, int timeout)
    throws IOException
  {
    os = os0;
    this.ses = ses;
    this.timeout = timeout;
    timeoutTask = new Callable()
    {
      public Object call() throws Exception {
        os.close();
        return null;
      }
    };
  }
  
  public synchronized void write(int b) throws IOException
  {
    if (b1 == null)
      b1 = new byte[1];
    b1[0] = ((byte)b);
    write(b1);
  }
  
  public synchronized void write(byte[] bs, int off, int len)
    throws IOException
  {
    if ((off < 0) || (off > bs.length) || (len < 0) || (off + len > bs.length) || (off + len < 0))
    {
      throw new IndexOutOfBoundsException(); }
    if (len == 0) {
      return;
    }
    

    ScheduledFuture<Object> sf = null;
    try {
      try {
        if (timeout > 0) {
          sf = ses.schedule(timeoutTask, timeout, TimeUnit.MILLISECONDS);
        }
      }
      catch (RejectedExecutionException localRejectedExecutionException) {}
      

      os.write(bs, off, len);
    } finally {
      if (sf != null) {
        sf.cancel(true);
      }
    }
  }
  
  public void close() throws IOException {
    os.close();
  }
}
