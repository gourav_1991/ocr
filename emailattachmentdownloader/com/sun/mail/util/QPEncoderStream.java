package com.sun.mail.util;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;














































public class QPEncoderStream
  extends FilterOutputStream
{
  private int count = 0;
  private int bytesPerLine;
  private boolean gotSpace = false;
  private boolean gotCR = false;
  






  public QPEncoderStream(OutputStream out, int bytesPerLine)
  {
    super(out);
    

    this.bytesPerLine = (bytesPerLine - 1);
  }
  




  public QPEncoderStream(OutputStream out)
  {
    this(out, 76);
  }
  









  public void write(byte[] b, int off, int len)
    throws IOException
  {
    for (int i = 0; i < len; i++) {
      write(b[(off + i)]);
    }
  }
  



  public void write(byte[] b)
    throws IOException
  {
    write(b, 0, b.length);
  }
  




  public void write(int c)
    throws IOException
  {
    c &= 0xFF;
    if (gotSpace) {
      if ((c == 13) || (c == 10))
      {
        output(32, true);
      } else
        output(32, false);
      gotSpace = false;
    }
    
    if (c == 13) {
      gotCR = true;
      outputCRLF();
    } else {
      if (c == 10) {
        if (!gotCR)
        {



          outputCRLF(); }
      } else if (c == 32) {
        gotSpace = true;
      } else if ((c < 32) || (c >= 127) || (c == 61))
      {
        output(c, true);
      } else {
        output(c, false);
      }
      gotCR = false;
    }
  }
  




  public void flush()
    throws IOException
  {
    if (gotSpace) {
      output(32, true);
      gotSpace = false;
    }
    out.flush();
  }
  





  public void close()
    throws IOException
  {
    flush();
    out.close();
  }
  
  private void outputCRLF() throws IOException {
    out.write(13);
    out.write(10);
    count = 0;
  }
  

  private static final char[] hex = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
  

  protected void output(int c, boolean encode)
    throws IOException
  {
    if (encode) {
      if (this.count += 3 > bytesPerLine) {
        out.write(61);
        out.write(13);
        out.write(10);
        count = 3;
      }
      out.write(61);
      out.write(hex[(c >> 4)]);
      out.write(hex[(c & 0xF)]);
    } else {
      if (++count > bytesPerLine) {
        out.write(61);
        out.write(13);
        out.write(10);
        count = 1;
      }
      out.write(c);
    }
  }
}
