package com.sun.activation.viewers;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Panel;
import java.awt.Toolkit;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import javax.activation.CommandObject;
import javax.activation.DataHandler;
































public class ImageViewer
  extends Panel
  implements CommandObject
{
  private ImageViewerCanvas canvas = null;
  


  private Image image = null;
  private DataHandler _dh = null;
  
  private boolean DEBUG = false;
  



  public ImageViewer()
  {
    canvas = new ImageViewerCanvas();
    add(canvas);
  }
  

  public void setCommandContext(String verb, DataHandler dh)
    throws IOException
  {
    _dh = dh;
    setInputStream(_dh.getInputStream());
  }
  



  private void setInputStream(InputStream ins)
    throws IOException
  {
    MediaTracker mt = new MediaTracker(this);
    int bytes_read = 0;
    byte[] data = new byte['Ѐ'];
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    
    while ((bytes_read = ins.read(data)) > 0)
      baos.write(data, 0, bytes_read);
    ins.close();
    

    image = getToolkit().createImage(baos.toByteArray());
    
    mt.addImage(image, 0);
    try
    {
      mt.waitForID(0);
      mt.waitForAll();
      if (mt.statusID(0, true) != 8) {
        System.out.println("Error occured in image loading = " + mt.getErrorsID(0));
      }
      

    }
    catch (InterruptedException e)
    {
      throw new IOException("Error reading image data");
    }
    
    canvas.setImage(image);
    if (DEBUG) {
      System.out.println("calling invalidate");
    }
  }
  
  public void addNotify() {
    super.addNotify();
    invalidate();
    validate();
    doLayout();
  }
  
  public Dimension getPreferredSize() {
    return canvas.getPreferredSize();
  }
}
