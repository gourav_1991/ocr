package com.sun.activation.viewers;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;





































public class ImageViewerCanvas
  extends Canvas
{
  private Image canvas_image = null;
  





  public ImageViewerCanvas() {}
  




  public void setImage(Image new_image)
  {
    canvas_image = new_image;
    invalidate();
    repaint();
  }
  



  public Dimension getPreferredSize()
  {
    Dimension d = null;
    
    if (canvas_image == null)
    {
      d = new Dimension(200, 200);
    }
    else {
      d = new Dimension(canvas_image.getWidth(this), canvas_image.getHeight(this));
    }
    
    return d;
  }
  



  public void paint(Graphics g)
  {
    if (canvas_image != null) {
      g.drawImage(canvas_image, 0, 0, this);
    }
  }
}
