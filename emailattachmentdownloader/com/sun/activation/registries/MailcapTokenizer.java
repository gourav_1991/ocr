package com.sun.activation.registries;




public class MailcapTokenizer
{
  public static final int UNKNOWN_TOKEN = 0;
  


  public static final int START_TOKEN = 1;
  


  public static final int STRING_TOKEN = 2;
  


  public static final int EOI_TOKEN = 5;
  


  public static final int SLASH_TOKEN = 47;
  


  public static final int SEMICOLON_TOKEN = 59;
  


  public static final int EQUALS_TOKEN = 61;
  


  private String data;
  


  private int dataIndex;
  


  private int dataLength;
  


  private int currentToken;
  

  private String currentTokenValue;
  

  private boolean isAutoquoting;
  

  private char autoquoteChar;
  


  public MailcapTokenizer(String inputString)
  {
    data = inputString;
    dataIndex = 0;
    dataLength = inputString.length();
    
    currentToken = 1;
    currentTokenValue = "";
    
    isAutoquoting = false;
    autoquoteChar = ';';
  }
  









  public void setIsAutoquoting(boolean value)
  {
    isAutoquoting = value;
  }
  




  public int getCurrentToken()
  {
    return currentToken;
  }
  


  public static String nameForToken(int token)
  {
    String name = "really unknown";
    
    switch (token) {
    case 0: 
      name = "unknown";
      break;
    case 1: 
      name = "start";
      break;
    case 2: 
      name = "string";
      break;
    case 5: 
      name = "EOI";
      break;
    case 47: 
      name = "'/'";
      break;
    case 59: 
      name = "';'";
      break;
    case 61: 
      name = "'='";
    }
    
    
    return name;
  }
  




  public String getCurrentTokenValue()
  {
    return currentTokenValue;
  }
  




  public int nextToken()
  {
    if (dataIndex < dataLength)
    {
      while ((dataIndex < dataLength) && (isWhiteSpaceChar(data.charAt(dataIndex))))
      {
        dataIndex += 1;
      }
      
      if (dataIndex < dataLength)
      {
        char c = data.charAt(dataIndex);
        if (isAutoquoting) {
          if ((c == ';') || (c == '=')) {
            currentToken = c;
            currentTokenValue = new Character(c).toString();
            dataIndex += 1;
          } else {
            processAutoquoteToken();
          }
        }
        else if (isStringTokenChar(c)) {
          processStringToken();
        } else if ((c == '/') || (c == ';') || (c == '=')) {
          currentToken = c;
          currentTokenValue = new Character(c).toString();
          dataIndex += 1;
        } else {
          currentToken = 0;
          currentTokenValue = new Character(c).toString();
          dataIndex += 1;
        }
      }
      else {
        currentToken = 5;
        currentTokenValue = null;
      }
    } else {
      currentToken = 5;
      currentTokenValue = null;
    }
    
    return currentToken;
  }
  
  private void processStringToken()
  {
    int initialIndex = dataIndex;
    

    while ((dataIndex < dataLength) && (isStringTokenChar(data.charAt(dataIndex))))
    {
      dataIndex += 1;
    }
    
    currentToken = 2;
    currentTokenValue = data.substring(initialIndex, dataIndex);
  }
  
  private void processAutoquoteToken()
  {
    int initialIndex = dataIndex;
    


    boolean foundTerminator = false;
    while ((dataIndex < dataLength) && (!foundTerminator)) {
      char c = data.charAt(dataIndex);
      if (c != autoquoteChar) {
        dataIndex += 1;
      } else {
        foundTerminator = true;
      }
    }
    
    currentToken = 2;
    currentTokenValue = fixEscapeSequences(data.substring(initialIndex, dataIndex));
  }
  
  private static boolean isSpecialChar(char c)
  {
    boolean lAnswer = false;
    
    switch (c) {
    case '"': 
    case '(': 
    case ')': 
    case ',': 
    case '/': 
    case ':': 
    case ';': 
    case '<': 
    case '=': 
    case '>': 
    case '?': 
    case '@': 
    case '[': 
    case '\\': 
    case ']': 
      lAnswer = true;
    }
    
    
    return lAnswer;
  }
  
  private static boolean isControlChar(char c) {
    return Character.isISOControl(c);
  }
  
  private static boolean isWhiteSpaceChar(char c) {
    return Character.isWhitespace(c);
  }
  
  private static boolean isStringTokenChar(char c) {
    return (!isSpecialChar(c)) && (!isControlChar(c)) && (!isWhiteSpaceChar(c));
  }
  
  private static String fixEscapeSequences(String inputString) {
    int inputLength = inputString.length();
    StringBuffer buffer = new StringBuffer();
    buffer.ensureCapacity(inputLength);
    
    for (int i = 0; i < inputLength; i++) {
      char currentChar = inputString.charAt(i);
      if (currentChar != '\\') {
        buffer.append(currentChar);
      }
      else if (i < inputLength - 1) {
        char nextChar = inputString.charAt(i + 1);
        buffer.append(nextChar);
        

        i++;
      } else {
        buffer.append(currentChar);
      }
    }
    

    return buffer.toString();
  }
}
