
package com.emailattachmentdownloader.rfc;

import java.util.concurrent.CountDownLatch;

import com.emailattachmentdownloader.rfc.CustomDestinationDataProvider.MyDestinationDataProvider;
import com.sap.conn.jco.AbapException;
import com.sap.conn.jco.JCoContext;
import com.sap.conn.jco.JCoDestination;
import com.sap.conn.jco.JCoDestinationManager;
import com.sap.conn.jco.JCoException;
import com.sap.conn.jco.JCoField;
import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoFunctionTemplate;
import com.sap.conn.jco.JCoStructure;
import com.sap.conn.jco.JCoTable;

/**
 * basic examples for Java to ABAP communication
 */
public class VendorValidatorRfc {

	public static void step3SimpleCall(String vendorcode, String name1, String name2) throws JCoException {

		MyDestinationDataProvider myProvider = new MyDestinationDataProvider();

		try {
			com.sap.conn.jco.ext.Environment.registerDestinationDataProvider(myProvider);
		}

		catch (IllegalStateException providerAlreadyRegisteredException) {

			throw new Error(providerAlreadyRegisteredException);
		}

		String destName = "ABAP_AS";
		CustomDestinationDataProvider test = new CustomDestinationDataProvider();

		myProvider.changeProperties(destName, CustomDestinationDataProvider.getDestinationPropertiesFromUI());

		JCoDestination destination = JCoDestinationManager.getDestination(destName);
		destination.ping();

		JCoFunction function = destination.getRepository().getFunction("ZVENDOR_VALIDATION");

		if (function == null) {
			throw new RuntimeException(function.getName() + " not found in SAP.");
		}

		setVendorData(function, vendorcode, name1, name2);

		try {

			function.execute(destination);
		} catch (AbapException e) {
			System.out.println(e.toString());
			return;
		}

		System.out.println("STFC_CONNECTION finished:");
		
		JCoTable codes = function.getExportParameterList().getTable("GT_RESULT");
		
		
		 for (int i = 0; i < codes.getNumRows(); i++) 
	        {
	            codes.setRow(i);
	            System.out.println( "FLAG="+codes.getString("FLAG"));
	            
	        }
		

	}

	protected static void setVendorData(JCoFunction function, String vendorcode, String name1, String name2) {

		
		
		 
		// Adding Rows in jcoTable
		JCoTable codes = function.getImportParameterList().getTable("GT_VENDOR");
		
	    codes.appendRow();
	    codes.setValue("LV_VEN",vendorcode);
	    
	//    codes.appendRow();
	   //codes.setValue("LV_VEN","0010300081");
	    
	 //   codes.appendRow();
	  //  codes.setValue("LV_VEN","10300081");
	    
	    function.getImportParameterList().setValue("GT_VENDOR",codes);

	}
}
