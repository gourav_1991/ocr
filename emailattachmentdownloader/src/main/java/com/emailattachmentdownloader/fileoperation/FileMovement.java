package com.emailattachmentdownloader.fileoperation;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemOptions;
import org.apache.commons.vfs2.Selectors;
import org.apache.commons.vfs2.UserAuthenticator;
import org.apache.commons.vfs2.VFS;
import org.apache.commons.vfs2.auth.StaticUserAuthenticator;
import org.apache.commons.vfs2.impl.DefaultFileSystemConfigBuilder;

public class FileMovement {

	private static String FILE_TYPE = "PDF";

	public static void move(File sourceLocation) throws IOException {

		try {

			File[] files = sourceLocation.listFiles();

			for (int i = 0; i < files.length; i++) {

				if (FilenameUtils.getExtension(files[i].getName()).equalsIgnoreCase(FILE_TYPE)) {
					files[i].renameTo(new File("G:\\processed\\" + files[i].getName()));

				}

				else
					files[i].delete();

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void moveTo() throws IOException {

		File destFolder = new File("\\\\Sapours\\E\\OCR\\processed\\AMD+Vat-Minda Light (2).pdf");
		if (destFolder.exists()) {
			destFolder.delete();
		}
		destFolder.createNewFile();
		FileObject destn = VFS.getManager().resolveFile(destFolder.getAbsolutePath());
		UserAuthenticator auth = new StaticUserAuthenticator("sapours", "GUEST", "Sapours@2017");
		FileSystemOptions opts = new FileSystemOptions();
		DefaultFileSystemConfigBuilder.getInstance().setUserAuthenticator(opts, auth);
		// FileObject
		// fo=VFS.getManager().resolveFile("\\\\sapours\\E\\OCR\\processed\\test.txt",opts);
		FileObject src = VFS.getManager().resolveFile("G:\\Attachment\\AMD+Vat-Minda Light (2).pdf");
		destn.copyFrom(src, Selectors.SELECT_SELF);
		src.delete();
		destn.close();

	}

}
