package com.emailattachmentdownloader.emaildownloader;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.search.FlagTerm;

import com.emailattachmentdownloader.pdfextractor.PdfExtractor;
import com.sap.conn.jco.JCoException;

import net.sourceforge.tess4j.TesseractException;

public class EmailDownloader {
	public String mailAccess() {

		System.out.println("mail  Access  method invoked ");
		Properties props = new Properties();
		try {
			props.load(new FileInputStream(new File("G:\\smtp.properties")));
			Session session1 = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication("professional3193@gmail.com", "Professional3193@");
				}
			});
			Store store = session1.getStore("imaps");
			store.connect("smtp.gmail.com", "professional3193@gmail.com", "Professional3193@");
			Folder folder = store.getFolder("inbox");
			folder.open(Folder.READ_ONLY);
			int messageCount = folder.getMessageCount();
			Message[] messages = folder.getMessages();
			for (Message message : messages) {
				System.out.println("Mail Subject:- " + message.getSubject());
				String contentType = message.getContentType();
				pdfDownloader(contentType, message);
			}
			folder.close(true);
			store.close();
		}

		catch (Exception e) {
			e.printStackTrace();
		}

		return "success";

	}

	protected void pdfDownloader(String contentType, Message message)
			throws IOException, MessagingException, JCoException, TesseractException {

		if (contentType.contains("multipart")) {
			// this message may contain attachment
			Multipart multiPart = (Multipart) message.getContent();
			for (int i = 0; i < multiPart.getCount(); i++) {
				MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(i);
				if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {

					// save an attachment from a MimeBodyPart to a file
					String destFilePath = "G:/unprocessed/" + part.getFileName();

					InputStream input = part.getInputStream();
					BufferedInputStream in = null;
					in = new BufferedInputStream(input);

					FileOutputStream output = new FileOutputStream(destFilePath);

					byte[] buffer = new byte[4096];

					int byteRead;

					while ((byteRead = input.read(buffer)) != -1) {
						output.write(buffer, 0, byteRead);
					}

					System.out.println("FileOutPutStream is Being Closed");
					output.close();
					PdfExtractor.createImagesFromPdf(destFilePath);

				}
			}

		}

	}

}
