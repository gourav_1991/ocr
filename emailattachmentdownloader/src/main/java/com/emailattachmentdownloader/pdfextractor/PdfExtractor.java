package com.emailattachmentdownloader.pdfextractor;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.emailattachmentdownloader.dao.VendorValidationDao;
import com.emailattachmentdownloader.emailsender.EmailSender;
import com.emailattachmentdownloader.fileoperation.FileMovement;
import com.emailattachmentdownloader.model.VendorModel;
import com.emailattachmentdownloader.rfc.VendorValidatorRfc;
import com.sap.conn.jco.JCoException;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

public class PdfExtractor {

	private static String vatNo;
	private static String vendorCode;
	private static String cstTinNo;
	private static String industryName1;
	private static String industryName2;
	private static String industryFullName;
	
	
	
	public static boolean fetchContent(File outputfile) throws JCoException

	{
		/*Resource resource=new ClassPathResource("applicationContext.xml");  
	    @SuppressWarnings("deprecation")
		BeanFactory factory=new XmlBeanFactory(resource);  
		VendorValidationDao validationDao=(VendorValidationDao) factory.getBean("vendorValidation");
		    List<VendorModel>vendorModels=validationDao.getVendors();
		System.out.println("vendors="+validationDao.getVendors());*/
		 
		ITesseract instance = new Tesseract();
		try {
			String result = instance.doOCR(outputfile);
			System.out.println("result="+result);
			instance.setDatapath("C:\\neon_workspace\\emailattachmentdownloader\tessdata");
			String sp[] = result.split(System.lineSeparator());
			// Logic For Minda Industry

			for (String st : sp) {
				if (st.contains("VATTINNO")) {
					String lastHalf = st.split("VATTINNO ")[1];
					vatNo = lastHalf.substring(0, lastHalf.indexOf("wef"));
					System.out.println("vatno="+vatNo);
					
				
					
				}

				if (st.contains("VENDORCODE")) {
					String lastHalf = st.split("VENDORCODE 993 ")[1];
					vendorCode = lastHalf.substring(0, lastHalf.indexOf("UiLOC"));
				}
				if (st.contains("CSTTINNO")) {
					String lastHalf = st.split("CSTTINNO. : ")[1];
					cstTinNo = lastHalf.substring(0, lastHalf.indexOf("w.e."));

				}

				if (st.contains("MINDA")) {
					String lastHaft = st.split("MINDA ")[1];
					industryFullName = "MINDA " + lastHaft.substring(0, lastHaft.indexOf(" V�"));

				}

			}

			/*if (industryFullName.length() > 35) {
				VendorValidatorRfc.step3SimpleCall(vendorCode, industryFullName.substring(0, 35), industryFullName
						.substring(industryFullName.substring(0, 35).length() + 1, industryFullName.length()));
				return true;
			}

			else {
				VendorValidatorRfc.step3SimpleCall(vendorCode, industryFullName, "");
				return true;
			}
*/
		} catch (TesseractException e) {
			// Handle exceptions
		}
		return false;
	}

	public static void createImagesFromPdf(String sourceFilePath) throws IOException, JCoException, TesseractException

	{
		String destinationDir = "G:/unprocessed/";
		File sourceFile = new File(sourceFilePath);
		File destinationFile = new File(destinationDir);
		if (!destinationFile.exists()) {
			destinationFile.mkdir();
			System.out.println("Folder Created -> " + destinationFile.getAbsolutePath());
		}
		if (sourceFile.exists()) {
			System.out.println("Images copied to Folder: " + destinationFile.getName());
			PDDocument document = PDDocument.load(sourceFilePath);
			List<PDPage> list = document.getDocumentCatalog().getAllPages();
			System.out.println("Total files to be converted -> " + list.size());
			String fileName = sourceFile.getName().replace(".pdf", "");
			int pageNumber = 1;
			for (PDPage page : list) {
				PDRectangle mediaBox = page.findMediaBox();
				boolean isLandscape = mediaBox.getWidth() > mediaBox.getHeight();
				if (isLandscape == true)
					page.setRotation(90);
				BufferedImage image = page.convertToImage();
				File outputfile = new File(destinationDir + fileName + "_" + pageNumber + ".tif");
				System.out.println("Image Created -> " + outputfile.getName());
				ImageIO.write(image, "tif", outputfile);
				boolean status = fetchContent(outputfile);
				if (status) {
					FileMovement.move(destinationFile);
					EmailSender.send("amreshdaruvala@gmail.com", "9939019419", "gourav.kumar511991@gmail.com",
							"Delivery Status", "Processed");

				}

				pageNumber++;
			}
			document.close();
			// System.out.println("Converted Images are saved at -> " +
			// destinationFile.getAbsolutePath());
		} else {
			System.err.println(sourceFile.getName() + " File not exists");
		}

	}
	
	

}
