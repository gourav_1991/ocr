package com.emailattachmentdownloader.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import com.emailattachmentdownloader.model.VendorModel;

public class VendorValidationDao {

	
	
	private JdbcTemplate jdbcTemplate;

	public JdbcTemplate getJdbcTemplate() {
		
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	public List<VendorModel> getVendors()
	{
		  String sql = "select * from vendor";
		  List<VendorModel>vendorModels=jdbcTemplate.query(sql,new VendorModelMapper() );
		  
			return vendorModels;
		
	
	}
	
	
	 class VendorModelMapper implements RowMapper<VendorModel>
	  {
		  public  VendorModel mapRow(ResultSet rs, int arg1) throws SQLException 
		  {
			  VendorModel vendorModel =new VendorModel();
			  
			  
			  
			  vendorModel.setEccNo(rs.getString("ECCNO"));
			  vendorModel.setVatNo(rs.getString("VATNO"));
			  vendorModel.setPinCode(rs.getString("PINCODE"));
			  return vendorModel ;   
			  
		  }
	  }
}
