package org.slf4j.impl;

import java.io.Serializable;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.slf4j.Marker;
import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MarkerIgnoringBase;
import org.slf4j.helpers.MessageFormatter;
import org.slf4j.spi.LocationAwareLogger;

















































public final class Log4jLoggerAdapter
  extends MarkerIgnoringBase
  implements LocationAwareLogger, Serializable
{
  private static final long serialVersionUID = 6182834493563598289L;
  final transient Logger logger;
  static final String FQCN = Log4jLoggerAdapter.class.getName();
  


  final boolean traceCapable;
  


  Log4jLoggerAdapter(Logger logger)
  {
    this.logger = logger;
    name = logger.getName();
    traceCapable = isTraceCapable();
  }
  
  private boolean isTraceCapable() {
    try {
      logger.isTraceEnabled();
      return true;
    } catch (NoSuchMethodError e) {}
    return false;
  }
  





  public boolean isTraceEnabled()
  {
    if (traceCapable) {
      return logger.isTraceEnabled();
    }
    return logger.isDebugEnabled();
  }
  






  public void trace(String msg)
  {
    logger.log(FQCN, traceCapable ? Level.TRACE : Level.DEBUG, msg, null);
  }
  













  public void trace(String format, Object arg)
  {
    if (isTraceEnabled()) {
      FormattingTuple ft = MessageFormatter.format(format, arg);
      logger.log(FQCN, traceCapable ? Level.TRACE : Level.DEBUG, ft.getMessage(), ft.getThrowable());
    }
  }
  
















  public void trace(String format, Object arg1, Object arg2)
  {
    if (isTraceEnabled()) {
      FormattingTuple ft = MessageFormatter.format(format, arg1, arg2);
      logger.log(FQCN, traceCapable ? Level.TRACE : Level.DEBUG, ft.getMessage(), ft.getThrowable());
    }
  }
  














  public void trace(String format, Object[] argArray)
  {
    if (isTraceEnabled()) {
      FormattingTuple ft = MessageFormatter.arrayFormat(format, argArray);
      logger.log(FQCN, traceCapable ? Level.TRACE : Level.DEBUG, ft.getMessage(), ft.getThrowable());
    }
  }
  








  public void trace(String msg, Throwable t)
  {
    logger.log(FQCN, traceCapable ? Level.TRACE : Level.DEBUG, msg, t);
  }
  




  public boolean isDebugEnabled()
  {
    return logger.isDebugEnabled();
  }
  





  public void debug(String msg)
  {
    logger.log(FQCN, Level.DEBUG, msg, null);
  }
  













  public void debug(String format, Object arg)
  {
    if (logger.isDebugEnabled()) {
      FormattingTuple ft = MessageFormatter.format(format, arg);
      logger.log(FQCN, Level.DEBUG, ft.getMessage(), ft.getThrowable());
    }
  }
  















  public void debug(String format, Object arg1, Object arg2)
  {
    if (logger.isDebugEnabled()) {
      FormattingTuple ft = MessageFormatter.format(format, arg1, arg2);
      logger.log(FQCN, Level.DEBUG, ft.getMessage(), ft.getThrowable());
    }
  }
  













  public void debug(String format, Object[] argArray)
  {
    if (logger.isDebugEnabled()) {
      FormattingTuple ft = MessageFormatter.arrayFormat(format, argArray);
      logger.log(FQCN, Level.DEBUG, ft.getMessage(), ft.getThrowable());
    }
  }
  







  public void debug(String msg, Throwable t)
  {
    logger.log(FQCN, Level.DEBUG, msg, t);
  }
  




  public boolean isInfoEnabled()
  {
    return logger.isInfoEnabled();
  }
  





  public void info(String msg)
  {
    logger.log(FQCN, Level.INFO, msg, null);
  }
  












  public void info(String format, Object arg)
  {
    if (logger.isInfoEnabled()) {
      FormattingTuple ft = MessageFormatter.format(format, arg);
      logger.log(FQCN, Level.INFO, ft.getMessage(), ft.getThrowable());
    }
  }
  















  public void info(String format, Object arg1, Object arg2)
  {
    if (logger.isInfoEnabled()) {
      FormattingTuple ft = MessageFormatter.format(format, arg1, arg2);
      logger.log(FQCN, Level.INFO, ft.getMessage(), ft.getThrowable());
    }
  }
  













  public void info(String format, Object[] argArray)
  {
    if (logger.isInfoEnabled()) {
      FormattingTuple ft = MessageFormatter.arrayFormat(format, argArray);
      logger.log(FQCN, Level.INFO, ft.getMessage(), ft.getThrowable());
    }
  }
  








  public void info(String msg, Throwable t)
  {
    logger.log(FQCN, Level.INFO, msg, t);
  }
  




  public boolean isWarnEnabled()
  {
    return logger.isEnabledFor(Level.WARN);
  }
  





  public void warn(String msg)
  {
    logger.log(FQCN, Level.WARN, msg, null);
  }
  













  public void warn(String format, Object arg)
  {
    if (logger.isEnabledFor(Level.WARN)) {
      FormattingTuple ft = MessageFormatter.format(format, arg);
      logger.log(FQCN, Level.WARN, ft.getMessage(), ft.getThrowable());
    }
  }
  















  public void warn(String format, Object arg1, Object arg2)
  {
    if (logger.isEnabledFor(Level.WARN)) {
      FormattingTuple ft = MessageFormatter.format(format, arg1, arg2);
      logger.log(FQCN, Level.WARN, ft.getMessage(), ft.getThrowable());
    }
  }
  













  public void warn(String format, Object[] argArray)
  {
    if (logger.isEnabledFor(Level.WARN)) {
      FormattingTuple ft = MessageFormatter.arrayFormat(format, argArray);
      logger.log(FQCN, Level.WARN, ft.getMessage(), ft.getThrowable());
    }
  }
  








  public void warn(String msg, Throwable t)
  {
    logger.log(FQCN, Level.WARN, msg, t);
  }
  




  public boolean isErrorEnabled()
  {
    return logger.isEnabledFor(Level.ERROR);
  }
  





  public void error(String msg)
  {
    logger.log(FQCN, Level.ERROR, msg, null);
  }
  













  public void error(String format, Object arg)
  {
    if (logger.isEnabledFor(Level.ERROR)) {
      FormattingTuple ft = MessageFormatter.format(format, arg);
      logger.log(FQCN, Level.ERROR, ft.getMessage(), ft.getThrowable());
    }
  }
  















  public void error(String format, Object arg1, Object arg2)
  {
    if (logger.isEnabledFor(Level.ERROR)) {
      FormattingTuple ft = MessageFormatter.format(format, arg1, arg2);
      logger.log(FQCN, Level.ERROR, ft.getMessage(), ft.getThrowable());
    }
  }
  













  public void error(String format, Object[] argArray)
  {
    if (logger.isEnabledFor(Level.ERROR)) {
      FormattingTuple ft = MessageFormatter.arrayFormat(format, argArray);
      logger.log(FQCN, Level.ERROR, ft.getMessage(), ft.getThrowable());
    }
  }
  








  public void error(String msg, Throwable t)
  {
    logger.log(FQCN, Level.ERROR, msg, t);
  }
  
  public void log(Marker marker, String callerFQCN, int level, String msg, Object[] argArray, Throwable t)
  {
    Level log4jLevel;
    switch (level) {
    case 0: 
      log4jLevel = traceCapable ? Level.TRACE : Level.DEBUG;
      break;
    case 10: 
      log4jLevel = Level.DEBUG;
      break;
    case 20: 
      log4jLevel = Level.INFO;
      break;
    case 30: 
      log4jLevel = Level.WARN;
      break;
    case 40: 
      log4jLevel = Level.ERROR;
      break;
    default: 
      throw new IllegalStateException("Level number " + level + " is not recognized.");
    }
    
    logger.log(callerFQCN, log4jLevel, msg, t);
  }
}
