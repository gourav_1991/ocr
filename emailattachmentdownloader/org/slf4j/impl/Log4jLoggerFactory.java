package org.slf4j.impl;

import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.LogManager;
import org.slf4j.ILoggerFactory;
































public class Log4jLoggerFactory
  implements ILoggerFactory
{
  Map loggerMap;
  
  public Log4jLoggerFactory()
  {
    loggerMap = new HashMap();
  }
  




  public org.slf4j.Logger getLogger(String name)
  {
    org.slf4j.Logger slf4jLogger = null;
    
    synchronized (this) {
      slf4jLogger = (org.slf4j.Logger)loggerMap.get(name);
      if (slf4jLogger == null) { org.apache.log4j.Logger log4jLogger;
        org.apache.log4j.Logger log4jLogger;
        if (name.equalsIgnoreCase("ROOT")) {
          log4jLogger = LogManager.getRootLogger();
        } else {
          log4jLogger = LogManager.getLogger(name);
        }
        slf4jLogger = new Log4jLoggerAdapter(log4jLogger);
        loggerMap.put(name, slf4jLogger);
      }
    }
    return slf4jLogger;
  }
}
