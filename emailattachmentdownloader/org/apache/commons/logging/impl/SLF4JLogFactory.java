package org.apache.commons.logging.impl;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogConfigurationException;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.spi.LocationAwareLogger;











































public class SLF4JLogFactory
  extends LogFactory
{
  Map loggerMap;
  public static final String LOG_PROPERTY = "org.apache.commons.logging.Log";
  
  public SLF4JLogFactory()
  {
    loggerMap = new HashMap();
  }
  













  protected Hashtable attributes = new Hashtable();
  









  public Object getAttribute(String name)
  {
    return attributes.get(name);
  }
  






  public String[] getAttributeNames()
  {
    List names = new ArrayList();
    Enumeration keys = attributes.keys();
    while (keys.hasMoreElements()) {
      names.add((String)keys.nextElement());
    }
    String[] results = new String[names.size()];
    for (int i = 0; i < results.length; i++) {
      results[i] = ((String)names.get(i));
    }
    return results;
  }
  










  public Log getInstance(Class clazz)
    throws LogConfigurationException
  {
    return getInstance(clazz.getName());
  }
  













  public Log getInstance(String name)
    throws LogConfigurationException
  {
    Log instance = null;
    
    synchronized (loggerMap) {
      instance = (Log)loggerMap.get(name);
      if (instance == null) {
        Logger logger = LoggerFactory.getLogger(name);
        if ((logger instanceof LocationAwareLogger)) {
          instance = new SLF4JLocationAwareLog((LocationAwareLogger)logger);
        } else {
          instance = new SLF4JLog(logger);
        }
        loggerMap.put(name, instance);
      }
    }
    return instance;
  }
  















  public void release()
  {
    System.out.println("WARN: The method " + SLF4JLogFactory.class + "#release() was invoked.");
    
    System.out.println("WARN: Please see http://www.slf4j.org/codes.html#release for an explanation.");
    
    System.out.flush();
  }
  






  public void removeAttribute(String name)
  {
    attributes.remove(name);
  }
  











  public void setAttribute(String name, Object value)
  {
    if (value == null) {
      attributes.remove(name);
    } else {
      attributes.put(name, value);
    }
  }
}
