package org.aspectj.runtime.reflect;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;
import org.aspectj.lang.reflect.MethodSignature;












class MethodSignatureImpl
  extends CodeSignatureImpl
  implements MethodSignature
{
  private Method method;
  Class returnType;
  
  MethodSignatureImpl(int modifiers, String name, Class declaringType, Class[] parameterTypes, String[] parameterNames, Class[] exceptionTypes, Class returnType)
  {
    super(modifiers, name, declaringType, parameterTypes, parameterNames, exceptionTypes);
    this.returnType = returnType;
  }
  
  MethodSignatureImpl(String stringRep) {
    super(stringRep);
  }
  
  public Class getReturnType()
  {
    if (returnType == null)
      returnType = extractType(6);
    return returnType;
  }
  
  protected String createToString(StringMaker sm) {
    StringBuffer buf = new StringBuffer();
    buf.append(sm.makeModifiersString(getModifiers()));
    if (includeArgs)
      buf.append(sm.makeTypeName(getReturnType()));
    if (includeArgs)
      buf.append(" ");
    buf.append(sm.makePrimaryTypeName(getDeclaringType(), getDeclaringTypeName()));
    buf.append(".");
    buf.append(getName());
    sm.addSignature(buf, getParameterTypes());
    sm.addThrows(buf, getExceptionTypes());
    return buf.toString();
  }
  




  public Method getMethod()
  {
    if (method == null) {
      Class dtype = getDeclaringType();
      try {
        method = dtype.getDeclaredMethod(getName(), getParameterTypes());
      }
      catch (NoSuchMethodException nsmEx) {
        Set searched = new HashSet();
        searched.add(dtype);
        method = search(dtype, getName(), getParameterTypes(), searched);
      }
    }
    return method;
  }
  








  private Method search(Class type, String name, Class[] params, Set searched)
  {
    if (type == null) {
      return null;
    }
    if (!searched.contains(type)) {
      searched.add(type);
      try {
        return type.getDeclaredMethod(name, params);
      }
      catch (NoSuchMethodException nsme) {}
    }
    
    Method m = search(type.getSuperclass(), name, params, searched);
    if (m != null) {
      return m;
    }
    Class[] superinterfaces = type.getInterfaces();
    if (superinterfaces != null) {
      for (int i = 0; i < superinterfaces.length; i++) {
        m = search(superinterfaces[i], name, params, searched);
        if (m != null) {
          return m;
        }
      }
    }
    return null;
  }
}
