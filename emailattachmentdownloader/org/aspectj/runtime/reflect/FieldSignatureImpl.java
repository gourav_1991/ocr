package org.aspectj.runtime.reflect;

import java.lang.reflect.Field;
import org.aspectj.lang.reflect.FieldSignature;














public class FieldSignatureImpl
  extends MemberSignatureImpl
  implements FieldSignature
{
  Class fieldType;
  private Field field;
  
  FieldSignatureImpl(int modifiers, String name, Class declaringType, Class fieldType)
  {
    super(modifiers, name, declaringType);
    this.fieldType = fieldType;
  }
  
  FieldSignatureImpl(String stringRep) {
    super(stringRep);
  }
  
  public Class getFieldType() {
    if (fieldType == null) fieldType = extractType(3);
    return fieldType;
  }
  
  protected String createToString(StringMaker sm) {
    StringBuffer buf = new StringBuffer();
    buf.append(sm.makeModifiersString(getModifiers()));
    if (includeArgs) buf.append(sm.makeTypeName(getFieldType()));
    if (includeArgs) buf.append(" ");
    buf.append(sm.makePrimaryTypeName(getDeclaringType(), getDeclaringTypeName()));
    buf.append(".");
    buf.append(getName());
    return buf.toString();
  }
  


  public Field getField()
  {
    if (field == null) {
      try {
        field = getDeclaringType().getDeclaredField(getName());
      }
      catch (Exception ex) {}
    }
    
    return field;
  }
}
