package org.aspectj.runtime.reflect;

import org.aspectj.lang.reflect.SourceLocation;












class SourceLocationImpl
  implements SourceLocation
{
  Class withinType;
  String fileName;
  int line;
  
  SourceLocationImpl(Class withinType, String fileName, int line)
  {
    this.withinType = withinType;
    this.fileName = fileName;
    this.line = line;
  }
  
  public Class getWithinType() { return withinType; }
  public String getFileName() { return fileName; }
  public int getLine() { return line; }
  public int getColumn() { return -1; }
  
  public String toString() {
    return getFileName() + ":" + getLine();
  }
}
