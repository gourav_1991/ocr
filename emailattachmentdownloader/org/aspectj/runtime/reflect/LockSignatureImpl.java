package org.aspectj.runtime.reflect;

import org.aspectj.lang.reflect.LockSignature;











class LockSignatureImpl
  extends SignatureImpl
  implements LockSignature
{
  private Class parameterType;
  
  LockSignatureImpl(Class c)
  {
    super(8, "lock", c);
    parameterType = c;
  }
  
  LockSignatureImpl(String stringRep) {
    super(stringRep);
  }
  
  protected String createToString(StringMaker sm) {
    if (parameterType == null) parameterType = extractType(3);
    return "lock(" + sm.makeTypeName(parameterType) + ")";
  }
  
  public Class getParameterType() {
    if (parameterType == null) parameterType = extractType(3);
    return parameterType;
  }
}
