package org.aspectj.runtime.reflect;

import org.aspectj.lang.JoinPoint.EnclosingStaticPart;
import org.aspectj.lang.JoinPoint.StaticPart;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.reflect.SourceLocation;
import org.aspectj.runtime.internal.AroundClosure;


class JoinPointImpl
  implements ProceedingJoinPoint
{
  Object _this;
  Object target;
  Object[] args;
  JoinPoint.StaticPart staticPart;
  private AroundClosure arc;
  
  static class StaticPartImpl
    implements JoinPoint.StaticPart
  {
    String kind;
    Signature signature;
    SourceLocation sourceLocation;
    private int id;
    
    public StaticPartImpl(int id, String kind, Signature signature, SourceLocation sourceLocation)
    {
      this.kind = kind;
      this.signature = signature;
      this.sourceLocation = sourceLocation;
      this.id = id;
    }
    
    public int getId() {
      return id;
    }
    
    public String getKind() {
      return kind;
    }
    
    public Signature getSignature() {
      return signature;
    }
    
    public SourceLocation getSourceLocation() {
      return sourceLocation;
    }
    
    String toString(StringMaker sm) {
      StringBuffer buf = new StringBuffer();
      buf.append(sm.makeKindName(getKind()));
      buf.append("(");
      buf.append(((SignatureImpl)getSignature()).toString(sm));
      buf.append(")");
      return buf.toString();
    }
    
    public final String toString() {
      return toString(StringMaker.middleStringMaker);
    }
    
    public final String toShortString() {
      return toString(StringMaker.shortStringMaker);
    }
    
    public final String toLongString() {
      return toString(StringMaker.longStringMaker);
    }
  }
  
  static class EnclosingStaticPartImpl extends JoinPointImpl.StaticPartImpl implements JoinPoint.EnclosingStaticPart {
    public EnclosingStaticPartImpl(int count, String kind, Signature signature, SourceLocation sourceLocation) {
      super(kind, signature, sourceLocation);
    }
  }
  




  public JoinPointImpl(JoinPoint.StaticPart staticPart, Object _this, Object target, Object[] args)
  {
    this.staticPart = staticPart;
    this._this = _this;
    this.target = target;
    this.args = args;
  }
  
  public Object getThis() {
    return _this;
  }
  
  public Object getTarget() {
    return target;
  }
  
  public Object[] getArgs() {
    if (args == null) {
      args = new Object[0];
    }
    Object[] argsCopy = new Object[args.length];
    System.arraycopy(args, 0, argsCopy, 0, args.length);
    return argsCopy;
  }
  
  public JoinPoint.StaticPart getStaticPart() {
    return staticPart;
  }
  
  public String getKind() {
    return staticPart.getKind();
  }
  
  public Signature getSignature() {
    return staticPart.getSignature();
  }
  
  public SourceLocation getSourceLocation() {
    return staticPart.getSourceLocation();
  }
  
  public final String toString() {
    return staticPart.toString();
  }
  
  public final String toShortString() {
    return staticPart.toShortString();
  }
  
  public final String toLongString() {
    return staticPart.toLongString();
  }
  


  public void set$AroundClosure(AroundClosure arc)
  {
    this.arc = arc;
  }
  
  public Object proceed() throws Throwable
  {
    if (arc == null) {
      return null;
    }
    return arc.run(arc.getState());
  }
  
  public Object proceed(Object[] adviceBindings) throws Throwable
  {
    if (arc == null) {
      return null;
    }
    



    int flags = arc.getFlags();
    boolean unset = (flags & 0x100000) != 0;
    boolean thisTargetTheSame = (flags & 0x10000) != 0;
    boolean hasThis = (flags & 0x1000) != 0;
    boolean bindsThis = (flags & 0x100) != 0;
    boolean hasTarget = (flags & 0x10) != 0;
    boolean bindsTarget = (flags & 0x1) != 0;
    

    Object[] state = arc.getState();
    






    int firstArgumentIndexIntoAdviceBindings = 0;
    int firstArgumentIndexIntoState = 0;
    firstArgumentIndexIntoState += (hasThis ? 1 : 0);
    firstArgumentIndexIntoState += ((hasTarget) && (!thisTargetTheSame) ? 1 : 0);
    if ((hasThis) && 
      (bindsThis))
    {
      firstArgumentIndexIntoAdviceBindings = 1;
      state[0] = adviceBindings[0];
    }
    


    if ((hasTarget) && 
      (bindsTarget)) {
      if (thisTargetTheSame)
      {
        firstArgumentIndexIntoAdviceBindings = 1 + (bindsThis ? 1 : 0);
        state[0] = adviceBindings[0];

      }
      else
      {
        firstArgumentIndexIntoAdviceBindings = (hasThis ? 1 : 0) + 1;
        state[(hasThis ? 1 : 0)] = adviceBindings[0];
      }
    }
    




    for (int i = firstArgumentIndexIntoAdviceBindings; i < adviceBindings.length; i++) {
      state[(firstArgumentIndexIntoState + (i - firstArgumentIndexIntoAdviceBindings))] = adviceBindings[i];
    }
    







    return arc.run(state);
  }
}
