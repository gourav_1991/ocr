package org.aspectj.runtime.reflect;

import java.lang.reflect.Modifier;














class StringMaker
{
  boolean shortTypeNames = true;
  boolean includeArgs = true;
  boolean includeThrows = false;
  boolean includeModifiers = false;
  boolean shortPrimaryTypeNames = false;
  
  boolean includeJoinPointTypeName = true;
  boolean includeEnclosingPoint = true;
  boolean shortKindName = true;
  

  int cacheOffset;
  
  static StringMaker shortStringMaker = new StringMaker();
  static { shortStringMakershortTypeNames = true;
    shortStringMakerincludeArgs = false;
    shortStringMakerincludeThrows = false;
    shortStringMakerincludeModifiers = false;
    shortStringMakershortPrimaryTypeNames = true;
    
    shortStringMakerincludeJoinPointTypeName = false;
    shortStringMakerincludeEnclosingPoint = false;
    
    shortStringMakercacheOffset = 0;
    



    middleStringMaker = new StringMaker();
    middleStringMakershortTypeNames = true;
    middleStringMakerincludeArgs = true;
    middleStringMakerincludeThrows = false;
    middleStringMakerincludeModifiers = false;
    middleStringMakershortPrimaryTypeNames = false;
    
    shortStringMakercacheOffset = 1;
    



    longStringMaker = new StringMaker();
    longStringMakershortTypeNames = false;
    longStringMakerincludeArgs = true;
    longStringMakerincludeThrows = false;
    longStringMakerincludeModifiers = true;
    longStringMakershortPrimaryTypeNames = false;
    longStringMakershortKindName = false;
    
    longStringMakercacheOffset = 2; }
  
  static StringMaker middleStringMaker;
  static StringMaker longStringMaker;
  String makeKindName(String name) { int dash = name.lastIndexOf('-');
    if (dash == -1) return name;
    return name.substring(dash + 1);
  }
  
  String makeModifiersString(int modifiers) {
    if (!includeModifiers) return "";
    String str = Modifier.toString(modifiers);
    if (str.length() == 0) return "";
    return str + " ";
  }
  
  String stripPackageName(String name) {
    int dot = name.lastIndexOf('.');
    if (dot == -1) return name;
    return name.substring(dot + 1);
  }
  
  String makeTypeName(Class type, String typeName, boolean shortName) {
    if (type == null) return "ANONYMOUS";
    if (type.isArray()) {
      Class componentType = type.getComponentType();
      return makeTypeName(componentType, componentType.getName(), shortName) + "[]";
    }
    if (shortName) {
      return stripPackageName(typeName).replace('$', '.');
    }
    return typeName.replace('$', '.');
  }
  
  public String makeTypeName(Class type)
  {
    return makeTypeName(type, type.getName(), shortTypeNames);
  }
  
  public String makePrimaryTypeName(Class type, String typeName) {
    return makeTypeName(type, typeName, shortPrimaryTypeNames);
  }
  
  public void addTypeNames(StringBuffer buf, Class[] types) {
    for (int i = 0; i < types.length; i++) {
      if (i > 0) buf.append(", ");
      buf.append(makeTypeName(types[i]));
    }
  }
  
  public void addSignature(StringBuffer buf, Class[] types) {
    if (types == null) return;
    if (!includeArgs) {
      if (types.length == 0) {
        buf.append("()");
        return;
      }
      buf.append("(..)");
      return;
    }
    
    buf.append("(");
    addTypeNames(buf, types);
    buf.append(")");
  }
  
  public void addThrows(StringBuffer buf, Class[] types) {
    if ((!includeThrows) || (types == null) || (types.length == 0)) { return;
    }
    buf.append(" throws ");
    addTypeNames(buf, types);
  }
  
  StringMaker() {}
}
