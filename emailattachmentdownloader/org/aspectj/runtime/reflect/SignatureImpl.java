package org.aspectj.runtime.reflect;

import java.lang.ref.SoftReference;
import java.util.StringTokenizer;
import org.aspectj.lang.Signature;














abstract class SignatureImpl
  implements Signature
{
  private static boolean useCache = true;
  
  int modifiers = -1;
  String name;
  String declaringTypeName;
  Class declaringType;
  Cache stringCache;
  private String stringRep;
  
  SignatureImpl(int modifiers, String name, Class declaringType) { this.modifiers = modifiers;
    this.name = name;
    this.declaringType = declaringType;
  }
  
  protected abstract String createToString(StringMaker paramStringMaker);
  
  String toString(StringMaker sm)
  {
    String result = null;
    if (useCache) {
      if (stringCache == null) {
        try {
          stringCache = new CacheImpl();
        } catch (Throwable t) {
          useCache = false;
        }
      } else {
        result = stringCache.get(cacheOffset);
      }
    }
    if (result == null) {
      result = createToString(sm);
    }
    if (useCache) {
      stringCache.set(cacheOffset, result);
    }
    return result;
  }
  
  public final String toString() { return toString(StringMaker.middleStringMaker); }
  public final String toShortString() { return toString(StringMaker.shortStringMaker); }
  public final String toLongString() { return toString(StringMaker.longStringMaker); }
  
  public int getModifiers() {
    if (modifiers == -1) modifiers = extractInt(0);
    return modifiers;
  }
  
  public String getName() { if (name == null) name = extractString(1);
    return name;
  }
  
  public Class getDeclaringType() { if (declaringType == null) declaringType = extractType(2);
    return declaringType;
  }
  
  public String getDeclaringTypeName() { if (declaringTypeName == null) {
      declaringTypeName = getDeclaringType().getName();
    }
    return declaringTypeName;
  }
  
  String fullTypeName(Class type) {
    if (type == null) return "ANONYMOUS";
    if (type.isArray()) return fullTypeName(type.getComponentType()) + "[]";
    return type.getName().replace('$', '.');
  }
  
  String stripPackageName(String name) {
    int dot = name.lastIndexOf('.');
    if (dot == -1) return name;
    return name.substring(dot + 1);
  }
  
  String shortTypeName(Class type) {
    if (type == null) return "ANONYMOUS";
    if (type.isArray()) return shortTypeName(type.getComponentType()) + "[]";
    return stripPackageName(type.getName()).replace('$', '.');
  }
  
  void addFullTypeNames(StringBuffer buf, Class[] types) {
    for (int i = 0; i < types.length; i++) {
      if (i > 0) buf.append(", ");
      buf.append(fullTypeName(types[i]));
    }
  }
  
  void addShortTypeNames(StringBuffer buf, Class[] types) { for (int i = 0; i < types.length; i++) {
      if (i > 0) buf.append(", ");
      buf.append(shortTypeName(types[i]));
    }
  }
  
  void addTypeArray(StringBuffer buf, Class[] types) {
    addFullTypeNames(buf, types);
  }
  


  ClassLoader lookupClassLoader = null;
  static final char SEP = '-';
  
  public void setLookupClassLoader(ClassLoader loader) { lookupClassLoader = loader; }
  
  private ClassLoader getLookupClassLoader()
  {
    if (lookupClassLoader == null) lookupClassLoader = getClass().getClassLoader();
    return lookupClassLoader;
  }
  
  public SignatureImpl(String stringRep) {
    this.stringRep = stringRep;
  }
  



  String extractString(int n)
  {
    int startIndex = 0;
    int endIndex = stringRep.indexOf('-');
    while (n-- > 0) {
      startIndex = endIndex + 1;
      endIndex = stringRep.indexOf('-', startIndex);
    }
    if (endIndex == -1) { endIndex = stringRep.length();
    }
    

    return stringRep.substring(startIndex, endIndex);
  }
  
  int extractInt(int n) {
    String s = extractString(n);
    return Integer.parseInt(s, 16);
  }
  
  Class extractType(int n) {
    String s = extractString(n);
    return Factory.makeClass(s, getLookupClassLoader());
  }
  


  static String[] EMPTY_STRING_ARRAY = new String[0];
  static Class[] EMPTY_CLASS_ARRAY = new Class[0];
  static final String INNER_SEP = ":";
  
  String[] extractStrings(int n)
  {
    String s = extractString(n);
    StringTokenizer st = new StringTokenizer(s, ":");
    int N = st.countTokens();
    String[] ret = new String[N];
    for (int i = 0; i < N; i++) ret[i] = st.nextToken();
    return ret;
  }
  
  Class[] extractTypes(int n) { String s = extractString(n);
    StringTokenizer st = new StringTokenizer(s, ":");
    int N = st.countTokens();
    Class[] ret = new Class[N];
    for (int i = 0; i < N; i++) ret[i] = Factory.makeClass(st.nextToken(), getLookupClassLoader());
    return ret;
  }
  


  static void setUseCache(boolean b)
  {
    useCache = b;
  }
  
  static boolean getUseCache() {
    return useCache;
  }
  



  private static final class CacheImpl
    implements SignatureImpl.Cache
  {
    private SoftReference toStringCacheRef;
    



    public CacheImpl()
    {
      makeCache();
    }
    
    public String get(int cacheOffset) {
      String[] cachedArray = array();
      if (cachedArray == null) {
        return null;
      }
      return cachedArray[cacheOffset];
    }
    
    public void set(int cacheOffset, String result) {
      String[] cachedArray = array();
      if (cachedArray == null) {
        cachedArray = makeCache();
      }
      cachedArray[cacheOffset] = result;
    }
    
    private String[] array() {
      return (String[])toStringCacheRef.get();
    }
    
    private String[] makeCache() {
      String[] array = new String[3];
      toStringCacheRef = new SoftReference(array);
      return array;
    }
  }
  
  private static abstract interface Cache
  {
    public abstract String get(int paramInt);
    
    public abstract void set(int paramInt, String paramString);
  }
}
