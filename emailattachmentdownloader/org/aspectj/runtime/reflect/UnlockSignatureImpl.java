package org.aspectj.runtime.reflect;

import org.aspectj.lang.reflect.UnlockSignature;











class UnlockSignatureImpl
  extends SignatureImpl
  implements UnlockSignature
{
  private Class parameterType;
  
  UnlockSignatureImpl(Class c)
  {
    super(8, "unlock", c);
    parameterType = c;
  }
  
  UnlockSignatureImpl(String stringRep) {
    super(stringRep);
  }
  
  protected String createToString(StringMaker sm) {
    if (parameterType == null) parameterType = extractType(3);
    return "unlock(" + sm.makeTypeName(parameterType) + ")";
  }
  
  public Class getParameterType() {
    if (parameterType == null) parameterType = extractType(3);
    return parameterType;
  }
}
