package org.aspectj.runtime.reflect;

import java.lang.reflect.Method;
import java.util.StringTokenizer;
import org.aspectj.lang.reflect.AdviceSignature;













class AdviceSignatureImpl
  extends CodeSignatureImpl
  implements AdviceSignature
{
  Class returnType;
  private Method adviceMethod = null;
  


  AdviceSignatureImpl(int modifiers, String name, Class declaringType, Class[] parameterTypes, String[] parameterNames, Class[] exceptionTypes, Class returnType)
  {
    super(modifiers, name, declaringType, parameterTypes, parameterNames, exceptionTypes);
    
    this.returnType = returnType;
  }
  
  AdviceSignatureImpl(String stringRep) {
    super(stringRep);
  }
  

  public Class getReturnType()
  {
    if (returnType == null) returnType = extractType(6);
    return returnType;
  }
  
  protected String createToString(StringMaker sm) {
    StringBuffer buf = new StringBuffer();
    
    if (includeArgs) buf.append(sm.makeTypeName(getReturnType()));
    if (includeArgs) buf.append(" ");
    buf.append(sm.makePrimaryTypeName(getDeclaringType(), getDeclaringTypeName()));
    buf.append(".");
    buf.append(toAdviceName(getName()));
    sm.addSignature(buf, getParameterTypes());
    sm.addThrows(buf, getExceptionTypes());
    return buf.toString();
  }
  
  private String toAdviceName(String methodName) {
    if (methodName.indexOf('$') == -1) return methodName;
    StringTokenizer strTok = new StringTokenizer(methodName, "$");
    while (strTok.hasMoreTokens()) {
      String token = strTok.nextToken();
      if ((token.startsWith("before")) || (token.startsWith("after")) || (token.startsWith("around")))
      {
        return token; }
    }
    return methodName;
  }
  


  public Method getAdvice()
  {
    if (adviceMethod == null) {
      try {
        adviceMethod = getDeclaringType().getDeclaredMethod(getName(), getParameterTypes());
      }
      catch (Exception ex) {}
    }
    
    return adviceMethod;
  }
}
