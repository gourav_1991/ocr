package org.aspectj.runtime.internal;

import org.aspectj.lang.ProceedingJoinPoint;





















public abstract class AroundClosure
{
  protected Object[] state;
  protected int bitflags = 1048576;
  protected Object[] preInitializationState;
  
  public AroundClosure() {}
  
  public AroundClosure(Object[] state)
  {
    this.state = state;
  }
  
  public int getFlags() { return bitflags; }
  
  public Object[] getState() {
    return state;
  }
  
  public Object[] getPreInitializationState() {
    return preInitializationState;
  }
  




  public abstract Object run(Object[] paramArrayOfObject)
    throws Throwable;
  



  public ProceedingJoinPoint linkClosureAndJoinPoint()
  {
    ProceedingJoinPoint jp = (ProceedingJoinPoint)state[(state.length - 1)];
    jp.set$AroundClosure(this);
    return jp;
  }
  




  public ProceedingJoinPoint linkClosureAndJoinPoint(int flags)
  {
    ProceedingJoinPoint jp = (ProceedingJoinPoint)state[(state.length - 1)];
    jp.set$AroundClosure(this);
    bitflags = flags;
    return jp;
  }
}
