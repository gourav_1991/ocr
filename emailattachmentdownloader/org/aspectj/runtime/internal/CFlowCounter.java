package org.aspectj.runtime.internal;

import org.aspectj.runtime.internal.cflowstack.ThreadCounter;
import org.aspectj.runtime.internal.cflowstack.ThreadStackFactory;
import org.aspectj.runtime.internal.cflowstack.ThreadStackFactoryImpl;
import org.aspectj.runtime.internal.cflowstack.ThreadStackFactoryImpl11;



















public class CFlowCounter
{
  private static ThreadStackFactory tsFactory;
  private ThreadCounter flowHeightHandler;
  
  public CFlowCounter()
  {
    flowHeightHandler = tsFactory.getNewThreadCounter();
  }
  
  public void inc() {
    flowHeightHandler.inc();
  }
  
  public void dec() {
    flowHeightHandler.dec();
  }
  
  public boolean isValid() {
    return flowHeightHandler.isNotZero();
  }
  

  private static ThreadStackFactory getThreadLocalStackFactory() { return new ThreadStackFactoryImpl(); }
  private static ThreadStackFactory getThreadLocalStackFactoryFor11() { return new ThreadStackFactoryImpl11(); }
  
  private static void selectFactoryForVMVersion() {
    String override = getSystemPropertyWithoutSecurityException("aspectj.runtime.cflowstack.usethreadlocal", "unspecified");
    boolean useThreadLocalImplementation = false;
    if (override.equals("unspecified")) {
      String v = System.getProperty("java.class.version", "0.0");
      
      useThreadLocalImplementation = v.compareTo("46.0") >= 0;
    } else {
      useThreadLocalImplementation = (override.equals("yes")) || (override.equals("true"));
    }
    
    if (useThreadLocalImplementation) {
      tsFactory = getThreadLocalStackFactory();
    } else {
      tsFactory = getThreadLocalStackFactoryFor11();
    }
  }
  
  private static String getSystemPropertyWithoutSecurityException(String aPropertyName, String aDefaultValue)
  {
    try {
      return System.getProperty(aPropertyName, aDefaultValue);
    }
    catch (SecurityException ex) {}
    return aDefaultValue;
  }
  

  public static String getThreadStackFactoryClassName()
  {
    return tsFactory.getClass().getName();
  }
  
  static {}
}
