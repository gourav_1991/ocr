package org.aspectj.runtime.internal;

import java.util.Stack;
import java.util.Vector;
import org.aspectj.lang.NoAspectBoundException;
import org.aspectj.runtime.CFlow;
import org.aspectj.runtime.internal.cflowstack.ThreadStack;
import org.aspectj.runtime.internal.cflowstack.ThreadStackFactory;
import org.aspectj.runtime.internal.cflowstack.ThreadStackFactoryImpl;
import org.aspectj.runtime.internal.cflowstack.ThreadStackFactoryImpl11;













































public class CFlowStack
{
  private static ThreadStackFactory tsFactory;
  private ThreadStack stackProxy;
  
  public CFlowStack()
  {
    stackProxy = tsFactory.getNewThreadStack();
  }
  
  private Stack getThreadStack() {
    return stackProxy.getThreadStack();
  }
  
  public void push(Object obj)
  {
    getThreadStack().push(obj);
  }
  
  public void pushInstance(Object obj) {
    getThreadStack().push(new CFlow(obj));
  }
  
  public void push(Object[] obj) {
    getThreadStack().push(new CFlowPlusState(obj));
  }
  
  public void pop() {
    getThreadStack().pop();
  }
  
  public Object peek() {
    Stack stack = getThreadStack();
    if (stack.isEmpty()) throw new NoAspectBoundException();
    return stack.peek();
  }
  
  public Object get(int index) {
    CFlow cf = peekCFlow();
    return null == cf ? null : cf.get(index);
  }
  
  public Object peekInstance() {
    CFlow cf = peekCFlow();
    if (cf != null) return cf.getAspect();
    throw new NoAspectBoundException();
  }
  
  public CFlow peekCFlow() {
    Stack stack = getThreadStack();
    if (stack.isEmpty()) return null;
    return (CFlow)stack.peek();
  }
  
  public CFlow peekTopCFlow() {
    Stack stack = getThreadStack();
    if (stack.isEmpty()) return null;
    return (CFlow)stack.elementAt(0);
  }
  
  public boolean isValid() {
    return !getThreadStack().isEmpty();
  }
  
  private static ThreadStackFactory getThreadLocalStackFactory() { return new ThreadStackFactoryImpl(); }
  private static ThreadStackFactory getThreadLocalStackFactoryFor11() { return new ThreadStackFactoryImpl11(); }
  
  private static void selectFactoryForVMVersion() {
    String override = getSystemPropertyWithoutSecurityException("aspectj.runtime.cflowstack.usethreadlocal", "unspecified");
    boolean useThreadLocalImplementation = false;
    if (override.equals("unspecified")) {
      String v = System.getProperty("java.class.version", "0.0");
      
      useThreadLocalImplementation = v.compareTo("46.0") >= 0;
    } else {
      useThreadLocalImplementation = (override.equals("yes")) || (override.equals("true"));
    }
    
    if (useThreadLocalImplementation) {
      tsFactory = getThreadLocalStackFactory();
    } else {
      tsFactory = getThreadLocalStackFactoryFor11();
    }
  }
  
  private static String getSystemPropertyWithoutSecurityException(String aPropertyName, String aDefaultValue) {
    try {
      return System.getProperty(aPropertyName, aDefaultValue);
    }
    catch (SecurityException ex) {}
    return aDefaultValue;
  }
  


  public static String getThreadStackFactoryClassName()
  {
    return tsFactory.getClass().getName();
  }
  
  static {}
}
