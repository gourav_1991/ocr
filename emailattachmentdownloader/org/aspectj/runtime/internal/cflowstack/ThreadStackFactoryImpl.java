package org.aspectj.runtime.internal.cflowstack;

import java.util.Stack;






public class ThreadStackFactoryImpl
  implements ThreadStackFactory
{
  public ThreadStackFactoryImpl() {}
  
  private static class ThreadStackImpl
    extends ThreadLocal
    implements ThreadStack
  {
    ThreadStackImpl(ThreadStackFactoryImpl.1 x0) { this(); }
    
    public Object initialValue() { return new Stack(); }
    

    public Stack getThreadStack() { return (Stack)get(); }
    
    private ThreadStackImpl() {}
  }
  
  public ThreadStack getNewThreadStack() { return new ThreadStackImpl(null); }
  
  private static class ThreadCounterImpl extends ThreadLocal implements ThreadCounter {
    ThreadCounterImpl(ThreadStackFactoryImpl.1 x0) { this(); }
    
    public Object initialValue() {
      return new Counter();
    }
    
    public Counter getThreadCounter() { return (Counter)get(); }
    

    public void inc() { getThreadCountervalue += 1; }
    public void dec() { getThreadCountervalue -= 1; }
    public boolean isNotZero() { return getThreadCountervalue != 0; }
    
    static class Counter { Counter() {}
      protected int value = 0;
      
       }
    
    private ThreadCounterImpl() {} }
  public ThreadCounter getNewThreadCounter() { return new ThreadCounterImpl(null); }
}
