package org.aspectj.runtime.internal.cflowstack;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Stack;
import java.util.Vector;











public class ThreadStackImpl11
  implements ThreadStack
{
  private Hashtable stacks = new Hashtable();
  private Thread cached_thread;
  private Stack cached_stack;
  private int change_count = 0;
  private static final int COLLECT_AT = 20000;
  
  public ThreadStackImpl11() {}
  
  public synchronized Stack getThreadStack() { if (Thread.currentThread() != cached_thread) {
      cached_thread = Thread.currentThread();
      cached_stack = ((Stack)stacks.get(cached_thread));
      if (cached_stack == null) {
        cached_stack = new Stack();
        stacks.put(cached_thread, cached_stack);
      }
      change_count += 1;
      
      int size = Math.max(1, stacks.size());
      if (change_count > Math.max(100, 20000 / size)) {
        Stack dead_stacks = new Stack();
        for (Enumeration e = stacks.keys(); e.hasMoreElements();) {
          Thread t = (Thread)e.nextElement();
          if (!t.isAlive()) dead_stacks.push(t);
        }
        for (Enumeration e = dead_stacks.elements(); e.hasMoreElements();) {
          Thread t = (Thread)e.nextElement();
          stacks.remove(t);
        }
        change_count = 0;
      }
    }
    return cached_stack;
  }
  
  private static final int MIN_COLLECT_AT = 100;
}
