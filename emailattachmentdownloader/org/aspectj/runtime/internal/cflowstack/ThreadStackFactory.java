package org.aspectj.runtime.internal.cflowstack;

public abstract interface ThreadStackFactory
{
  public abstract ThreadStack getNewThreadStack();
  
  public abstract ThreadCounter getNewThreadCounter();
}
