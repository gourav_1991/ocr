package org.aspectj.runtime.internal.cflowstack;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;



public class ThreadCounterImpl11
  implements ThreadCounter
{
  private Hashtable counters;
  private Thread cached_thread;
  private Counter cached_counter;
  private int change_count;
  private static final int COLLECT_AT = 20000;
  private static final int MIN_COLLECT_AT = 100;
  
  public ThreadCounterImpl11()
  {
    counters = new Hashtable();
    


    change_count = 0;
  }
  
  static class Counter
  {
    protected int value = 0;
    
    Counter() {} }
  
  private synchronized Counter getThreadCounter() { if (Thread.currentThread() != cached_thread) {
      cached_thread = Thread.currentThread();
      cached_counter = ((Counter)counters.get(cached_thread));
      if (cached_counter == null) {
        cached_counter = new Counter();
        counters.put(cached_thread, cached_counter);
      }
      change_count += 1;
      
      int size = Math.max(1, counters.size());
      if (change_count > Math.max(100, 20000 / size)) {
        List dead_stacks = new ArrayList();
        for (Enumeration e = counters.keys(); e.hasMoreElements();) {
          Thread t = (Thread)e.nextElement();
          if (!t.isAlive()) dead_stacks.add(t);
        }
        for (Iterator e = dead_stacks.iterator(); e.hasNext();) {
          Thread t = (Thread)e.next();
          counters.remove(t);
        }
        change_count = 0;
      }
    }
    return cached_counter;
  }
  
  public void inc() {
    getThreadCountervalue += 1;
  }
  
  public void dec() {
    getThreadCountervalue -= 1;
  }
  
  public boolean isNotZero() {
    return getThreadCountervalue != 0;
  }
}
