package org.aspectj.runtime.internal.cflowstack;

public abstract interface ThreadCounter
{
  public abstract void inc();
  
  public abstract void dec();
  
  public abstract boolean isNotZero();
}
