package org.aspectj.lang;

public abstract interface Signature
{
  public abstract String toString();
  
  public abstract String toShortString();
  
  public abstract String toLongString();
  
  public abstract String getName();
  
  public abstract int getModifiers();
  
  public abstract Class getDeclaringType();
  
  public abstract String getDeclaringTypeName();
}
