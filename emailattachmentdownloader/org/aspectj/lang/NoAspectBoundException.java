package org.aspectj.lang;








public class NoAspectBoundException
  extends RuntimeException
{
  Throwable cause;
  







  public NoAspectBoundException(String aspectName, Throwable inner)
  {
    super("Exception while initializing " + aspectName + ": " + inner);
    
    cause = inner;
  }
  
  public NoAspectBoundException() {}
  
  public Throwable getCause() {
    return cause;
  }
}
