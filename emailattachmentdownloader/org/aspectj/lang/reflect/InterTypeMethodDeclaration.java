package org.aspectj.lang.reflect;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

public abstract interface InterTypeMethodDeclaration
  extends InterTypeDeclaration
{
  public abstract String getName();
  
  public abstract AjType<?> getReturnType();
  
  public abstract Type getGenericReturnType();
  
  public abstract AjType<?>[] getParameterTypes();
  
  public abstract Type[] getGenericParameterTypes();
  
  public abstract TypeVariable<Method>[] getTypeParameters();
  
  public abstract AjType<?>[] getExceptionTypes();
}
