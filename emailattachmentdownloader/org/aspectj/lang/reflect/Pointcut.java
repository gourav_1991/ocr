package org.aspectj.lang.reflect;

public abstract interface Pointcut
{
  public abstract String getName();
  
  public abstract int getModifiers();
  
  public abstract AjType<?>[] getParameterTypes();
  
  public abstract String[] getParameterNames();
  
  public abstract AjType getDeclaringType();
  
  public abstract PointcutExpression getPointcutExpression();
}
