package org.aspectj.lang.reflect;

import java.lang.reflect.Type;

public abstract interface InterTypeConstructorDeclaration
  extends InterTypeDeclaration
{
  public abstract AjType<?>[] getParameterTypes();
  
  public abstract Type[] getGenericParameterTypes();
  
  public abstract AjType<?>[] getExceptionTypes();
}
