package org.aspectj.lang.reflect;

import java.lang.annotation.Annotation;


public abstract interface DeclareAnnotation
{
  public abstract AjType<?> getDeclaringType();
  
  public abstract Kind getKind();
  
  public abstract SignaturePattern getSignaturePattern();
  
  public abstract TypePattern getTypePattern();
  
  public abstract Annotation getAnnotation();
  
  public abstract String getAnnotationAsText();
  
  public static enum Kind
  {
    Field,  Method,  Constructor,  Type;
    
    private Kind() {}
  }
}
