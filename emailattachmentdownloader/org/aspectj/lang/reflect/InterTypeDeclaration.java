package org.aspectj.lang.reflect;

public abstract interface InterTypeDeclaration
{
  public abstract AjType<?> getDeclaringType();
  
  public abstract AjType<?> getTargetType()
    throws ClassNotFoundException;
  
  public abstract int getModifiers();
}
