package org.aspectj.lang.reflect;

public abstract interface SourceLocation
{
  public abstract Class getWithinType();
  
  public abstract String getFileName();
  
  public abstract int getLine();
  
  /**
   * @deprecated
   */
  public abstract int getColumn();
}
