package org.aspectj.internal.lang.reflect;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import org.aspectj.lang.annotation.AdviceName;
import org.aspectj.lang.reflect.Advice;
import org.aspectj.lang.reflect.AdviceKind;
import org.aspectj.lang.reflect.AjType;
import org.aspectj.lang.reflect.AjTypeSystem;
import org.aspectj.lang.reflect.PointcutExpression;

















public class AdviceImpl
  implements Advice
{
  private static final String AJC_INTERNAL = "org.aspectj.runtime.internal";
  private final AdviceKind kind;
  private final Method adviceMethod;
  private PointcutExpression pointcutExpression;
  private boolean hasExtraParam = false;
  private Type[] genericParameterTypes;
  private AjType[] parameterTypes;
  private AjType[] exceptionTypes;
  
  protected AdviceImpl(Method method, String pointcut, AdviceKind type) {
    kind = type;
    adviceMethod = method;
    pointcutExpression = new PointcutExpressionImpl(pointcut);
  }
  
  protected AdviceImpl(Method method, String pointcut, AdviceKind type, String extraParamName) {
    this(method, pointcut, type);
  }
  
  public AjType getDeclaringType()
  {
    return AjTypeSystem.getAjType(adviceMethod.getDeclaringClass());
  }
  
  public Type[] getGenericParameterTypes() {
    if (genericParameterTypes == null) {
      Type[] genTypes = adviceMethod.getGenericParameterTypes();
      int syntheticCount = 0;
      for (Type t : genTypes) {
        if (((t instanceof Class)) && 
          (((Class)t).getPackage().getName().equals("org.aspectj.runtime.internal"))) { syntheticCount++;
        }
      }
      genericParameterTypes = new Type[genTypes.length - syntheticCount];
      for (int i = 0; i < genericParameterTypes.length; i++) {
        if ((genTypes[i] instanceof Class)) {
          genericParameterTypes[i] = AjTypeSystem.getAjType((Class)genTypes[i]);
        } else {
          genericParameterTypes[i] = genTypes[i];
        }
      }
    }
    return genericParameterTypes;
  }
  
  public AjType<?>[] getParameterTypes() {
    if (parameterTypes == null) {
      Class<?>[] ptypes = adviceMethod.getParameterTypes();
      int syntheticCount = 0;
      for (Class<?> c : ptypes) {
        if (c.getPackage().getName().equals("org.aspectj.runtime.internal")) syntheticCount++;
      }
      parameterTypes = new AjType[ptypes.length - syntheticCount];
      for (int i = 0; i < parameterTypes.length; i++) {
        parameterTypes[i] = AjTypeSystem.getAjType(ptypes[i]);
      }
    }
    return parameterTypes;
  }
  
  public AjType<?>[] getExceptionTypes() {
    if (exceptionTypes == null) {
      Class<?>[] exTypes = adviceMethod.getExceptionTypes();
      exceptionTypes = new AjType[exTypes.length];
      for (int i = 0; i < exTypes.length; i++) {
        exceptionTypes[i] = AjTypeSystem.getAjType(exTypes[i]);
      }
    }
    return exceptionTypes;
  }
  
  public AdviceKind getKind() {
    return kind;
  }
  
  public String getName() {
    String adviceName = adviceMethod.getName();
    if (adviceName.startsWith("ajc$")) {
      adviceName = "";
      AdviceName name = (AdviceName)adviceMethod.getAnnotation(AdviceName.class);
      if (name != null) adviceName = name.value();
    }
    return adviceName;
  }
  
  public PointcutExpression getPointcutExpression() {
    return pointcutExpression;
  }
  
  public String toString() {
    StringBuffer sb = new StringBuffer();
    if (getName().length() > 0) {
      sb.append("@AdviceName(\"");
      sb.append(getName());
      sb.append("\") ");
    }
    if (getKind() == AdviceKind.AROUND) {
      sb.append(adviceMethod.getGenericReturnType().toString());
      sb.append(" ");
    }
    switch (1.$SwitchMap$org$aspectj$lang$reflect$AdviceKind[getKind().ordinal()]) {
    case 1: 
      sb.append("after(");
      break;
    case 2: 
      sb.append("after(");
      break;
    case 3: 
      sb.append("after(");
      break;
    case 4: 
      sb.append("around(");
      break;
    case 5: 
      sb.append("before(");
    }
    
    AjType<?>[] ptypes = getParameterTypes();
    int len = ptypes.length;
    if (hasExtraParam) len--;
    for (int i = 0; i < len; i++) {
      sb.append(ptypes[i].getName());
      if (i + 1 < len) sb.append(",");
    }
    sb.append(") ");
    switch (1.$SwitchMap$org$aspectj$lang$reflect$AdviceKind[getKind().ordinal()]) {
    case 2: 
      sb.append("returning");
      if (hasExtraParam) {
        sb.append("(");
        sb.append(ptypes[(len - 1)].getName());
        sb.append(") ");
      }
    case 3: 
      sb.append("throwing");
      if (hasExtraParam) {
        sb.append("(");
        sb.append(ptypes[(len - 1)].getName());
        sb.append(") ");
      }
      break;
    }
    AjType<?>[] exTypes = getExceptionTypes();
    if (exTypes.length > 0) {
      sb.append("throws ");
      for (int i = 0; i < exTypes.length; i++) {
        sb.append(exTypes[i].getName());
        if (i + 1 < exTypes.length) sb.append(",");
      }
      sb.append(" ");
    }
    sb.append(": ");
    sb.append(getPointcutExpression().asString());
    return sb.toString();
  }
}
