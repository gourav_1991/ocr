package org.aspectj.internal.lang.reflect;

import java.lang.annotation.Annotation;
import org.aspectj.lang.reflect.AjType;
import org.aspectj.lang.reflect.DeclareAnnotation;
import org.aspectj.lang.reflect.DeclareAnnotation.Kind;
import org.aspectj.lang.reflect.SignaturePattern;
import org.aspectj.lang.reflect.TypePattern;














public class DeclareAnnotationImpl
  implements DeclareAnnotation
{
  private Annotation theAnnotation;
  private String annText;
  private AjType<?> declaringType;
  private DeclareAnnotation.Kind kind;
  private TypePattern typePattern;
  private SignaturePattern signaturePattern;
  
  public DeclareAnnotationImpl(AjType<?> declaring, String kindString, String pattern, Annotation ann, String annText)
  {
    declaringType = declaring;
    if (kindString.equals("at_type")) { kind = DeclareAnnotation.Kind.Type;
    } else if (kindString.equals("at_field")) { kind = DeclareAnnotation.Kind.Field;
    } else if (kindString.equals("at_method")) { kind = DeclareAnnotation.Kind.Method;
    } else if (kindString.equals("at_constructor")) kind = DeclareAnnotation.Kind.Constructor; else
      throw new IllegalStateException("Unknown declare annotation kind: " + kindString);
    if (kind == DeclareAnnotation.Kind.Type) {
      typePattern = new TypePatternImpl(pattern);
    } else {
      signaturePattern = new SignaturePatternImpl(pattern);
    }
    theAnnotation = ann;
    this.annText = annText;
  }
  


  public AjType<?> getDeclaringType()
  {
    return declaringType;
  }
  


  public DeclareAnnotation.Kind getKind()
  {
    return kind;
  }
  


  public SignaturePattern getSignaturePattern()
  {
    return signaturePattern;
  }
  


  public TypePattern getTypePattern()
  {
    return typePattern;
  }
  


  public Annotation getAnnotation()
  {
    return theAnnotation;
  }
  
  public String getAnnotationAsText() {
    return annText;
  }
  
  public String toString() {
    StringBuffer sb = new StringBuffer();
    sb.append("declare @");
    switch (1.$SwitchMap$org$aspectj$lang$reflect$DeclareAnnotation$Kind[getKind().ordinal()]) {
    case 1: 
      sb.append("type : ");
      sb.append(getTypePattern().asString());
      break;
    case 2: 
      sb.append("method : ");
      sb.append(getSignaturePattern().asString());
      break;
    case 3: 
      sb.append("field : ");
      sb.append(getSignaturePattern().asString());
      break;
    case 4: 
      sb.append("constructor : ");
      sb.append(getSignaturePattern().asString());
    }
    
    sb.append(" : ");
    sb.append(getAnnotationAsText());
    return sb.toString();
  }
}
