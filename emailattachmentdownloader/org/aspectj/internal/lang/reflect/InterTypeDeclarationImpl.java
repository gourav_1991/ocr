package org.aspectj.internal.lang.reflect;

import org.aspectj.lang.reflect.AjType;
import org.aspectj.lang.reflect.InterTypeDeclaration;














public class InterTypeDeclarationImpl
  implements InterTypeDeclaration
{
  private AjType<?> declaringType;
  protected String targetTypeName;
  private AjType<?> targetType;
  private int modifiers;
  
  public InterTypeDeclarationImpl(AjType<?> decType, String target, int mods)
  {
    declaringType = decType;
    targetTypeName = target;
    modifiers = mods;
    try {
      targetType = ((AjType)StringToType.stringToType(target, decType.getJavaClass()));
    }
    catch (ClassNotFoundException cnf) {}
  }
  
  public InterTypeDeclarationImpl(AjType<?> decType, AjType<?> targetType, int mods)
  {
    declaringType = decType;
    this.targetType = targetType;
    targetTypeName = targetType.getName();
    modifiers = mods;
  }
  


  public AjType<?> getDeclaringType()
  {
    return declaringType;
  }
  

  public AjType<?> getTargetType()
    throws ClassNotFoundException
  {
    if (targetType == null) throw new ClassNotFoundException(targetTypeName);
    return targetType;
  }
  


  public int getModifiers()
  {
    return modifiers;
  }
}
