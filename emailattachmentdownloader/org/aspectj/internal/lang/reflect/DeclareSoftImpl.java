package org.aspectj.internal.lang.reflect;

import org.aspectj.lang.reflect.AjType;
import org.aspectj.lang.reflect.AjTypeSystem;
import org.aspectj.lang.reflect.DeclareSoft;
import org.aspectj.lang.reflect.PointcutExpression;















public class DeclareSoftImpl
  implements DeclareSoft
{
  private AjType<?> declaringType;
  private PointcutExpression pointcut;
  private AjType<?> exceptionType;
  private String missingTypeName;
  
  public DeclareSoftImpl(AjType<?> declaringType, String pcut, String exceptionTypeName)
  {
    this.declaringType = declaringType;
    pointcut = new PointcutExpressionImpl(pcut);
    try {
      ClassLoader cl = declaringType.getJavaClass().getClassLoader();
      exceptionType = AjTypeSystem.getAjType(Class.forName(exceptionTypeName, false, cl));
    } catch (ClassNotFoundException ex) {
      missingTypeName = exceptionTypeName;
    }
  }
  


  public AjType getDeclaringType()
  {
    return declaringType;
  }
  

  public AjType getSoftenedExceptionType()
    throws ClassNotFoundException
  {
    if (missingTypeName != null) throw new ClassNotFoundException(missingTypeName);
    return exceptionType;
  }
  


  public PointcutExpression getPointcutExpression()
  {
    return pointcut;
  }
  
  public String toString() {
    StringBuffer sb = new StringBuffer();
    sb.append("declare soft : ");
    if (missingTypeName != null) {
      sb.append(exceptionType.getName());
    } else {
      sb.append(missingTypeName);
    }
    sb.append(" : ");
    sb.append(getPointcutExpression().asString());
    return sb.toString();
  }
}
