package org.aspectj.internal.lang.reflect;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.StringTokenizer;
import org.aspectj.lang.reflect.AjTypeSystem;














public class StringToType
{
  public StringToType() {}
  
  public static Type[] commaSeparatedListToTypeArray(String typeNames, Class classScope)
    throws ClassNotFoundException
  {
    StringTokenizer strTok = new StringTokenizer(typeNames, ",");
    Type[] ret = new Type[strTok.countTokens()];
    int index = 0;
    
    while (strTok.hasMoreTokens()) {
      String typeName = strTok.nextToken().trim();
      ret[(index++)] = stringToType(typeName, classScope);
    }
    return ret;
  }
  
  public static Type stringToType(String typeName, Class classScope) throws ClassNotFoundException
  {
    try {
      if (typeName.indexOf("<") == -1) {
        return AjTypeSystem.getAjType(Class.forName(typeName, false, classScope.getClassLoader()));
      }
      return makeParameterizedType(typeName, classScope);
    }
    catch (ClassNotFoundException e)
    {
      TypeVariable[] tVars = classScope.getTypeParameters();
      for (int i = 0; i < tVars.length; i++) {
        if (tVars[i].getName().equals(typeName)) {
          return tVars[i];
        }
      }
      throw new ClassNotFoundException(typeName);
    }
  }
  
  private static Type makeParameterizedType(String typeName, Class classScope) throws ClassNotFoundException
  {
    int paramStart = typeName.indexOf('<');
    String baseName = typeName.substring(0, paramStart);
    final Class baseClass = Class.forName(baseName, false, classScope.getClassLoader());
    int paramEnd = typeName.lastIndexOf('>');
    String params = typeName.substring(paramStart + 1, paramEnd);
    Type[] typeParams = commaSeparatedListToTypeArray(params, classScope);
    new ParameterizedType()
    {
      public Type[] getActualTypeArguments() {
        return val$typeParams;
      }
      
      public Type getRawType() {
        return baseClass;
      }
      
      public Type getOwnerType() {
        return baseClass.getEnclosingClass();
      }
    };
  }
}
