package org.aspectj.internal.lang.reflect;

import java.lang.reflect.Method;
import java.util.StringTokenizer;
import org.aspectj.lang.reflect.AjType;
import org.aspectj.lang.reflect.AjTypeSystem;
import org.aspectj.lang.reflect.Pointcut;
import org.aspectj.lang.reflect.PointcutExpression;
















public class PointcutImpl
  implements Pointcut
{
  private final String name;
  private final PointcutExpression pc;
  private final Method baseMethod;
  private final AjType declaringType;
  private String[] parameterNames = new String[0];
  
  protected PointcutImpl(String name, String pc, Method method, AjType declaringType, String pNames) {
    this.name = name;
    this.pc = new PointcutExpressionImpl(pc);
    baseMethod = method;
    this.declaringType = declaringType;
    parameterNames = splitOnComma(pNames);
  }
  


  public PointcutExpression getPointcutExpression()
  {
    return pc;
  }
  
  public String getName() {
    return name;
  }
  
  public int getModifiers() {
    return baseMethod.getModifiers();
  }
  
  public AjType<?>[] getParameterTypes() {
    Class<?>[] baseParamTypes = baseMethod.getParameterTypes();
    AjType<?>[] ajParamTypes = new AjType[baseParamTypes.length];
    for (int i = 0; i < ajParamTypes.length; i++) {
      ajParamTypes[i] = AjTypeSystem.getAjType(baseParamTypes[i]);
    }
    return ajParamTypes;
  }
  
  public AjType getDeclaringType() {
    return declaringType;
  }
  
  public String[] getParameterNames() {
    return parameterNames;
  }
  
  private String[] splitOnComma(String s) {
    StringTokenizer strTok = new StringTokenizer(s, ",");
    String[] ret = new String[strTok.countTokens()];
    for (int i = 0; i < ret.length; i++) {
      ret[i] = strTok.nextToken().trim();
    }
    return ret;
  }
  
  public String toString() {
    StringBuffer sb = new StringBuffer();
    sb.append(getName());
    sb.append("(");
    AjType<?>[] ptypes = getParameterTypes();
    for (int i = 0; i < ptypes.length; i++) {
      sb.append(ptypes[i].getName());
      if ((parameterNames != null) && (parameterNames[i] != null)) {
        sb.append(" ");
        sb.append(parameterNames[i]);
      }
      if (i + 1 < ptypes.length) sb.append(",");
    }
    sb.append(") : ");
    sb.append(getPointcutExpression().asString());
    return sb.toString();
  }
}
