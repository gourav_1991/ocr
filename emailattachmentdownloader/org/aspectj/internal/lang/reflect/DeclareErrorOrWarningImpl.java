package org.aspectj.internal.lang.reflect;

import org.aspectj.lang.reflect.AjType;
import org.aspectj.lang.reflect.DeclareErrorOrWarning;
import org.aspectj.lang.reflect.PointcutExpression;














public class DeclareErrorOrWarningImpl
  implements DeclareErrorOrWarning
{
  private PointcutExpression pc;
  private String msg;
  private boolean isError;
  private AjType declaringType;
  
  public DeclareErrorOrWarningImpl(String pointcut, String message, boolean isError, AjType decType)
  {
    pc = new PointcutExpressionImpl(pointcut);
    msg = message;
    this.isError = isError;
    declaringType = decType;
  }
  
  public AjType getDeclaringType() { return declaringType; }
  


  public PointcutExpression getPointcutExpression()
  {
    return pc;
  }
  


  public String getMessage()
  {
    return msg;
  }
  


  public boolean isError()
  {
    return isError;
  }
  
  public String toString() {
    StringBuffer sb = new StringBuffer();
    sb.append("declare ");
    sb.append(isError() ? "error : " : "warning : ");
    sb.append(getPointcutExpression().asString());
    sb.append(" : ");
    sb.append("\"");
    sb.append(getMessage());
    sb.append("\"");
    return sb.toString();
  }
}
