package org.aspectj.internal.lang.reflect;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import org.aspectj.lang.reflect.AjType;
import org.aspectj.lang.reflect.AjTypeSystem;
import org.aspectj.lang.reflect.InterTypeMethodDeclaration;















public class InterTypeMethodDeclarationImpl
  extends InterTypeDeclarationImpl
  implements InterTypeMethodDeclaration
{
  private String name;
  private Method baseMethod;
  private int parameterAdjustmentFactor = 1;
  
  private AjType<?>[] parameterTypes;
  
  private Type[] genericParameterTypes;
  
  private AjType<?> returnType;
  
  private Type genericReturnType;
  
  private AjType<?>[] exceptionTypes;
  
  public InterTypeMethodDeclarationImpl(AjType<?> decType, String target, int mods, String name, Method itdInterMethod)
  {
    super(decType, target, mods);
    this.name = name;
    baseMethod = itdInterMethod;
  }
  
  public InterTypeMethodDeclarationImpl(AjType<?> decType, AjType<?> targetType, Method base, int modifiers) {
    super(decType, targetType, modifiers);
    parameterAdjustmentFactor = 0;
    name = base.getName();
    baseMethod = base;
  }
  


  public String getName()
  {
    return name;
  }
  


  public AjType<?> getReturnType()
  {
    return AjTypeSystem.getAjType(baseMethod.getReturnType());
  }
  


  public Type getGenericReturnType()
  {
    Type gRet = baseMethod.getGenericReturnType();
    if ((gRet instanceof Class)) {
      return AjTypeSystem.getAjType((Class)gRet);
    }
    return gRet;
  }
  


  public AjType<?>[] getParameterTypes()
  {
    Class<?>[] baseTypes = baseMethod.getParameterTypes();
    AjType<?>[] ret = new AjType[baseTypes.length - parameterAdjustmentFactor];
    for (int i = parameterAdjustmentFactor; i < baseTypes.length; i++) {
      ret[(i - parameterAdjustmentFactor)] = AjTypeSystem.getAjType(baseTypes[i]);
    }
    return ret;
  }
  


  public Type[] getGenericParameterTypes()
  {
    Type[] baseTypes = baseMethod.getGenericParameterTypes();
    Type[] ret = new AjType[baseTypes.length - parameterAdjustmentFactor];
    for (int i = parameterAdjustmentFactor; i < baseTypes.length; i++) {
      if ((baseTypes[i] instanceof Class)) {
        ret[(i - parameterAdjustmentFactor)] = AjTypeSystem.getAjType((Class)baseTypes[i]);
      } else {
        ret[(i - parameterAdjustmentFactor)] = baseTypes[i];
      }
    }
    return ret;
  }
  


  public TypeVariable<Method>[] getTypeParameters()
  {
    return baseMethod.getTypeParameters();
  }
  
  public AjType<?>[] getExceptionTypes() {
    Class<?>[] baseTypes = baseMethod.getExceptionTypes();
    AjType<?>[] ret = new AjType[baseTypes.length];
    for (int i = 0; i < baseTypes.length; i++) {
      ret[i] = AjTypeSystem.getAjType(baseTypes[i]);
    }
    return ret;
  }
  
  public String toString() {
    StringBuffer sb = new StringBuffer();
    sb.append(Modifier.toString(getModifiers()));
    sb.append(" ");
    sb.append(getReturnType().toString());
    sb.append(" ");
    sb.append(targetTypeName);
    sb.append(".");
    sb.append(getName());
    sb.append("(");
    AjType<?>[] pTypes = getParameterTypes();
    for (int i = 0; i < pTypes.length - 1; i++) {
      sb.append(pTypes[i].toString());
      sb.append(", ");
    }
    if (pTypes.length > 0) {
      sb.append(pTypes[(pTypes.length - 1)].toString());
    }
    sb.append(")");
    return sb.toString();
  }
}
