package org.aspectj.internal.lang.reflect;

import org.aspectj.lang.reflect.PerClauseKind;
import org.aspectj.lang.reflect.PointcutBasedPerClause;
import org.aspectj.lang.reflect.PointcutExpression;















public class PointcutBasedPerClauseImpl
  extends PerClauseImpl
  implements PointcutBasedPerClause
{
  private final PointcutExpression pointcutExpression;
  
  public PointcutBasedPerClauseImpl(PerClauseKind kind, String pointcutExpression)
  {
    super(kind);
    this.pointcutExpression = new PointcutExpressionImpl(pointcutExpression);
  }
  
  public PointcutExpression getPointcutExpression() {
    return pointcutExpression;
  }
  
  public String toString() {
    StringBuffer sb = new StringBuffer();
    switch (1.$SwitchMap$org$aspectj$lang$reflect$PerClauseKind[getKind().ordinal()]) {
    case 1:  sb.append("percflow("); break;
    case 2:  sb.append("percflowbelow("); break;
    case 3:  sb.append("pertarget("); break;
    case 4:  sb.append("perthis(");
    }
    sb.append(pointcutExpression.asString());
    sb.append(")");
    return sb.toString();
  }
}
