package org.aspectj.internal.lang.reflect;

import java.lang.reflect.Type;
import org.aspectj.lang.reflect.AjType;
import org.aspectj.lang.reflect.DeclareParents;
import org.aspectj.lang.reflect.TypePattern;
















public class DeclareParentsImpl
  implements DeclareParents
{
  private AjType<?> declaringType;
  private TypePattern targetTypesPattern;
  private Type[] parents;
  private String parentsString;
  private String firstMissingTypeName;
  private boolean isExtends;
  private boolean parentsError = false;
  




  public DeclareParentsImpl(String targets, String parentsAsString, boolean isExtends, AjType<?> declaring)
  {
    targetTypesPattern = new TypePatternImpl(targets);
    this.isExtends = isExtends;
    declaringType = declaring;
    parentsString = parentsAsString;
    try {
      parents = StringToType.commaSeparatedListToTypeArray(parentsAsString, declaring.getJavaClass());
    } catch (ClassNotFoundException cnfEx) {
      parentsError = true;
      firstMissingTypeName = cnfEx.getMessage();
    }
  }
  


  public AjType getDeclaringType()
  {
    return declaringType;
  }
  


  public TypePattern getTargetTypesPattern()
  {
    return targetTypesPattern;
  }
  


  public boolean isExtends()
  {
    return isExtends;
  }
  


  public boolean isImplements()
  {
    return !isExtends;
  }
  

  public Type[] getParentTypes()
    throws ClassNotFoundException
  {
    if (parentsError) {
      throw new ClassNotFoundException(firstMissingTypeName);
    }
    return parents;
  }
  
  public String toString() {
    StringBuffer sb = new StringBuffer();
    sb.append("declare parents : ");
    sb.append(getTargetTypesPattern().asString());
    sb.append(isExtends() ? " extends " : " implements ");
    sb.append(parentsString);
    return sb.toString();
  }
}
