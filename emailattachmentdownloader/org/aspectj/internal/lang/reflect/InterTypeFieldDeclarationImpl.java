package org.aspectj.internal.lang.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import org.aspectj.lang.reflect.AjType;
import org.aspectj.lang.reflect.AjTypeSystem;
import org.aspectj.lang.reflect.InterTypeFieldDeclaration;




















public class InterTypeFieldDeclarationImpl
  extends InterTypeDeclarationImpl
  implements InterTypeFieldDeclaration
{
  private String name;
  private AjType<?> type;
  private Type genericType;
  
  public InterTypeFieldDeclarationImpl(AjType<?> decType, String target, int mods, String name, AjType<?> type, Type genericType)
  {
    super(decType, target, mods);
    this.name = name;
    this.type = type;
    this.genericType = genericType;
  }
  
  public InterTypeFieldDeclarationImpl(AjType<?> decType, AjType<?> targetType, Field base) {
    super(decType, targetType, base.getModifiers());
    name = base.getName();
    type = AjTypeSystem.getAjType(base.getType());
    Type gt = base.getGenericType();
    if ((gt instanceof Class)) {
      genericType = AjTypeSystem.getAjType((Class)gt);
    } else {
      genericType = gt;
    }
  }
  


  public String getName()
  {
    return name;
  }
  


  public AjType<?> getType()
  {
    return type;
  }
  


  public Type getGenericType()
  {
    return genericType;
  }
  
  public String toString() {
    StringBuffer sb = new StringBuffer();
    sb.append(Modifier.toString(getModifiers()));
    sb.append(" ");
    sb.append(getType().toString());
    sb.append(" ");
    sb.append(targetTypeName);
    sb.append(".");
    sb.append(getName());
    return sb.toString();
  }
}
