package javax.activation;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Locale;















































public class MimeType
  implements Externalizable
{
  private String primaryType;
  private String subType;
  private MimeTypeParameterList parameters;
  private static final String TSPECIALS = "()<>@,;:/[]?=\\\"";
  
  public MimeType()
  {
    primaryType = "application";
    subType = "*";
    parameters = new MimeTypeParameterList();
  }
  



  public MimeType(String rawdata)
    throws MimeTypeParseException
  {
    parse(rawdata);
  }
  








  public MimeType(String primary, String sub)
    throws MimeTypeParseException
  {
    if (isValidToken(primary)) {
      primaryType = primary.toLowerCase(Locale.ENGLISH);
    } else {
      throw new MimeTypeParseException("Primary type is invalid.");
    }
    

    if (isValidToken(sub)) {
      subType = sub.toLowerCase(Locale.ENGLISH);
    } else {
      throw new MimeTypeParseException("Sub type is invalid.");
    }
    
    parameters = new MimeTypeParameterList();
  }
  

  private void parse(String rawdata)
    throws MimeTypeParseException
  {
    int slashIndex = rawdata.indexOf('/');
    int semIndex = rawdata.indexOf(';');
    if ((slashIndex < 0) && (semIndex < 0))
    {

      throw new MimeTypeParseException("Unable to find a sub type."); }
    if ((slashIndex < 0) && (semIndex >= 0))
    {

      throw new MimeTypeParseException("Unable to find a sub type."); }
    if ((slashIndex >= 0) && (semIndex < 0))
    {
      primaryType = rawdata.substring(0, slashIndex).trim().toLowerCase(Locale.ENGLISH);
      
      subType = rawdata.substring(slashIndex + 1).trim().toLowerCase(Locale.ENGLISH);
      
      parameters = new MimeTypeParameterList();
    } else if (slashIndex < semIndex)
    {
      primaryType = rawdata.substring(0, slashIndex).trim().toLowerCase(Locale.ENGLISH);
      
      subType = rawdata.substring(slashIndex + 1, semIndex).trim().toLowerCase(Locale.ENGLISH);
      
      parameters = new MimeTypeParameterList(rawdata.substring(semIndex));
    }
    else
    {
      throw new MimeTypeParseException("Unable to find a sub type.");
    }
    



    if (!isValidToken(primaryType)) {
      throw new MimeTypeParseException("Primary type is invalid.");
    }
    
    if (!isValidToken(subType)) {
      throw new MimeTypeParseException("Sub type is invalid.");
    }
  }
  



  public String getPrimaryType()
  {
    return primaryType;
  }
  






  public void setPrimaryType(String primary)
    throws MimeTypeParseException
  {
    if (!isValidToken(primaryType))
      throw new MimeTypeParseException("Primary type is invalid.");
    primaryType = primary.toLowerCase(Locale.ENGLISH);
  }
  




  public String getSubType()
  {
    return subType;
  }
  






  public void setSubType(String sub)
    throws MimeTypeParseException
  {
    if (!isValidToken(subType))
      throw new MimeTypeParseException("Sub type is invalid.");
    subType = sub.toLowerCase(Locale.ENGLISH);
  }
  




  public MimeTypeParameterList getParameters()
  {
    return parameters;
  }
  






  public String getParameter(String name)
  {
    return parameters.get(name);
  }
  






  public void setParameter(String name, String value)
  {
    parameters.set(name, value);
  }
  




  public void removeParameter(String name)
  {
    parameters.remove(name);
  }
  


  public String toString()
  {
    return getBaseType() + parameters.toString();
  }
  





  public String getBaseType()
  {
    return primaryType + "/" + subType;
  }
  






  public boolean match(MimeType type)
  {
    return (primaryType.equals(type.getPrimaryType())) && ((subType.equals("*")) || (type.getSubType().equals("*")) || (subType.equals(type.getSubType())));
  }
  








  public boolean match(String rawdata)
    throws MimeTypeParseException
  {
    return match(new MimeType(rawdata));
  }
  







  public void writeExternal(ObjectOutput out)
    throws IOException
  {
    out.writeUTF(toString());
    out.flush();
  }
  









  public void readExternal(ObjectInput in)
    throws IOException, ClassNotFoundException
  {
    try
    {
      parse(in.readUTF());
    } catch (MimeTypeParseException e) {
      throw new IOException(e.toString());
    }
  }
  




  private static boolean isTokenChar(char c)
  {
    return (c > ' ') && (c < '') && ("()<>@,;:/[]?=\\\"".indexOf(c) < 0);
  }
  


  private boolean isValidToken(String s)
  {
    int len = s.length();
    if (len > 0) {
      for (int i = 0; i < len; i++) {
        char c = s.charAt(i);
        if (!isTokenChar(c)) {
          return false;
        }
      }
      return true;
    }
    return false;
  }
}
