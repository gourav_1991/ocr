package javax.activation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;































































public class FileDataSource
  implements DataSource
{
  private File _file = null;
  private FileTypeMap typeMap = null;
  






  public FileDataSource(File file)
  {
    _file = file;
  }
  







  public FileDataSource(String name)
  {
    this(new File(name));
  }
  






  public InputStream getInputStream()
    throws IOException
  {
    return new FileInputStream(_file);
  }
  






  public OutputStream getOutputStream()
    throws IOException
  {
    return new FileOutputStream(_file);
  }
  











  public String getContentType()
  {
    if (typeMap == null) {
      return FileTypeMap.getDefaultFileTypeMap().getContentType(_file);
    }
    return typeMap.getContentType(_file);
  }
  






  public String getName()
  {
    return _file.getName();
  }
  



  public File getFile()
  {
    return _file;
  }
  




  public void setFileTypeMap(FileTypeMap map)
  {
    typeMap = map;
  }
}
