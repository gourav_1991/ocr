package javax.activation;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;


















































public class URLDataSource
  implements DataSource
{
  private URL url = null;
  private URLConnection url_conn = null;
  






  public URLDataSource(URL url)
  {
    this.url = url;
  }
  










  public String getContentType()
  {
    String type = null;
    try
    {
      if (url_conn == null) {
        url_conn = url.openConnection();
      }
    } catch (IOException e) {}
    if (url_conn != null) {
      type = url_conn.getContentType();
    }
    if (type == null) {
      type = "application/octet-stream";
    }
    return type;
  }
  





  public String getName()
  {
    return url.getFile();
  }
  




  public InputStream getInputStream()
    throws IOException
  {
    return url.openStream();
  }
  







  public OutputStream getOutputStream()
    throws IOException
  {
    url_conn = url.openConnection();
    
    if (url_conn != null) {
      url_conn.setDoOutput(true);
      return url_conn.getOutputStream();
    }
    return null;
  }
  




  public URL getURL()
  {
    return url;
  }
}
