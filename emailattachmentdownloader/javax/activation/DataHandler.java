package javax.activation;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.net.URL;















































































public class DataHandler
  implements Transferable
{
  private DataSource dataSource = null;
  private DataSource objDataSource = null;
  



  private Object object = null;
  private String objectMimeType = null;
  

  private CommandMap currentCommandMap = null;
  

  private static final DataFlavor[] emptyFlavors = new DataFlavor[0];
  private DataFlavor[] transferFlavors = emptyFlavors;
  

  private DataContentHandler dataContentHandler = null;
  private DataContentHandler factoryDCH = null;
  

  private static DataContentHandlerFactory factory = null;
  private DataContentHandlerFactory oldFactory = null;
  
  private String shortType = null;
  







  public DataHandler(DataSource ds)
  {
    dataSource = ds;
    oldFactory = factory;
  }
  








  public DataHandler(Object obj, String mimeType)
  {
    object = obj;
    objectMimeType = mimeType;
    oldFactory = factory;
  }
  






  public DataHandler(URL url)
  {
    dataSource = new URLDataSource(url);
    oldFactory = factory;
  }
  


  private synchronized CommandMap getCommandMap()
  {
    if (currentCommandMap != null) {
      return currentCommandMap;
    }
    return CommandMap.getDefaultCommandMap();
  }
  













  public DataSource getDataSource()
  {
    if (dataSource == null)
    {
      if (objDataSource == null)
        objDataSource = new DataHandlerDataSource(this);
      return objDataSource;
    }
    return dataSource;
  }
  







  public String getName()
  {
    if (dataSource != null) {
      return dataSource.getName();
    }
    return null;
  }
  






  public String getContentType()
  {
    if (dataSource != null) {
      return dataSource.getContentType();
    }
    return objectMimeType;
  }
  






















  public InputStream getInputStream()
    throws IOException
  {
    InputStream ins = null;
    
    if (dataSource != null) {
      ins = dataSource.getInputStream();
    } else {
      DataContentHandler dch = getDataContentHandler();
      
      if (dch == null) {
        throw new UnsupportedDataTypeException("no DCH for MIME type " + getBaseType());
      }
      
      if (((dch instanceof ObjectDataContentHandler)) && 
        (((ObjectDataContentHandler)dch).getDCH() == null)) {
        throw new UnsupportedDataTypeException("no object DCH for MIME type " + getBaseType());
      }
      

      final DataContentHandler fdch = dch;
      






      final PipedOutputStream pos = new PipedOutputStream();
      PipedInputStream pin = new PipedInputStream(pos);
      new Thread(new Runnable() {
        private final DataContentHandler val$fdch;
        private final PipedOutputStream val$pos;
        
        public void run() { try { fdch.writeTo(object, objectMimeType, pos); return;
          }
          catch (IOException e) {}finally
          {
            try {
              pos.close(); } catch (IOException ie) {} } } }, "DataHandler.getInputStream").start();
      




      ins = pin;
    }
    
    return ins;
  }
  














  public void writeTo(OutputStream os)
    throws IOException
  {
    if (dataSource != null) {
      InputStream is = null;
      byte[] data = new byte[' '];
      

      is = dataSource.getInputStream();
      try {
        int bytes_read;
        while ((bytes_read = is.read(data)) > 0) {
          os.write(data, 0, bytes_read);
        }
      } finally {
        is.close();
        is = null;
      }
    } else {
      DataContentHandler dch = getDataContentHandler();
      dch.writeTo(object, objectMimeType, os);
    }
  }
  










  public OutputStream getOutputStream()
    throws IOException
  {
    if (dataSource != null) {
      return dataSource.getOutputStream();
    }
    return null;
  }
  























  public synchronized DataFlavor[] getTransferDataFlavors()
  {
    if (factory != oldFactory) {
      transferFlavors = emptyFlavors;
    }
    
    if (transferFlavors == emptyFlavors)
      transferFlavors = getDataContentHandler().getTransferDataFlavors();
    return transferFlavors;
  }
  











  public boolean isDataFlavorSupported(DataFlavor flavor)
  {
    DataFlavor[] lFlavors = getTransferDataFlavors();
    
    for (int i = 0; i < lFlavors.length; i++) {
      if (lFlavors[i].equals(flavor))
        return true;
    }
    return false;
  }
  
































  public Object getTransferData(DataFlavor flavor)
    throws UnsupportedFlavorException, IOException
  {
    return getDataContentHandler().getTransferData(flavor, dataSource);
  }
  











  public synchronized void setCommandMap(CommandMap commandMap)
  {
    if ((commandMap != currentCommandMap) || (commandMap == null))
    {
      transferFlavors = emptyFlavors;
      dataContentHandler = null;
      
      currentCommandMap = commandMap;
    }
  }
  












  public CommandInfo[] getPreferredCommands()
  {
    if (dataSource != null) {
      return getCommandMap().getPreferredCommands(getBaseType(), dataSource);
    }
    
    return getCommandMap().getPreferredCommands(getBaseType());
  }
  











  public CommandInfo[] getAllCommands()
  {
    if (dataSource != null) {
      return getCommandMap().getAllCommands(getBaseType(), dataSource);
    }
    return getCommandMap().getAllCommands(getBaseType());
  }
  











  public CommandInfo getCommand(String cmdName)
  {
    if (dataSource != null) {
      return getCommandMap().getCommand(getBaseType(), cmdName, dataSource);
    }
    
    return getCommandMap().getCommand(getBaseType(), cmdName);
  }
  















  public Object getContent()
    throws IOException
  {
    if (object != null) {
      return object;
    }
    return getDataContentHandler().getContent(getDataSource());
  }
  











  public Object getBean(CommandInfo cmdinfo)
  {
    Object bean = null;
    
    try
    {
      ClassLoader cld = null;
      
      cld = SecuritySupport.getContextClassLoader();
      if (cld == null)
        cld = getClass().getClassLoader();
      bean = cmdinfo.getCommandObject(this, cld);
    }
    catch (IOException e) {}catch (ClassNotFoundException e) {}
    
    return bean;
  }
  


















  private synchronized DataContentHandler getDataContentHandler()
  {
    if (factory != oldFactory) {
      oldFactory = factory;
      factoryDCH = null;
      dataContentHandler = null;
      transferFlavors = emptyFlavors;
    }
    
    if (dataContentHandler != null) {
      return dataContentHandler;
    }
    String simpleMT = getBaseType();
    
    if ((factoryDCH == null) && (factory != null)) {
      factoryDCH = factory.createDataContentHandler(simpleMT);
    }
    if (factoryDCH != null) {
      dataContentHandler = factoryDCH;
    }
    if (dataContentHandler == null) {
      if (dataSource != null) {
        dataContentHandler = getCommandMap().createDataContentHandler(simpleMT, dataSource);
      }
      else {
        dataContentHandler = getCommandMap().createDataContentHandler(simpleMT);
      }
    }
    


    if (dataSource != null) {
      dataContentHandler = new DataSourceDataContentHandler(dataContentHandler, dataSource);
    }
    else
    {
      dataContentHandler = new ObjectDataContentHandler(dataContentHandler, object, objectMimeType);
    }
    

    return dataContentHandler;
  }
  



  private synchronized String getBaseType()
  {
    if (shortType == null) {
      String ct = getContentType();
      try {
        MimeType mt = new MimeType(ct);
        shortType = mt.getBaseType();
      } catch (MimeTypeParseException e) {
        shortType = ct;
      }
    }
    return shortType;
  }
  













  public static synchronized void setDataContentHandlerFactory(DataContentHandlerFactory newFactory)
  {
    if (factory != null) {
      throw new Error("DataContentHandlerFactory already defined");
    }
    SecurityManager security = System.getSecurityManager();
    if (security != null) {
      try
      {
        security.checkSetFactory();

      }
      catch (SecurityException ex)
      {
        if (DataHandler.class.getClassLoader() != newFactory.getClass().getClassLoader())
        {
          throw ex; }
      }
    }
    factory = newFactory;
  }
}
