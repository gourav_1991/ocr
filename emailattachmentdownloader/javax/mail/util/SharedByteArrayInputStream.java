package javax.mail.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import javax.mail.internet.SharedInputStream;

















































public class SharedByteArrayInputStream
  extends ByteArrayInputStream
  implements SharedInputStream
{
  protected int start = 0;
  





  public SharedByteArrayInputStream(byte[] buf)
  {
    super(buf);
  }
  








  public SharedByteArrayInputStream(byte[] buf, int offset, int length)
  {
    super(buf, offset, length);
    start = offset;
  }
  






  public long getPosition()
  {
    return pos - start;
  }
  












  public InputStream newStream(long start, long end)
  {
    if (start < 0L)
      throw new IllegalArgumentException("start < 0");
    if (end == -1L)
      end = count - this.start;
    return new SharedByteArrayInputStream(buf, this.start + (int)start, (int)(end - start));
  }
}
