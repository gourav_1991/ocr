package javax.mail.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.activation.DataSource;
import javax.mail.internet.ContentType;
import javax.mail.internet.MimeUtility;
import javax.mail.internet.ParseException;












































public class ByteArrayDataSource
  implements DataSource
{
  private byte[] data;
  private int len = -1;
  private String type;
  private String name = "";
  
  static class DSByteArrayOutputStream extends ByteArrayOutputStream { DSByteArrayOutputStream() {}
    
    public byte[] getBuf() { return buf; }
    
    public int getCount()
    {
      return count;
    }
  }
  








  public ByteArrayDataSource(InputStream is, String type)
    throws IOException
  {
    DSByteArrayOutputStream os = new DSByteArrayOutputStream();
    byte[] buf = new byte[' '];
    int len;
    while ((len = is.read(buf)) > 0)
      os.write(buf, 0, len);
    data = os.getBuf();
    this.len = os.getCount();
    







    if (data.length - this.len > 262144) {
      data = os.toByteArray();
      this.len = data.length;
    }
    this.type = type;
  }
  






  public ByteArrayDataSource(byte[] data, String type)
  {
    this.data = data;
    this.type = type;
  }
  










  public ByteArrayDataSource(String data, String type)
    throws IOException
  {
    String charset = null;
    try {
      ContentType ct = new ContentType(type);
      charset = ct.getParameter("charset");
    }
    catch (ParseException localParseException) {}
    
    charset = MimeUtility.javaCharset(charset);
    if (charset == null) {
      charset = MimeUtility.getDefaultJavaCharset();
    }
    this.data = data.getBytes(charset);
    this.type = type;
  }
  







  public InputStream getInputStream()
    throws IOException
  {
    if (data == null)
      throw new IOException("no data");
    if (len < 0)
      len = data.length;
    return new SharedByteArrayInputStream(data, 0, len);
  }
  






  public OutputStream getOutputStream()
    throws IOException
  {
    throw new IOException("cannot do this");
  }
  





  public String getContentType()
  {
    return type;
  }
  






  public String getName()
  {
    return name;
  }
  




  public void setName(String name)
  {
    this.name = name;
  }
}
