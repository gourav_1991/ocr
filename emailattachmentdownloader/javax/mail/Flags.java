package javax.mail;

import java.io.Serializable;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Set;
import java.util.Vector;


























































































public class Flags
  implements Cloneable, Serializable
{
  private int system_flags = 0;
  

  private Hashtable<String, String> user_flags = null;
  
  private static final int ANSWERED_BIT = 1;
  
  private static final int DELETED_BIT = 2;
  
  private static final int DRAFT_BIT = 4;
  
  private static final int FLAGGED_BIT = 8;
  
  private static final int RECENT_BIT = 16;
  
  private static final int SEEN_BIT = 32;
  
  private static final int USER_BIT = Integer.MIN_VALUE;
  
  private static final long serialVersionUID = 6243590407214169028L;
  public Flags() {}
  
  public static final class Flag
  {
    public static final Flag ANSWERED = new Flag(1);
    





    public static final Flag DELETED = new Flag(2);
    




    public static final Flag DRAFT = new Flag(4);
    




    public static final Flag FLAGGED = new Flag(8);
    







    public static final Flag RECENT = new Flag(16);
    









    public static final Flag SEEN = new Flag(32);
    









    public static final Flag USER = new Flag(Integer.MIN_VALUE);
    private int bit;
    
    private Flag(int bit)
    {
      this.bit = bit;
    }
  }
  











  public Flags(Flags flags)
  {
    system_flags = system_flags;
    if (user_flags != null) {
      user_flags = ((Hashtable)user_flags.clone());
    }
  }
  



  public Flags(Flag flag)
  {
    system_flags |= bit;
  }
  




  public Flags(String flag)
  {
    user_flags = new Hashtable(1);
    user_flags.put(flag.toLowerCase(Locale.ENGLISH), flag);
  }
  




  public void add(Flag flag)
  {
    system_flags |= bit;
  }
  




  public void add(String flag)
  {
    if (user_flags == null)
      user_flags = new Hashtable(1);
    user_flags.put(flag.toLowerCase(Locale.ENGLISH), flag);
  }
  





  public void add(Flags f)
  {
    system_flags |= system_flags;
    
    if (user_flags != null) {
      if (user_flags == null) {
        user_flags = new Hashtable(1);
      }
      Enumeration<String> e = user_flags.keys();
      
      while (e.hasMoreElements()) {
        String s = (String)e.nextElement();
        user_flags.put(s, user_flags.get(s));
      }
    }
  }
  




  public void remove(Flag flag)
  {
    system_flags &= (bit ^ 0xFFFFFFFF);
  }
  




  public void remove(String flag)
  {
    if (user_flags != null) {
      user_flags.remove(flag.toLowerCase(Locale.ENGLISH));
    }
  }
  




  public void remove(Flags f)
  {
    system_flags &= (system_flags ^ 0xFFFFFFFF);
    
    if (user_flags != null) {
      if (user_flags == null) {
        return;
      }
      Enumeration<String> e = user_flags.keys();
      while (e.hasMoreElements()) {
        user_flags.remove(e.nextElement());
      }
    }
  }
  








  public boolean retainAll(Flags f)
  {
    boolean changed = false;
    int sf = system_flags & system_flags;
    if (system_flags != sf) {
      system_flags = sf;
      changed = true;
    }
    


    if ((user_flags != null) && ((system_flags & 0x80000000) == 0)) {
      if (user_flags != null) {
        Enumeration<String> e = user_flags.keys();
        while (e.hasMoreElements()) {
          String key = (String)e.nextElement();
          if (!user_flags.containsKey(key)) {
            user_flags.remove(key);
            changed = true;
          }
        }
      }
      else {
        changed = user_flags.size() > 0;
        user_flags = null;
      }
    }
    return changed;
  }
  





  public boolean contains(Flag flag)
  {
    return (system_flags & bit) != 0;
  }
  





  public boolean contains(String flag)
  {
    if (user_flags == null) {
      return false;
    }
    return user_flags.containsKey(flag.toLowerCase(Locale.ENGLISH));
  }
  








  public boolean contains(Flags f)
  {
    if ((system_flags & system_flags) != system_flags) {
      return false;
    }
    
    if (user_flags != null) {
      if (user_flags == null)
        return false;
      Enumeration<String> e = user_flags.keys();
      
      while (e.hasMoreElements()) {
        if (!user_flags.containsKey(e.nextElement())) {
          return false;
        }
      }
    }
    
    return true;
  }
  





  public boolean equals(Object obj)
  {
    if (!(obj instanceof Flags)) {
      return false;
    }
    Flags f = (Flags)obj;
    

    if (system_flags != system_flags) {
      return false;
    }
    
    int size = user_flags == null ? 0 : user_flags.size();
    int fsize = user_flags == null ? 0 : user_flags.size();
    if ((size == 0) && (fsize == 0))
      return true;
    if ((user_flags != null) && (user_flags != null) && (fsize == size)) {
      return user_flags.keySet().equals(user_flags.keySet());
    }
    return false;
  }
  





  public int hashCode()
  {
    int hash = system_flags;
    if (user_flags != null) {
      Enumeration<String> e = user_flags.keys();
      while (e.hasMoreElements())
        hash += ((String)e.nextElement()).hashCode();
    }
    return hash;
  }
  





  public Flag[] getSystemFlags()
  {
    Vector<Flag> v = new Vector();
    if ((system_flags & 0x1) != 0)
      v.addElement(Flag.ANSWERED);
    if ((system_flags & 0x2) != 0)
      v.addElement(Flag.DELETED);
    if ((system_flags & 0x4) != 0)
      v.addElement(Flag.DRAFT);
    if ((system_flags & 0x8) != 0)
      v.addElement(Flag.FLAGGED);
    if ((system_flags & 0x10) != 0)
      v.addElement(Flag.RECENT);
    if ((system_flags & 0x20) != 0)
      v.addElement(Flag.SEEN);
    if ((system_flags & 0x80000000) != 0) {
      v.addElement(Flag.USER);
    }
    Flag[] f = new Flag[v.size()];
    v.copyInto(f);
    return f;
  }
  





  public String[] getUserFlags()
  {
    Vector<String> v = new Vector();
    if (user_flags != null) {
      Enumeration<String> e = user_flags.elements();
      
      while (e.hasMoreElements()) {
        v.addElement(e.nextElement());
      }
    }
    String[] f = new String[v.size()];
    v.copyInto(f);
    return f;
  }
  




  public void clearSystemFlags()
  {
    system_flags = 0;
  }
  




  public void clearUserFlags()
  {
    user_flags = null;
  }
  




  public Object clone()
  {
    Flags f = null;
    try {
      f = (Flags)super.clone();
    }
    catch (CloneNotSupportedException localCloneNotSupportedException) {}
    
    if (user_flags != null)
      user_flags = ((Hashtable)user_flags.clone());
    return f;
  }
  



  public String toString()
  {
    StringBuilder sb = new StringBuilder();
    
    if ((system_flags & 0x1) != 0)
      sb.append("\\Answered ");
    if ((system_flags & 0x2) != 0)
      sb.append("\\Deleted ");
    if ((system_flags & 0x4) != 0)
      sb.append("\\Draft ");
    if ((system_flags & 0x8) != 0)
      sb.append("\\Flagged ");
    if ((system_flags & 0x10) != 0)
      sb.append("\\Recent ");
    if ((system_flags & 0x20) != 0)
      sb.append("\\Seen ");
    if ((system_flags & 0x80000000) != 0) {
      sb.append("\\* ");
    }
    boolean first = true;
    if (user_flags != null) {
      Enumeration<String> e = user_flags.elements();
      
      while (e.hasMoreElements()) {
        if (first) {
          first = false;
        } else
          sb.append(' ');
        sb.append((String)e.nextElement());
      }
    }
    
    if ((first) && (sb.length() > 0)) {
      sb.setLength(sb.length() - 1);
    }
    return sb.toString();
  }
}
