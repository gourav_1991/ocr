package javax.mail;










public class Provider
{
  private Type type;
  








  private String protocol;
  







  private String className;
  







  private String vendor;
  







  private String version;
  








  public static class Type
  {
    public static final Type STORE = new Type("STORE");
    public static final Type TRANSPORT = new Type("TRANSPORT");
    private String type;
    
    private Type(String type)
    {
      this.type = type;
    }
    
    public String toString()
    {
      return type;
    }
  }
  














  public Provider(Type type, String protocol, String classname, String vendor, String version)
  {
    this.type = type;
    this.protocol = protocol;
    className = classname;
    this.vendor = vendor;
    this.version = version;
  }
  




  public Type getType()
  {
    return type;
  }
  




  public String getProtocol()
  {
    return protocol;
  }
  




  public String getClassName()
  {
    return className;
  }
  





  public String getVendor()
  {
    return vendor;
  }
  




  public String getVersion()
  {
    return version;
  }
  

  public String toString()
  {
    String s = "javax.mail.Provider[" + type + "," + protocol + "," + className;
    

    if (vendor != null) {
      s = s + "," + vendor;
    }
    if (version != null) {
      s = s + "," + version;
    }
    s = s + "]";
    return s;
  }
}
