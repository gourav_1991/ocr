package javax.mail.search;

















public abstract class StringTerm
  extends SearchTerm
{
  protected String pattern;
  















  protected boolean ignoreCase;
  















  private static final long serialVersionUID = 1274042129007696269L;
  















  protected StringTerm(String pattern)
  {
    this.pattern = pattern;
    ignoreCase = true;
  }
  





  protected StringTerm(String pattern, boolean ignoreCase)
  {
    this.pattern = pattern;
    this.ignoreCase = ignoreCase;
  }
  




  public String getPattern()
  {
    return pattern;
  }
  




  public boolean getIgnoreCase()
  {
    return ignoreCase;
  }
  
  protected boolean match(String s) {
    int len = s.length() - pattern.length();
    for (int i = 0; i <= len; i++) {
      if (s.regionMatches(ignoreCase, i, pattern, 0, pattern
        .length()))
        return true;
    }
    return false;
  }
  



  public boolean equals(Object obj)
  {
    if (!(obj instanceof StringTerm))
      return false;
    StringTerm st = (StringTerm)obj;
    if (ignoreCase) {
      return (pattern.equalsIgnoreCase(pattern)) && (ignoreCase == ignoreCase);
    }
    
    return (pattern.equals(pattern)) && (ignoreCase == ignoreCase);
  }
  




  public int hashCode()
  {
    return ignoreCase ? pattern.hashCode() : pattern.hashCode() ^ 0xFFFFFFFF;
  }
}
