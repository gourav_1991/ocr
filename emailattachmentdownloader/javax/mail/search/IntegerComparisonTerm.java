package javax.mail.search;


















public abstract class IntegerComparisonTerm
  extends ComparisonTerm
{
  protected int number;
  















  private static final long serialVersionUID = -6963571240154302484L;
  
















  protected IntegerComparisonTerm(int comparison, int number)
  {
    this.comparison = comparison;
    this.number = number;
  }
  




  public int getNumber()
  {
    return number;
  }
  




  public int getComparison()
  {
    return comparison;
  }
  
  protected boolean match(int i) {
    switch (comparison) {
    case 1: 
      return i <= number;
    case 2: 
      return i < number;
    case 3: 
      return i == number;
    case 4: 
      return i != number;
    case 5: 
      return i > number;
    case 6: 
      return i >= number;
    }
    return false;
  }
  




  public boolean equals(Object obj)
  {
    if (!(obj instanceof IntegerComparisonTerm))
      return false;
    IntegerComparisonTerm ict = (IntegerComparisonTerm)obj;
    return (number == number) && (super.equals(obj));
  }
  



  public int hashCode()
  {
    return number + super.hashCode();
  }
}
