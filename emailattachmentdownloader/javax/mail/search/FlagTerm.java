package javax.mail.search;

import javax.mail.Flags;
import javax.mail.Flags.Flag;
import javax.mail.Message;
import javax.mail.MessagingException;































































public final class FlagTerm
  extends SearchTerm
{
  private boolean set;
  private Flags flags;
  private static final long serialVersionUID = -142991500302030647L;
  
  public FlagTerm(Flags flags, boolean set)
  {
    this.flags = flags;
    this.set = set;
  }
  




  public Flags getFlags()
  {
    return (Flags)flags.clone();
  }
  




  public boolean getTestSet()
  {
    return set;
  }
  






  public boolean match(Message msg)
  {
    try
    {
      Flags f = msg.getFlags();
      if (set) {
        if (f.contains(flags)) {
          return true;
        }
        return false;
      }
      




      Flags.Flag[] sf = flags.getSystemFlags();
      

      for (int i = 0; i < sf.length; i++) {
        if (f.contains(sf[i]))
        {
          return false;
        }
      }
      String[] s = flags.getUserFlags();
      

      for (int i = 0; i < s.length; i++) {
        if (f.contains(s[i]))
        {
          return false;
        }
      }
      return true;
    }
    catch (MessagingException e) {
      return false;
    } catch (RuntimeException e) {}
    return false;
  }
  




  public boolean equals(Object obj)
  {
    if (!(obj instanceof FlagTerm))
      return false;
    FlagTerm ft = (FlagTerm)obj;
    return (set == set) && (flags.equals(flags));
  }
  



  public int hashCode()
  {
    return set ? flags.hashCode() : flags.hashCode() ^ 0xFFFFFFFF;
  }
}
