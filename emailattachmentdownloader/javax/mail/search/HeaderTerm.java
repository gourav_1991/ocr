package javax.mail.search;

import java.util.Locale;
import javax.mail.Message;

























































public final class HeaderTerm
  extends StringTerm
{
  private String headerName;
  private static final long serialVersionUID = 8342514650333389122L;
  
  public HeaderTerm(String headerName, String pattern)
  {
    super(pattern);
    this.headerName = headerName;
  }
  




  public String getHeaderName()
  {
    return headerName;
  }
  







  public boolean match(Message msg)
  {
    try
    {
      headers = msg.getHeader(headerName);
    } catch (Exception e) { String[] headers;
      return false;
    }
    String[] headers;
    if (headers == null) {
      return false;
    }
    for (int i = 0; i < headers.length; i++)
      if (super.match(headers[i]))
        return true;
    return false;
  }
  



  public boolean equals(Object obj)
  {
    if (!(obj instanceof HeaderTerm))
      return false;
    HeaderTerm ht = (HeaderTerm)obj;
    
    return (headerName.equalsIgnoreCase(headerName)) && (super.equals(ht));
  }
  




  public int hashCode()
  {
    return 
      headerName.toLowerCase(Locale.ENGLISH).hashCode() + super.hashCode();
  }
}
