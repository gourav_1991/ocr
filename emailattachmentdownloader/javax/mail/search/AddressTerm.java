package javax.mail.search;

import javax.mail.Address;



















































public abstract class AddressTerm
  extends SearchTerm
{
  protected Address address;
  private static final long serialVersionUID = 2005405551929769980L;
  
  protected AddressTerm(Address address)
  {
    this.address = address;
  }
  




  public Address getAddress()
  {
    return address;
  }
  





  protected boolean match(Address a)
  {
    return a.equals(address);
  }
  



  public boolean equals(Object obj)
  {
    if (!(obj instanceof AddressTerm))
      return false;
    AddressTerm at = (AddressTerm)obj;
    return address.equals(address);
  }
  



  public int hashCode()
  {
    return address.hashCode();
  }
}
