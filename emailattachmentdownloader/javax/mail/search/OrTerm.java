package javax.mail.search;

import javax.mail.Message;


























































public final class OrTerm
  extends SearchTerm
{
  private SearchTerm[] terms;
  private static final long serialVersionUID = 5380534067523646936L;
  
  public OrTerm(SearchTerm t1, SearchTerm t2)
  {
    terms = new SearchTerm[2];
    terms[0] = t1;
    terms[1] = t2;
  }
  




  public OrTerm(SearchTerm[] t)
  {
    terms = new SearchTerm[t.length];
    for (int i = 0; i < t.length; i++) {
      terms[i] = t[i];
    }
  }
  



  public SearchTerm[] getTerms()
  {
    return (SearchTerm[])terms.clone();
  }
  











  public boolean match(Message msg)
  {
    for (int i = 0; i < terms.length; i++)
      if (terms[i].match(msg))
        return true;
    return false;
  }
  



  public boolean equals(Object obj)
  {
    if (!(obj instanceof OrTerm))
      return false;
    OrTerm ot = (OrTerm)obj;
    if (terms.length != terms.length)
      return false;
    for (int i = 0; i < terms.length; i++)
      if (!terms[i].equals(terms[i]))
        return false;
    return true;
  }
  



  public int hashCode()
  {
    int hash = 0;
    for (int i = 0; i < terms.length; i++)
      hash += terms[i].hashCode();
    return hash;
  }
}
