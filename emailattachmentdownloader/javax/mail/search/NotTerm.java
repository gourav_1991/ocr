package javax.mail.search;

import javax.mail.Message;


















































public final class NotTerm
  extends SearchTerm
{
  private SearchTerm term;
  private static final long serialVersionUID = 7152293214217310216L;
  
  public NotTerm(SearchTerm t)
  {
    term = t;
  }
  




  public SearchTerm getTerm()
  {
    return term;
  }
  

  public boolean match(Message msg)
  {
    return !term.match(msg);
  }
  



  public boolean equals(Object obj)
  {
    if (!(obj instanceof NotTerm))
      return false;
    NotTerm nt = (NotTerm)obj;
    return term.equals(term);
  }
  



  public int hashCode()
  {
    return term.hashCode() << 1;
  }
}
