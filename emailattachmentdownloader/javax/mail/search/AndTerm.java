package javax.mail.search;

import javax.mail.Message;



























































public final class AndTerm
  extends SearchTerm
{
  private SearchTerm[] terms;
  private static final long serialVersionUID = -3583274505380989582L;
  
  public AndTerm(SearchTerm t1, SearchTerm t2)
  {
    terms = new SearchTerm[2];
    terms[0] = t1;
    terms[1] = t2;
  }
  




  public AndTerm(SearchTerm[] t)
  {
    terms = new SearchTerm[t.length];
    for (int i = 0; i < t.length; i++) {
      terms[i] = t[i];
    }
  }
  



  public SearchTerm[] getTerms()
  {
    return (SearchTerm[])terms.clone();
  }
  










  public boolean match(Message msg)
  {
    for (int i = 0; i < terms.length; i++)
      if (!terms[i].match(msg))
        return false;
    return true;
  }
  



  public boolean equals(Object obj)
  {
    if (!(obj instanceof AndTerm))
      return false;
    AndTerm at = (AndTerm)obj;
    if (terms.length != terms.length)
      return false;
    for (int i = 0; i < terms.length; i++)
      if (!terms[i].equals(terms[i]))
        return false;
    return true;
  }
  



  public int hashCode()
  {
    int hash = 0;
    for (int i = 0; i < terms.length; i++)
      hash += terms[i].hashCode();
    return hash;
  }
}
