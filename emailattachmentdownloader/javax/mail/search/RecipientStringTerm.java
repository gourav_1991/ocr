package javax.mail.search;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.Message.RecipientType;





























































public final class RecipientStringTerm
  extends AddressStringTerm
{
  private Message.RecipientType type;
  private static final long serialVersionUID = -8293562089611618849L;
  
  public RecipientStringTerm(Message.RecipientType type, String pattern)
  {
    super(pattern);
    this.type = type;
  }
  




  public Message.RecipientType getRecipientType()
  {
    return type;
  }
  









  public boolean match(Message msg)
  {
    try
    {
      recipients = msg.getRecipients(type);
    } catch (Exception e) { Address[] recipients;
      return false;
    }
    Address[] recipients;
    if (recipients == null) {
      return false;
    }
    for (int i = 0; i < recipients.length; i++)
      if (super.match(recipients[i]))
        return true;
    return false;
  }
  



  public boolean equals(Object obj)
  {
    if (!(obj instanceof RecipientStringTerm))
      return false;
    RecipientStringTerm rst = (RecipientStringTerm)obj;
    return (type.equals(type)) && (super.equals(obj));
  }
  



  public int hashCode()
  {
    return type.hashCode() + super.hashCode();
  }
}
