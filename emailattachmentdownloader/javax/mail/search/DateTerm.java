package javax.mail.search;

import java.util.Date;























































public abstract class DateTerm
  extends ComparisonTerm
{
  protected Date date;
  private static final long serialVersionUID = 4818873430063720043L;
  
  protected DateTerm(int comparison, Date date)
  {
    this.comparison = comparison;
    this.date = date;
  }
  




  public Date getDate()
  {
    return new Date(date.getTime());
  }
  




  public int getComparison()
  {
    return comparison;
  }
  





  protected boolean match(Date d)
  {
    switch (comparison) {
    case 1: 
      return (d.before(date)) || (d.equals(date));
    case 2: 
      return d.before(date);
    case 3: 
      return d.equals(date);
    case 4: 
      return !d.equals(date);
    case 5: 
      return d.after(date);
    case 6: 
      return (d.after(date)) || (d.equals(date));
    }
    return false;
  }
  




  public boolean equals(Object obj)
  {
    if (!(obj instanceof DateTerm))
      return false;
    DateTerm dt = (DateTerm)obj;
    return (date.equals(date)) && (super.equals(obj));
  }
  



  public int hashCode()
  {
    return date.hashCode() + super.hashCode();
  }
}
