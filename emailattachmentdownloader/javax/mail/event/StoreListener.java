package javax.mail.event;

import java.util.EventListener;

public abstract interface StoreListener
  extends EventListener
{
  public abstract void notification(StoreEvent paramStoreEvent);
}
