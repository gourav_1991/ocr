package javax.mail.event;

import java.util.EventListener;

public abstract interface MessageChangedListener
  extends EventListener
{
  public abstract void messageChanged(MessageChangedEvent paramMessageChangedEvent);
}
