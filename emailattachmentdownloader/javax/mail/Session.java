package javax.mail;

import com.sun.mail.util.LineInputStream;
import com.sun.mail.util.MailLogger;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.net.InetAddress;
import java.net.URL;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.concurrent.Executor;
import java.util.logging.Level;



































































































































































public final class Session
{
  private final Properties props;
  private final Authenticator authenticator;
  private final Hashtable<URLName, PasswordAuthentication> authTable = new Hashtable();
  
  private boolean debug = false;
  private PrintStream out;
  private MailLogger logger;
  private final Vector<Provider> providers = new Vector();
  private final Hashtable<String, Provider> providersByProtocol = new Hashtable();
  
  private final Hashtable<String, Provider> providersByClassName = new Hashtable();
  
  private final Properties addressMap = new Properties();
  

  private final EventQueue q;
  

  private static Session defaultSession = null;
  private static final String confDir;
  
  static
  {
    String dir = null;
    try {
      dir = (String)AccessController.doPrivileged(new PrivilegedAction()
      {
        public String run()
        {
          String home = System.getProperty("java.home");
          String newdir = home + File.separator + "conf";
          File conf = new File(newdir);
          if (conf.exists()) {
            return newdir + File.separator;
          }
          return home + File.separator + "lib" + File.separator;
        }
      });
    }
    catch (Exception localException) {}
    

    confDir = dir;
  }
  
  private Session(Properties props, Authenticator authenticator)
  {
    this.props = props;
    this.authenticator = authenticator;
    
    if (Boolean.valueOf(props.getProperty("mail.debug")).booleanValue()) {
      debug = true;
    }
    initLogger();
    logger.log(Level.CONFIG, "JavaMail version {0}", "1.6.0");
    
    Class<?> cl;
    Class<?> cl;
    if (authenticator != null) {
      cl = authenticator.getClass();
    } else {
      cl = getClass();
    }
    loadProviders(cl);
    loadAddressMap(cl);
    q = new EventQueue((Executor)props.get("mail.event.executor"));
  }
  
  private final synchronized void initLogger() {
    logger = new MailLogger(getClass(), "DEBUG", debug, getDebugOut());
  }
  
















  public static Session getInstance(Properties props, Authenticator authenticator)
  {
    return new Session(props, authenticator);
  }
  












  public static Session getInstance(Properties props)
  {
    return new Session(props, null);
  }
  
















































  public static synchronized Session getDefaultInstance(Properties props, Authenticator authenticator)
  {
    if (defaultSession == null) {
      SecurityManager security = System.getSecurityManager();
      if (security != null)
        security.checkSetFactory();
      defaultSession = new Session(props, authenticator);

    }
    else if (defaultSessionauthenticator != authenticator)
    {
      if ((defaultSessionauthenticator == null) || (authenticator == null) || 
      

        (defaultSessionauthenticator.getClass().getClassLoader() != authenticator.getClass().getClassLoader()))
      {


        throw new SecurityException("Access to default session denied");
      }
    }
    return defaultSession;
  }
  




















  public static Session getDefaultInstance(Properties props)
  {
    return getDefaultInstance(props, null);
  }
  














  public synchronized void setDebug(boolean debug)
  {
    this.debug = debug;
    initLogger();
    logger.log(Level.CONFIG, "setDebug: JavaMail version {0}", "1.6.0");
  }
  





  public synchronized boolean getDebug()
  {
    return debug;
  }
  









  public synchronized void setDebugOut(PrintStream out)
  {
    this.out = out;
    initLogger();
  }
  






  public synchronized PrintStream getDebugOut()
  {
    if (out == null) {
      return System.out;
    }
    return out;
  }
  






  public synchronized Provider[] getProviders()
  {
    Provider[] _providers = new Provider[providers.size()];
    providers.copyInto(_providers);
    return _providers;
  }
  














  public synchronized Provider getProvider(String protocol)
    throws NoSuchProviderException
  {
    if ((protocol == null) || (protocol.length() <= 0)) {
      throw new NoSuchProviderException("Invalid protocol: null");
    }
    
    Provider _provider = null;
    

    String _className = props.getProperty("mail." + protocol + ".class");
    if (_className != null) {
      if (logger.isLoggable(Level.FINE)) {
        logger.fine("mail." + protocol + ".class property exists and points to " + _className);
      }
      

      _provider = (Provider)providersByClassName.get(_className);
    }
    
    if (_provider != null) {
      return _provider;
    }
    
    _provider = (Provider)providersByProtocol.get(protocol);
    

    if (_provider == null) {
      throw new NoSuchProviderException("No provider for " + protocol);
    }
    if (logger.isLoggable(Level.FINE)) {
      logger.fine("getProvider() returning " + _provider.toString());
    }
    return _provider;
  }
  









  public synchronized void setProvider(Provider provider)
    throws NoSuchProviderException
  {
    if (provider == null) {
      throw new NoSuchProviderException("Can't set null provider");
    }
    providersByProtocol.put(provider.getProtocol(), provider);
    props.put("mail." + provider.getProtocol() + ".class", provider
      .getClassName());
  }
  









  public Store getStore()
    throws NoSuchProviderException
  {
    return getStore(getProperty("mail.store.protocol"));
  }
  








  public Store getStore(String protocol)
    throws NoSuchProviderException
  {
    return getStore(new URLName(protocol, null, -1, null, null, null));
  }
  













  public Store getStore(URLName url)
    throws NoSuchProviderException
  {
    String protocol = url.getProtocol();
    Provider p = getProvider(protocol);
    return getStore(p, url);
  }
  







  public Store getStore(Provider provider)
    throws NoSuchProviderException
  {
    return getStore(provider, null);
  }
  














  private Store getStore(Provider provider, URLName url)
    throws NoSuchProviderException
  {
    if ((provider == null) || (provider.getType() != Provider.Type.STORE)) {
      throw new NoSuchProviderException("invalid provider");
    }
    
    return (Store)getService(provider, url, Store.class);
  }
  
























  public Folder getFolder(URLName url)
    throws MessagingException
  {
    Store store = getStore(url);
    store.connect();
    return store.getFolder(url);
  }
  







  public Transport getTransport()
    throws NoSuchProviderException
  {
    String prot = getProperty("mail.transport.protocol");
    if (prot != null) {
      return getTransport(prot);
    }
    prot = (String)addressMap.get("rfc822");
    if (prot != null)
      return getTransport(prot);
    return getTransport("smtp");
  }
  









  public Transport getTransport(String protocol)
    throws NoSuchProviderException
  {
    return getTransport(new URLName(protocol, null, -1, null, null, null));
  }
  












  public Transport getTransport(URLName url)
    throws NoSuchProviderException
  {
    String protocol = url.getProtocol();
    Provider p = getProvider(protocol);
    return getTransport(p, url);
  }
  








  public Transport getTransport(Provider provider)
    throws NoSuchProviderException
  {
    return getTransport(provider, null);
  }
  












  public Transport getTransport(Address address)
    throws NoSuchProviderException
  {
    String transportProtocol = getProperty("mail.transport.protocol." + address.getType());
    if (transportProtocol != null)
      return getTransport(transportProtocol);
    transportProtocol = (String)addressMap.get(address.getType());
    if (transportProtocol != null) {
      return getTransport(transportProtocol);
    }
    throw new NoSuchProviderException("No provider for Address type: " + address.getType());
  }
  










  private Transport getTransport(Provider provider, URLName url)
    throws NoSuchProviderException
  {
    if ((provider == null) || (provider.getType() != Provider.Type.TRANSPORT)) {
      throw new NoSuchProviderException("invalid provider");
    }
    
    return (Transport)getService(provider, url, Transport.class);
  }
  














  private <T extends Service> T getService(Provider provider, URLName url, Class<T> type)
    throws NoSuchProviderException
  {
    if (provider == null) {
      throw new NoSuchProviderException("null");
    }
    

    if (url == null) {
      url = new URLName(provider.getProtocol(), null, -1, null, null, null);
    }
    

    Object service = null;
    
    ClassLoader cl;
    ClassLoader cl;
    if (authenticator != null) {
      cl = authenticator.getClass().getClassLoader();
    } else {
      cl = getClass().getClassLoader();
    }
    
    Class<?> serviceClass = null;
    try
    {
      ClassLoader ccl = getContextClassLoader();
      if (ccl != null) {
        try
        {
          serviceClass = Class.forName(provider.getClassName(), false, ccl);
        }
        catch (ClassNotFoundException localClassNotFoundException) {}
      }
      if ((serviceClass == null) || (!type.isAssignableFrom(serviceClass)))
      {
        serviceClass = Class.forName(provider.getClassName(), false, cl);
      }
      if (!type.isAssignableFrom(serviceClass))
      {
        throw new ClassCastException(type.getName() + " " + serviceClass.getName());
      }
    }
    catch (Exception ex1)
    {
      try {
        serviceClass = Class.forName(provider.getClassName());
        if (!type.isAssignableFrom(serviceClass))
        {
          throw new ClassCastException(type.getName() + " " + serviceClass.getName());
        }
      } catch (Exception ex) {
        logger.log(Level.FINE, "Exception loading provider", ex);
        throw new NoSuchProviderException(provider.getProtocol());
      }
    }
    
    try
    {
      Class<?>[] c = { Session.class, URLName.class };
      Constructor<?> cons = serviceClass.getConstructor(c);
      
      Object[] o = { this, url };
      service = cons.newInstance(o);
    }
    catch (Exception ex) {
      logger.log(Level.FINE, "Exception loading provider", ex);
      throw new NoSuchProviderException(provider.getProtocol());
    }
    
    return (Service)type.cast(service);
  }
  











  public void setPasswordAuthentication(URLName url, PasswordAuthentication pw)
  {
    if (pw == null) {
      authTable.remove(url);
    } else {
      authTable.put(url, pw);
    }
  }
  





  public PasswordAuthentication getPasswordAuthentication(URLName url)
  {
    return (PasswordAuthentication)authTable.get(url);
  }
  






















  public PasswordAuthentication requestPasswordAuthentication(InetAddress addr, int port, String protocol, String prompt, String defaultUserName)
  {
    if (authenticator != null) {
      return authenticator.requestPasswordAuthentication(addr, port, protocol, prompt, defaultUserName);
    }
    
    return null;
  }
  





  public Properties getProperties()
  {
    return props;
  }
  






  public String getProperty(String name)
  {
    return props.getProperty(name);
  }
  


  private void loadProviders(Class<?> cl)
  {
    StreamLoader loader = new StreamLoader()
    {
      public void load(InputStream is) throws IOException {
        Session.this.loadProvidersFromStream(is);
      }
    };
    

    try
    {
      if (confDir != null) {
        loadFile(confDir + "javamail.providers", loader);
      }
    }
    catch (SecurityException localSecurityException) {}
    loadAllResources("META-INF/javamail.providers", cl, loader);
    

    loadResource("/META-INF/javamail.default.providers", cl, loader, true);
    
    if (providers.size() == 0) {
      logger.config("failed to load any providers, using defaults");
      
      addProvider(new Provider(Provider.Type.STORE, "imap", "com.sun.mail.imap.IMAPStore", "Oracle", "1.6.0"));
      

      addProvider(new Provider(Provider.Type.STORE, "imaps", "com.sun.mail.imap.IMAPSSLStore", "Oracle", "1.6.0"));
      

      addProvider(new Provider(Provider.Type.STORE, "pop3", "com.sun.mail.pop3.POP3Store", "Oracle", "1.6.0"));
      

      addProvider(new Provider(Provider.Type.STORE, "pop3s", "com.sun.mail.pop3.POP3SSLStore", "Oracle", "1.6.0"));
      

      addProvider(new Provider(Provider.Type.TRANSPORT, "smtp", "com.sun.mail.smtp.SMTPTransport", "Oracle", "1.6.0"));
      

      addProvider(new Provider(Provider.Type.TRANSPORT, "smtps", "com.sun.mail.smtp.SMTPSSLTransport", "Oracle", "1.6.0"));
    }
    


    if (logger.isLoggable(Level.CONFIG))
    {
      logger.config("Tables of loaded providers");
      logger.config("Providers Listed By Class Name: " + providersByClassName
        .toString());
      logger.config("Providers Listed By Protocol: " + providersByProtocol
        .toString());
    }
  }
  
  private void loadProvidersFromStream(InputStream is) throws IOException
  {
    if (is != null) {
      LineInputStream lis = new LineInputStream(is);
      
      String currLine;
      
      while ((currLine = lis.readLine()) != null)
      {
        if ((!currLine.startsWith("#")) && 
        
          (currLine.trim().length() != 0))
        {
          Provider.Type type = null;
          String protocol = null;String className = null;
          String vendor = null;String version = null;
          

          StringTokenizer tuples = new StringTokenizer(currLine, ";");
          while (tuples.hasMoreTokens()) {
            String currTuple = tuples.nextToken().trim();
            

            int sep = currTuple.indexOf("=");
            if (currTuple.startsWith("protocol=")) {
              protocol = currTuple.substring(sep + 1);
            } else if (currTuple.startsWith("type=")) {
              String strType = currTuple.substring(sep + 1);
              if (strType.equalsIgnoreCase("store")) {
                type = Provider.Type.STORE;
              } else if (strType.equalsIgnoreCase("transport")) {
                type = Provider.Type.TRANSPORT;
              }
            } else if (currTuple.startsWith("class=")) {
              className = currTuple.substring(sep + 1);
            } else if (currTuple.startsWith("vendor=")) {
              vendor = currTuple.substring(sep + 1);
            } else if (currTuple.startsWith("version=")) {
              version = currTuple.substring(sep + 1);
            }
          }
          

          if ((type == null) || (protocol == null) || (className == null) || 
            (protocol.length() <= 0) || (className.length() <= 0))
          {
            logger.log(Level.CONFIG, "Bad provider entry: {0}", currLine);
          }
          else
          {
            Provider provider = new Provider(type, protocol, className, vendor, version);
            


            addProvider(provider);
          }
        }
      }
    }
  }
  



  public synchronized void addProvider(Provider provider)
  {
    providers.addElement(provider);
    providersByClassName.put(provider.getClassName(), provider);
    if (!providersByProtocol.containsKey(provider.getProtocol())) {
      providersByProtocol.put(provider.getProtocol(), provider);
    }
  }
  
  private void loadAddressMap(Class<?> cl)
  {
    StreamLoader loader = new StreamLoader()
    {
      public void load(InputStream is) throws IOException {
        addressMap.load(is);
      }
      

    };
    loadResource("/META-INF/javamail.default.address.map", cl, loader, true);
    

    loadAllResources("META-INF/javamail.address.map", cl, loader);
    

    try
    {
      if (confDir != null) {
        loadFile(confDir + "javamail.address.map", loader);
      }
    } catch (SecurityException localSecurityException) {}
    if (addressMap.isEmpty()) {
      logger.config("failed to load address map, using defaults");
      addressMap.put("rfc822", "smtp");
    }
  }
  











  public synchronized void setProtocolForAddress(String addresstype, String protocol)
  {
    if (protocol == null) {
      addressMap.remove(addresstype);
    } else {
      addressMap.put(addresstype, protocol);
    }
  }
  

  private void loadFile(String name, StreamLoader loader)
  {
    InputStream clis = null;
    try {
      clis = new BufferedInputStream(new FileInputStream(name));
      loader.load(clis);
      logger.log(Level.CONFIG, "successfully loaded file: {0}", name); return;
    }
    catch (FileNotFoundException localFileNotFoundException) {}catch (IOException e)
    {
      if (logger.isLoggable(Level.CONFIG))
        logger.log(Level.CONFIG, "not loading file: " + name, e);
    } catch (SecurityException sex) {
      if (logger.isLoggable(Level.CONFIG))
        logger.log(Level.CONFIG, "not loading file: " + name, sex);
    } finally {
      try {
        if (clis != null) {
          clis.close();
        }
      }
      catch (IOException localIOException5) {}
    }
  }
  

  private void loadResource(String name, Class<?> cl, StreamLoader loader, boolean expected)
  {
    InputStream clis = null;
    try {
      clis = getResourceAsStream(cl, name);
      if (clis != null) {
        loader.load(clis);
        logger.log(Level.CONFIG, "successfully loaded resource: {0}", name);

      }
      else if (expected) {
        logger.log(Level.WARNING, "expected resource not found: {0}", name);
      }
      return;
    } catch (IOException e) {
      logger.log(Level.CONFIG, "Exception loading resource", e);
    } catch (SecurityException sex) {
      logger.log(Level.CONFIG, "Exception loading resource", sex);
    } finally {
      try {
        if (clis != null) {
          clis.close();
        }
      }
      catch (IOException localIOException4) {}
    }
  }
  

  private void loadAllResources(String name, Class<?> cl, StreamLoader loader)
  {
    boolean anyLoaded = false;
    try
    {
      ClassLoader cld = null;
      
      cld = getContextClassLoader();
      if (cld == null)
        cld = cl.getClassLoader();
      URL[] urls; if (cld != null) {
        urls = getResources(cld, name);
      } else
        urls = getSystemResources(name);
      if (urls != null)
        for (i = 0; i < urls.length;) {
          URL url = urls[i];
          InputStream clis = null;
          logger.log(Level.CONFIG, "URL {0}", url);
          try {
            clis = openStream(url);
            if (clis != null) {
              loader.load(clis);
              anyLoaded = true;
              logger.log(Level.CONFIG, "successfully loaded resource: {0}", url);
            }
            else {
              logger.log(Level.CONFIG, "not loading resource: {0}", url);
            }
            








            try
            {
              if (clis != null) {
                clis.close();
              }
            }
            catch (IOException localIOException1) {}
            i++;







          }
          catch (FileNotFoundException localFileNotFoundException) {}catch (IOException ioex)
          {







            logger.log(Level.CONFIG, "Exception loading resource", ioex);
          }
          catch (SecurityException sex) {
            logger.log(Level.CONFIG, "Exception loading resource", sex);
          }
          finally {
            try {
              if (clis != null)
                clis.close();
            } catch (IOException localIOException5) {}
          }
        }
    } catch (Exception ex) { URL[] urls;
      int i;
      logger.log(Level.CONFIG, "Exception loading resource", ex);
    }
    

    if (!anyLoaded)
    {


      loadResource("/" + name, cl, loader, false);
    }
  }
  



  static ClassLoader getContextClassLoader()
  {
    (ClassLoader)AccessController.doPrivileged(new PrivilegedAction()
    {
      public ClassLoader run()
      {
        ClassLoader cl = null;
        try {
          cl = Thread.currentThread().getContextClassLoader();
        }
        catch (SecurityException localSecurityException) {}
        return cl;
      }
    });
  }
  
  private static InputStream getResourceAsStream(Class<?> c, final String name) throws IOException
  {
    try
    {
      (InputStream)AccessController.doPrivileged(new PrivilegedExceptionAction()
      {
        public InputStream run() throws IOException
        {
          try {
            return val$c.getResourceAsStream(name);
          }
          catch (RuntimeException e) {
            IOException ioex = new IOException("ClassLoader.getResourceAsStream failed");
            
            ioex.initCause(e);
            throw ioex;
          }
        }
      });
    }
    catch (PrivilegedActionException e) {
      throw ((IOException)e.getException());
    }
  }
  
  private static URL[] getResources(ClassLoader cl, final String name) {
    (URL[])AccessController.doPrivileged(new PrivilegedAction()
    {
      public URL[] run() {
        URL[] ret = null;
        try {
          List<URL> v = Collections.list(val$cl.getResources(name));
          if (!v.isEmpty()) {
            ret = new URL[v.size()];
            v.toArray(ret);
          }
        }
        catch (IOException localIOException) {}catch (SecurityException localSecurityException) {}
        return ret;
      }
    });
  }
  
  private static URL[] getSystemResources(String name) {
    (URL[])AccessController.doPrivileged(new PrivilegedAction()
    {
      public URL[] run() {
        URL[] ret = null;
        try {
          List<URL> v = Collections.list(
            ClassLoader.getSystemResources(val$name));
          if (!v.isEmpty()) {
            ret = new URL[v.size()];
            v.toArray(ret);
          }
        }
        catch (IOException localIOException) {}catch (SecurityException localSecurityException) {}
        return ret;
      }
    });
  }
  
  private static InputStream openStream(URL url) throws IOException {
    try {
      (InputStream)AccessController.doPrivileged(new PrivilegedExceptionAction()
      {
        public InputStream run() throws IOException
        {
          return val$url.openStream();
        }
      });
    }
    catch (PrivilegedActionException e) {
      throw ((IOException)e.getException());
    }
  }
  
  EventQueue getEventQueue() {
    return q;
  }
}
