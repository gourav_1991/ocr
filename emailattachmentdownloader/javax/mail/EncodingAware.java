package javax.mail;

public abstract interface EncodingAware
{
  public abstract String getEncoding();
}
