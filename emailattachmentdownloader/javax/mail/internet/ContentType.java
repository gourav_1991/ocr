package javax.mail.internet;














public class ContentType
{
  private String primaryType;
  












  private String subType;
  











  private ParameterList list;
  












  public ContentType() {}
  












  public ContentType(String primaryType, String subType, ParameterList list)
  {
    this.primaryType = primaryType;
    this.subType = subType;
    this.list = list;
  }
  






  public ContentType(String s)
    throws ParseException
  {
    HeaderTokenizer h = new HeaderTokenizer(s, "()<>@,;:\\\"\t []/?=");
    


    HeaderTokenizer.Token tk = h.next();
    if (tk.getType() != -1)
    {

      throw new ParseException("In Content-Type string <" + s + ">, expected MIME type, got " + tk.getValue()); }
    primaryType = tk.getValue();
    

    tk = h.next();
    if ((char)tk.getType() != '/')
    {
      throw new ParseException("In Content-Type string <" + s + ">, expected '/', got " + tk.getValue());
    }
    
    tk = h.next();
    if (tk.getType() != -1)
    {

      throw new ParseException("In Content-Type string <" + s + ">, expected MIME subtype, got " + tk.getValue()); }
    subType = tk.getValue();
    

    String rem = h.getRemainder();
    if (rem != null) {
      list = new ParameterList(rem);
    }
  }
  


  public String getPrimaryType()
  {
    return primaryType;
  }
  



  public String getSubType()
  {
    return subType;
  }
  






  public String getBaseType()
  {
    if ((primaryType == null) || (subType == null))
      return "";
    return primaryType + '/' + subType;
  }
  






  public String getParameter(String name)
  {
    if (list == null) {
      return null;
    }
    return list.get(name);
  }
  





  public ParameterList getParameterList()
  {
    return list;
  }
  



  public void setPrimaryType(String primaryType)
  {
    this.primaryType = primaryType;
  }
  



  public void setSubType(String subType)
  {
    this.subType = subType;
  }
  






  public void setParameter(String name, String value)
  {
    if (list == null) {
      list = new ParameterList();
    }
    list.set(name, value);
  }
  



  public void setParameterList(ParameterList list)
  {
    this.list = list;
  }
  







  public String toString()
  {
    if ((primaryType == null) || (subType == null)) {
      return "";
    }
    StringBuffer sb = new StringBuffer();
    sb.append(primaryType).append('/').append(subType);
    if (list != null)
    {


      sb.append(list.toString(sb.length() + 14));
    }
    return sb.toString();
  }
  



















  public boolean match(ContentType cType)
  {
    if (((primaryType != null) || (cType.getPrimaryType() != null)) && ((primaryType == null) || 
    
      (!primaryType.equalsIgnoreCase(cType.getPrimaryType())))) {
      return false;
    }
    String sType = cType.getSubType();
    

    if (((subType != null) && (subType.startsWith("*"))) || ((sType != null) && 
      (sType.startsWith("*")))) {
      return true;
    }
    
    return ((subType == null) && (sType == null)) || ((subType != null) && 
      (subType.equalsIgnoreCase(sType)));
  }
  

















  public boolean match(String s)
  {
    try
    {
      return match(new ContentType(s));
    } catch (ParseException pex) {}
    return false;
  }
}
