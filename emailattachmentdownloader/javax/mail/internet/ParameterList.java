package javax.mail.internet;

import com.sun.mail.util.ASCIIUtility;
import com.sun.mail.util.PropUtil;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;






































































public class ParameterList
{
  private Map<String, Object> list = new LinkedHashMap();
  















  private Set<String> multisegmentNames;
  














  private Map<String, Object> slist;
  














  private String lastName = null;
  

  private static final boolean encodeParameters = PropUtil.getBooleanSystemProperty("mail.mime.encodeparameters", true);
  
  private static final boolean decodeParameters = PropUtil.getBooleanSystemProperty("mail.mime.decodeparameters", true);
  
  private static final boolean decodeParametersStrict = PropUtil.getBooleanSystemProperty("mail.mime.decodeparameters.strict", false);
  

  private static final boolean applehack = PropUtil.getBooleanSystemProperty("mail.mime.applefilenames", false);
  
  private static final boolean windowshack = PropUtil.getBooleanSystemProperty("mail.mime.windowsfilenames", false);
  
  private static final boolean parametersStrict = PropUtil.getBooleanSystemProperty("mail.mime.parameters.strict", true);
  
  private static final boolean splitLongParameters = PropUtil.getBooleanSystemProperty("mail.mime.splitlongparameters", true);
  


  private static class Value
  {
    String value;
    

    String charset;
    

    String encodedValue;
    

    private Value() {}
  }
  

  private static class LiteralValue
  {
    String value;
    

    private LiteralValue() {}
  }
  

  private static class MultiValue
    extends ArrayList<Object>
  {
    private static final long serialVersionUID = 699561094618751023L;
    
    String value;
    

    private MultiValue() {}
  }
  

  private static class ParamEnum
    implements Enumeration<String>
  {
    private Iterator<String> it;
    

    ParamEnum(Iterator<String> it)
    {
      this.it = it;
    }
    
    public boolean hasMoreElements()
    {
      return it.hasNext();
    }
    
    public String nextElement()
    {
      return (String)it.next();
    }
  }
  



  public ParameterList()
  {
    if (decodeParameters) {
      multisegmentNames = new HashSet();
      slist = new HashMap();
    }
  }
  








  public ParameterList(String s)
    throws ParseException
  {
    this();
    
    HeaderTokenizer h = new HeaderTokenizer(s, "()<>@,;:\\\"\t []/?=");
    for (;;) {
      HeaderTokenizer.Token tk = h.next();
      int type = tk.getType();
      

      if (type == -4) {
        break;
      }
      if ((char)type == ';')
      {
        tk = h.next();
        
        if (tk.getType() == -4) {
          break;
        }
        if (tk.getType() != -1)
        {

          throw new ParseException("In parameter list <" + s + ">, expected parameter name, got \"" + tk.getValue() + "\""); }
        String name = tk.getValue().toLowerCase(Locale.ENGLISH);
        

        tk = h.next();
        if ((char)tk.getType() != '=')
        {

          throw new ParseException("In parameter list <" + s + ">, expected '=', got \"" + tk.getValue() + "\"");
        }
        
        if ((windowshack) && (
          (name.equals("name")) || (name.equals("filename")))) {
          tk = h.next(';', true);
        } else if (parametersStrict) {
          tk = h.next();
        } else
          tk = h.next(';');
        type = tk.getType();
        
        if ((type != -1) && (type != -2))
        {


          throw new ParseException("In parameter list <" + s + ">, expected parameter value, got \"" + tk.getValue() + "\"");
        }
        String value = tk.getValue();
        lastName = name;
        if (decodeParameters) {
          putEncodedName(name, value);
        } else {
          list.put(name, value);


        }
        


      }
      else if ((type == -1) && (lastName != null) && (((applehack) && (
      
        (lastName.equals("name")) || 
        (lastName.equals("filename")))) || (!parametersStrict)))
      {


        String lastValue = (String)list.get(lastName);
        String value = lastValue + " " + tk.getValue();
        list.put(lastName, value);
      }
      else
      {
        throw new ParseException("In parameter list <" + s + ">, expected ';', got \"" + tk.getValue() + "\"");
      }
    }
    

    if (decodeParameters)
    {



      combineMultisegmentNames(false);
    }
  }
  



















  public void combineSegments()
  {
    if ((decodeParameters) && (multisegmentNames.size() > 0)) {
      try {
        combineMultisegmentNames(true);
      }
      catch (ParseException localParseException) {}
    }
  }
  










  private void putEncodedName(String name, String value)
    throws ParseException
  {
    int star = name.indexOf('*');
    if (star < 0)
    {
      list.put(name, value);
    } else if (star == name.length() - 1)
    {
      name = name.substring(0, star);
      Value v = extractCharset(value);
      try {
        value = decodeBytes(value, charset);
      } catch (UnsupportedEncodingException ex) {
        if (decodeParametersStrict)
          throw new ParseException(ex.toString());
      }
      list.put(name, v);
    }
    else {
      String rname = name.substring(0, star);
      multisegmentNames.add(rname);
      list.put(rname, "");
      
      Object v;
      if (name.endsWith("*")) {
        Object v;
        if (name.endsWith("*0*")) {
          v = extractCharset(value);
        } else {
          Object v = new Value(null);
          encodedValue = value;
          value = value;
        }
        name = name.substring(0, name.length() - 1);
      }
      else {
        v = value;
      }
      slist.put(name, v);
    }
  }
  






  private void combineMultisegmentNames(boolean keepConsistentOnFailure)
    throws ParseException
  {
    boolean success = false;
    try {
      Iterator<String> it = multisegmentNames.iterator();
      while (it.hasNext()) {
        String name = (String)it.next();
        MultiValue mv = new MultiValue(null);
        



        String charset = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        
        for (int segment = 0;; segment++) {
          String sname = name + "*" + segment;
          Object v = slist.get(sname);
          if (v == null)
            break;
          mv.add(v);
          try {
            if ((v instanceof Value)) {
              Value vv = (Value)v;
              if (segment == 0)
              {

                charset = charset;
              }
              else if (charset == null)
              {
                multisegmentNames.remove(name);
                break;
              }
              
              decodeBytes(value, bos);
            } else {
              bos.write(ASCIIUtility.getBytes((String)v));
            }
          }
          catch (IOException localIOException) {}
          
          slist.remove(sname);
        }
        if (segment == 0)
        {
          list.remove(name);
        } else {
          try {
            if (charset != null)
              charset = MimeUtility.javaCharset(charset);
            if ((charset == null) || (charset.length() == 0))
              charset = MimeUtility.getDefaultJavaCharset();
            if (charset != null) {
              value = bos.toString(charset);
            } else
              value = bos.toString();
          } catch (UnsupportedEncodingException uex) {
            if (decodeParametersStrict) {
              throw new ParseException(uex.toString());
            }
            try {
              value = bos.toString("iso-8859-1");
            }
            catch (UnsupportedEncodingException localUnsupportedEncodingException1) {}
          }
          
          list.put(name, mv);
        }
      }
      success = true;
    }
    finally
    {
      Iterator<Object> sit;
      Object v;
      Value vv;
      if ((keepConsistentOnFailure) || (success))
      {

        if (slist.size() > 0)
        {
          Iterator<Object> sit = slist.values().iterator();
          while (sit.hasNext()) {
            Object v = sit.next();
            if ((v instanceof Value)) {
              Value vv = (Value)v;
              try
              {
                value = decodeBytes(value, charset);
              } catch (UnsupportedEncodingException ex) {
                if (decodeParametersStrict)
                  throw new ParseException(ex.toString());
              }
            }
          }
          list.putAll(slist);
        }
        

        multisegmentNames.clear();
        slist.clear();
      }
    }
  }
  




  public int size()
  {
    return list.size();
  }
  









  public String get(String name)
  {
    Object v = list.get(name.trim().toLowerCase(Locale.ENGLISH));
    String value; String value; if ((v instanceof MultiValue)) {
      value = value; } else { String value;
      if ((v instanceof LiteralValue)) {
        value = value; } else { String value;
        if ((v instanceof Value)) {
          value = value;
        } else
          value = (String)v; } }
    return value;
  }
  






  public void set(String name, String value)
  {
    name = name.trim().toLowerCase(Locale.ENGLISH);
    if (decodeParameters) {
      try {
        putEncodedName(name, value);
      }
      catch (ParseException pex) {
        list.put(name, value);
      }
    } else {
      list.put(name, value);
    }
  }
  










  public void set(String name, String value, String charset)
  {
    if (encodeParameters) {
      Value ev = encodeValue(value, charset);
      
      if (ev != null) {
        list.put(name.trim().toLowerCase(Locale.ENGLISH), ev);
      } else
        set(name, value);
    } else {
      set(name, value);
    }
  }
  






  void setLiteral(String name, String value)
  {
    LiteralValue lv = new LiteralValue(null);
    value = value;
    list.put(name, lv);
  }
  





  public void remove(String name)
  {
    list.remove(name.trim().toLowerCase(Locale.ENGLISH));
  }
  





  public Enumeration<String> getNames()
  {
    return new ParamEnum(list.keySet().iterator());
  }
  






  public String toString()
  {
    return toString(0);
  }
  













  public String toString(int used)
  {
    ToStringBuffer sb = new ToStringBuffer(used);
    Iterator<Map.Entry<String, Object>> e = list.entrySet().iterator();
    
    while (e.hasNext()) {
      Map.Entry<String, Object> ent = (Map.Entry)e.next();
      String name = (String)ent.getKey();
      
      Object v = ent.getValue();
      if ((v instanceof MultiValue)) {
        MultiValue vv = (MultiValue)v;
        name = name + "*";
        for (int i = 0; i < vv.size(); i++) {
          Object va = vv.get(i);
          String value;
          String ns; String value; if ((va instanceof Value)) {
            String ns = name + i + "*";
            value = encodedValue;
          } else {
            ns = name + i;
            value = (String)va;
          }
          sb.addNV(ns, quote(value));
        }
      } else if ((v instanceof LiteralValue)) {
        String value = value;
        sb.addNV(name, quote(value));
      } else if ((v instanceof Value))
      {



        name = name + "*";
        String value = encodedValue;
        sb.addNV(name, quote(value));
      } else {
        String value = (String)v;
        











        if ((value.length() > 60) && (splitLongParameters) && (encodeParameters))
        {
          int seg = 0;
          name = name + "*";
          while (value.length() > 60) {
            sb.addNV(name + seg, quote(value.substring(0, 60)));
            value = value.substring(60);
            seg++;
          }
          if (value.length() > 0)
            sb.addNV(name + seg, quote(value));
        } else {
          sb.addNV(name, quote(value));
        }
      }
    }
    return sb.toString();
  }
  


  private static class ToStringBuffer
  {
    private int used;
    

    private StringBuffer sb = new StringBuffer();
    
    public ToStringBuffer(int used) {
      this.used = used;
    }
    
    public void addNV(String name, String value) {
      sb.append("; ");
      used += 2;
      int len = name.length() + value.length() + 1;
      if (used + len > 76) {
        sb.append("\r\n\t");
        used = 8;
      }
      sb.append(name).append('=');
      used += name.length() + 1;
      if (used + value.length() > 76)
      {
        String s = MimeUtility.fold(used, value);
        sb.append(s);
        int lastlf = s.lastIndexOf('\n');
        if (lastlf >= 0) {
          used += s.length() - lastlf - 1;
        } else
          used += s.length();
      } else {
        sb.append(value);
        used += value.length();
      }
    }
    
    public String toString()
    {
      return sb.toString();
    }
  }
  
  private static String quote(String value)
  {
    return MimeUtility.quote(value, "()<>@,;:\\\"\t []/?=");
  }
  
  private static final char[] hex = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
  








  private static Value encodeValue(String value, String charset)
  {
    if (MimeUtility.checkAscii(value) == 1) {
      return null;
    }
    try
    {
      b = value.getBytes(MimeUtility.javaCharset(charset));
    } catch (UnsupportedEncodingException ex) { byte[] b;
      return null; }
    byte[] b;
    StringBuffer sb = new StringBuffer(b.length + charset.length() + 2);
    sb.append(charset).append("''");
    for (int i = 0; i < b.length; i++) {
      char c = (char)(b[i] & 0xFF);
      
      if ((c <= ' ') || (c >= '') || (c == '*') || (c == '\'') || (c == '%') || 
        ("()<>@,;:\\\"\t []/?=".indexOf(c) >= 0)) {
        sb.append('%').append(hex[(c >> '\004')]).append(hex[(c & 0xF)]);
      } else
        sb.append(c);
    }
    Value v = new Value(null);
    charset = charset;
    value = value;
    encodedValue = sb.toString();
    return v;
  }
  


  private static Value extractCharset(String value)
    throws ParseException
  {
    Value v = new Value(null);
    encodedValue = value;value = value;
    try {
      int i = value.indexOf('\'');
      if (i < 0) {
        if (decodeParametersStrict) {
          throw new ParseException("Missing charset in encoded value: " + value);
        }
        return v;
      }
      String charset = value.substring(0, i);
      int li = value.indexOf('\'', i + 1);
      if (li < 0) {
        if (decodeParametersStrict) {
          throw new ParseException("Missing language in encoded value: " + value);
        }
        return v;
      }
      
      value = value.substring(li + 1);
      charset = charset;
    } catch (NumberFormatException nex) {
      if (decodeParametersStrict)
        throw new ParseException(nex.toString());
    } catch (StringIndexOutOfBoundsException ex) {
      if (decodeParametersStrict)
        throw new ParseException(ex.toString());
    }
    return v;
  }
  










  private static String decodeBytes(String value, String charset)
    throws ParseException, UnsupportedEncodingException
  {
    byte[] b = new byte[value.length()];
    
    int i = 0; for (int bi = 0; i < value.length(); i++) {
      char c = value.charAt(i);
      if (c == '%') {
        try {
          String hex = value.substring(i + 1, i + 3);
          c = (char)Integer.parseInt(hex, 16);
          i += 2;
        } catch (NumberFormatException ex) {
          if (decodeParametersStrict)
            throw new ParseException(ex.toString());
        } catch (StringIndexOutOfBoundsException ex) {
          if (decodeParametersStrict)
            throw new ParseException(ex.toString());
        }
      }
      b[(bi++)] = ((byte)c);
    }
    if (charset != null)
      charset = MimeUtility.javaCharset(charset);
    if ((charset == null) || (charset.length() == 0))
      charset = MimeUtility.getDefaultJavaCharset();
    return new String(b, 0, bi, charset);
  }
  







  private static void decodeBytes(String value, OutputStream os)
    throws ParseException, IOException
  {
    for (int i = 0; i < value.length(); i++) {
      char c = value.charAt(i);
      if (c == '%') {
        try {
          String hex = value.substring(i + 1, i + 3);
          c = (char)Integer.parseInt(hex, 16);
          i += 2;
        } catch (NumberFormatException ex) {
          if (decodeParametersStrict)
            throw new ParseException(ex.toString());
        } catch (StringIndexOutOfBoundsException ex) {
          if (decodeParametersStrict)
            throw new ParseException(ex.toString());
        }
      }
      os.write((byte)c);
    }
  }
}
