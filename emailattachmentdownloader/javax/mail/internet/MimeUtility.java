package javax.mail.internet;

import com.sun.mail.util.ASCIIUtility;
import com.sun.mail.util.BASE64DecoderStream;
import com.sun.mail.util.BASE64EncoderStream;
import com.sun.mail.util.BEncoderStream;
import com.sun.mail.util.LineInputStream;
import com.sun.mail.util.QDecoderStream;
import com.sun.mail.util.QEncoderStream;
import com.sun.mail.util.QPDecoderStream;
import com.sun.mail.util.QPEncoderStream;
import com.sun.mail.util.UUDecoderStream;
import com.sun.mail.util.UUEncoderStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.EncodingAware;
import javax.mail.MessagingException;





















































































































































public class MimeUtility
{
  public static final int ALL = -1;
  private static final Map<String, Boolean> nonAsciiCharsetMap;
  private static final boolean decodeStrict;
  private static final boolean encodeEolStrict;
  private static final boolean ignoreUnknownEncoding;
  private static final boolean allowUtf8;
  private static final boolean foldEncodedWords;
  private static final boolean foldText;
  private static String defaultJavaCharset;
  private static String defaultMIMECharset;
  private static Map<String, String> mime2java;
  private static Map<String, String> java2mime;
  static final int ALL_ASCII = 1;
  static final int MOSTLY_ASCII = 2;
  static final int MOSTLY_NONASCII = 3;
  
  private MimeUtility() {}
  
  public static String getEncoding(DataSource ds)
  {
    ContentType cType = null;
    InputStream is = null;
    encoding = null;
    
    if ((ds instanceof EncodingAware)) {
      encoding = ((EncodingAware)ds).getEncoding();
      if (encoding != null)
        return encoding;
    }
    try {
      cType = new ContentType(ds.getContentType());
      is = ds.getInputStream();
      
      boolean isText = cType.match("text/*");
      
      int i = checkAscii(is, -1, !isText);
      switch (i) {
      case 1: 
        encoding = "7bit";
        break;
      case 2: 
        if ((isText) && (nonAsciiCharset(cType))) {
          encoding = "base64";
        } else
          encoding = "quoted-printable";
        break;
      }
      return "base64";

    }
    catch (Exception ex)
    {
      return "base64";
    }
    finally {
      try {
        if (is != null) {
          is.close();
        }
      }
      catch (IOException localIOException2) {}
    }
  }
  








  private static boolean nonAsciiCharset(ContentType ct)
  {
    String charset = ct.getParameter("charset");
    if (charset == null)
      return false;
    charset = charset.toLowerCase(Locale.ENGLISH);
    Boolean bool;
    synchronized (nonAsciiCharsetMap) {
      bool = (Boolean)nonAsciiCharsetMap.get(charset); }
    Boolean bool;
    if (bool == null) {
      try {
        byte[] b = "\r\n".getBytes(charset);
        bool = Boolean.valueOf((b.length != 2) || (b[0] != 13) || (b[1] != 10));
      }
      catch (UnsupportedEncodingException uex) {
        bool = Boolean.FALSE;
      } catch (RuntimeException ex) {
        bool = Boolean.TRUE;
      }
      synchronized (nonAsciiCharsetMap) {
        nonAsciiCharsetMap.put(charset, bool);
      }
    }
    return bool.booleanValue();
  }
  














  public static String getEncoding(DataHandler dh)
  {
    ContentType cType = null;
    String encoding = null;
    













    if (dh.getName() != null) {
      return getEncoding(dh.getDataSource());
    }
    try {
      cType = new ContentType(dh.getContentType());
    } catch (Exception ex) {
      return "base64";
    }
    
    if (cType.match("text/*"))
    {
      AsciiOutputStream aos = new AsciiOutputStream(false, false);
      try {
        dh.writeTo(aos);
      }
      catch (IOException localIOException) {}
      
      switch (aos.getAscii()) {
      case 1: 
        encoding = "7bit";
        break;
      case 2: 
        encoding = "quoted-printable";
        break;
      default: 
        encoding = "base64";
      }
      
    }
    else
    {
      AsciiOutputStream aos = new AsciiOutputStream(true, encodeEolStrict);
      try
      {
        dh.writeTo(aos);
      } catch (IOException localIOException1) {}
      if (aos.getAscii() == 1) {
        encoding = "7bit";
      } else {
        encoding = "base64";
      }
    }
    return encoding;
  }
  
















  public static InputStream decode(InputStream is, String encoding)
    throws MessagingException
  {
    if (encoding.equalsIgnoreCase("base64"))
      return new BASE64DecoderStream(is);
    if (encoding.equalsIgnoreCase("quoted-printable"))
      return new QPDecoderStream(is);
    if ((encoding.equalsIgnoreCase("uuencode")) || 
      (encoding.equalsIgnoreCase("x-uuencode")) || 
      (encoding.equalsIgnoreCase("x-uue")))
      return new UUDecoderStream(is);
    if ((encoding.equalsIgnoreCase("binary")) || 
      (encoding.equalsIgnoreCase("7bit")) || 
      (encoding.equalsIgnoreCase("8bit"))) {
      return is;
    }
    if (!ignoreUnknownEncoding)
      throw new MessagingException("Unknown encoding: " + encoding);
    return is;
  }
  












  public static OutputStream encode(OutputStream os, String encoding)
    throws MessagingException
  {
    if (encoding == null)
      return os;
    if (encoding.equalsIgnoreCase("base64"))
      return new BASE64EncoderStream(os);
    if (encoding.equalsIgnoreCase("quoted-printable"))
      return new QPEncoderStream(os);
    if ((encoding.equalsIgnoreCase("uuencode")) || 
      (encoding.equalsIgnoreCase("x-uuencode")) || 
      (encoding.equalsIgnoreCase("x-uue")))
      return new UUEncoderStream(os);
    if ((encoding.equalsIgnoreCase("binary")) || 
      (encoding.equalsIgnoreCase("7bit")) || 
      (encoding.equalsIgnoreCase("8bit"))) {
      return os;
    }
    throw new MessagingException("Unknown encoding: " + encoding);
  }
  

















  public static OutputStream encode(OutputStream os, String encoding, String filename)
    throws MessagingException
  {
    if (encoding == null)
      return os;
    if (encoding.equalsIgnoreCase("base64"))
      return new BASE64EncoderStream(os);
    if (encoding.equalsIgnoreCase("quoted-printable"))
      return new QPEncoderStream(os);
    if ((encoding.equalsIgnoreCase("uuencode")) || 
      (encoding.equalsIgnoreCase("x-uuencode")) || 
      (encoding.equalsIgnoreCase("x-uue")))
      return new UUEncoderStream(os, filename);
    if ((encoding.equalsIgnoreCase("binary")) || 
      (encoding.equalsIgnoreCase("7bit")) || 
      (encoding.equalsIgnoreCase("8bit"))) {
      return os;
    }
    throw new MessagingException("Unknown encoding: " + encoding);
  }
  



































  public static String encodeText(String text)
    throws UnsupportedEncodingException
  {
    return encodeText(text, null, null);
  }
  



























  public static String encodeText(String text, String charset, String encoding)
    throws UnsupportedEncodingException
  {
    return encodeWord(text, charset, encoding, false);
  }
  





































  public static String decodeText(String etext)
    throws UnsupportedEncodingException
  {
    String lwsp = " \t\n\r";
    










    if (etext.indexOf("=?") == -1) {
      return etext;
    }
    

    StringTokenizer st = new StringTokenizer(etext, lwsp, true);
    StringBuffer sb = new StringBuffer();
    StringBuffer wsb = new StringBuffer();
    boolean prevWasEncoded = false;
    
    while (st.hasMoreTokens())
    {
      String s = st.nextToken();
      char c;
      if (((c = s.charAt(0)) == ' ') || (c == '\t') || (c == '\r') || (c == '\n'))
      {
        wsb.append(c);
      }
      else {
        String word;
        try {
          String word = decodeWord(s);
          
          if ((!prevWasEncoded) && (wsb.length() > 0))
          {


            sb.append(wsb);
          }
          prevWasEncoded = true;
        }
        catch (ParseException pex) {
          word = s;
          
          if (!decodeStrict) {
            String dword = decodeInnerWords(word);
            if (dword != word)
            {

              if ((!prevWasEncoded) || (!word.startsWith("=?")))
              {



                if (wsb.length() > 0) {
                  sb.append(wsb);
                }
              }
              prevWasEncoded = word.endsWith("?=");
              word = dword;
            }
            else {
              if (wsb.length() > 0)
                sb.append(wsb);
              prevWasEncoded = false;
            }
          }
          else {
            if (wsb.length() > 0)
              sb.append(wsb);
            prevWasEncoded = false;
          }
        }
        sb.append(word);
        wsb.setLength(0);
      }
    }
    sb.append(wsb);
    return sb.toString();
  }
  




















  public static String encodeWord(String word)
    throws UnsupportedEncodingException
  {
    return encodeWord(word, null, null);
  }
  






















  public static String encodeWord(String word, String charset, String encoding)
    throws UnsupportedEncodingException
  {
    return encodeWord(word, charset, encoding, true);
  }
  










  private static String encodeWord(String string, String charset, String encoding, boolean encodingWord)
    throws UnsupportedEncodingException
  {
    int ascii = checkAscii(string);
    if (ascii == 1) {
      return string;
    }
    
    String jcharset;
    if (charset == null) {
      String jcharset = getDefaultJavaCharset();
      charset = getDefaultMIMECharset();
    } else {
      jcharset = javaCharset(charset);
    }
    
    if (encoding == null) {
      if (ascii != 3) {
        encoding = "Q";
      } else {
        encoding = "B";
      }
    }
    boolean b64;
    if (encoding.equalsIgnoreCase("B")) {
      b64 = true; } else { boolean b64;
      if (encoding.equalsIgnoreCase("Q")) {
        b64 = false;
      } else
        throw new UnsupportedEncodingException("Unknown transfer encoding: " + encoding);
    }
    boolean b64;
    StringBuffer outb = new StringBuffer();
    doEncode(string, b64, jcharset, 68 - charset
    


      .length(), "=?" + charset + "?" + encoding + "?", true, encodingWord, outb);
    


    return outb.toString();
  }
  




  private static void doEncode(String string, boolean b64, String jcharset, int avail, String prefix, boolean first, boolean encodingWord, StringBuffer buf)
    throws UnsupportedEncodingException
  {
    byte[] bytes = string.getBytes(jcharset);
    int len;
    int len; if (b64) {
      len = BEncoderStream.encodedLength(bytes);
    } else {
      len = QEncoderStream.encodedLength(bytes, encodingWord);
    }
    int size;
    if ((len > avail) && ((size = string.length()) > 1))
    {


      int split = size / 2;
      if (Character.isHighSurrogate(string.charAt(split - 1)))
        split--;
      if (split > 0) {
        doEncode(string.substring(0, split), b64, jcharset, avail, prefix, first, encodingWord, buf);
      }
      doEncode(string.substring(split, size), b64, jcharset, avail, prefix, false, encodingWord, buf);
    }
    else
    {
      ByteArrayOutputStream os = new ByteArrayOutputStream();
      OutputStream eos;
      OutputStream eos; if (b64) {
        eos = new BEncoderStream(os);
      } else {
        eos = new QEncoderStream(os, encodingWord);
      }
      try {
        eos.write(bytes);
        eos.close();
      }
      catch (IOException localIOException) {}
      byte[] encodedBytes = os.toByteArray();
      

      if (!first) {
        if (foldEncodedWords) {
          buf.append("\r\n ");
        } else
          buf.append(" ");
      }
      buf.append(prefix);
      for (int i = 0; i < encodedBytes.length; i++)
        buf.append((char)encodedBytes[i]);
      buf.append("?=");
    }
  }
  














  public static String decodeWord(String eword)
    throws ParseException, UnsupportedEncodingException
  {
    if (!eword.startsWith("=?")) {
      throw new ParseException("encoded word does not start with \"=?\": " + eword);
    }
    

    int start = 2;
    int pos; if ((pos = eword.indexOf('?', start)) == -1) {
      throw new ParseException("encoded word does not include charset: " + eword);
    }
    String charset = eword.substring(start, pos);
    int lpos = charset.indexOf('*');
    if (lpos >= 0)
      charset = charset.substring(0, lpos);
    charset = javaCharset(charset);
    

    start = pos + 1;
    if ((pos = eword.indexOf('?', start)) == -1) {
      throw new ParseException("encoded word does not include encoding: " + eword);
    }
    String encoding = eword.substring(start, pos);
    

    start = pos + 1;
    if ((pos = eword.indexOf("?=", start)) == -1) {
      throw new ParseException("encoded word does not end with \"?=\": " + eword);
    }
    






    String word = eword.substring(start, pos);
    try {
      String decodedWord;
      String decodedWord;
      if (word.length() > 0)
      {

        ByteArrayInputStream bis = new ByteArrayInputStream(ASCIIUtility.getBytes(word));
        
        InputStream is;
        
        if (encoding.equalsIgnoreCase("B")) {
          is = new BASE64DecoderStream(bis); } else { InputStream is;
          if (encoding.equalsIgnoreCase("Q")) {
            is = new QDecoderStream(bis);
          } else {
            throw new UnsupportedEncodingException("unknown encoding: " + encoding);
          }
        }
        

        InputStream is;
        

        int count = bis.available();
        byte[] bytes = new byte[count];
        
        count = is.read(bytes, 0, count);
        


        decodedWord = count <= 0 ? "" : new String(bytes, 0, count, charset);
      }
      else
      {
        decodedWord = ""; }
      String rest;
      if (pos + 2 < eword.length())
      {
        rest = eword.substring(pos + 2);
        if (!decodeStrict)
          rest = decodeInnerWords(rest); }
      return decodedWord + rest;

    }
    catch (UnsupportedEncodingException uex)
    {

      throw uex;
    }
    catch (IOException ioex) {
      throw new ParseException(ioex.toString());



    }
    catch (IllegalArgumentException iex)
    {


      throw new UnsupportedEncodingException(charset);
    }
  }
  




  private static String decodeInnerWords(String word)
    throws UnsupportedEncodingException
  {
    int start = 0;
    StringBuffer buf = new StringBuffer();
    int i; while ((i = word.indexOf("=?", start)) >= 0) {
      buf.append(word.substring(start, i));
      
      int end = word.indexOf('?', i + 2);
      if (end < 0) {
        break;
      }
      end = word.indexOf('?', end + 1);
      if (end < 0) {
        break;
      }
      end = word.indexOf("?=", end + 1);
      if (end < 0)
        break;
      String s = word.substring(i, end + 2);
      try {
        s = decodeWord(s);
      }
      catch (ParseException localParseException) {}
      
      buf.append(s);
      start = end + 2;
    }
    if (start == 0)
      return word;
    if (start < word.length())
      buf.append(word.substring(start));
    return buf.toString();
  }
  















  public static String quote(String word, String specials)
  {
    int len = word == null ? 0 : word.length();
    if (len == 0) {
      return "\"\"";
    }
    



    boolean needQuoting = false;
    for (int i = 0; i < len; i++) {
      char c = word.charAt(i);
      if ((c == '"') || (c == '\\') || (c == '\r') || (c == '\n'))
      {
        StringBuffer sb = new StringBuffer(len + 3);
        sb.append('"');
        sb.append(word.substring(0, i));
        int lastc = 0;
        for (int j = i; j < len; j++) {
          char cc = word.charAt(j);
          if ((cc == '"') || (cc == '\\') || (cc == '\r') || (cc == '\n'))
          {
            if ((cc != '\n') || (lastc != 13))
            {

              sb.append('\\'); } }
          sb.append(cc);
          lastc = cc;
        }
        sb.append('"');
        return sb.toString(); }
      if ((c < ' ') || ((c >= '') && (!allowUtf8)) || 
        (specials.indexOf(c) >= 0))
      {
        needQuoting = true;
      }
    }
    if (needQuoting) {
      StringBuffer sb = new StringBuffer(len + 2);
      sb.append('"').append(word).append('"');
      return sb.toString();
    }
    return word;
  }
  















  public static String fold(int used, String s)
  {
    if (!foldText) {
      return s;
    }
    


    for (int end = s.length() - 1; end >= 0; end--) {
      char c = s.charAt(end);
      if ((c != ' ') && (c != '\t') && (c != '\r') && (c != '\n'))
        break;
    }
    if (end != s.length() - 1) {
      s = s.substring(0, end + 1);
    }
    
    if (used + s.length() <= 76) {
      return makesafe(s);
    }
    
    StringBuilder sb = new StringBuilder(s.length() + 4);
    char lastc = '\000';
    while (used + s.length() > 76) {
      int lastspace = -1;
      for (int i = 0; i < s.length(); i++) {
        if ((lastspace != -1) && (used + i > 76))
          break;
        char c = s.charAt(i);
        if (((c == ' ') || (c == '\t')) && 
          (lastc != ' ') && (lastc != '\t'))
          lastspace = i;
        lastc = c;
      }
      if (lastspace == -1)
      {
        sb.append(s);
        s = "";
        used = 0;
        break;
      }
      sb.append(s.substring(0, lastspace));
      sb.append("\r\n");
      lastc = s.charAt(lastspace);
      sb.append(lastc);
      s = s.substring(lastspace + 1);
      used = 1;
    }
    sb.append(s);
    return makesafe(sb);
  }
  





  private static String makesafe(CharSequence s)
  {
    for (int i = 0; i < s.length(); i++) {
      char c = s.charAt(i);
      if ((c == '\r') || (c == '\n'))
        break;
    }
    if (i == s.length()) {
      return s.toString();
    }
    

    StringBuilder sb = new StringBuilder(s.length() + 1);
    BufferedReader r = new BufferedReader(new StringReader(s.toString()));
    try {
      String line;
      while ((line = r.readLine()) != null) {
        if (line.trim().length() != 0)
        {
          if (sb.length() > 0) {
            sb.append("\r\n");
            assert (line.length() > 0);
            char c = line.charAt(0);
            if ((c != ' ') && (c != '\t'))
              sb.append(' ');
          }
          sb.append(line);
        }
      }
    } catch (IOException ex) {
      return s.toString(); }
    String line;
    return sb.toString();
  }
  







  public static String unfold(String s)
  {
    if (!foldText) {
      return s;
    }
    StringBuffer sb = null;
    int i;
    while ((i = indexOfAny(s, "\r\n")) >= 0) {
      int start = i;
      int slen = s.length();
      i++;
      if ((i < slen) && (s.charAt(i - 1) == '\r') && (s.charAt(i) == '\n'))
        i++;
      if ((start > 0) && (s.charAt(start - 1) == '\\'))
      {

        if (sb == null)
          sb = new StringBuffer(s.length());
        sb.append(s.substring(0, start - 1));
        sb.append(s.substring(start, i));
        s = s.substring(i);
      }
      else
      {
        char c;
        
        if ((i >= slen) || ((c = s.charAt(i)) == ' ') || (c == '\t')) {
          if (sb == null)
            sb = new StringBuffer(s.length());
          sb.append(s.substring(0, start));
          s = s.substring(i);
        } else {
          char c;
          if (sb == null)
            sb = new StringBuffer(s.length());
          sb.append(s.substring(0, i));
          s = s.substring(i);
        }
      }
    }
    if (sb != null) {
      sb.append(s);
      return sb.toString();
    }
    return s;
  }
  





  private static int indexOfAny(String s, String any)
  {
    return indexOfAny(s, any, 0);
  }
  
  private static int indexOfAny(String s, String any, int start) {
    try {
      int len = s.length();
      for (int i = start; i < len; i++) {
        if (any.indexOf(s.charAt(i)) >= 0)
          return i;
      }
      return -1;
    } catch (StringIndexOutOfBoundsException e) {}
    return -1;
  }
  







  public static String javaCharset(String charset)
  {
    if ((mime2java == null) || (charset == null))
    {
      return charset;
    }
    String alias = (String)mime2java.get(charset.toLowerCase(Locale.ENGLISH));
    if (alias != null) {
      try
      {
        Charset.forName(alias);
      } catch (Exception ex) {
        alias = null;
      }
    }
    return alias == null ? charset : alias;
  }
  












  public static String mimeCharset(String charset)
  {
    if ((java2mime == null) || (charset == null))
    {
      return charset;
    }
    String alias = (String)java2mime.get(charset.toLowerCase(Locale.ENGLISH));
    return alias == null ? charset : alias;
  }
  












  public static String getDefaultJavaCharset()
  {
    if (defaultJavaCharset == null)
    {



      String mimecs = null;
      try {
        mimecs = System.getProperty("mail.mime.charset");
      } catch (SecurityException localSecurityException1) {}
      if ((mimecs != null) && (mimecs.length() > 0)) {
        defaultJavaCharset = javaCharset(mimecs);
        return defaultJavaCharset;
      }
      try
      {
        defaultJavaCharset = System.getProperty("file.encoding", "8859_1");



      }
      catch (SecurityException sex)
      {



        InputStreamReader reader = new InputStreamReader(new InputStream()
        {
          public int read()
          {
            return 0;
          }
          

        });
        defaultJavaCharset = reader.getEncoding();
        if (defaultJavaCharset == null) {
          defaultJavaCharset = "8859_1";
        }
      }
    }
    return defaultJavaCharset;
  }
  


  static String getDefaultMIMECharset()
  {
    if (defaultMIMECharset == null) {
      try {
        defaultMIMECharset = System.getProperty("mail.mime.charset");
      } catch (SecurityException localSecurityException) {}
    }
    if (defaultMIMECharset == null)
      defaultMIMECharset = mimeCharset(getDefaultJavaCharset());
    return defaultMIMECharset;
  }
  






































































































  private static void loadMappings(LineInputStream is, Map<String, String> table)
  {
    for (;;)
    {
      try
      {
        currLine = is.readLine();
      } catch (IOException ioex) { String currLine;
        break;
      }
      String currLine;
      if (currLine == null)
        break;
      if ((currLine.startsWith("--")) && (currLine.endsWith("--"))) {
        break;
      }
      

      if ((currLine.trim().length() != 0) && (!currLine.startsWith("#")))
      {



        StringTokenizer tk = new StringTokenizer(currLine, " \t");
        try {
          String key = tk.nextToken();
          String value = tk.nextToken();
          table.put(key.toLowerCase(Locale.ENGLISH), value);
        }
        catch (NoSuchElementException localNoSuchElementException) {}
      }
    }
  }
  









  static int checkAscii(String s)
  {
    int ascii = 0;int non_ascii = 0;
    int l = s.length();
    
    for (int i = 0; i < l; i++) {
      if (nonascii(s.charAt(i))) {
        non_ascii++;
      } else {
        ascii++;
      }
    }
    if (non_ascii == 0)
      return 1;
    if (ascii > non_ascii) {
      return 2;
    }
    return 3;
  }
  









  static int checkAscii(byte[] b)
  {
    int ascii = 0;int non_ascii = 0;
    
    for (int i = 0; i < b.length; i++)
    {


      if (nonascii(b[i] & 0xFF)) {
        non_ascii++;
      } else {
        ascii++;
      }
    }
    if (non_ascii == 0)
      return 1;
    if (ascii > non_ascii) {
      return 2;
    }
    return 3;
  }
  




















  static int checkAscii(InputStream is, int max, boolean breakOnNonAscii)
  {
    int ascii = 0;int non_ascii = 0;
    
    int block = 4096;
    int linelen = 0;
    boolean longLine = false;boolean badEOL = false;
    boolean checkEOL = (encodeEolStrict) && (breakOnNonAscii);
    byte[] buf = null;
    if (max != 0) {
      block = max == -1 ? 4096 : Math.min(max, 4096);
      buf = new byte[block];
    }
    while (max != 0) {
      try { int len;
        if ((len = is.read(buf, 0, block)) == -1)
          break;
        int lastb = 0;
        for (int i = 0; i < len; i++)
        {



          int b = buf[i] & 0xFF;
          if ((checkEOL) && (((lastb == 13) && (b != 10)) || ((lastb != 13) && (b == 10))))
          {

            badEOL = true; }
          if ((b == 13) || (b == 10)) {
            linelen = 0;
          } else {
            linelen++;
            if (linelen > 998)
              longLine = true;
          }
          if (nonascii(b)) {
            if (breakOnNonAscii) {
              return 3;
            }
            non_ascii++;
          } else {
            ascii++; }
          lastb = b;
        }
      } catch (IOException ioex) { break;
      }
      int len;
      if (max != -1) {
        max -= len;
      }
    }
    if ((max == 0) && (breakOnNonAscii))
    {





      return 3;
    }
    if (non_ascii == 0)
    {



      if (badEOL) {
        return 3;
      }
      if (longLine) {
        return 2;
      }
      return 1;
    }
    if (ascii > non_ascii)
      return 2;
    return 3;
  }
  
  static final boolean nonascii(int b) {
    return (b >= 127) || ((b < 32) && (b != 13) && (b != 10) && (b != 9));
  }
  
  /* Error */
  static
  {
    // Byte code:
    //   0: ldc 10
    //   2: invokevirtual 194	java/lang/Class:desiredAssertionStatus	()Z
    //   5: ifne +7 -> 12
    //   8: iconst_1
    //   9: goto +4 -> 13
    //   12: iconst_0
    //   13: putstatic 163	javax/mail/internet/MimeUtility:$assertionsDisabled	Z
    //   16: new 195	java/util/HashMap
    //   19: dup
    //   20: invokespecial 196	java/util/HashMap:<init>	()V
    //   23: putstatic 23	javax/mail/internet/MimeUtility:nonAsciiCharsetMap	Ljava/util/Map;
    //   26: ldc -59
    //   28: iconst_1
    //   29: invokestatic 198	com/sun/mail/util/PropUtil:getBooleanSystemProperty	(Ljava/lang/String;Z)Z
    //   32: putstatic 88	javax/mail/internet/MimeUtility:decodeStrict	Z
    //   35: ldc -57
    //   37: iconst_0
    //   38: invokestatic 198	com/sun/mail/util/PropUtil:getBooleanSystemProperty	(Ljava/lang/String;Z)Z
    //   41: putstatic 43	javax/mail/internet/MimeUtility:encodeEolStrict	Z
    //   44: ldc -56
    //   46: iconst_0
    //   47: invokestatic 198	com/sun/mail/util/PropUtil:getBooleanSystemProperty	(Ljava/lang/String;Z)Z
    //   50: putstatic 56	javax/mail/internet/MimeUtility:ignoreUnknownEncoding	Z
    //   53: ldc -55
    //   55: iconst_0
    //   56: invokestatic 198	com/sun/mail/util/PropUtil:getBooleanSystemProperty	(Ljava/lang/String;Z)Z
    //   59: putstatic 148	javax/mail/internet/MimeUtility:allowUtf8	Z
    //   62: ldc -54
    //   64: iconst_0
    //   65: invokestatic 198	com/sun/mail/util/PropUtil:getBooleanSystemProperty	(Ljava/lang/String;Z)Z
    //   68: putstatic 121	javax/mail/internet/MimeUtility:foldEncodedWords	Z
    //   71: ldc -53
    //   73: iconst_1
    //   74: invokestatic 198	com/sun/mail/util/PropUtil:getBooleanSystemProperty	(Ljava/lang/String;Z)Z
    //   77: putstatic 149	javax/mail/internet/MimeUtility:foldText	Z
    //   80: new 195	java/util/HashMap
    //   83: dup
    //   84: bipush 40
    //   86: invokespecial 204	java/util/HashMap:<init>	(I)V
    //   89: putstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   92: new 195	java/util/HashMap
    //   95: dup
    //   96: bipush 14
    //   98: invokespecial 204	java/util/HashMap:<init>	(I)V
    //   101: putstatic 169	javax/mail/internet/MimeUtility:mime2java	Ljava/util/Map;
    //   104: ldc 10
    //   106: ldc -51
    //   108: invokevirtual 206	java/lang/Class:getResourceAsStream	(Ljava/lang/String;)Ljava/io/InputStream;
    //   111: astore_0
    //   112: aload_0
    //   113: ifnull +54 -> 167
    //   116: new 207	com/sun/mail/util/LineInputStream
    //   119: dup
    //   120: aload_0
    //   121: invokespecial 208	com/sun/mail/util/LineInputStream:<init>	(Ljava/io/InputStream;)V
    //   124: astore_0
    //   125: aload_0
    //   126: checkcast 207	com/sun/mail/util/LineInputStream
    //   129: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   132: invokestatic 209	javax/mail/internet/MimeUtility:loadMappings	(Lcom/sun/mail/util/LineInputStream;Ljava/util/Map;)V
    //   135: aload_0
    //   136: checkcast 207	com/sun/mail/util/LineInputStream
    //   139: getstatic 169	javax/mail/internet/MimeUtility:mime2java	Ljava/util/Map;
    //   142: invokestatic 209	javax/mail/internet/MimeUtility:loadMappings	(Lcom/sun/mail/util/LineInputStream;Ljava/util/Map;)V
    //   145: aload_0
    //   146: invokevirtual 16	java/io/InputStream:close	()V
    //   149: goto +18 -> 167
    //   152: astore_1
    //   153: goto +14 -> 167
    //   156: astore_2
    //   157: aload_0
    //   158: invokevirtual 16	java/io/InputStream:close	()V
    //   161: goto +4 -> 165
    //   164: astore_3
    //   165: aload_2
    //   166: athrow
    //   167: goto +4 -> 171
    //   170: astore_0
    //   171: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   174: invokeinterface 210 1 0
    //   179: ifeq +463 -> 642
    //   182: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   185: ldc -79
    //   187: ldc -45
    //   189: invokeinterface 33 3 0
    //   194: pop
    //   195: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   198: ldc -44
    //   200: ldc -45
    //   202: invokeinterface 33 3 0
    //   207: pop
    //   208: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   211: ldc -43
    //   213: ldc -45
    //   215: invokeinterface 33 3 0
    //   220: pop
    //   221: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   224: ldc -42
    //   226: ldc -41
    //   228: invokeinterface 33 3 0
    //   233: pop
    //   234: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   237: ldc -40
    //   239: ldc -41
    //   241: invokeinterface 33 3 0
    //   246: pop
    //   247: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   250: ldc -39
    //   252: ldc -41
    //   254: invokeinterface 33 3 0
    //   259: pop
    //   260: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   263: ldc -38
    //   265: ldc -37
    //   267: invokeinterface 33 3 0
    //   272: pop
    //   273: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   276: ldc -36
    //   278: ldc -37
    //   280: invokeinterface 33 3 0
    //   285: pop
    //   286: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   289: ldc -35
    //   291: ldc -37
    //   293: invokeinterface 33 3 0
    //   298: pop
    //   299: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   302: ldc -34
    //   304: ldc -33
    //   306: invokeinterface 33 3 0
    //   311: pop
    //   312: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   315: ldc -32
    //   317: ldc -33
    //   319: invokeinterface 33 3 0
    //   324: pop
    //   325: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   328: ldc -31
    //   330: ldc -33
    //   332: invokeinterface 33 3 0
    //   337: pop
    //   338: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   341: ldc -30
    //   343: ldc -29
    //   345: invokeinterface 33 3 0
    //   350: pop
    //   351: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   354: ldc -28
    //   356: ldc -29
    //   358: invokeinterface 33 3 0
    //   363: pop
    //   364: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   367: ldc -27
    //   369: ldc -29
    //   371: invokeinterface 33 3 0
    //   376: pop
    //   377: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   380: ldc -26
    //   382: ldc -25
    //   384: invokeinterface 33 3 0
    //   389: pop
    //   390: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   393: ldc -24
    //   395: ldc -25
    //   397: invokeinterface 33 3 0
    //   402: pop
    //   403: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   406: ldc -23
    //   408: ldc -25
    //   410: invokeinterface 33 3 0
    //   415: pop
    //   416: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   419: ldc -22
    //   421: ldc -21
    //   423: invokeinterface 33 3 0
    //   428: pop
    //   429: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   432: ldc -20
    //   434: ldc -21
    //   436: invokeinterface 33 3 0
    //   441: pop
    //   442: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   445: ldc -19
    //   447: ldc -21
    //   449: invokeinterface 33 3 0
    //   454: pop
    //   455: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   458: ldc -18
    //   460: ldc -17
    //   462: invokeinterface 33 3 0
    //   467: pop
    //   468: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   471: ldc -16
    //   473: ldc -17
    //   475: invokeinterface 33 3 0
    //   480: pop
    //   481: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   484: ldc -15
    //   486: ldc -17
    //   488: invokeinterface 33 3 0
    //   493: pop
    //   494: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   497: ldc -14
    //   499: ldc -13
    //   501: invokeinterface 33 3 0
    //   506: pop
    //   507: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   510: ldc -12
    //   512: ldc -13
    //   514: invokeinterface 33 3 0
    //   519: pop
    //   520: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   523: ldc -11
    //   525: ldc -13
    //   527: invokeinterface 33 3 0
    //   532: pop
    //   533: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   536: ldc -10
    //   538: ldc -9
    //   540: invokeinterface 33 3 0
    //   545: pop
    //   546: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   549: ldc -8
    //   551: ldc -7
    //   553: invokeinterface 33 3 0
    //   558: pop
    //   559: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   562: ldc -6
    //   564: ldc -7
    //   566: invokeinterface 33 3 0
    //   571: pop
    //   572: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   575: ldc -5
    //   577: ldc -4
    //   579: invokeinterface 33 3 0
    //   584: pop
    //   585: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   588: ldc -3
    //   590: ldc -2
    //   592: invokeinterface 33 3 0
    //   597: pop
    //   598: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   601: ldc -1
    //   603: ldc_w 256
    //   606: invokeinterface 33 3 0
    //   611: pop
    //   612: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   615: ldc_w 257
    //   618: ldc_w 258
    //   621: invokeinterface 33 3 0
    //   626: pop
    //   627: getstatic 171	javax/mail/internet/MimeUtility:java2mime	Ljava/util/Map;
    //   630: ldc_w 259
    //   633: ldc_w 260
    //   636: invokeinterface 33 3 0
    //   641: pop
    //   642: getstatic 169	javax/mail/internet/MimeUtility:mime2java	Ljava/util/Map;
    //   645: invokeinterface 210 1 0
    //   650: ifeq +211 -> 861
    //   653: getstatic 169	javax/mail/internet/MimeUtility:mime2java	Ljava/util/Map;
    //   656: ldc_w 261
    //   659: ldc_w 262
    //   662: invokeinterface 33 3 0
    //   667: pop
    //   668: getstatic 169	javax/mail/internet/MimeUtility:mime2java	Ljava/util/Map;
    //   671: ldc_w 263
    //   674: ldc_w 264
    //   677: invokeinterface 33 3 0
    //   682: pop
    //   683: getstatic 169	javax/mail/internet/MimeUtility:mime2java	Ljava/util/Map;
    //   686: ldc_w 265
    //   689: ldc_w 266
    //   692: invokeinterface 33 3 0
    //   697: pop
    //   698: getstatic 169	javax/mail/internet/MimeUtility:mime2java	Ljava/util/Map;
    //   701: ldc_w 267
    //   704: ldc_w 266
    //   707: invokeinterface 33 3 0
    //   712: pop
    //   713: getstatic 169	javax/mail/internet/MimeUtility:mime2java	Ljava/util/Map;
    //   716: ldc_w 268
    //   719: ldc_w 269
    //   722: invokeinterface 33 3 0
    //   727: pop
    //   728: getstatic 169	javax/mail/internet/MimeUtility:mime2java	Ljava/util/Map;
    //   731: ldc_w 270
    //   734: ldc_w 271
    //   737: invokeinterface 33 3 0
    //   742: pop
    //   743: getstatic 169	javax/mail/internet/MimeUtility:mime2java	Ljava/util/Map;
    //   746: ldc_w 260
    //   749: ldc_w 272
    //   752: invokeinterface 33 3 0
    //   757: pop
    //   758: getstatic 169	javax/mail/internet/MimeUtility:mime2java	Ljava/util/Map;
    //   761: ldc_w 273
    //   764: ldc_w 272
    //   767: invokeinterface 33 3 0
    //   772: pop
    //   773: getstatic 169	javax/mail/internet/MimeUtility:mime2java	Ljava/util/Map;
    //   776: ldc_w 274
    //   779: ldc -45
    //   781: invokeinterface 33 3 0
    //   786: pop
    //   787: getstatic 169	javax/mail/internet/MimeUtility:mime2java	Ljava/util/Map;
    //   790: ldc_w 275
    //   793: ldc -45
    //   795: invokeinterface 33 3 0
    //   800: pop
    //   801: getstatic 169	javax/mail/internet/MimeUtility:mime2java	Ljava/util/Map;
    //   804: ldc_w 276
    //   807: ldc_w 277
    //   810: invokeinterface 33 3 0
    //   815: pop
    //   816: getstatic 169	javax/mail/internet/MimeUtility:mime2java	Ljava/util/Map;
    //   819: ldc_w 278
    //   822: ldc_w 277
    //   825: invokeinterface 33 3 0
    //   830: pop
    //   831: getstatic 169	javax/mail/internet/MimeUtility:mime2java	Ljava/util/Map;
    //   834: ldc_w 279
    //   837: ldc_w 277
    //   840: invokeinterface 33 3 0
    //   845: pop
    //   846: getstatic 169	javax/mail/internet/MimeUtility:mime2java	Ljava/util/Map;
    //   849: ldc_w 280
    //   852: ldc_w 277
    //   855: invokeinterface 33 3 0
    //   860: pop
    //   861: return
    // Line number table:
    //   Java source line #143	-> byte code offset #0
    //   Java source line #152	-> byte code offset #16
    //   Java source line #155	-> byte code offset #26
    //   Java source line #156	-> byte code offset #29
    //   Java source line #157	-> byte code offset #35
    //   Java source line #158	-> byte code offset #38
    //   Java source line #159	-> byte code offset #44
    //   Java source line #160	-> byte code offset #47
    //   Java source line #162	-> byte code offset #53
    //   Java source line #163	-> byte code offset #56
    //   Java source line #170	-> byte code offset #62
    //   Java source line #171	-> byte code offset #65
    //   Java source line #172	-> byte code offset #71
    //   Java source line #173	-> byte code offset #74
    //   Java source line #1344	-> byte code offset #80
    //   Java source line #1345	-> byte code offset #92
    //   Java source line #1350	-> byte code offset #104
    //   Java source line #1351	-> byte code offset #108
    //   Java source line #1354	-> byte code offset #112
    //   Java source line #1356	-> byte code offset #116
    //   Java source line #1359	-> byte code offset #125
    //   Java source line #1362	-> byte code offset #135
    //   Java source line #1365	-> byte code offset #145
    //   Java source line #1368	-> byte code offset #149
    //   Java source line #1366	-> byte code offset #152
    //   Java source line #1369	-> byte code offset #153
    //   Java source line #1364	-> byte code offset #156
    //   Java source line #1365	-> byte code offset #157
    //   Java source line #1368	-> byte code offset #161
    //   Java source line #1366	-> byte code offset #164
    //   Java source line #1368	-> byte code offset #165
    //   Java source line #1371	-> byte code offset #167
    //   Java source line #1376	-> byte code offset #171
    //   Java source line #1377	-> byte code offset #182
    //   Java source line #1378	-> byte code offset #195
    //   Java source line #1379	-> byte code offset #208
    //   Java source line #1381	-> byte code offset #221
    //   Java source line #1382	-> byte code offset #234
    //   Java source line #1383	-> byte code offset #247
    //   Java source line #1385	-> byte code offset #260
    //   Java source line #1386	-> byte code offset #273
    //   Java source line #1387	-> byte code offset #286
    //   Java source line #1389	-> byte code offset #299
    //   Java source line #1390	-> byte code offset #312
    //   Java source line #1391	-> byte code offset #325
    //   Java source line #1393	-> byte code offset #338
    //   Java source line #1394	-> byte code offset #351
    //   Java source line #1395	-> byte code offset #364
    //   Java source line #1397	-> byte code offset #377
    //   Java source line #1398	-> byte code offset #390
    //   Java source line #1399	-> byte code offset #403
    //   Java source line #1401	-> byte code offset #416
    //   Java source line #1402	-> byte code offset #429
    //   Java source line #1403	-> byte code offset #442
    //   Java source line #1405	-> byte code offset #455
    //   Java source line #1406	-> byte code offset #468
    //   Java source line #1407	-> byte code offset #481
    //   Java source line #1409	-> byte code offset #494
    //   Java source line #1410	-> byte code offset #507
    //   Java source line #1411	-> byte code offset #520
    //   Java source line #1413	-> byte code offset #533
    //   Java source line #1414	-> byte code offset #546
    //   Java source line #1415	-> byte code offset #559
    //   Java source line #1416	-> byte code offset #572
    //   Java source line #1417	-> byte code offset #585
    //   Java source line #1418	-> byte code offset #598
    //   Java source line #1419	-> byte code offset #612
    //   Java source line #1420	-> byte code offset #627
    //   Java source line #1422	-> byte code offset #642
    //   Java source line #1423	-> byte code offset #653
    //   Java source line #1424	-> byte code offset #668
    //   Java source line #1425	-> byte code offset #683
    //   Java source line #1426	-> byte code offset #698
    //   Java source line #1427	-> byte code offset #713
    //   Java source line #1428	-> byte code offset #728
    //   Java source line #1429	-> byte code offset #743
    //   Java source line #1430	-> byte code offset #758
    //   Java source line #1431	-> byte code offset #773
    //   Java source line #1432	-> byte code offset #787
    //   Java source line #1433	-> byte code offset #801
    //   Java source line #1434	-> byte code offset #816
    //   Java source line #1435	-> byte code offset #831
    //   Java source line #1436	-> byte code offset #846
    //   Java source line #1438	-> byte code offset #861
    // Local variable table:
    //   start	length	slot	name	signature
    //   111	47	0	is	InputStream
    //   170	1	0	localException2	Exception
    //   152	1	1	localException	Exception
    //   156	10	2	localObject	Object
    //   164	1	3	localException1	Exception
    // Exception table:
    //   from	to	target	type
    //   145	149	152	java/lang/Exception
    //   116	145	156	finally
    //   157	161	164	java/lang/Exception
    //   104	167	170	java/lang/Exception
  }
}
