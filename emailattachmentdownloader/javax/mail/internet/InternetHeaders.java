package javax.mail.internet;

import com.sun.mail.util.LineInputStream;
import com.sun.mail.util.PropUtil;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import javax.mail.Header;
import javax.mail.MessagingException;






































































public class InternetHeaders
{
  private static final boolean ignoreWhitespaceLines = PropUtil.getBooleanSystemProperty("mail.mime.ignorewhitespacelines", false);
  







  protected List<InternetHeader> headers;
  








  protected static final class InternetHeader
    extends Header
  {
    String line;
    








    public InternetHeader(String l)
    {
      super("");
      int i = l.indexOf(':');
      if (i < 0)
      {
        name = l.trim();
      } else {
        name = l.substring(0, i).trim();
      }
      line = l;
    }
    





    public InternetHeader(String n, String v)
    {
      super("");
      if (v != null) {
        line = (n + ": " + v);
      } else {
        line = null;
      }
    }
    


    public String getValue()
    {
      int i = line.indexOf(':');
      if (i < 0) {
        return line;
      }
      
      for (int j = i + 1; j < line.length(); j++) {
        char c = line.charAt(j);
        if ((c != ' ') && (c != '\t') && (c != '\r') && (c != '\n'))
          break;
      }
      return line.substring(j);
    }
  }
  


  static class MatchEnum
  {
    private Iterator<InternetHeaders.InternetHeader> e;
    

    private String[] names;
    

    private boolean match;
    

    private boolean want_line;
    

    private InternetHeaders.InternetHeader next_header;
    

    MatchEnum(List<InternetHeaders.InternetHeader> v, String[] n, boolean m, boolean l)
    {
      e = v.iterator();
      names = n;
      match = m;
      want_line = l;
      next_header = null;
    }
    




    public boolean hasMoreElements()
    {
      if (next_header == null)
        next_header = nextMatch();
      return next_header != null;
    }
    


    public Object nextElement()
    {
      if (next_header == null) {
        next_header = nextMatch();
      }
      if (next_header == null) {
        throw new NoSuchElementException("No more headers");
      }
      InternetHeaders.InternetHeader h = next_header;
      next_header = null;
      if (want_line) {
        return line;
      }
      return new Header(h.getName(), h.getValue());
    }
    




    private InternetHeaders.InternetHeader nextMatch()
    {
      while (e.hasNext()) {
        InternetHeaders.InternetHeader h = (InternetHeaders.InternetHeader)e.next();
        

        if (line != null)
        {


          if (names == null) {
            return match ? null : h;
          }
          
          for (int i = 0;; i++) { if (i >= names.length) break label97;
            if (names[i].equalsIgnoreCase(h.getName())) {
              if (!match) break;
              return h;
            }
          }
          


          label97:
          

          if (!match)
            return h;
        } }
      return null;
    }
  }
  
  static class MatchStringEnum extends InternetHeaders.MatchEnum implements Enumeration<String>
  {
    MatchStringEnum(List<InternetHeaders.InternetHeader> v, String[] n, boolean m)
    {
      super(n, m, true);
    }
    
    public String nextElement()
    {
      return (String)super.nextElement();
    }
  }
  
  static class MatchHeaderEnum
    extends InternetHeaders.MatchEnum implements Enumeration<Header>
  {
    MatchHeaderEnum(List<InternetHeaders.InternetHeader> v, String[] n, boolean m)
    {
      super(n, m, false);
    }
    
    public Header nextElement()
    {
      return (Header)super.nextElement();
    }
  }
  





















  public InternetHeaders()
  {
    headers = new ArrayList(40);
    headers.add(new InternetHeader("Return-Path", null));
    headers.add(new InternetHeader("Received", null));
    headers.add(new InternetHeader("Resent-Date", null));
    headers.add(new InternetHeader("Resent-From", null));
    headers.add(new InternetHeader("Resent-Sender", null));
    headers.add(new InternetHeader("Resent-To", null));
    headers.add(new InternetHeader("Resent-Cc", null));
    headers.add(new InternetHeader("Resent-Bcc", null));
    headers.add(new InternetHeader("Resent-Message-Id", null));
    headers.add(new InternetHeader("Date", null));
    headers.add(new InternetHeader("From", null));
    headers.add(new InternetHeader("Sender", null));
    headers.add(new InternetHeader("Reply-To", null));
    headers.add(new InternetHeader("To", null));
    headers.add(new InternetHeader("Cc", null));
    headers.add(new InternetHeader("Bcc", null));
    headers.add(new InternetHeader("Message-Id", null));
    headers.add(new InternetHeader("In-Reply-To", null));
    headers.add(new InternetHeader("References", null));
    headers.add(new InternetHeader("Subject", null));
    headers.add(new InternetHeader("Comments", null));
    headers.add(new InternetHeader("Keywords", null));
    headers.add(new InternetHeader("Errors-To", null));
    headers.add(new InternetHeader("MIME-Version", null));
    headers.add(new InternetHeader("Content-Type", null));
    headers.add(new InternetHeader("Content-Transfer-Encoding", null));
    headers.add(new InternetHeader("Content-MD5", null));
    headers.add(new InternetHeader(":", null));
    headers.add(new InternetHeader("Content-Length", null));
    headers.add(new InternetHeader("Status", null));
  }
  













  public InternetHeaders(InputStream is)
    throws MessagingException
  {
    this(is, false);
  }
  
















  public InternetHeaders(InputStream is, boolean allowutf8)
    throws MessagingException
  {
    headers = new ArrayList(40);
    load(is, allowutf8);
  }
  












  public void load(InputStream is)
    throws MessagingException
  {
    load(is, false);
  }
  


















  public void load(InputStream is, boolean allowutf8)
    throws MessagingException
  {
    LineInputStream lis = new LineInputStream(is, allowutf8);
    String prevline = null;
    
    StringBuffer lineBuffer = new StringBuffer();
    


    try
    {
      boolean first = true;
      String line;
      do { line = lis.readLine();
        if ((line != null) && (
          (line.startsWith(" ")) || (line.startsWith("\t"))))
        {
          if (prevline != null) {
            lineBuffer.append(prevline);
            prevline = null;
          }
          if (first) {
            String lt = line.trim();
            if (lt.length() > 0)
              lineBuffer.append(lt);
          } else {
            if (lineBuffer.length() > 0)
              lineBuffer.append("\r\n");
            lineBuffer.append(line);
          }
        }
        else {
          if (prevline != null) {
            addHeaderLine(prevline);
          } else if (lineBuffer.length() > 0)
          {
            addHeaderLine(lineBuffer.toString());
            lineBuffer.setLength(0);
          }
          prevline = line;
        }
        first = false;
        if (line == null) break; } while (!isEmpty(line));
    } catch (IOException ioex) {
      throw new MessagingException("Error in input stream", ioex);
    }
    
    String line;
  }
  
  private static final boolean isEmpty(String line)
  {
    return (line.length() == 0) || ((ignoreWhitespaceLines) && 
      (line.trim().length() == 0));
  }
  







  public String[] getHeader(String name)
  {
    Iterator<InternetHeader> e = headers.iterator();
    
    List<String> v = new ArrayList();
    
    while (e.hasNext()) {
      InternetHeader h = (InternetHeader)e.next();
      if ((name.equalsIgnoreCase(h.getName())) && (line != null)) {
        v.add(h.getValue());
      }
    }
    if (v.size() == 0) {
      return null;
    }
    String[] r = new String[v.size()];
    r = (String[])v.toArray(r);
    return r;
  }
  











  public String getHeader(String name, String delimiter)
  {
    String[] s = getHeader(name);
    
    if (s == null) {
      return null;
    }
    if ((s.length == 1) || (delimiter == null)) {
      return s[0];
    }
    StringBuffer r = new StringBuffer(s[0]);
    for (int i = 1; i < s.length; i++) {
      r.append(delimiter);
      r.append(s[i]);
    }
    return r.toString();
  }
  









  public void setHeader(String name, String value)
  {
    boolean found = false;
    
    for (int i = 0; i < headers.size(); i++) {
      InternetHeader h = (InternetHeader)headers.get(i);
      if (name.equalsIgnoreCase(h.getName())) {
        if (!found) {
          int j;
          if ((line != null) && ((j = line.indexOf(':')) >= 0)) {
            line = (line.substring(0, j + 1) + " " + value);
          }
          else {
            line = (name + ": " + value);
          }
          found = true;
        } else {
          headers.remove(i);
          i--;
        }
      }
    }
    
    if (!found) {
      addHeader(name, value);
    }
  }
  














  public void addHeader(String name, String value)
  {
    int pos = headers.size();
    

    boolean addReverse = (name.equalsIgnoreCase("Received")) || (name.equalsIgnoreCase("Return-Path"));
    if (addReverse)
      pos = 0;
    for (int i = headers.size() - 1; i >= 0; i--) {
      InternetHeader h = (InternetHeader)headers.get(i);
      if (name.equalsIgnoreCase(h.getName())) {
        if (addReverse) {
          pos = i;
        } else {
          headers.add(i + 1, new InternetHeader(name, value));
          return;
        }
      }
      
      if ((!addReverse) && (h.getName().equals(":")))
        pos = i;
    }
    headers.add(pos, new InternetHeader(name, value));
  }
  



  public void removeHeader(String name)
  {
    for (int i = 0; i < headers.size(); i++) {
      InternetHeader h = (InternetHeader)headers.get(i);
      if (name.equalsIgnoreCase(h.getName())) {
        line = null;
      }
    }
  }
  







  public Enumeration<Header> getAllHeaders()
  {
    return new MatchHeaderEnum(headers, null, false);
  }
  





  public Enumeration<Header> getMatchingHeaders(String[] names)
  {
    return new MatchHeaderEnum(headers, names, true);
  }
  





  public Enumeration<Header> getNonMatchingHeaders(String[] names)
  {
    return new MatchHeaderEnum(headers, names, false);
  }
  








  public void addHeaderLine(String line)
  {
    try
    {
      char c = line.charAt(0);
      if ((c == ' ') || (c == '\t')) {
        InternetHeader h = (InternetHeader)headers.get(headers.size() - 1); InternetHeader 
          tmp50_49 = h;5049line = (5049line + "\r\n" + line);
      } else {
        headers.add(new InternetHeader(line));
      }
    }
    catch (StringIndexOutOfBoundsException e) {}catch (NoSuchElementException localNoSuchElementException) {}
  }
  







  public Enumeration<String> getAllHeaderLines()
  {
    return getNonMatchingHeaderLines(null);
  }
  





  public Enumeration<String> getMatchingHeaderLines(String[] names)
  {
    return new MatchStringEnum(headers, names, true);
  }
  





  public Enumeration<String> getNonMatchingHeaderLines(String[] names)
  {
    return new MatchStringEnum(headers, names, false);
  }
}
