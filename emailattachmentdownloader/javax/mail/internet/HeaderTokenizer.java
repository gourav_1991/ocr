package javax.mail.internet;






public class HeaderTokenizer
{
  private String string;
  




  private boolean skipComments;
  




  private String delimiters;
  




  private int currentPos;
  




  private int maxPos;
  




  private int nextPos;
  




  private int peekPos;
  



  public static final String RFC822 = "()<>@,;:\\\"\t .[]";
  



  public static final String MIME = "()<>@,;:\\\"\t []/?=";
  




  public static class Token
  {
    private int type;
    



    private String value;
    



    public static final int ATOM = -1;
    



    public static final int QUOTEDSTRING = -2;
    



    public static final int COMMENT = -3;
    



    public static final int EOF = -4;
    




    public Token(int type, String value)
    {
      this.type = type;
      this.value = value;
    }
    

















    public int getType()
    {
      return type;
    }
    







    public String getValue()
    {
      return value;
    }
  }
  



















  private static final Token EOFToken = new Token(-4, null);
  











  public HeaderTokenizer(String header, String delimiters, boolean skipComments)
  {
    string = (header == null ? "" : header);
    this.skipComments = skipComments;
    this.delimiters = delimiters;
    currentPos = (this.nextPos = this.peekPos = 0);
    maxPos = string.length();
  }
  





  public HeaderTokenizer(String header, String delimiters)
  {
    this(header, delimiters, true);
  }
  






  public HeaderTokenizer(String header)
  {
    this(header, "()<>@,;:\\\"\t .[]");
  }
  







  public Token next()
    throws ParseException
  {
    return next('\000', false);
  }
  











  public Token next(char endOfAtom)
    throws ParseException
  {
    return next(endOfAtom, false);
  }
  















  public Token next(char endOfAtom, boolean keepEscapes)
    throws ParseException
  {
    currentPos = nextPos;
    Token tk = getNext(endOfAtom, keepEscapes);
    nextPos = (this.peekPos = currentPos);
    return tk;
  }
  









  public Token peek()
    throws ParseException
  {
    currentPos = peekPos;
    Token tk = getNext('\000', false);
    peekPos = currentPos;
    return tk;
  }
  





  public String getRemainder()
  {
    if (nextPos >= string.length())
      return null;
    return string.substring(nextPos);
  }
  





  private Token getNext(char endOfAtom, boolean keepEscapes)
    throws ParseException
  {
    if (currentPos >= maxPos) {
      return EOFToken;
    }
    
    if (skipWhiteSpace() == -4) {
      return EOFToken;
    }
    

    boolean filter = false;
    
    char c = string.charAt(currentPos);
    


    while (c == '(')
    {

      int start = ++currentPos;int nesting = 1;
      for (; (nesting > 0) && (currentPos < maxPos); 
          currentPos += 1) {
        c = string.charAt(currentPos);
        if (c == '\\') {
          currentPos += 1;
          filter = true;
        } else if (c == '\r') {
          filter = true;
        } else if (c == '(') {
          nesting++;
        } else if (c == ')') {
          nesting--;
        } }
      if (nesting != 0) {
        throw new ParseException("Unbalanced comments");
      }
      if (!skipComments)
      {
        String s;
        String s;
        if (filter) {
          s = filterToken(string, start, currentPos - 1, keepEscapes);
        } else {
          s = string.substring(start, currentPos - 1);
        }
        return new Token(-3, s);
      }
      

      if (skipWhiteSpace() == -4)
        return EOFToken;
      c = string.charAt(currentPos);
    }
    


    if (c == '"') {
      currentPos += 1;
      return collectString('"', keepEscapes);
    }
    

    if ((c < ' ') || (c >= '') || (delimiters.indexOf(c) >= 0)) {
      if ((endOfAtom > 0) && (c != endOfAtom))
      {

        return collectString(endOfAtom, keepEscapes);
      }
      currentPos += 1;
      char[] ch = new char[1];
      ch[0] = c;
      return new Token(c, new String(ch));
    }
    

    for (int start = currentPos; currentPos < maxPos; currentPos += 1) {
      c = string.charAt(currentPos);
      

      if ((c < ' ') || (c >= '') || (c == '(') || (c == ' ') || (c == '"') || 
        (delimiters.indexOf(c) >= 0)) {
        if ((endOfAtom <= 0) || (c == endOfAtom)) {
          break;
        }
        currentPos = start;
        return collectString(endOfAtom, keepEscapes);
      }
    }
    

    return new Token(-1, string.substring(start, currentPos));
  }
  
  private Token collectString(char eos, boolean keepEscapes)
    throws ParseException
  {
    boolean filter = false;
    for (int start = currentPos; currentPos < maxPos; currentPos += 1) {
      char c = string.charAt(currentPos);
      if (c == '\\') {
        currentPos += 1;
        filter = true;
      } else if (c == '\r') {
        filter = true;
      } else if (c == eos) {
        currentPos += 1;
        String s;
        String s;
        if (filter) {
          s = filterToken(string, start, currentPos - 1, keepEscapes);
        } else {
          s = string.substring(start, currentPos - 1);
        }
        if (c != '"') {
          s = trimWhiteSpace(s);
          currentPos -= 1;
        }
        
        return new Token(-2, s);
      }
    }
    



    if (eos == '"') {
      throw new ParseException("Unbalanced quoted string");
    }
    
    String s;
    if (filter) {
      s = filterToken(string, start, currentPos, keepEscapes);
    } else
      s = string.substring(start, currentPos);
    String s = trimWhiteSpace(s);
    return new Token(-2, s);
  }
  
  private int skipWhiteSpace()
  {
    for (; 
        currentPos < maxPos; currentPos += 1) { char c;
      if (((c = string.charAt(currentPos)) != ' ') && (c != '\t') && (c != '\r') && (c != '\n'))
      {
        return currentPos; } }
    return -4;
  }
  


  private static String trimWhiteSpace(String s)
  {
    for (int i = s.length() - 1; i >= 0; i--) { char c;
      if (((c = s.charAt(i)) != ' ') && (c != '\t') && (c != '\r') && (c != '\n')) {
        break;
      }
    }
    if (i <= 0) {
      return "";
    }
    return s.substring(0, i + 1);
  }
  



  private static String filterToken(String s, int start, int end, boolean keepEscapes)
  {
    StringBuffer sb = new StringBuffer();
    
    boolean gotEscape = false;
    boolean gotCR = false;
    
    for (int i = start; i < end; i++) {
      char c = s.charAt(i);
      if ((c == '\n') && (gotCR))
      {

        gotCR = false;
      }
      else
      {
        gotCR = false;
        if (!gotEscape)
        {
          if (c == '\\') {
            gotEscape = true;
          } else if (c == '\r') {
            gotCR = true;
          } else {
            sb.append(c);
          }
          

        }
        else
        {

          if (keepEscapes)
            sb.append('\\');
          sb.append(c);
          gotEscape = false;
        }
      } }
    return sb.toString();
  }
}
