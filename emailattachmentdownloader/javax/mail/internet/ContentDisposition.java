package javax.mail.internet;

















public class ContentDisposition
{
  private String disposition;
  














  private ParameterList list;
  















  public ContentDisposition() {}
  















  public ContentDisposition(String disposition, ParameterList list)
  {
    this.disposition = disposition;
    this.list = list;
  }
  







  public ContentDisposition(String s)
    throws ParseException
  {
    HeaderTokenizer h = new HeaderTokenizer(s, "()<>@,;:\\\"\t []/?=");
    


    HeaderTokenizer.Token tk = h.next();
    if (tk.getType() != -1)
    {
      throw new ParseException("Expected disposition, got " + tk.getValue()); }
    disposition = tk.getValue();
    

    String rem = h.getRemainder();
    if (rem != null) {
      list = new ParameterList(rem);
    }
  }
  



  public String getDisposition()
  {
    return disposition;
  }
  







  public String getParameter(String name)
  {
    if (list == null) {
      return null;
    }
    return list.get(name);
  }
  






  public ParameterList getParameterList()
  {
    return list;
  }
  




  public void setDisposition(String disposition)
  {
    this.disposition = disposition;
  }
  







  public void setParameter(String name, String value)
  {
    if (list == null) {
      list = new ParameterList();
    }
    list.set(name, value);
  }
  




  public void setParameterList(ParameterList list)
  {
    this.list = list;
  }
  








  public String toString()
  {
    if (disposition == null) {
      return "";
    }
    if (list == null) {
      return disposition;
    }
    StringBuffer sb = new StringBuffer(disposition);
    



    sb.append(list.toString(sb.length() + 21));
    return sb.toString();
  }
}
