package javax.mail;

public abstract interface MessageAware
{
  public abstract MessageContext getMessageContext();
}
