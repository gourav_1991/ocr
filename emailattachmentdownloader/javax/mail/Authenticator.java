package javax.mail;

import java.net.InetAddress;



















































































public abstract class Authenticator
{
  private InetAddress requestingSite;
  private int requestingPort;
  private String requestingProtocol;
  private String requestingPrompt;
  private String requestingUserName;
  
  public Authenticator() {}
  
  final synchronized PasswordAuthentication requestPasswordAuthentication(InetAddress addr, int port, String protocol, String prompt, String defaultUserName)
  {
    requestingSite = addr;
    requestingPort = port;
    requestingProtocol = protocol;
    requestingPrompt = prompt;
    requestingUserName = defaultUserName;
    return getPasswordAuthentication();
  }
  



  protected final InetAddress getRequestingSite()
  {
    return requestingSite;
  }
  


  protected final int getRequestingPort()
  {
    return requestingPort;
  }
  







  protected final String getRequestingProtocol()
  {
    return requestingProtocol;
  }
  


  protected final String getRequestingPrompt()
  {
    return requestingPrompt;
  }
  


  protected final String getDefaultUserName()
  {
    return requestingUserName;
  }
  










  protected PasswordAuthentication getPasswordAuthentication()
  {
    return null;
  }
}
